#ifndef _DEF_H_
#define _DEF_H_

// Calculation
#define ASIZE			1026
#define SIZE100 		2048
#define SIZE50  		4096
#define DELAY  			1       // Delay time for next integration (unit: msec)
#define MIN_INT_TIME	200     // Minimum integration time unit in msec

// Network
#define MSGLEN 512
#define LOCAL_ADDRESS "127.0.0.1"
//#define REMOTE_ADDRESS "192.168.0.1"
#define REMOTE_ADDRESS "211.234.32.10"
#define PORT 9734
#define MAX_CLIENT 5

#define OEHIGH_OR	0x10
#define OELOW_AND	0x81
#define STBHIGH_OR	0x01
#define STBLOW_AND	0x90

#endif
