/////////////////////////////////////////////////////////////////////////////
// Name:        TestCal.c
// Purpose:     Correlator Operating System
// Author:      Young-Zoo Yoon, Edited by Hyun Woo Kang with C lang
// Modified by:
// Created:     12/17/2007
// Copyright:   KASI
// Licence:    
/////////////////////////////////////////////////////////////////////////////

#include <sys/unistd.h>
#include <sys/io.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <fftw.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
//#include <wx/wx.h>
#include "define.h"
#include "ansi8255.h"
#include "ansicosCal.h"
#include "cmdfromspecom.h"
#include "fft.h"
#define DATAMAX	4096
#define CORDATASIZE	(20*4+DATAMAX*8)	// 20*integer + max 4096*double
static cosData pcosData;
double sendData[DATAMAX];

int lib_tseg=500;

static int good_or_bad=1; 	//good =1 , bad = 0
static int save_raw=1;

void wrBinData(int socket_fd,int tick, char buf[]);

void SetGoodFlag(void)
{
		good_or_bad=1;
		return;
}

void SetBadFlag(void)
{
		good_or_bad=0;
		return;
}

int ConfirmFlag(void)
{
		return good_or_bad;
}

void WRITESTATUS(int corr_status)
{	
	static int status=-1;
	
  	char filename[50]="/data/share/STATUS2";
	FILE *fp;
	
	if((fp=fopen(filename,"w"))==NULL) {
		   	fprintf(stderr,"Error status saving\n");
			return;
	}
	if (status==corr_status) return;
	status = corr_status;
	fprintf(stderr,"status saving\n");
	switch(corr_status){
			case 1 : //READY
					fprintf(fp,"READY");
					break;
			case 2 : //INTEGRATION
					fprintf(fp,"INTEG");
					break;
	}
	fclose(fp);
		
	return;
}
void SENDRAWDATA(int socket_fd, int status, int tick, char buf[])
{
	if (status<0 && save_raw>0) {
		SEGWRITERAWDATA(tick);
		fprintf(stderr,"segment %d\n",tick);
	}
	else {	// Send data to DAS
			if (socket_fd > 0) {
				fprintf(stderr,"Sending...\n");
				wrBinData(socket_fd,tick,buf);
			}
	}
	
	DataInit();
	return;
}
void SEGWRITERAWDATA(int tick)
{	
	int i;
  	char filename[50];
	FILE *fp;

//	RunFFT();
	sprintf(filename,"/data/share/accraw%d.dat",tick);
	if((fp=fopen(filename,"w"))==NULL) {
		   	fprintf(stderr,"Error saving\n");
			return;
	}
	if(pcosData.nBandMode==1) {
			fprintf(stderr,"ACR0 & 1 : %lf\t%lf\n",pcosData.dAcrData100[0],pcosData.dAcrData100[1]);
			spec(pcosData.dFFTData100,pcosData.dAcrData100,SIZE100);
		//	for(i=0;i<SIZE100;i++) fprintf(fp, "%lf\n",pcosData.dFFTData100[i]);
			for(i=0;i<SIZE100;i++) fprintf(fp, "%lf\n",pcosData.dAcrData100[i]);
	}
	else {
			spec(pcosData.dFFTData50,pcosData.dAcrData50,SIZE50);
			fprintf(stderr,"ACR0 & 1 : %lf\t%lf\n",pcosData.dAcrData50[0],pcosData.dAcrData50[1]);
			for(i=0;i<SIZE50;i++) fprintf(fp, "%lf\n",pcosData.dFFTData50[i]);
	}
	fclose(fp);
		
	return;
}

void WRITERAWDATA(void)
{	
	int i;
  	char filename[50]="/data/share/accraw2.dat";
	FILE *fp;
	
	if((fp=fopen(filename,"w"))==NULL) {
		   	fprintf(stderr,"Error saving\n");
			return;
	}
	fprintf(stderr,"saving... and bandmode(1:100MHz, 2:50MHz) is %d\n",pcosData.nBandMode);
	if(pcosData.nBandMode==1) for(i=0;i<=SIZE100;i++) fprintf(fp, "%lf\n",pcosData.dAcrData100[i]);
	else for(i=0;i<=SIZE50;i++) fprintf(fp, "%lf\n",pcosData.dAcrData50[i]);
	fclose(fp);
		
	return;
}

int CheckCommandFromRealSpecom(char *buf, struct CorrInfo *corr)
{
		int result=0;
		char ch,tmp,tmp2;
		int tint, tseg;

		ch=buf[0];
		if(ch=='1') {
			sscanf(buf,"%s %d %d",&tmp,&tint, &tseg);
			result=atoi(&tmp);
			corr->tint=tint;
			corr->tseg=tseg;
			lib_tseg=tseg;
		}
		else if(ch=='2' || ch=='3') result=atoi(&ch);

		else if(ch=='4') corr->flag=1; //SetGoodFlag();
		else if(ch=='5') corr->flag=0; //SetBadFlag();

		else if(ch=='6') {
			sscanf(buf,"%s %s",&tmp,&tmp2);
			tint=atoi(&tmp2);
//			fprintf(stderr,"BAND mode is %d\n",tint);
			corr->band=tint;
//			ModeSet(tint);			// 1 is 100MHz, 2 is 50MHz
			result=6;
		}
		
		else if(ch=='0') result=0;
		else if(ch=='7') {
			sscanf(buf,"%s %s",&tmp,&tmp2);
			tint=atoi(&tmp2);
			save_raw=tint;
			result=7;
		}
		
		return result;
}
void SetProgressTime(void)
{
	pcosData.nProgressTime=1;
	return;
}
void ClrProgressTime(void)
{
	pcosData.nProgressTime=0;
	return;
}
int IntegFlag(void)
{
		return pcosData.nIntegFlag;
}
void SetIntegFlag(void)
{
	pcosData.nIntegFlag=1;
	return;
}
void ClrIntegFlag(void)
{
	pcosData.nIntegFlag=0;
	return;
}
void SetObsFlag(void)
{
	pcosData.nObsFlag=1;
	return;
}
void ClrObsFlag(void)
{
	pcosData.nObsFlag=0;
	return;
}
void OpenCal()
{
//	pcosData = new cosData();
    ioperm(0x300, 32, 1);
	fprintf(stderr,"Set ioperm\n");
	pcosData.dCalCoeff = 1.0;
	pcosData.cBitMask = 0x80;
}

void CloseCal()
{
//	delete pcosData;
}

// Initialize CXDIO96 Board
void InitDIO96()
{
   	outb(PIIOO, portd0);  
	outb(PIIOO, portd1);
	outb(PIIOO, portd2);
	outb(PIIOO, portd3);
	outb(PIIOO, portd4);
	outb(PIIOO, portd5);
	outb(PIIOO, portd6);
	outb(PIIOO, portd7);
}

// Initialize Correlator
void InitCorr()         
{
   	// Initial State
	outb(0X90, portc0);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc2);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc4);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc6);         // BLK=1 INT=0 RST=1 SE=0
	
	pcosData.cBitMask = 0x80;
	outb(pcosData.cBitMask, portc1);	   // 100MHz=1 OE=0  STB=0
	outb(0X00, portc3);		   // OE=0  STB=0
	outb(0X00, portc5);		   // OE=0  STB=0
	outb(0X00, portc7);		   // OE=0  STB=0

	// Accumulator Reset
	outb(0X84, portc0);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc2);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc4);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc6);         // BLK=1 INT=0 RST=0 SE=1
	usleep(1000);
	outb(0X94, portc0);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc2);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc4);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc6);         // BLK=1 INT=0 RST=1 SE=1
	usleep(1000);

	// Data Input Port Initialization
	inw(porta0);
	inw(porta1);
	inw(porta2);
	inw(porta3);
	inw(porta4);
	inw(porta5);
	inw(porta6);
	inw(porta7);
	
	// Initial Integration
	outb(0X54, portc0);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc2);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc4);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc6);         // BLK=0 INT=1 RST=1 SE=1
	usleep(100000);
	outb(0X94, portc0);         // BLK=1 INT=0 SE=1
	outb(0X94, portc2);         // BLK=1 INT=0 SE=1
	outb(0X94, portc4);         // BLK=1 INT=0 SE=1
	outb(0X94, portc6);         // BLK=1 INT=0 SE=1
	usleep(1000);

	// Accumulator Reset
	outb(0X84, portc0);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc2);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc4);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc6);         // BLK=1 INT=0 RST=0 SE=1
	usleep(1000);
	outb(0X94, portc0);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc2);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc4);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc6);         // BLK=1 INT=0 RST=1 SE=1
	usleep(1000);

	/* SE is always high until the program is finished. */
}

void ModeSet(int Mode)     /* Set Modes of Correlator by User Input */
{
   	switch(Mode){
		case 1 : 
		{	// 200 MHz CLOCK, 100 MHz BW, 2048 CHANNEL
			pcosData.nBandMode = 1; 	//100MHz BW
			pcosData.cBitMask = 0X80;
			outb(pcosData.cBitMask, portc1);         // 100MHz=1 OE=0 STB=0
			usleep(1000);
			break;
		}
		case 2 : 
		{	// 100 MHz CLOCK, 50 MHz BW, 4096 CHANNEL
			pcosData.nBandMode = 2; 	//50MHz BW
			pcosData.cBitMask = 0X00;
			outb(pcosData.cBitMask, portc1);         // 100MHz=0 OE=0 STB=0
			usleep(1000);
			break;
		}
		default : 
		{	// 200 MHz CLOCK, 100 MHz BW, 2048 CHANNEL
			break;
		}
	}
}

void DataInit()
{
   	int i;

//	pcosData.nBandMode = 1; 	//100MHz BW
	
	for(i=0; i<ASIZE; i++)
	{
		pcosData.nDataLSB[i] = 0;
		pcosData.nDataMSB[i] = 0;
		pcosData.dAcrData1[i] = 0.0;
		pcosData.dAcrData2[i] = 0.0;
		pcosData.dAcrData3[i] = 0.0;
		pcosData.dAcrData4[i] = 0.0;
	}
	for(i=0; i<SIZE100+1; i++)
	{
		pcosData.dAcrData100[i]=0.0;
	}
	
	for(i=0; i<SIZE50+1; i++)
	{
		pcosData.dAcrData50[i]=0.0;
	}		

}

void AccReset()         /* Accumulator Reset & Intial Integration */
{
	// Accumulator Reset
	outb(0X84, portc0);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc2);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc4);         // BLK=1 INT=0 RST=0 SE=1
	outb(0X84, portc6);         // BLK=1 INT=0 RST=0 SE=1
	usleep(1000);
	outb(0X94, portc0);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc2);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc4);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc6);         // BLK=1 INT=0 RST=1 SE=1
	usleep(1000);
}
/*
int IntegSegForCorr(int tseg) {
	// Start Integration
	outb(0X54, portc0);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc2);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc4);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc6);         // BLK=0 INT=1 RST=1 SE=1
	usleep(tseg*1000);

	// Finish Integration
	outb(0X94, portc0);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc2);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc4);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94 ,portc6);         // BLK=1 INT=0 RST=1 SE=1

	usleep(1000);

	// Chip1
	pcosData.cBitMask = pcosData.cBitMask | OEHIGH_OR;
	pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
	outb(pcosData.cBitMask, portc1);     // OE=1 STB=0
	usleep(1000);
	DataAcquisition1();
	DataProcessing1();
	pcosData.cBitMask = pcosData.cBitMask & OELOW_AND;
	pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
	outb(pcosData.cBitMask, portc1);	   // OE=0 STB=0
	outb(0X20, portc3);         // OE=1 STB=0
	usleep(1000);
	DataAcquisition2();
	DataProcessing2();
	outb(0X00, portc3);         // OE=0 STB=0
	outb(0X40, portc5);         // OE=1 STB=0
	usleep(1000);
	DataAcquisition3();
	DataProcessing3();
	outb(0X00, portc5);         // OE=0 STB=0
	outb(0X80, portc7);         // OE=1 STB=0
	usleep(1000);
	DataAcquisition4();
	DataProcessing4();
	outb(0X00, portc7);         // OE=0 STB=0
	
	if(pcosData.nIntegFlag == 0)
	{
		return -1;
	}
	pcosData.nProgressTime++;
		
	return 0;		
}
*/

int IntegSegForCorr(int tseg) {
	// Start Integration
	outb(0X54, portc0);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc2);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc4);         // BLK=0 INT=1 RST=1 SE=1
	outb(0X54, portc6);         // BLK=0 INT=1 RST=1 SE=1
	usleep(tseg*1000);

	// Finish Integration
	outb(0X94, portc0);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc2);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94, portc4);         // BLK=1 INT=0 RST=1 SE=1
	outb(0X94 ,portc6);         // BLK=1 INT=0 RST=1 SE=1

	usleep(1000);

	// Chip1
	pcosData.cBitMask = pcosData.cBitMask | OEHIGH_OR;
	pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
	outb(pcosData.cBitMask, portc1);     // OE=1 STB=0
	usleep(100);
	outb(0X20, portc3);         // OE=1 STB=0
	usleep(100);
	outb(0X40, portc5);         // OE=1 STB=0
	usleep(100);
	outb(0X80, portc7);         // OE=1 STB=0
	usleep(100);
	DataAcquisition1();
	DataAcquisition2();
	DataAcquisition3();
	DataAcquisition4();
//	usleep(1000);
	DataProcessing1();
	DataProcessing2();
	DataProcessing3();
	DataProcessing4();
	pcosData.cBitMask = pcosData.cBitMask & OELOW_AND;
	pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
	outb(pcosData.cBitMask, portc1);	   // OE=0 STB=0
	usleep(100);
	outb(0X00, portc3);         // OE=0 STB=0
	usleep(100);
	outb(0X00, portc5);         // OE=0 STB=0
	usleep(100);
	outb(0X00, portc7);         // OE=0 STB=0
	usleep(100);
	
	if(pcosData.nIntegFlag == 0)
	{
		return -1;
	}
	pcosData.nProgressTime++;
		
	return 0;		
}
// Processing Integration
int Integration(int IntegTime)           
{
	unsigned int ctr=0;
  	unsigned int i=0, j=0;

	pcosData.nProgressTime = 1;
	DataInit();              // Data Buffer intialization
	AccReset();              // Accumulator Reset & Initail Integration
	
	ctr=1000/MIN_INT_TIME;           // Loop Controller Calculation

	for(i=0; i<IntegTime; i++)
	{
		for(j=0; j<ctr; j++)
		{
			// Start Integration
			outb(0X54, portc0);         // BLK=0 INT=1 RST=1 SE=1
			outb(0X54, portc2);         // BLK=0 INT=1 RST=1 SE=1
			outb(0X54, portc4);         // BLK=0 INT=1 RST=1 SE=1
			outb(0X54, portc6);         // BLK=0 INT=1 RST=1 SE=1
			usleep(MIN_INT_TIME*1000);

			// Finish Integration
			outb(0X94, portc0);         // BLK=1 INT=0 RST=1 SE=1
			outb(0X94, portc2);         // BLK=1 INT=0 RST=1 SE=1
			outb(0X94, portc4);         // BLK=1 INT=0 RST=1 SE=1
			outb(0X94 ,portc6);         // BLK=1 INT=0 RST=1 SE=1

			usleep(1000);

			// Chip1
			pcosData.cBitMask = pcosData.cBitMask | OEHIGH_OR;
			pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
			outb(pcosData.cBitMask, portc1);     // OE=1 STB=0
			usleep(1000);
			DataAcquisition1();
			DataProcessing1();
			pcosData.cBitMask = pcosData.cBitMask & OELOW_AND;
			pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
			outb(pcosData.cBitMask, portc1);	   // OE=0 STB=0

			// Chip2
			outb(0X20, portc3);         // OE=1 STB=0
			usleep(1000);
			DataAcquisition2();
			DataProcessing2();
			outb(0X00, portc3);         // OE=0 STB=0

			// Chip3
			outb(0X40, portc5);         // OE=1 STB=0
			usleep(1000);
			DataAcquisition3();
			DataProcessing3();
			outb(0X00, portc5);         // OE=0 STB=0

			// Chip4
			outb(0X80, portc7);         // OE=1 STB=0
			usleep(1000);
			DataAcquisition4();
			DataProcessing4();
			outb(0X00, portc7);         // OE=0 STB=0
			
			if(pcosData.nIntegFlag == 0)
			{
				goto INTEG_QUIT;
			}
		}
		pcosData.nProgressTime++;
	}
	usleep(DELAY*1000);
	
INTEG_QUIT:
	if(pcosData.nIntegFlag == 0)
	{
		return -1;
	}else{
		return 0;
	}
	// Integration Aborted
}

void FindError()
{
	if(pcosData.dAcrData1[1025] != 0) pcosData.nChipErrorFlag1 = 1;
	if(pcosData.dAcrData2[1025] != 0) pcosData.nChipErrorFlag2 = 1;
	if(pcosData.dAcrData3[1025] != 0) pcosData.nChipErrorFlag3 = 1;
	if(pcosData.dAcrData4[1025] != 0) pcosData.nChipErrorFlag4 = 1;
}

void DataAcquisition1()         /* Data Aquisition Loop */
{
  unsigned int i=0;

	for(i=0;i<ASIZE;i++){
		// Read Control High
		pcosData.cBitMask = pcosData.cBitMask | OEHIGH_OR;
		pcosData.cBitMask = pcosData.cBitMask | STBHIGH_OR;
		outb(pcosData.cBitMask, portc1);  // OE=1 STB=1
		
		// Read Control Low
		pcosData.cBitMask = pcosData.cBitMask | OEHIGH_OR;
		pcosData.cBitMask = pcosData.cBitMask & STBLOW_AND;
		outb(pcosData.cBitMask, portc1);  // OE=1 STB=0

		pcosData.nDataLSB[i]=inw(porta0);   // LSB Data Input
		pcosData.nDataMSB[i]=inw(porta1);   // MSB Data Input
	}

	return;
}

void DataAcquisition2()         /* Data Aquisition Loop */
{
  unsigned int i=0;

	for(i=0;i<ASIZE;i++){
		// Read Control High
		outb(0X22, portc3);     // OE=1 STB=1
		// Read Control Low
		outb(0X20, portc3);     // OE=1 STB=0

		pcosData.nDataLSB[i]=inw(porta2);   // LSB Data Input
		pcosData.nDataMSB[i]=inw(porta3);   // MSB Data Input
	}

	return;
}

void DataAcquisition3()         /* Data Aquisition Loop */
{
  unsigned int i=0;

	for(i=0;i<ASIZE;i++){
		// Read Control High
		outb(0X44, portc5);     // OE=1 STB=1
		// Read Control Low
		outb(0X40, portc5);     // OE=1 STB=0

		pcosData.nDataLSB[i]=inw(porta4);   // LSB Data Input
		pcosData.nDataMSB[i]=inw(porta5);   // MSB Data Input
	}

	return;
}

void DataAcquisition4()         /* Data Aquisition Loop */
{
  unsigned int i=0;

	for(i=0;i<ASIZE;i++){
		// Read Control High
		outb(0X88, portc7);     // OE=1 STB=1
		// Read Control Low
		outb(0X80, portc7);     // OE=1 STB=0

		pcosData.nDataLSB[i]=inw(porta6);   // LSB Data Input
		pcosData.nDataMSB[i]=inw(porta7);   // MSB Data Input
	}

	return;
}

void DataProcessing1()            /* 8 bit to 32 bit Data Processing Module */
{
  unsigned int i;

	for(i=0;i<ASIZE;i++){
		pcosData.dAcrData1[i]+=((float)((pcosData.nDataLSB[i]&0X0000FFFF)|(((pcosData.nDataMSB[i]&0X0000FFFF)<<16)&0XFFFF0000)));
	}

	return;
}

void DataProcessing2()            /* 8 bit to 32 bit Data Processing Module */
{
  unsigned int i;

	for(i=0;i<ASIZE;i++){
		pcosData.dAcrData2[i]+=((float)((pcosData.nDataLSB[i]&0X0000FFFF)|(((pcosData.nDataMSB[i]&0X0000FFFF)<<16)&0XFFFF0000)));
	}

	return;
}

void DataProcessing3()            /* 8 bit to 32 bit Data Processing Module */
{
  unsigned int i;

	for(i=0;i<ASIZE;i++){
		pcosData.dAcrData3[i]+=((float)((pcosData.nDataLSB[i]&0X0000FFFF)|(((pcosData.nDataMSB[i]&0X0000FFFF)<<16)&0XFFFF0000)));
	}

	return;
}

void DataProcessing4()            /* 8 bit to 32 bit Data Processing Module */
{
  unsigned int i;

	for(i=0;i<ASIZE;i++){
		pcosData.dAcrData4[i]+=((float)((pcosData.nDataLSB[i]&0X0000FFFF)|(((pcosData.nDataMSB[i]&0X0000FFFF)<<16)&0XFFFF0000)));
	}

	return;
}

void SegDataProcessingAll(double IntegTime)
{
	int i;
	float DataTemp[ASIZE-2];

	i = 0;
	
	if(pcosData.nBandMode == 1)
	{
		SegDataAverage(IntegTime, pcosData.dAcrData1);
		SegDataAverage(IntegTime, pcosData.dAcrData2);
		SegDataAverage(IntegTime, pcosData.dAcrData3);
		SegDataAverage(IntegTime, pcosData.dAcrData4);
		
		//PrintData();
		MakeAC(0, 1023, pcosData.dAcrData1);
		MakeAC(0, 1023, pcosData.dAcrData2);
		MakeAC(0, 1023, pcosData.dAcrData3);
		MakeAC(0, 1023, pcosData.dAcrData4);

		for(i=0; i<ASIZE-2; i++) // 0 to 1023
		{
			// 0,2,4...2046 index
	    pcosData.dAcrData100[i*2] = pcosData.dAcrData1[i] + pcosData.dAcrData4[i];
//		pcosData.dAcrData100[i*2] = 2.0*pcosData.dAcrData1[i]/pcosData.dAcrData1[1024]-1.0 + 
//				                    2.0*pcosData.dAcrData4[i]/pcosData.dAcrData4[1024]-1.0;
		}  
		
		// Manipulate AcrData2 -.
		for(i=0; i<ASIZE-3; i++) // 0 to 1022
		{
			DataTemp[i] = pcosData.dAcrData2[i+1] + pcosData.dAcrData3[i];
//			DataTemp[i] = 2.0*pcosData.dAcrData2[i+1]/pcosData.dAcrData2[1024]-1.0 + 
//					      2.0*pcosData.dAcrData3[i]/pcosData.dAcrData3[1024]-1.0;
		}
		DataTemp[ASIZE-3] = pcosData.dAcrData3[ASIZE-3]*2.0; // 1023 index
//		DataTemp[ASIZE-3] = (2.0*pcosData.dAcrData3[ASIZE-3]/pcosData.dAcrData3[1024]-1.0)*2.0; // 1023 index
		// <--
		
		for(i=1; i<ASIZE-1; i++) // 1 to 1024
		{
			// 1,3,5...2047 index
			pcosData.dAcrData100[i*2-1] = DataTemp[i-1];
		}
		
		for(i=0; i<SIZE100; i++)
		{
			pcosData.dAcrData100[i] /= 2.0;
		}			
		
//		DataNormalize();
		
		// For FFT Processing
		pcosData.dAcrData100[SIZE100] = 1.0;
	}else{
		SegDataAverage(IntegTime, pcosData.dAcrData1);
		SegDataAverage(IntegTime, pcosData.dAcrData2);
		SegDataAverage(IntegTime, pcosData.dAcrData3);
		SegDataAverage(IntegTime, pcosData.dAcrData4);
		
//		PrintData();
		
		MakeAC(0, 1023, pcosData.dAcrData1);
		MakeAC(0, 1023, pcosData.dAcrData2);
		MakeAC(0, 1023, pcosData.dAcrData3);
		MakeAC(0, 1023, pcosData.dAcrData4);
		
		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i] = pcosData.dAcrData1[i];
		}  

		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i+ASIZE-2] = pcosData.dAcrData2[i];
		}         
		
		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i+2*(ASIZE-2)] = pcosData.dAcrData3[i];
		}
		
		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i+3*(ASIZE-2)] = pcosData.dAcrData4[i];
		}
		
		//DataNormalize();
		
		// For FFT Processing
		pcosData.dAcrData50[SIZE50] = 1.0;
	}
}

// Data Average to nIntTime
void SegDataAverage(double IntegTime, double Data[])
{
  	int i;
  	//int ctr;
  	
  	i = 0;
 	//ctr = 1000/MIN_INT_TIME;		// Loop Controller Calculation

	for(i=0; i<ASIZE; i++)			// Average Data to IntegTime
	{        	
		//Data[i]/=(IntegTime*ctrl);
		Data[i]/=(IntegTime);
	}
}

/* Normailize Data to Total number of Samples */
void DataProcessingAll(int IntegTime)
{
	int i;
	float DataTemp[ASIZE-2];

	i = 0;
	
	if(pcosData.nBandMode == 1)
	{
		DataAverage(IntegTime, pcosData.dAcrData1);
		DataAverage(IntegTime, pcosData.dAcrData2);
		DataAverage(IntegTime, pcosData.dAcrData3);
		DataAverage(IntegTime, pcosData.dAcrData4);
		
		//PrintData();
		MakeAC(0, 1023, pcosData.dAcrData1);
		MakeAC(0, 1023, pcosData.dAcrData2);
		MakeAC(0, 1023, pcosData.dAcrData3);
		MakeAC(0, 1023, pcosData.dAcrData4);

		for(i=0; i<ASIZE-2; i++) // 0 to 1023
		{
			// 0,2,4...2046 index
//	    pcosData.dAcrData100[i*2] = pcosData.dAcrData1[i] + pcosData.dAcrData4[i];
		pcosData.dAcrData100[i*2] = 2.0*pcosData.dAcrData1[i]/pcosData.dAcrData1[1024]-1.0 + 
				                    2.0*pcosData.dAcrData4[i]/pcosData.dAcrData4[1024]-1.0;
		}  
		
		// Manipulate AcrData2 -.
		for(i=0; i<ASIZE-3; i++) // 0 to 1022
		{
//			DataTemp[i] = pcosData.dAcrData2[i+1] + pcosData.dAcrData3[i];
			DataTemp[i] = 2.0*pcosData.dAcrData2[i+1]/pcosData.dAcrData2[1024]-1.0 + 
					      2.0*pcosData.dAcrData3[i]/pcosData.dAcrData3[1024]-1.0;
		}
//		DataTemp[ASIZE-3] = pcosData.dAcrData3[ASIZE-3]*2.0; // 1023 index
		DataTemp[ASIZE-3] = (2.0*pcosData.dAcrData3[ASIZE-3]/pcosData.dAcrData3[1024]-1.0)*2.0; // 1023 index
		// <--
		
		for(i=1; i<ASIZE-1; i++) // 1 to 1024
		{
			// 1,3,5...2047 index
			pcosData.dAcrData100[i*2-1] = DataTemp[i-1];
		}
		
		for(i=0; i<SIZE100; i++)
		{
			pcosData.dAcrData100[i] /= 2.0;
		}			
		
		//DataNormalize();
		
		// For FFT Processing
		pcosData.dAcrData100[SIZE100] = 1.0;
	}else{
		DataAverage(IntegTime, pcosData.dAcrData1);
		DataAverage(IntegTime, pcosData.dAcrData2);
		DataAverage(IntegTime, pcosData.dAcrData3);
		DataAverage(IntegTime, pcosData.dAcrData4);
		
//		PrintData();
		
		MakeAC(0, 1023, pcosData.dAcrData1);
		MakeAC(0, 1023, pcosData.dAcrData2);
		MakeAC(0, 1023, pcosData.dAcrData3);
		MakeAC(0, 1023, pcosData.dAcrData4);
		
		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i] = pcosData.dAcrData1[i];
		}  

		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i+ASIZE-2] = pcosData.dAcrData2[i];
		}         
		
		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i+2*(ASIZE-2)] = pcosData.dAcrData3[i];
		}
		
		for(i=0; i<ASIZE-2; i++)
		{
			pcosData.dAcrData50[i+3*(ASIZE-2)] = pcosData.dAcrData4[i];
		}
		
		//DataNormalize();
		
		// For FFT Processing
		pcosData.dAcrData50[SIZE50] = 1.0;
	}
}

// Data Average to nIntTime
void DataAverage(int IntegTime, double Data[])
{
  	int i;
  	//int ctr;
  	
  	i = 0;
 	//ctr = 1000/MIN_INT_TIME;		// Loop Controller Calculation

	for(i=0; i<ASIZE; i++)			// Average Data to IntegTime
	{        	
		//Data[i]/=(IntegTime*ctrl);
		Data[i]/=(IntegTime);
	}
}

/* Normailize Data to Total number of Samples */
void DataNormalize()   	 
{
	int i;
	double NormalFactor;

	i = 0;
	NormalFactor = 1.0;
	
	if(pcosData.nBandMode == 1)
	{
		NormalFactor = (pcosData.dAcrData1[ASIZE-2] +
						pcosData.dAcrData2[ASIZE-2] +
						pcosData.dAcrData3[ASIZE-2] +
						pcosData.dAcrData4[ASIZE-2])/2.0;
			
		//NormalFactor = 200000000.;
		//printf("100 MHz chip1: %f\t%f\n", dAcrData1[ASIZE-2], dAcrData1[ASIZE-1]);
		//printf("100 MHz chip2: %f\t%f\n", dAcrData2[ASIZE-2], dAcrData2[ASIZE-1]);
		//printf("100 MHz chip3: %f\t%f\n", dAcrData3[ASIZE-2], dAcrData3[ASIZE-1]);
		//printf("100 MHz chip4: %f\t%f\n\n", dAcrData4[ASIZE-2], dAcrData4[ASIZE-1]);
		
		// Make AC Correction
		for(i=0; i<SIZE100; i++)
		{
			pcosData.dAcrData100[i] = 
					2.0*(pcosData.dAcrData100[i]/NormalFactor) - 1.0;
		}

		// for Processing in FFT routine
		pcosData.dAcrData100[SIZE100] = 1.0;
	}else{
		NormalFactor = (pcosData.dAcrData1[ASIZE-2] +
						pcosData.dAcrData2[ASIZE-2] +
						pcosData.dAcrData3[ASIZE-2] +
						pcosData.dAcrData4[ASIZE-2])/4.0;
		
		//NormalFactor = 100000000.;
		//printf("50 MHz chip1: %f\t%f\n", dAcrData1[ASIZE-2], dAcrData1[ASIZE-1]);
		//printf("50 MHz chip2: %f\t%f\n", dAcrData2[ASIZE-2], dAcrData2[ASIZE-1]);
		//printf("50 MHz chip3: %f\t%f\n", dAcrData3[ASIZE-2], dAcrData3[ASIZE-1]);
		//printf("50 MHz chip4: %f\t%f\n\n", dAcrData4[ASIZE-2], dAcrData4[ASIZE-1]);
		
		// Make AC Correction
		for(i=0; i<SIZE50; i++)
		{
			pcosData.dAcrData50[i] = 
					2.0*(pcosData.dAcrData50[i]/NormalFactor) - 1.0;
		}
		
		// for Processing in FFT routine
		pcosData.dAcrData50[SIZE50] = 1.0;
	}
}

void PrintData()
{
	if(pcosData.nBandMode == 1)
	{
		printf("100 MHz chip1: %f\t%f\n", pcosData.dAcrData1[ASIZE-2], pcosData.dAcrData1[ASIZE-1]);
		printf("100 MHz chip2: %f\t%f\n", pcosData.dAcrData2[ASIZE-2], pcosData.dAcrData2[ASIZE-1]);
		printf("100 MHz chip3: %f\t%f\n", pcosData.dAcrData3[ASIZE-2], pcosData.dAcrData3[ASIZE-1]);
		printf("100 MHz chip4: %f\t%f\n\n", pcosData.dAcrData4[ASIZE-2], pcosData.dAcrData4[ASIZE-1]);
	}else{
		printf("50 MHz chip1: %f\t%f\n", pcosData.dAcrData1[ASIZE-2], pcosData.dAcrData1[ASIZE-1]);
		printf("50 MHz chip2: %f\t%f\n", pcosData.dAcrData2[ASIZE-2], pcosData.dAcrData2[ASIZE-1]);
		printf("50 MHz chip3: %f\t%f\n", pcosData.dAcrData3[ASIZE-2], pcosData.dAcrData3[ASIZE-1]);
		printf("50 MHz chip4: %f\t%f\n\n", pcosData.dAcrData4[ASIZE-2], pcosData.dAcrData4[ASIZE-1]);
	}
}

void InitSys(int Mode)
{
	InitDIO96();
	fprintf(stderr,"InitDIO96 is done\n");
	InitCorr();
	fprintf(stderr,"InitCorr is done\n");
	ModeSet(Mode);
}

void ResetErrorFlag(void)
{
	// Reset Error Flag
	pcosData.nChipErrorFlag1 = 0;
	pcosData.nChipErrorFlag2 = 0;
	pcosData.nChipErrorFlag3 = 0;
	pcosData.nChipErrorFlag4 = 0;

	pcosData.nIntegFlag=1;

	pcosData.nProgressTime = 1;
	return;
}

void StartObs(int IntegTime)
{
	// Reset Error Flag
	pcosData.nChipErrorFlag1 = 0;
	pcosData.nChipErrorFlag2 = 0;
	pcosData.nChipErrorFlag3 = 0;
	pcosData.nChipErrorFlag4 = 0;

	pcosData.nIntegFlag=1;

	Integration(IntegTime);
	DataProcessingAll(IntegTime);
	
	if(pcosData.nIntegFlag == 1) // Integration success
	{
		FindError();
						
		//FFT start
		RunFFT();
		//FFT end

		// added by zoo
		usleep(5000);
		// end of zoo
		//printf("obs success\n");
		pcosData.nObsFlag = 1; // obs success
	}else{
		//printf("obs fail\n");
		pcosData.nObsFlag = 0; // obs fail
	}
}

void StopObs()
{
	InitCorr();

	// Initial State
	outb(0X90, portc0);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc2);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc4);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc6);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X80, portc1);		   // 100MHz=1 OE=0  STB=0
	outb(0X00, portc3);		   // OE=0  STB=0
	outb(0X00, portc5);		   // OE=0  STB=0
	outb(0X00, portc7);		   // OE=0  STB=0
}

void QuitSys()
{
	InitCorr();

	// Initial State
	outb(0X90, portc0);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc2);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc4);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X90, portc6);         // BLK=1 INT=0 RST=1 SE=0
	outb(0X80, portc1);		   // 100MHz=1 OE=0  STB=0
	outb(0X00, portc3);		   // OE=0  STB=0
	outb(0X00, portc5);		   // OE=0  STB=0
	outb(0X00, portc7);		   // OE=0  STB=0
}

void ApplyWinFunc(int Select, double Data[])
{
    int i, WinCoeffNum;
	double *dWinFuncCoef;
    
	if(pcosData.nBandMode == 1)
	{
		WinCoeffNum = SIZE100;
	}else{
		WinCoeffNum = SIZE50;
	}
	dWinFuncCoef = (double *)malloc(WinCoeffNum*sizeof(double));
    
	switch(Select)
    {
        case 0: // Hanning
            for(i=0; i<WinCoeffNum; i++)
            {
                dWinFuncCoef[i] = 0.5*( 1.0 - cos( 2.0*M_PI*i/(WinCoeffNum - 1) ) );
            }
            break;

        case 1: // Hamming
            for(i=0; i<WinCoeffNum; i++)
            {
                dWinFuncCoef[i] = 0.54 - 0.46*cos( 2.0*M_PI*i/(WinCoeffNum - 1) ); 
            }
			break;

		case 2: // Blackman-Harris
            for(i=0; i<WinCoeffNum; i++)
            {
                dWinFuncCoef[i] = 0.35875 - 0.48829*cos( 2.0*M_PI*i/(WinCoeffNum - 1) )
                                          + 0.14128*cos( 4.0*M_PI*i/(WinCoeffNum - 1) )
                                          - 0.01168*cos( 6.0*M_PI*i/(WinCoeffNum - 1) );
            }
			break;

        case 3: // Bartlett
            for(i=0; i<(WinCoeffNum/2); i++)
            {
                dWinFuncCoef[i] = 2.0*i/(WinCoeffNum - 1);
            }
            for(i=(WinCoeffNum/2); i<WinCoeffNum; i++)
            {
                dWinFuncCoef[i] = ( 2.0*(WinCoeffNum - i - 1) )/(WinCoeffNum - 1);
            }
			break;

        case 4: // Rectangular
            for(i=0; i<WinCoeffNum; i++)
            {
                dWinFuncCoef[i] = 1.0;
            }
			break;
		
		default: // Hanning
            for(i=0; i<WinCoeffNum; i++)
            {
                dWinFuncCoef[i] = 0.5*( 1.0 - cos( 2.0*M_PI*i/(WinCoeffNum - 1) ) );
            }
            break;
    }

	if(pcosData.nBandMode == 1)
	{
		for(i=0; i<SIZE100; i++)
		{
			Data[i] = pcosData.dAcrData100[i]*dWinFuncCoef[i];
		}
	}else{
		for(i=0; i<SIZE50; i++)
		{
			Data[i] = pcosData.dAcrData50[i]*dWinFuncCoef[i];
		}
	}
	
	free(dWinFuncCoef);
    // for test
    /*
    FILE *fout;
  	fout=fopen("winfunc.txt", "wt");
	for(i=0; i<nFFTCounter; i++)
	{
		fprintf(fout, "%10.6f \n", dWinFuncCoef[i]);
	}
	fclose(fout);
    */
    // for test
}

void RunFFT()
{
	fftw_complex *in, *out;
    fftw_plan p;
    double NormalFactor, CTR;
	double RTemp, ITemp;
	double *dDataPoint;
	int i, nFFTSize;

	// Processing Data
	// Normalize
	if(pcosData.nBandMode == 1)
	{
		nFFTSize = SIZE100;
		NormalFactor = pcosData.dAcrData100[nFFTSize];
		dDataPoint = (double *)malloc(nFFTSize*sizeof(double));
		for(i=0; i<SIZE100; i++)
		{
			dDataPoint[i] = pcosData.dAcrData100[i];
		}
	}else{
		nFFTSize = SIZE50;
		NormalFactor = pcosData.dAcrData50[nFFTSize];
		dDataPoint = (double *)malloc(nFFTSize*sizeof(double));
		for(i=0; i<SIZE50; i++)
		{
			dDataPoint[i] = pcosData.dAcrData50[i];
		}
	}

	// ACR Correction
	if(pcosData.nAcrCorrect == 1)
	{
		CTR=dDataPoint[0];
		for(i=0; i<nFFTSize; i++)
		{
			if(dDataPoint[i]>=0.5)
			{
				dDataPoint[i]=cos(3.78862019*(CTR - dDataPoint[i]));
			}else if(dDataPoint[i]<0.5 && dDataPoint[i]>-0.5)
			{
				dDataPoint[i]=2.28453222*dDataPoint[i];
			}else
			{
				dDataPoint[i]=(-1.)*cos(3.78862019*(CTR - dDataPoint[i]));
			}
		}
	}
	// The End of Processing Data
	
	// Apply Window Function
    ApplyWinFunc(pcosData.nWinFuncSel, dDataPoint);
	    
	in = (fftw_complex *)malloc(2*nFFTSize * sizeof(fftw_complex));
	out = (fftw_complex *)malloc(2*nFFTSize * sizeof(fftw_complex));

	// Paste Data For FFT
	for (i=0; i<nFFTSize; i++)
	{
		in[i].re = dDataPoint[i];
		in[i].im = 0.;
	}
	for (i=nFFTSize; i<(2*nFFTSize); i++)
	{
		in[i].re = dDataPoint[(2*nFFTSize) - i - 1];
		in[i].im = 0.;
	}

    p = fftw_create_plan(2*nFFTSize, FFTW_FORWARD, FFTW_ESTIMATE);

	fftw_one(p, in, out);

	for (i=0; i<nFFTSize; i++) {
		RTemp = c_re(out[i]);
		ITemp = c_im(out[i]);
		dDataPoint[i] = sqrt(pow(RTemp, 2) + pow(ITemp, 2));
		//dDataPoint[i] = 10.*log10(dDataPoint[i]);
	}
	
	if(pcosData.nBandMode == 1)
	{
		for(i=0; i<SIZE100; i++)
		{
			pcosData.dFFTData100[i] = dDataPoint[i];
		}
	}else{
		for(i=0; i<SIZE50; i++)
		{
			pcosData.dFFTData50[i] = dDataPoint[i];
		}
	}
	
	pcosData.dCalCoeff = 1.0;
	/*
	dCalCoeff = 0.010847097;
	if(pcosData.nBandMode == 1)
	{
		for(i=0; i<SIZE100; i++)
		{
			pcosData.dFFTData100[i] = dDataPoint[i]*dCalCoeff;
		}
	}else{
		for(i=0; i<SIZE50; i++)
		{
			pcosData.dFFTData50[i] = dDataPoint[i]*dCalCoeff;
		}
	}
	*/
    fftw_destroy_plan(p);
    fftw_free(in);
	fftw_free(out);
	free(dDataPoint);
}

void cFileSave(const char* Header, double Data[], const char* FileName, int Range)
{
	FILE *fout;
	int i = 0;

	fout = fopen(FileName, "wt");


	fprintf(fout, "%s", Header);

	for(i=0; i<Range; i++)
	{
		fprintf(fout, "%40.20f\n", Data[i]);
	}

	fclose(fout);
}

void MakeAC(int first, int second, double dData[])
{
	double dMid, dTemp;
	int i;
	
	dTemp = 0.0;
	for(i=first; i<=second; i++)
	{
		dTemp += dData[i];
	}
	
	dMid = dTemp/(second - first + 1);
	
	for(i=first; i<=second; i++)
	{
//		dData[i] = (dData[i]/dMid) - 1.0;
		dData[i] = (dData[i] - dMid)/100000000;
	}

//	fprintf(stderr,"dMid : %lf\n", dMid);

	/*
	dMid = dTemp/(second - first + 1);
	
	for(i=first; i<=second; i++)
	{
		dData[i] = (dData[i]/dMid) - 1.0;
	}
	*/
}

void SendStopSignal(void)
{
		pcosData.nIntegFlag=0;
		return;
}

int GetBandMode(void)
{
		return (pcosData.nBandMode);
}

void CommandFileSave(char *header, char *filename)
{
	if(pcosData.nBandMode==1)
	{
			cFileSave(header,pcosData.dFFTData100,filename, SIZE100);
	}
	else {
			cFileSave(header,pcosData.dFFTData50,filename, SIZE50);
	}
	return;
}
void wrBinData(int socket_fd, int tick, char buf[])
{
	struct timeval tv;
	struct timezone tz;
	struct tm *tm;
	int wbuf;
	char sendbuf[CORDATASIZE+1];
	int i=0, i_buf=0;
	double acr_0=0;
	
	gettimeofday(&tv,&tz);
	tm = localtime(&tv.tv_sec);
	//
	wbuf=(1900+tm->tm_year);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(1+tm->tm_mon);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(tm->tm_mday);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(tm->tm_hour);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(tm->tm_min);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(tm->tm_sec);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=((int)(tv.tv_usec/1000));
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(tick);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	if(pcosData.nBandMode==1) wbuf=(100);
	else wbuf=(50);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	wbuf=(lib_tseg);
	memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;
	for(i=0;i<10;i++){ wbuf=(0); memcpy(sendbuf+i_buf,&wbuf,sizeof(int));i_buf+=4;}
	if(pcosData.nBandMode==1) {
		acr_0=pcosData.dAcrData100[0];
		fprintf(stderr,"ACR0 & 1 : %lf\t%lf\n",pcosData.dAcrData100[0],pcosData.dAcrData100[1]);
		spec(pcosData.dFFTData100,pcosData.dAcrData100,SIZE100);
		pcosData.dFFTData100[0]=acr_0;
		for(i=0;i<SIZE100;i++) sendData[i]=(pcosData.dFFTData100[i]);
		memcpy(sendbuf+i_buf,sendData,sizeof(double)*SIZE100);
		strncpy(sendbuf+i_buf+8*SIZE100,"\r",1);
	}
	else {
		acr_0=pcosData.dAcrData50[0];
		fprintf(stderr,"ACR0 & 1 : %lf\t%lf\n",pcosData.dAcrData100[0],pcosData.dAcrData100[1]);
		spec(pcosData.dFFTData50,pcosData.dAcrData50,SIZE50);
		pcosData.dFFTData50[0]=acr_0;
		for(i=0;i<SIZE50;i++) sendData[i]=(pcosData.dFFTData50[i]);
		memcpy(sendbuf+i_buf,sendData,sizeof(double)*SIZE50);
		strncpy(sendbuf+i_buf+8*SIZE50,"\r",1);
	}

	memcpy(buf,sendbuf,CORDATASIZE+1);
//	send(socket_fd,sendbuf,CORDATASIZE+1,0);
	
	return;
}
