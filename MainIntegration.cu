#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <signal.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>

//#include "define.h"		//Corr structure included
//#include "ansicosCal.h"
#include "main.h"
#include "socket.h"
#include "lib_kfftspec.h"
//#include "cmdfromspecom.h"

#define SHM_KEY_FS	7
#define READY	1
#define INTEG	2

// command definition
#define SET_NFRAME	0X0001
#define SET_MBPS	0X0002
#define SET_IF_NUM	0X0004
#define SET_BIT_N	0X0008
#define SET_RX		(SET_NFRAME)|(SET_MBPS)|(SET_IF_NUM)|(SET_BIT_N) 
#define SET_FILE	0X0010

#define START_RX	0X1000
#define STOP_RX		0X0000
#define INIT_RX		0X2000

#define DAS_COMMAND	1
#define DAS_REQUEST	2
#define NO_COMMAND	-1

#define DATAMAX		4096
#define CORDATASIZE	(20*4+DATAMAX*8)	// 20*integer + max 4096*double

void *mainprocessing(void *);
void *dasprocessing(void *);
void doprocessing(int sock);
int CheckCommand(char *buf, char *sndcmd, int *cmdtype);
int CheckMultiCommand(char *buf, char *sndcmd, int *cmdtype, int *cmd);
void SetTickVal(fftspec_config_t* fs);
//void HaltCorr(void);
void CheckFork(fftspec_config_t* fs);
int FindValues(char *buffer, int len, char *ivalue);
int CheckHalt(int flag);
int ConvertIPLEN(double msec);

int old_tick=0;
int integ_flag=0, halt_flag=0;
int result;
int server_fd;
int running=1;

int cmdlist=22;
//ALERT : below ivalue must be euqal to 'cmdlist'
char CMD_KEYWORD[22][32]={
	"RESET",	 "LOAD_CONFIG",	  "SAVE_CONFIG",    "SET_DEFAULT",
	"START_COR",	 "STOP_COR",      "SEL_MAINPORT",	"SEL_CORIPLEN",
	"SET_CORIPNUM",	 "SEL_CORWINDOW", "SEL_CORMODE",	"SEL_NRWBW",
	"SEL_NRWSTREAM", "SEL_COROUTMODE","SET_COROUTCH",   "SEL_COROUTSTREAM",	
	"SET_STATUSMSK", "SET_NUMFFT",	  "OBNM",			"SAVE",
	"INPUT_STREAM", "INPUT_FORMAT"
};

int main()
{

   int newsockfd;
   
   struct sockaddr_in client_address;
   int client_len;
   int pid, th_id;
   int th_das_id;

   pthread_t thread_t;
   pthread_t thread_t_das;
   
   fftspec_config_t *fs;
   int nattach;
   connectshm_FS(SHM_KEY_FS,&fs);
   memset(fs,0x00,sizeof(fftspec_config_t));
#ifdef DEBUG
printf("SHM is attached\n");
#endif

   th_id = 0;
   th_das_id = 0;
//   corr->das=-1;
//   corr->dasfd=-1;
   
   //
   // Below is command server
   // 
   server_fd=create_server(FS_PORT);
   client_len=sizeof(struct sockaddr_in);
   if(server_fd<0) return -1;
   
#ifdef DEBUG
printf("FS server is created\n");
#endif
   fftspec_init_condition(fs);
#ifdef DEBUG
printf("FFTSPEC is initialized\n");
#endif

   th_das_id = pthread_create(&thread_t_das,NULL,&dasprocessing,NULL);
   th_id = pthread_create(&thread_t,NULL,&mainprocessing,NULL);

   	while (running) {
		newsockfd= accept(server_fd, (struct sockaddr *)&client_address,(socklen_t*) &client_len);
		if (newsockfd <0)
        {
            perror("ERROR on accept");
            exit(1);
        }
        /* Create child process */
        pid = fork();
        if (pid < 0)
        {
            perror("ERROR on fork");
        exit(1);
        }
        else if (pid >= 0)
        {
            // This is the client process 

           // close(server_fd);
            doprocessing(newsockfd);
			close(newsockfd);
            //exit(0);
        }
        else
        {
            close(newsockfd);
        }
      	running = CheckHalt(running);
//		if(running==0) close(server_fd);
//Doing managing correlator
    } /* end of while */
	if((nattach=shmdt(fs))==-1) fprintf(stderr,"shmdt failed\n");
	usleep(1000);
	if(nattach==0) {
		fprintf(stderr,"OK, any process is not attached to the memory area\n");
		usleep(1000);
		if(shmctl(SHM_KEY_FS,IPC_RMID,0)==-1) {
			   	fprintf(stderr,"Share memory cannot be closed\n");
				fprintf(stderr,"I tried to close the memory, but I couldn't\n");
				fprintf(stderr,"Please use ipcs and ipcrm -m to eliminate the memory\n");
		}
	}
	else {
		fprintf(stderr,"Trying to kill the memory...\n");
		if(shmctl(SHM_KEY_FS,IPC_RMID,0)==-1) {
				fprintf(stderr,"Share memory cannot be closed\n");
				fprintf(stderr,"Please use ipcs and ipcrm -m to eliminate the memory\n");
		}
	}
	pthread_detach(thread_t);
	pthread_detach(thread_t_das);
	fprintf(stderr,"Good bye\n");
   return 0;
}

void *dasprocessing(void *args)
{
	fftspec_config_t* fs;
	int das_fd;
	int client_len;
	int das_running=1;
	int newsockfd;
	int now_fd=-1;
   	struct sockaddr_in client_address;
	//char readbuf[256];

   	das_fd=create_server(DAS_PORT);
   	client_len=sizeof(struct sockaddr_in);

	if(das_fd<0) {
		fprintf(stderr,"Cannot make das server\n");
		exit(1);
	}
	connectshm_FS(SHM_KEY_FS, &fs);
	
   	while (das_running) {
		newsockfd= accept(das_fd, (struct sockaddr *)&client_address,(socklen_t *) &client_len);
		if (newsockfd <0)
        {
            perror("ERROR on accept");
            exit(1);
        }
		else if (now_fd!=newsockfd) {
			if (now_fd>0) {
				printf("Another das client is trying connection\n");
				if (fs->dasfd<0) {
						close(now_fd);
						fs->dasfd=newsockfd;
						now_fd=newsockfd;
				}

			}
			else if(now_fd<0) {
				printf("Das client is newly connected at %d\n",newsockfd);
				fs->dasfd=newsockfd;
				now_fd=newsockfd;
			}
			else {
			}
		}

      	das_running = CheckHalt(das_running);
    } /* end of while */

	close(das_fd);
	return NULL;
}

int CheckCommand(char *buf, char *sndcmd, int *cmdtype)
{
	int i, cmd;
    int len;
	char dummy_buf[256];

	len=0;
	cmd=-1;

	strcpy(dummy_buf,buf);

	for(i=0;i<256;i++) if(strncmp(&buf[i],"=",1)==0) {
		len=i;
	 	*cmdtype=DAS_COMMAND;
	}		
	if(len==0) {
		for(i=0;i<256;i++) {
			if(strncmp(&buf[i],"?",1)==0) len=i;
			*cmdtype=DAS_REQUEST;
		}
	}
	if(len==0) {
		*cmdtype=NO_COMMAND;
		return -1;
	}
	for(i=0;i<cmdlist;i++){
		if(strncmp(buf,CMD_KEYWORD[i],len)==0) cmd=i;
	};

	if(cmd>0) {
		strncpy(sndcmd,buf,len);
		strcpy(dummy_buf,buf);
		strcpy(buf,dummy_buf+len+1);
	}

#ifdef DEBUG
	printf("sndcmd:%s buf:%s\n",sndcmd,buf);
#endif
	return cmd;
}

int CheckMultiCommand(char *buf, char *sndcmd, int *cmdtype, int *cmd)
{
	int i, j;
	int len, icmd, ilen;
	char dummy_buf[256];

	len=0;
	ilen=0;
	icmd=0;
	*cmd=-1;
	*cmdtype=NO_COMMAND;

	memset(dummy_buf,'\0',256);
	strcpy(dummy_buf,buf);

	for(i=0;i<256;i++) if(strncmp(&dummy_buf[i],";",1)==0) {
		icmd++;
		ilen=i+1;
		break;
	}

	if(icmd!=0) {
		memset(buf,'\0',256);
		memset(sndcmd,'\0',256);
		strcpy(buf,dummy_buf+ilen);
		strncpy(sndcmd,dummy_buf,ilen);
		if(strncmp(dummy_buf,"?",1)==0) *cmdtype=DAS_REQUEST;
	
		for(i=0;i<ilen;i++) if(strncmp(&dummy_buf[i]," ",1)==0) { len=i; }
		if(len==0) len=i-1;
		for(i=0;i<cmdlist;i++) if(strncmp(dummy_buf,CMD_KEYWORD[i],len)==0){
			*cmdtype=DAS_COMMAND; *cmd=i; 
		}
		if(*cmd==-1 && *cmdtype!=DAS_REQUEST) *cmdtype=NO_COMMAND;
	}

#ifdef DEBUG
	printf("sndcmd:%s buf:%s icmd:%d  ilen:%d\n",sndcmd,buf,icmd,ilen);
#endif
	return icmd;
}

int FindValues(char *buf, int len, char* ivalue)
{
	int i, total_len;
	char dummy_buf[256];

	total_len=len;

	for(i=0;i<total_len;i++)
	{
		if((buf[i]==' ') || (buf[i] =='\0')) {
#ifdef DEBUG
			printf("UNIT IS FOUND\n");
#endif
			strncpy(ivalue,buf,i);
			len-=(i+1);
			if(len>0) {
				strcpy(dummy_buf,buf+(i+1));
				strcpy(buf,dummy_buf);
			}
			break;
		} else if(i==total_len-1) {
			strcpy(ivalue,buf);
			len=0;
			memset(buf,'\0',total_len);
		}
	}
#ifdef DEBUG
	printf("value:%s buf:%s with tlen %d len %d\n",ivalue,buf,total_len, len);
#endif

	return len;
}

int ConvertIPLEN(double msec)
{
	int bit=0;

	if		(msec ==  10.24) bit=1;
	else if	(msec ==  51.2 ) bit=2;
	else if	(msec == 102.4 ) bit=3;
	else if	(msec == 512.0 ) bit=4;
	else if	(msec ==1024.0 ) bit=5;
	else bit=0;

	return bit;
}

void doprocessing(int sock)
{
    int n;
	int thread_running=1;
    char buffer[256];
	char sndcmd[32];
	char ivalue[32];
	int ret,cmd;
	int ret_i;
	double ret_lf;
	char ret_buf[64];
	int cmdtype, buf_len;
	int ip_num,rx_frame,rx_mbps,rx_nsub,rx_nbit;

	ip_num=0;

    fftspec_config_t* fs;

    connectshm_FS(SHM_KEY_FS,&fs);

    signal(SIGPIPE,SIG_IGN);

    bzero(buffer,256);
	while(thread_running) {
		memset(buffer,'\0',sizeof(char)*256);
		memset(sndcmd,'\0',32);
		memset(ivalue,'\0',32);
	    n = read(sock,buffer,256);
	    if (n < 0)
	    {
	        perror("Command client is closed");
			thread_running=0;
			break;
	       // exit(1);
	    }
		else if(n>0) {
#ifdef DEBUG
			printf("CMD:%s\n",buffer);
#endif
			while(CheckMultiCommand(buffer,sndcmd,&cmdtype, &cmd)>0) {
				if (cmd<0) {
					fprintf(stderr,"Unknown command\n");
					write(sock,"Unknown command comes\n",22);
					continue;
				}
				//Check command health
				if(cmdtype==DAS_REQUEST) {
					sprintf(ret_buf,"!%s:NOT_YET\n",sndcmd);
				}else if(cmdtype==DAS_COMMAND) {
					sprintf(ret_buf,"!%s",sndcmd);
					//Must Check whether other process is working!!
					//
					//
					switch(cmd){
						case 0:		//RESET			// NO STRING SUPPORT YET
							fs->rx_flag = INIT_FLAG;
							break;
						case 1:		//LOAD_CONFIG
					//		ret=LoadDefaultConfig(fs);
							if(ret==0) sprintf(ret_buf+strlen(ret_buf),":DONE\n");
							else sprintf(ret_buf+strlen(ret_buf),":FALSE\n");
							break;
						case 2:		//SAVE_CONFIG
					//		ret=SaveDefaultConfig(fs);
							if(ret==0) sprintf(ret_buf+strlen(ret_buf),":DONE\n");
							else sprintf(ret_buf+strlen(ret_buf),":FALSE\n");
							break;
						case 3:		//SET_DEFAULT
					//		ret=SetConfig(fs);
							if(ret==0) sprintf(ret_buf+strlen(ret_buf),":DONE\n");
							else sprintf(ret_buf+strlen(ret_buf),":FALSE\n");
							break;
						case 4:		//START_COR
							fs->rx_flag |= RUN_FLAG;
							sprintf(ret_buf+strlen(ret_buf),":DONE\n");
							break;
						case 5:		//STOP_COR
							fs->rx_flag &=~RUN_FLAG;
							sprintf(ret_buf+strlen(ret_buf),":DONE\n");
							break;
						case 6:		//SEL_MAINPORT
							buf_len=strlen(sndcmd)-13;
							buf_len=FindValues(sndcmd+13,buf_len,ivalue);
							ret_i=atoi(ivalue);
							if(ret_i<1 || ret_i>4) sprintf(ret_buf+strlen(ret_buf),":OVERRANGE\n");
							else sprintf(ret_buf+strlen(ret_buf),":NOT_YET\n");
							break;
						case 7:		//SEL_CORIPLEN		//STRING??
							buf_len=strlen(sndcmd)-13;
							buf_len=FindValues(sndcmd+13,buf_len,ivalue);
							ret_lf=atof(ivalue);
							if(ret_lf<0) sprintf(ret_buf+strlen(ret_buf),":OVERRANGE\n");
							else {
								fs->T_int_wish = ret_lf/1000.0;
								if((ret_i=ConvertIPLEN(ret_lf))>0) fs->h.ip_length[0]=ret_i+'0';
								else sprintf(ret_buf+strlen(ret_buf),":OVERRANGE\n");
								sprintf(ret_buf+strlen(ret_buf),":%6.2lf:OK\n",ret_lf);
							}
							break;
						case 8:		//SET_CORIPNUM
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							ret_i=atoi(ivalue);
							if(ret_i<0) sprintf(ret_buf+strlen(ret_buf),":OVERRANGE\n");
							else {
								fs->h.ip_number=ret_i;
								sprintf(ret_buf+strlen(ret_buf),":%d:OK\n",ret_i);
							}
							break;
						case 9:		//SEL_CORWINDOW
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							if		(strcmp(ivalue,"NONE")==0) 		fs->window_type=WINDOW_FUNCTION_NONE;
							else if	(strcmp(ivalue,"HAMMING")==0) 	fs->window_type=WINDOW_FUNCTION_HAMMING;
							else if	(strcmp(ivalue,"HANNING")==0) 	fs->window_type=WINDOW_FUNCTION_HANN;
							else fs->window_type=WINDOW_FUNCTION_NONE;
							sprintf(ret_buf+strlen(ret_buf),":%s:%d:OK\n",ivalue,fs->window_type);
							break;
	
						case 10:	//SEL_CORMODE
						case 11:	//SEL_NRWBW
						case 12:	//SEL_NRWSTREAM
						case 13:	//SEL_COROUTMODE
						case 14:	//SET_COROUTCH
						case 15:	//SEL_COROUTSTREAM
						case 16:	//SET_STATUSMSK
							sprintf(ret_buf+strlen(ret_buf),":NOT_YET\n");
							break;
						case 17:	//SET_NUMFFT
							sscanf(fs->raw_format+5,"%d-%d-%d-%d",&rx_frame,&rx_mbps,&rx_nsub,&rx_nbit);
							buf_len=strlen(sndcmd)-11;
							buf_len=FindValues(sndcmd+11,buf_len,ivalue);
							rx_frame=atoi(ivalue);
							if(rx_frame%1024==0) {
								sprintf(fs->raw_format,"VDIF_%d-%d-%d-%d",rx_frame,rx_mbps,rx_nsub,rx_nbit);
								sprintf(ret_buf+strlen(ret_buf),":%s:OK\n",fs->raw_format);
							}else {
								sprintf(ret_buf+strlen(ret_buf),":OVERRANGE\n");
							}
							break;
						case 18:	//OBNM
							buf_len=strlen(sndcmd)-5;
							buf_len=FindValues(sndcmd+5,buf_len,ivalue);
							strcpy(fs->observer,ivalue);
							sprintf(ret_buf+strlen(ret_buf),":%s:OK\n",ivalue);
							break;
						case 19:	//SAVE
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							ret_i=atoi(ivalue);
							if(ret_i==1) {  
								fs->rx_is_file=1;	//YES, IT IS FILE
								sprintf(ret_buf+strlen(ret_buf),":FILE_MODE:OK\n");
							}
							else if(ret_i==0) {
								fs->rx_is_file=0;	//NO, IT IS STREAMING
								sprintf(ret_buf+strlen(ret_buf),":STREAM_MODE:OK\n");
							}
							else {
								sprintf(ret_buf+strlen(ret_buf),":OVERRANGE\n");
							}
	
						case 20:	//INPUT_STRAM
							sprintf(ret_buf+strlen(ret_buf),":NOT_YET\n");
							break;
						case 21:	//INPUT_FORMAT
							sscanf(fs->raw_format+5,"%d-%d-%d-%d",&rx_frame,&rx_mbps,&rx_nsub,&rx_nbit);
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							ip_num=atoi(ivalue);
							memset(ivalue,'\0',32);
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							rx_mbps=atoi(ivalue);
							memset(ivalue,'\0',32);
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							rx_nsub=atoi(ivalue);
							memset(ivalue,'\0',32);
							buf_len=strlen(sndcmd);
							buf_len=FindValues(sndcmd,buf_len,ivalue);
							rx_nbit=atoi(ivalue);
							memset(ivalue,'\0',32);
							sprintf(fs->raw_format,"VDIF_%d-%d-%d-%d",rx_frame,rx_mbps,rx_nsub,rx_nbit);
							sprintf(ret_buf+strlen(ret_buf),":%s:OK\n",fs->raw_format);
							break;
					
					}  //case
					write(sock,ret_buf,strlen(ret_buf));
				}	//elseif
			}//while commands
		} //elseif(n>0)
	} //while running
	close(sock);	
	printf("Bye one client\n");
//	kill(getppid(),SIGTERM);
	if(shmdt(fs)==-1) fprintf(stderr,"shmdt failed\n");
//	if(shmctl(SHM_KEY_FS,IPC_RMID,0)==-1) fprintf(stderr,"FORK:I tried to kill the memory, but...\n");
	return ;
}

void CheckFork(fftspec_config_t* fs)
{
	return;
}
void SetTickVal(fftspec_config_t* fs)
{
	return;
}
int CheckHalt(int flag)
{
	if (halt_flag) return 0;
	return flag;
}

