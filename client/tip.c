// temporary interface program to test corr.c 

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "socket.h"
//#include "network.h"
#include "main.h"

#define		STR_LEN 	256
//#define         SOCKET_PORT     8001
//#define         SERVER_ADDRESS  "127.0.0.1"
#define		SLEEP_TIME	100000

int main()
{
    int sockfd;
    char buf[STR_LEN]; 
    int running = 1;
    u_int i;
	int n;
    
    sockfd=connect_server(SPECSRV_ADDRESS,CORR_PORT);

    while(running){
		memset(buf,'\0',STR_LEN);
		read(1,buf,STR_LEN);
		for(i=0;i<strlen(buf);i++){
		    if(buf[i]=='\n') buf[i]='\0';
		}
		if(strncmp(buf,"exit",4)==0) running=0;
		if(strncmp(buf,"quit",4)==0) running=0;
		write(sockfd,buf,strlen(buf));
		printf("waiting...\n");
		usleep(SLEEP_TIME*10);
		recv(sockfd,buf,STR_LEN,0);
		printf("%s\n",buf);
    } 
    unlink((char *)&SPECSRV_ADDRESS);
    close(sockfd);
    return 0;
}
