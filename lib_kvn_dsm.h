#ifndef LIB_KVN_DSM_H
#define LIB_KVN_DSM_H

#include <ctype.h>
#include <stdint.h>

#define AUTO1		0X0001
#define AUTO2		0x0002
#define CROSS12R	0x0004
#define CROSS12I	0x0008

#define AUTO3		0X0010
#define AUTO4		0x0020
#define CROSS34R	0x0040
#define CROSS34I	0x0080

#define AUTO5		0X0100
#define AUTO6		0x0200
#define CROSS56R	0x0400
#define CROSS56I	0x0800

#define AUTO7		0X1000
#define AUTO8		0x2000
#define CROSS78R	0x4000
#define CROSS78I	0x8000


typedef union kvn_dsm_streams_tt{
	unsigned int word16;
	struct bit_tt {
		uint16_t auto1:1,auto2:1,cross12r:1,cross12i:1,
				 auto3:1,auto4:1,cross34r:1,cross34i:1,
				 auto5:1,auto6:1,cross56r:1,cross56i:1,
				 auto7:1,auto8:1,cross68r:1,cross78i:1;
	} bit;
} kvn_dsm_streams_t;

typedef struct kvn_dsm_structure_tt {			//I(16bits)+7C(1*8*7)+1C(1*8)+I(16)+8Q??
	uint16_t 	ip_number;				//0		//0 to 10000-1
	char 		start_time[7];			//1
	char 		input_mode[1];			//8
//	uint16_t 	output_streams;			//9
	kvn_dsm_streams_t output_streams;	//
	uint16_t	valid_samples[8];		//10 4Q??
	uint16_t	invalid_samples[9];		//??
	char		ip_length[1];			//1byte
	char		output_mode[1];			//1byte
	uint8_t		start_channel_index;	//1bytes

} kvn_dsm_structure_t;

#endif
