
/////////////////////////////////////////////////////////////////////////////////////
//
// GPU Spectrometer v0.6.7
//
// Processes single or dual IF data present in VDIF format. The data can come either from
// the network (UDP stream) or from an existing file on disk/RAID. Utilizes
// a single graphics card.
//
// Basic processing is quite trivial :
//  - decode VDIF, from whatever input source
//  - perform non-overlapped FFT, input data optionally windowed
//  - accumulate squared-magnitude FFTs to get time-integrated autopower spectrum,
//    and optionally accumulate cross-powers as well
//  - output the integrated spectrum in regular intervals
//
// For dual IF data, optionally calculates cross-power terms. In this case
// it is expected that the input data is interleaving samples from the two IFs,
// like is the case with usual VLBI data (and "single-threaded" multi-channel VDIF).
// That is, input samples should be in [if0 if1 if0 if ... if0 if1] order.
// Spectra are then calculated with input stride 2, output stride 2, such
// that FFT output is interleaved as well. During accumulation this results
// in accumulated data in [XX YY XX YY ...] interleaving. Optionally, if
// cross-power calculation is enabled, this changes into an interleaved
// data layout of [XX Re{XY} Im{XY} YY XX Re{XY} Im{XY} YY...].
//
// To Do:
//  - add server component, allow spectrometer to be configured and started
//    over the network rather than command line; use ALMA/ACA-like commands?
//
// Compile with "-DHAVE_CUFFT_CALLBACKS -dc" on 64-bit Linux and link
// with "-lcufft_static -lculibos" to enable (optional!) cuFFT Callbacks.
//
// (C) 2015 Jan Wagner, Jongsoo Kim
//
// ---------------------------------------------------------------------------------
// 2017.6.21.
// Hyunwoo Kang converts it to library for other usage.
//
/////////////////////////////////////////////////////////////////////////////////////

//#define DEBUG  // comment out to disable debug printouts
#undef CHECK_TIMING
#ifndef DEBUG
    #define CHECK_TIMING 0
    #define FAKE_RECEIVE 0
#else
    #define CHECK_TIMING 1
    #define FAKE_RECEIVE 1
#endif

#include "vdif_receiver_rb.h"
#include "kfftspec.h"
#include "kfftspec_outfile.h"
#include "kvn_dsm.h"
#include "time_utils.h"
#include "math_ext.h"

#include "cuda_utils.cu"
#include "decoder_2b32f_kernels.cu"
#include "decoder_2b32f_split_kernels.cu"
#include "decoder_3b32f_kernels.cu"
#include "memory_split_kernels.cu"
#include "arithmetic_xmac_kernels.cu"
#include "arithmetic_autospec_kernels.cu"
#include "arithmetic_winfunc_kernels.cu"
#include "dumpfile_io.c"  // nvcc: easier to include .c rather than .h and link with .o
#include "time_utils.c"
#include "kvn_dsm.c"

#include <assert.h>
#include <getopt.h>
#include <malloc.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/shm.h>
#include <cuda.h>
#include <cufft.h>
#include <cufftXt.h>

#define SHM_KEY_FS	7

#define REPORT_SEGMENT_RESULTS 1 // 1 to report each completed segment

#ifndef DEBUG
    // Disable some performance killers if not in debug mode
    #undef  CUDA_CHECK_ERRORS
    #define CUDA_CHECK_ERRORS(x)
    #undef CUDA_TIMING_START
    #undef CUDA_TIMING_STOP
    #define CUDA_TIMING_START(x,y)
    #define CUDA_TIMING_STOP(x,y,a,b,c)
#endif

/////////////////////////////////////////////////////////////////////////////////////
static const char* C_windowfunc_names[4] = { "boxcar", "Hann", "Hamming", "HFT248D"};

static int fftspec_process_segment_singlepol(fftspec_config_t*, const int, const int);
static int fftspec_process_segment_singlepol_cufftcallback(fftspec_config_t*, const int, const int);
static int fftspec_process_segment_dualpol(fftspec_config_t*, const int, const int);
// static int fftspec_process_segment_dualpol_cufftcallback(fftspec_config_t*, const int, const int); // TODO
static int fftspec_store_results_KVNDSM(const fftspec_config_t *fs, const int g, const int s);
static int fftspec_store_results_KFFTSPEC(const fftspec_config_t *fs, const int g, const int s);
static int fftspec_decode_vdif(fftspec_config_t *fs);
static int fftspec_init_condition(fftspec_config_t *fs);


static int rx_udpoffset = 0, rx_cpu = 0, rx_is_file = 0;
static int do_terminate=0;
char *rx_source;
/////////////////////////////////////////////////////////////////////////////////////

size_t m_gcd(size_t a, size_t b)
{
  size_t t;
  while ( a != 0 ) {
     t = a;
     a = b % a;
     b = t;
  }
  return b;
}

/////////////////////////////////////////////////////////////////////////////////////

//int main(int argc, char** argv)
int fftspec_setup(fftspec_config_t *fs)
{
// Initializing functions
//
    /* Optimize settings (e.g., subintegration to fit GPU memory) */
    fftspec_adjust_config(fs);
    fftspec_init_condition(fs);
    fftspec_decode_vdif(fs);

    /* Print out final configuration */
//NEGLIGIBLE
//    fftspec_show_config(&fs);
    if (fftspec_check_config(fs) != 0) {
        exit(EXIT_FAILURE);
    }

    /* Allocate GPU and CPU -side arrays */
    if (fftspec_allocate(fs) != 0) {
        exit(EXIT_FAILURE);
    }
//
// Run area
    /* Start VDIF capture */
    vdif_receiver_start(fs->rx);
    return 0;
}

void *mainprocessing(void *args)
{
    int g,s;
    int out_of_data = 0;
    char tstr[64];
    size_t ngot=0;
    size_t nsegments_consumed = 0, nspectra_total = 0;
    off64_t nbytes_total_got = 0, nbytes_total_expected = 0;
    

    fftspec_config_t *fs;

    connectshm_FS(SHM_KEY_FS, &fs);

    while (!do_terminate) {

        for (g = 0; g < fs->nGPUs; g++) {

            /* Switch to the correct GPU */
            CUDA_CALL( cudaSetDevice(fs->devicemap[g]) );
            CUDA_PEEK_ERRORS();

            //if (1) { // uncomment to follow processing like Option 1 above
            for (s = 0; s < fs->nstreams; s++) { // uncomment to follow processing like Option 2 above

                /* Receive more data (typically slow input rate) */
                if (cudaEventQuery(fs->process_cuda_inputdata_overwriteable[g][s]) != cudaSuccess) {
                    fprintf(stderr, "Wait free  [%d][%d] : card %d (device %d) stream %d input area not ready yet.\n", g, s, g, fs->devicemap[g], s);
                }
                CUDA_CALL( cudaEventSynchronize(fs->process_cuda_inputdata_overwriteable[g][s]) );
#if FAKE_RECEIVE
                if (nsegments_consumed < (size_t)fs->nGPUs*fs->nstreams) {
                    //ngot = vdif_receiver_get_segment(fs->rx, fs->h_rawbufs[g][s], fs->T_int, &fs->h_rawbuf_midtimes[g][s]);
                    memset(fs->h_rawbufs[g][s], 0x1A, fs->rawbuflen);
                    //memcpy(fs->h_rawbufs[g][s], fs->h_random, fs->rawbuflen);
                    ngot = fs->rawbuflen;
                } else if (nsegments_consumed > (size_t)fs->nGPUs*fs->nstreams*256) {
                    ngot = 0;
                }
                gettimeofday(&fs->h_rawbuf_midtimes[g][s], NULL);
#else
                CUDA_CALL( cudaEventSynchronize(fs->process_cuda_inputdata_overwriteable[g][s]) );
                ngot = vdif_receiver_get_segment(fs->rx, fs->h_rawbufs[g][s], fs->T_int, &fs->h_rawbuf_midtimes[g][s]);
#endif
                nsegments_consumed++;

                /* Store earlier spectra results of this stream (once Host->Device copying completes) before overwriting them */
                if (cudaEventQuery(fs->process_cuda_spectrum_available[g][s]) != cudaSuccess) {
                    fprintf(stderr, "Wait result [%d][%d] : card %d (device %d) stream %d not ready yet.\n", g, s, g, fs->devicemap[g], s);
                    // continue; // can proceed to try find an idle GPUs&streams (but, time order of output data will be lost)
                }
                CUDA_CALL( cudaEventSynchronize(fs->process_cuda_spectrum_available[g][s]) );
                if (fs->spec_weight[g][s] != SPEC_INVALID) {
                    nspectra_total += fs->nspecs;
                    fs->h_powspecs[g][s][0] = 0.0f; // remove DC point, garbage if cuFFT odist=nchan+0 (optimal) instead of odist=nchan+1 (inconvenient)
                    fftspec_store_results(fs, g, s, OUT_FORMAT_KFFTSPEC);
                    //fftspec_store_results(&fs, g, s, OUT_DISCARD);
                }
                if (do_terminate) {
                    // s = (s + 1) % fs->nstreams; // if using Option 1 above
                    continue;
                }

                /* Check the new input data */
                if (ngot == 0) {
                    fs->spec_weight[g][s] = SPEC_INVALID;
                    out_of_data = 1;
                    continue;
                } else {
                    fs->spec_weight[g][s] = ((double)ngot)/((double)fs->rawbuflen);
                    nbytes_total_expected += fs->rawbuflen;
                    nbytes_total_got += ngot;
                }

                /* Report on new input data */
                timeval2YYYYMMDDhhmmss(&fs->h_rawbuf_midtimes[g][s], tstr, sizeof(tstr));
                fprintf(stderr, "New segment [%d][%d] : mid-time %s : %zu byte wanted, got %zu, lost %zd\n",
                        g, s, tstr, fs->rawbuflen, ngot, fs->rawbuflen-ngot
                );
                if (0) { RBstatus(fs->rx->rb,""); }

                /* Run spectral processing on the new input data */
                fftspec_process_segment(fs, g, s);

            }//for(nstreams)
        }//for(ngpu)

        /* Next stream */
        // s = (s + 1) % fs->nstreams; // if using Option 1 above

        /* When out of data, see if ongoing tasks have all completed */
        if (out_of_data) {
            int gg, ss, npending = 0;
            for (gg = 0; gg < fs->nGPUs; gg++) {
                for (ss = 0; ss < fs->nstreams; ss++) {
                    if (fs->spec_weight[gg][ss] != SPEC_INVALID) { npending++; }
                }
            }
            do_terminate = (npending==0);
        }

    }

    /* Clean up CUDA device resources. This is needed by 'cuda-memcheck'. */
    for (g = 0; g < fs->nGPUs; g++) {
        CUDA_CALL( cudaSetDevice(fs->devicemap[g]) );
        cudaDeviceSynchronize();
        CUDA_CALL( cudaDeviceReset() );
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Wrapper for processing one raw data segment.
 * Processing for single-subband data is easy.
 * Processing for multi-subband data is complicated by data layout.
 */
int fftspec_process_segment(fftspec_config_t* fs, const int g, const int s)
{
    int rc;
    assert(fs != NULL);
    switch (fs->raw_nsubbands) {
        case 1:
            #ifdef HAVE_CUFFT_CALLBACKS
            rc = fftspec_process_segment_singlepol_cufftcallback(fs, g, s);
            #else
            rc = fftspec_process_segment_singlepol(fs, g, s);
            #endif
            break;
        case 2:
            rc = fftspec_process_segment_dualpol(fs, g, s);
            break;
        default:
            rc = -1;
    }
    return rc;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * fftspec_process_segment_singlepol()
 *
 * Process single-polarization 2-bit or 3-bit data, maybe windowing used.
 */
int fftspec_process_segment_singlepol(fftspec_config_t* fs, const int g, const int s)
{
    size_t numBlocks, threadsPerBlock;
    size_t nfft_subint = fs->nfft / fs->nsubints;
    unsigned long maxPhysThreads = fs->devprops[g].multiProcessorCount * fs->devprops[g].maxThreadsPerMultiProcessor;

    /* Timing of whole processing */
    CUDA_CALL( cudaEventRecord(fs->process_cuda_starttimes[g][s], fs->sid[g][s]) );

    /* Move new data onto GPU (typically completes much faster than input rate) */
    CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
    CUDA_CALL( cudaMemcpyAsync(fs->d_rawbufs[g][s], fs->h_rawbufs[g][s], fs->rawbuflen, cudaMemcpyHostToDevice, fs->sid[g][s]) );
    CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "load", (2*fs->nchan)*fs->nfft);

    /* Event: raw input area can be overwritten on CPU side once it has been copied onto GPU */
    CUDA_CALL( cudaEventRecord(fs->process_cuda_inputdata_overwriteable[g][s], fs->sid[g][s]) );

    /* Reset the previous output results on GPU back to zero */
    CUDA_CALL( cudaMemsetAsync(fs->d_powspecs[g][s], 0x00, fs->nchan*sizeof(float), fs->sid[g][s]) );

    /* Split spectral processing into smaller subintegrations that still fit GPU memory */
    for (int isub = 0; isub < fs->nsubints; isub++) {

        /* Unpack */
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        if (fs->raw_nbits == 2) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            threadsPerBlock = 32; // or cuda dev prop. field 'warpsize'
            numBlocks = div2ceil(nbytes,threadsPerBlock);
            cu_decode_2bit1ch_1D_v2 <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
                fs->d_rawbufs[g][s] + isub*nbytes, (float4 *)fs->d_fft_in[g][s], nbytes, 0
            );
            CUDA_CHECK_ERRORS("cu_decode_2bit1ch_1D_v2");
        } else if (fs->raw_nbits == 3) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            size_t nwords24 = (nbytes*8)/24; // nr# of 24-bit words (each 24-bit group has 6 x 3-bit samples)
            threadsPerBlock = 32;
            numBlocks = div2ceil(nwords24,threadsPerBlock);
            cu_decode_3bit1ch <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
                fs->d_rawbufs[g][s] + isub*nbytes, (float4 *)fs->d_fft_in[g][s], nbytes
            );
            CUDA_CHECK_ERRORS("cu_decode_3bit1ch");
        } else if (fs->raw_nbits == 32) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            //CUDA_CALL( cudaMemcpyAsync(fs->d_fft_in[g][s], fs->d_rawbufs[g][s] + isub*nbytes, nbytes, cudaMemcpyDeviceToDevice, fs->sid[g][s]) );
            fs->d_fft_in[g][s] = (float*)(fs->d_rawbufs[g][s] + isub*nbytes);
            assert((nbytes % 4) == 0);
        }
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "decode_1ch", (fs->rawbuflen*8)/(fs->raw_nbits*fs->nsubints));

        /* Window the data */
        if (fs->do_window) {
            CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
            size_t nfloat4 = 2*fs->nchan/4; // nr of float4 elements per FFT
            threadsPerBlock = 32; // or cuda dev prop. field 'warpsize'
            numBlocks = div2ceil(nfft_subint*nfloat4,threadsPerBlock);
            switch (fs->window_type) {
                case WINDOW_FUNCTION_HANN:
                    cu_window_hann <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>>
                        ((float4*)fs->d_fft_in[g][s], nfloat4, nfft_subint*nfloat4);
                    break;
                case WINDOW_FUNCTION_HAMMING:
                    cu_window_hamming <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>>
                        ((float4*)fs->d_fft_in[g][s], nfloat4, nfft_subint*nfloat4);
                    break;
                case WINDOW_FUNCTION_HFT248D:
                default:
                    // not yet implemented, or unknown
                    break;
            }
            CUDA_CHECK_ERRORS("cu_window_xxx");
            CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "window", (2*fs->nchan)*nfft_subint);
        }

        /* FFT */
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        CUFFT_CALL( cufftExecR2C(fs->cufftplans[g][s], (cufftReal*)fs->d_fft_in[g][s], (cufftComplex*)fs->d_fft_out[g][s]) );
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "r2c FFT", (2*fs->nchan)*nfft_subint);

        /* Accumulate power */
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        threadsPerBlock = 64;
        numBlocks = div2ceil(max(maxPhysThreads,(unsigned long)(fs->nchan+0)), threadsPerBlock);
        autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
            (cufftComplex*)fs->d_fft_out[g][s], fs->d_powspecs[g][s], fs->nchan+0, nfft_subint
        );
        CUDA_CHECK_ERRORS("autoPowerSpectrum_v3");
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "ac(v3)", (fs->nchan+0)*nfft_subint); // complex input

    }//for(subints)

    /* Transfer single-pol results from Device to Host asynchronously (data inspected later!) */
    CUDA_CALL( cudaMemcpyAsync(fs->h_powspecs[g][s], fs->d_powspecs[g][s], sizeof(float)*(fs->nchan+0), cudaMemcpyDeviceToHost, fs->sid[g][s]) );
    CUDA_CALL( cudaEventRecord(fs->process_cuda_spectrum_available[g][s], fs->sid[g][s]) );
    CUDA_CALL( cudaEventRecord(fs->process_cuda_stoptimes[g][s], fs->sid[g][s]) );

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * fftspec_process_segment_singlepol_cufftcallback()
 *
 * Same as fftspec_process_segment_singlepol() except that cuFFT Callback
 * functionality is enabled. This integrates the data windowing (optional)
 * into the CUDA FFT input stage, and integrates power calculation already
 * into the CUDA FFT outut stage so it needs a different accumulation kernel.
 */
int fftspec_process_segment_singlepol_cufftcallback(fftspec_config_t* fs, const int g, const int s)
{
    size_t numBlocks, threadsPerBlock;
    size_t nfft_subint = fs->nfft / fs->nsubints;
    unsigned long maxPhysThreads = fs->devprops[g].multiProcessorCount * fs->devprops[g].maxThreadsPerMultiProcessor;

    /* Timing of whole processing */
    CUDA_CALL( cudaEventRecord(fs->process_cuda_starttimes[g][s], fs->sid[g][s]) );

    /* Move new data onto GPU (typically completes much faster than input rate) */
    CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
    CUDA_CALL( cudaMemcpyAsync(fs->d_rawbufs[g][s], fs->h_rawbufs[g][s], fs->rawbuflen, cudaMemcpyHostToDevice, fs->sid[g][s]) );
    CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "load", (2*fs->nchan)*nfft_subint);

    /* Event: raw input area can be overwritten on CPU side once it has been copied onto GPU */
    CUDA_CALL( cudaEventRecord(fs->process_cuda_inputdata_overwriteable[g][s], fs->sid[g][s]) );

    /* Reset the previous output results on GPU back to zero */
    CUDA_CALL( cudaMemsetAsync(fs->d_powspecs[g][s], 0x00, fs->nchan*sizeof(float), fs->sid[g][s]) );

    /* Split spectral accumulation into subintegration (fits GPU memory size) */
    for (int isub = 0; isub < fs->nsubints; isub++) {

        /* Unpack */
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        if (fs->raw_nbits == 2) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            threadsPerBlock = 32; // or cuda dev prop. field 'warpsize'
            numBlocks = div2ceil(nbytes,threadsPerBlock);
            cu_decode_2bit1ch_1D_v2 <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
                fs->d_rawbufs[g][s] + isub*nbytes, (float4 *)fs->d_fft_in[g][s], nbytes, 0
            );
            CUDA_CHECK_ERRORS("cu_decode_2bit1ch_1D_v2");
        } else if (fs->raw_nbits == 3) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            size_t nwords24 = (nbytes*8)/24; // nr# of 24-bit words (each 24-bit group has 6 x 3-bit samples)
            threadsPerBlock = 32;
            numBlocks = div2ceil(nwords24,threadsPerBlock);
            cu_decode_3bit1ch <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
                fs->d_rawbufs[g][s] + isub*nbytes, (float4 *)fs->d_fft_in[g][s], nbytes
            );
            CUDA_CHECK_ERRORS("cu_decode_3bit1ch");
        } else if (fs->raw_nbits == 32) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            CUDA_CALL( cudaMemcpyAsync(fs->d_fft_in[g][s], fs->d_rawbufs[g][s]+ isub*nbytes, nbytes, cudaMemcpyDeviceToDevice, fs->sid[g][s]) );
        }
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "decode_1ch", (fs->rawbuflen*8)/fs->raw_nbits);

        /* Window the data */
        // Windowing (if any) will be done in CUDA FFT input stage

        /* FFT */
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        CUFFT_CALL( cufftExecR2C(fs->cufftplans[g][s], (cufftReal*)fs->d_fft_in[g][s], (cufftComplex*)fs->d_fft_out[g][s]) );
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "r2c FFT", (2*fs->nchan)*nfft_subint);

        /* Accumulate */
        // Callback already reduced FFT output (cufftComplex; 2 floats) into power data (cufftReal; 1 float).
        // The kernel here just accumulates the power data, needs only half the memory bandwidth.
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        threadsPerBlock = 64;
        numBlocks = div2ceil(max(maxPhysThreads,(unsigned long)(fs->nchan+0)), threadsPerBlock);
        autoPowerSpectrum_v3_CB <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
            (cufftReal*)fs->d_fft_out[g][s], fs->d_powspecs[g][s], fs->nchan+0, nfft_subint
        );
        CUDA_CHECK_ERRORS("autoPowerSpectrum_v3_CB");
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "ac(v3)_cb", (fs->nchan+0)*nfft_subint); // real part input

    }//for(subints)

    /* Transfer single-pol results from Device to Host asynchronously (data inspected later!) */
    CUDA_CALL( cudaMemcpyAsync(fs->h_powspecs[g][s], fs->d_powspecs[g][s], sizeof(float)*(fs->nchan+0), cudaMemcpyDeviceToHost, fs->sid[g][s]) );
    CUDA_CALL( cudaEventRecord(fs->process_cuda_spectrum_available[g][s], fs->sid[g][s]) );
    CUDA_CALL( cudaEventRecord(fs->process_cuda_stoptimes[g][s], fs->sid[g][s]) );

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Processing of dual-polarization 2-bit (not 3-bit) data, maybe windowing used,
 * maybe cufft callback used, maybe cross-power spectra desired.
 *
 * The raw data must have 2 subbands. The raw format must contain samples of
 * the 2 subbands in interleaved order, i.e., [x0 y0 x1 y1 ... xn-1 yn-1]:
 * A 2-channel unpacker kernel is used to de-interleave these data, and it
 * stores x=[x0 x1 .. xn-1] in the lower and y=[y0 y1 ... yn-1] in the upper half
 * of the large d_fft_in[] array. Their respective Fourier transforms then land
 * in the lower and upper halfs of d_fft_out[]. If a cross-power spectrum is
 * requested the xmac kernel will be invoked, else the 1-subband accumulator
 * kernel is simply invoked twice.
 */
int fftspec_process_segment_dualpol(fftspec_config_t* fs, const int g, const int s)
{
    size_t numBlocks, threadsPerBlock;
    size_t nfft_subint = fs->nfft / fs->nsubints;
    unsigned long maxPhysThreads = fs->devprops[g].multiProcessorCount * fs->devprops[g].maxThreadsPerMultiProcessor;

    /* Timing of whole processing */
    CUDA_CALL( cudaEventRecord(fs->process_cuda_starttimes[g][s], fs->sid[g][s]) );

    /* Move new data onto GPU (typically completes much faster than input rate) */
    CUDA_CALL( cudaMemcpyAsync(fs->d_rawbufs[g][s], fs->h_rawbufs[g][s], fs->rawbuflen, cudaMemcpyHostToDevice, fs->sid[g][s]) );

    /* Event: raw input area can be overwritten on CPU side once it has been copied onto GPU */
    CUDA_CALL( cudaEventRecord(fs->process_cuda_inputdata_overwriteable[g][s], fs->sid[g][s]) );

    /* Reset the previous output results on GPU back to zero */
    CUDA_CALL( cudaMemsetAsync(fs->d_powspecs[g][s], 0x00,
                   sizeof(float)*(fs->nchan+0)*fs->raw_nsubbands*(fs->do_cross ? 2 : 1),
                   fs->sid[g][s]) );

    /* Split spectral accumulation into subintegration (fits GPU memory size) */
    for (int isub = 0; isub < fs->nsubints; isub++) {

        /* Unpack */
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        if (fs->raw_nbits == 2) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            float2 *d_xpol = (float2*)fs->d_fft_in[g][s];
            float2 *d_ypol = (float2*)fs->d_fft_in[g][s] + (fs->nchan+0)*nfft_subint;
            threadsPerBlock = 32;
            numBlocks = div2ceil(nbytes,threadsPerBlock);
            cu_decode_2bit2ch_split <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (
                fs->d_rawbufs[g][s] + isub*nbytes, d_xpol, d_ypol, nbytes
            );
            CUDA_CHECK_ERRORS("");
        } else if (fs->raw_nbits == 3) {
            // TODO: splitting unpacker for 3-bit, if ALMA-like data are in interleaved format (are they?)
            assert(fs->raw_nbits != 3);
        } else if (fs->raw_nbits == 32) {
            size_t nbytes = fs->rawbuflen / fs->nsubints;
            CUDA_CALL( cudaMemcpyAsync(fs->d_fft_in[g][s], fs->d_rawbufs[g][s] + isub*nbytes, nbytes, cudaMemcpyDeviceToDevice, fs->sid[g][s]) );
        }
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "decode_2ch", (fs->rawbuflen*8)/fs->raw_nbits);

        /* Window the data (unless already done via callback func) */
        // TODO: implement for dual IF case

        /* FFT (windowing may also be intrinsic to FFT call if Callbacks enabled) */
        // Note: X-pol in lower, Y-pol in upper half of d_fft_in, so a single batched FFT transforms both.
        CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
        CUFFT_CALL( cufftExecR2C(fs->cufftplans[g][s], (cufftReal*)fs->d_fft_in[g][s], (cufftComplex*)fs->d_fft_out[g][s]) );
        CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "r2c FFT", (2*fs->nchan)*nfft_subint*fs->raw_nsubbands);

        /* Accumulate */
        if (fs->do_cross) {
            CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
            cufftComplex *d_xpol = (cufftComplex*)fs->d_fft_out[g][s];
            cufftComplex *d_ypol = ((cufftComplex*)fs->d_fft_out[g][s]) + (fs->nchan+0)*nfft_subint;
            threadsPerBlock = 32;
            numBlocks = div2ceil(32*(fs->nchan+0),threadsPerBlock);
            cu_accumulate_2pol <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s]>>>
                ((float2*)d_xpol, (float2*)d_ypol, (float4*)fs->d_powspecs[g][s], fs->nchan, nfft_subint);
            CUDA_CHECK_ERRORS("cu_accumulate_2pol");
            CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "xmac(X,Y)", 2*fs->raw_nsubbands*(fs->nchan+0)*nfft_subint); // complex input
        } else {
#ifdef HAVE_CUFFT_CALLBACKS
            // Callback can be used in power spectrum calculation only. Not cross-power spectrum.
            // The callback reduces FFT output (cufftComplex; 2 floats) into power data (cufftReal; 1 float).
            // The kernel here just accumulates the power data, needs only half the memory bandwidth.
            // We use the same kernel call to accumulate 'nchan' points in all subbands.
            CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
            cufftReal *d_xpol = (cufftReal*)fs->d_fft_out[g][s];
            cufftReal *d_ypol = ((cufftReal*)fs->d_fft_out[g][s]) + (fs->nchan+0)*nfft_subint;
            cufftReal *d_XX   = (cufftReal*)fs->d_powspecs[g][s];
            cufftReal *d_YY   = ((cufftReal*)fs->d_powspecs[g][s]) + (fs->nchan+0);
            threadsPerBlock = 64;
            numBlocks = div2ceil(max(maxPhysThreads,(unsigned long)(fs->nchan+0)), threadsPerBlock);
            autoPowerSpectrum_v3_CB <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (d_xpol, d_XX, fs->nchan+0, nfft_subint);
            autoPowerSpectrum_v3_CB <<< numBlocks ,threadsPerBlock, 0, fs->sid[g][s] >>> (d_ypol, d_YY, fs->nchan+0, nfft_subint);
            CUDA_CHECK_ERRORS("autoPowerSpectrum_v3_CB");
            CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "ac_cb(X,Y)", fs->raw_nsubbands*(fs->nchan+0)*nfft_subint); // real part input
#else
            // TODO: optimal grid size depends on max physical threads distributed across fs->nchan*nfft_subint, and slowness of addAtomic()
            // We use the same kernel call to accumulate 'nchan' points in all subbands.
            CUDA_TIMING_START(fs->estart[g][s], fs->sid[g][s]);
            cufftComplex *d_xpol = (cufftComplex*)fs->d_fft_out[g][s];
            cufftComplex *d_ypol = ((cufftComplex*)fs->d_fft_out[g][s]) + (fs->nchan+0)*nfft_subint;
            cufftReal *d_XX      = (cufftReal*)fs->d_powspecs[g][s];
            cufftReal *d_YY      = ((cufftReal*)fs->d_powspecs[g][s]) + (fs->nchan+0);
            threadsPerBlock = 64;
            numBlocks = div2ceil(max(maxPhysThreads,(unsigned long)(fs->nchan+0)), threadsPerBlock);
            autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (d_xpol, d_XX, fs->nchan+0, nfft_subint);
            autoPowerSpectrum_v3 <<< numBlocks, threadsPerBlock, 0, fs->sid[g][s] >>> (d_ypol, d_YY, fs->nchan+0, nfft_subint);
            CUDA_CHECK_ERRORS("autoPowerSpectrum_v3");
            CUDA_TIMING_STOP(fs->estop[g][s], fs->estart[g][s], fs->sid[g][s], "ac(X,Y)", fs->raw_nsubbands*(fs->nchan+0)*nfft_subint); // complex input
#endif
        }

    }//for(subints)

    /* Transfer dual-pol results from Device to Host asynchronously (data inspected later!) */
    CUDA_CALL( cudaMemcpyAsync(fs->h_powspecs[g][s], fs->d_powspecs[g][s],
                   sizeof(float)*(fs->nchan+0)*fs->raw_nsubbands*(fs->do_cross ? 2 : 1),
                   cudaMemcpyDeviceToHost, fs->sid[g][s]) );
    CUDA_CALL( cudaEventRecord(fs->process_cuda_spectrum_available[g][s], fs->sid[g][s]) );
    CUDA_CALL( cudaEventRecord(fs->process_cuda_stoptimes[g][s], fs->sid[g][s]) );

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Parse a list of arguments (from command line, network, ...) and set up the
 * FFT spectrometer configuration accordingly. Will create and start a data
 * receiver associated with the spectrometer configuration.
 *
 * @param argc Number of arguments
 * @param argv String array with arguments
 * @param fs Pointer to FFT configuration to fill out.
 * @return 0 on success, non-zero otherwise
 */
static int fftspec_init_condition(fftspec_config_t *fs)
{

    assert(fs != NULL);

    /* Defaults */
    memset(fs, 0x00, sizeof(fftspec_config_t));
    fs->T_int     	= 0.256;
    fs->nchan     	= 32768;
    fs->do_cross  	= 0;		// No cross-spectrum mode
    fs->devicemap[0] 	= 0;		// GPU card number is zero only (fs->devicemap[n]=nn;)
    fs->nGPUs	  	= 1;		// By upper setting, just one GPU card is selected.
    // do_window (1:ON, 0:OFF). It runs with window_type (WINDOW_FUNCTION_HANN || WINDOW_FUNCTION_HAMMING || WINDOW_FUNCTION_NONE)
    fs->do_window 	= 0;
    fs->window_type	= WINDOW_FUNCTION_NONE;
    fs->nstreams  	= 1;		// Why just use one stream??
    fs->nsubints  	= 1;
    fs->scanname   	= NULL;
    fs->experiment 	= strdup(DEFAULT_EXPERIMENT_NAME);
    fs->station    	= strdup(DEFAULT_STATION_NAME);
    fs->observer   	= strdup(DEFAULT_OBSERVER_NAME);

    rx_udpoffset 	= 0;		// To cutoff UDP stream payload at first (see fs->rx below)
//    rx_cpu = 0;			// Hmmm... interesting. Fix a ordered CPU bound

    fs->raw_format 	= strdup("VDIF-10000-2048-1-2");
    rx_is_file     	= 1; 		// 1:YES, 0:NO, UDP streamming

    return 0;
}

static int fftspec_decode_vdif(fftspec_config_t *fs)
{
    // Decode the format string
    int framelen, R_Mbps;
    int nc = sscanf(fs->raw_format+5, "%d-%d-%d-%d", &framelen, &R_Mbps, &(fs->raw_nsubbands), &(fs->raw_nbits));
        if ((nc != 4) || (framelen<512) || (R_Mbps < 64)
                      || (fs->raw_nsubbands < 1)
                      || (fs->raw_nsubbands > FFTSPEC_MAX_SUBBANDS)
                      || (fs->raw_nbits != 2))
        {
            fprintf(stderr, "%s: format error! Use VDIF_<framelen>-<Mbps>-<nchan>-<nbit/sample> with valid values\n", fs->raw_format);
            return -1;
        }

        // Create a data receiver
        if (rx_is_file) {
            fs->rx = filesource_vdif_receiver(rx_source, framelen+32, 0, R_Mbps);
        } else {
            fs->rx = udp_vdif_receiver(rx_source, framelen+32, rx_udpoffset, R_Mbps);
        }
        if (!fs->rx) {
            fprintf(stderr, "Failed to open input data source\n");
            return -1;
        }

        // Find integration time that best matches the request, constrained by frame size and rate
        fs->bw = 1e6*R_Mbps/(2*fs->raw_nbits*fs->raw_nsubbands);
        fs->T_int = vdif_receiver_fit_segmenttime(fs->rx, fs->T_int_wish);
        fs->rawbuflen = vdif_receiver_get_segmentsize(fs->rx, fs->T_int);

    /* Bind background receiver to CPU */
    if ((rx_cpu > 0) && (fs->rx != NULL)) {
        vdif_receiver_bind_cpu(fs->rx, rx_cpu);
    }

    /* Determine number of r2c FFTs to accumulate in one integration period */
    fs->nfft = ((fs->rawbuflen*8)/fs->raw_nbits) / fs->raw_nsubbands;
    fs->nfft /= (2*fs->nchan);

    /* Determine total number of averaged output spectra per integration period to generate */
    if (fs->raw_nsubbands <=1) {
        fs->do_cross = 0;
    }
    fs->nspecs = fs->raw_nsubbands;
    if (fs->do_cross) {
        fs->nspecs += ((fs->raw_nsubbands-1)*fs->raw_nsubbands)/2;
    }
    if (fs->nspecs < 1) {
        return -1;
    }

    return 0;
}

/**
 * Check the validity of specrometer configuration parameters.
 * @param fs Pointer to FFT configuration to check
 * @return 0 on success, non-zero if configuration has errors
 */
int fftspec_check_config(const fftspec_config_t *fs)
{
    int devicecount, i;

    CUDA_CALL( cudaGetDeviceCount(&devicecount) );

    assert(fs != NULL);
    assert(fs->raw_format != NULL);

    /* Check whether the data format is fine */
    if (strncasecmp(fs->raw_format, "VDIF_", 5) == 0) {          // VDIF_<framelen>-<Mbps>-<nchan>-<nbit/sample>
    } else if (strncasecmp(fs->raw_format, "Mark5B_", 7) == 0) { // Mark5B-<Mbps>-<nchan>-<nbit/sample>
        fprintf(stderr, "Config error: format %s (Mark5B) is currently not yet supported!\n", fs->raw_format);
        return -1;
    } else {
        fprintf(stderr, "Config error: format %s unknown!\n", fs->raw_format);
        return -1;
    }

    /* Check the GPU configuration */
    if ((fs->nstreams < 1) || (fs->nstreams > FFTSPEC_MAX_STREAMS)) {
        fprintf(stderr, "Config error: number of streams %d not between 1 and %d!\n", fs->nstreams, FFTSPEC_MAX_STREAMS);
        return -1;
    }
    if ((fs->nGPUs < 1) || (fs->nGPUs > FFTSPEC_MAX_GPUS)) {
        fprintf(stderr, "Config error: number of selected GPUs %d not between 1 and %d!\n", fs->nGPUs, FFTSPEC_MAX_GPUS);
        return -1;
    }
    if (devicecount < fs->nGPUs) {
        fprintf(stderr, "Config warning: detected %d GPUs, but %d were specified.\n", devicecount, fs->nGPUs);
    }
    for (i = 0; i < fs->nGPUs; i++) {
        if ((fs->devicemap[i] < 0) || (fs->devicemap[i] >= devicecount)) {
            fprintf(stderr, "Config error: --devices entry %d is nonexistent device %d (allowed: 0 to %d)\n",
                   i+1, fs->devicemap[i], devicecount-1
            );
            return -1;
        }
    }

    /* Make sure derived settings are fine */
    assert(fs->rawbuflen > 0);
    assert(fs->nsubints >= 1);
    assert((fs->rawbuflen % fs->nsubints) == 0);
    assert((fs->nfft % fs->nsubints) == 0);
    assert(fs->nspecs >= fs->raw_nsubbands);

    assert((fs->raw_nsubbands == 1) || (fs->raw_nsubbands == 2)); // current limitation...

    // TODO: additional sanity checks and limitation checks

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Apply some GPU-dependent changes to the spectrometer configuration.
 * @param fs Pointer to configuration to adjust.
 * @return 0 on success, non-zero otherwise
 */
int fftspec_adjust_config(fftspec_config_t *fs)
{
    size_t min_avail_GPU_memory = (size_t)-1;
    size_t est_needed_GPU_memory;
    int g;

    assert(fs != NULL);

    /* Disable windowing if "wrong" window function (e.g., boxcar, or not yet implemented HFT248D */
    if (fs->do_window && (fs->window_type == WINDOW_FUNCTION_NONE || fs->window_type >=WINDOW_FUNCTION_HFT248D)) {
        fs->do_window = 0;
        fprintf(stderr, "Warning: disabling window function since 'boxcar' or not yet implemented.\n");
    }

    /* Find minimum GPU memory */
    for (g = 0; g < fs->nGPUs; g++) {
        size_t nfree, ntotal;
        CUDA_CALL( cudaSetDevice(fs->devicemap[g]) );
        CUDA_CALL( cudaMemGetInfo(&nfree,&ntotal) );
        min_avail_GPU_memory = (nfree < min_avail_GPU_memory) ? nfree : min_avail_GPU_memory;
    }

    /* Find how much GPU memory is needed if no sub-integrations (default) */
    fs->nsubints = 1;
    fftspec_estimate_memory(fs, &est_needed_GPU_memory, NULL);
    est_needed_GPU_memory += est_needed_GPU_memory/2; // add some margin

    /* Split integration period evenly into sub-ints that hopefully fit in GPU memory */
    while (min_avail_GPU_memory < est_needed_GPU_memory) {
        const size_t primes = 2*3*5*7*11*13;
        size_t subintsize = fs->nfft/fs->nsubints;
        size_t R1 = m_gcd(subintsize, primes);
        size_t R2 = m_gcd(fs->rawbuflen, primes);
        size_t R = m_gcd(R1, R2);
        // fprintf(stderr, " m_gcd(%zu,%zu)=%zu,  rawbuflen=%zu:R2=%zu --> R=%zu * nsubints=%zu = %zu\n", subintsize, primes, R1, fs->rawbuflen, R2, R, fs->nsubints, R*fs->nsubints);
        if ((subintsize % R) != 0 || (fs->rawbuflen % (R*fs->nsubints)) != 0 || (R <= 1)) {
           fprintf(stderr, "Warning: failed to split into more than %d sub-ints. May run into GPU memory limts.\n", fs->nsubints);
           break;
        }
        fs->nsubints *= R;
        est_needed_GPU_memory /= R;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Estimate the required memory on a GPU and on the host.
 * @param fs Pointer to FFT configuration in which to allocate arrays.
 * @param gpu Pointer where to store GPU memory requirement in bytes.
 * @param host Pointer where to store host memory requirement in bytes.
 * @return 0 on success, non-zero otherwise
 */
int fftspec_estimate_memory(const fftspec_config_t *fs, size_t *gpu, size_t *host)
{
    size_t cufft_worksize, mem_cpu, mem_gpu;

    assert(fs != NULL);

    /* Guesstimate host side RAM */
    mem_cpu  = fs->rawbuflen * fs->nGPUs * fs->nstreams; // raw data
    mem_cpu += fs->rx->rb->size; // ring buffer

    /* Guesstimate the size of a batched FFT plan */
    CUFFT_CALL( cufftEstimate1d(2*fs->nchan, CUFFT_R2C, fs->nfft / fs->nsubints, &cufft_worksize) );
    mem_gpu = cufft_worksize;

    /* Guesstimate the rest of GPU memory needed */
    mem_gpu += fs->rawbuflen;    // entire raw buffer
    mem_gpu += sizeof(float) * 2*fs->nchan * fs->raw_nsubbands * fs->nfft/fs->nsubints;     // unpacked raw used as FFT input data
    mem_gpu += 2*sizeof(float) * (fs->nchan+0) * fs->raw_nsubbands * fs->nfft/fs->nsubints; // r2c complex FFT out, half-Nyquist, Nyquist bin discarded
    mem_gpu += sizeof(float) * (fs->nchan+0) * fs->raw_nsubbands;  // accumulation, half-Nyquist, Nyquist bin discarded, auto and cross
    if (fs->do_cross) {
        mem_gpu += 2 * sizeof(float) * (fs->nchan+0) * fs->raw_nsubbands; // Re{XY},Im{XY}
    }

    mem_gpu *= fs->nstreams;

    if (gpu != NULL) { *gpu = mem_gpu; }
    if (host != NULL) { *host = mem_cpu; }
    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Allocate arrays (host, device) required to run FFT spectrometer.
 * @param fs Pointer to FFT configuration in which to allocate arrays.
 * @return 0 on success, non-zero otherwise
 */
int fftspec_allocate(fftspec_config_t *fs)
{
    int g, s;
    size_t nfft_subint;

    assert(fs != NULL);

    /* Perform actual allocations */
    for (g = 0; g < fs->nGPUs; g++) {

        nfft_subint = fs->nfft / fs->nsubints;

        /* Select correct GPU */
        CUDA_CALL( cudaSetDevice(fs->devicemap[g]) );
        CUDA_CALL( cudaDeviceReset() );
        CUDA_CALL( cudaGetDeviceProperties(&fs->devprops[g], fs->devicemap[g]) );
        CUDA_PRINT_MEMORY_INFO("after card reset");

        /* Create time and task tracking events */
        for (s = 0; s < fs->nstreams; s++) {
            CUDA_CALL( cudaEventCreateWithFlags(&fs->process_cuda_starttimes[g][s], cudaEventBlockingSync) );
            CUDA_CALL( cudaEventCreateWithFlags(&fs->process_cuda_stoptimes[g][s], cudaEventBlockingSync) );
            CUDA_CALL( cudaEventCreateWithFlags(&fs->process_cuda_inputdata_overwriteable[g][s], cudaEventDisableTiming|cudaEventBlockingSync) );
            CUDA_CALL( cudaEventCreateWithFlags(&fs->process_cuda_spectrum_available[g][s], cudaEventDisableTiming|cudaEventBlockingSync) );
            CUDA_CALL( cudaEventCreate(&fs->estart[g][s]) );
            CUDA_CALL( cudaEventCreate(&fs->estop[g][s]) );
        }

        /* Set up streams */
        for (s = 0; s < fs->nstreams; s++) {
            CUDA_CALL( cudaStreamCreate( &(fs->sid[g][s]) ) );
        }

        /* Raw input data (GPU side) */
        for (s = 0; s < fs->nstreams; s++) {
            CUDA_CALL( cudaMalloc( (void **)&(fs->d_rawbufs[g][s]), fs->rawbuflen ) );
            CUDA_CALL( cudaMemsetAsync( fs->d_rawbufs[g][s], 0xFF, fs->rawbuflen, fs->sid[g][s]) );
        }
        CUDA_PRINT_MEMORY_INFO("after raw data alloc");

        /* Raw input data (CPU side) */
        for (s = 0; s < fs->nstreams; s++) {
            CUDA_CALL( cudaMallocHost( (void **)&(fs->h_rawbufs[g][s]), fs->rawbuflen, cudaHostAllocWriteCombined|cudaHostAllocPortable ) );
            memset(fs->h_rawbufs[g][s], 0xFF, fs->rawbuflen);
        }
        //CUDA_CALL( cudaMallocHost( (void **)&(fs->h_random), fs->rawbuflen, cudaHostAllocWriteCombined|cudaHostAllocPortable ) );
        //for (n = 0; n < fs->rawbuflen; n++) {
        //    fs->h_random[n] = rand() % 256;
        //}

        /* FFT-related arrays */
        // TODO: any fs->window_overlap != 0.0f would need output buf allocs and FFT input dist 'idist' to be updated!
        for (s = 0; s < fs->nstreams; s++) {
            size_t nbytes_in  = sizeof(float)*2*fs->nchan*nfft_subint*fs->raw_nsubbands;
            size_t nbytes_out = 2*sizeof(float)*(fs->nchan+0)*nfft_subint*fs->raw_nsubbands;
            CUDA_CALL( cudaMalloc( (void **)&(fs->d_fft_in[g][s]),  nbytes_in  ) );
            CUDA_CALL( cudaMalloc( (void **)&(fs->d_fft_out[g][s]), nbytes_out ) );
            CUDA_CALL( cudaMemsetAsync( fs->d_fft_in[g][s],   0x00, nbytes_in  ) );
            CUDA_CALL( cudaMemsetAsync( fs->d_fft_out[g][s],  0x00, nbytes_out ) );
        }
        CUDA_PRINT_MEMORY_INFO("after FFT in&out alloc");

        /* Time integrated output data */
        for (s = 0; s < fs->nstreams; s++) {
            // Single-pol output layout: [XX   XX ...             ] : 1 x Lfft x Nfft
            // Dual-pol output layout  : [XX   XX ... YY   YY ... ] : 2 x Lfft x Nfft
            // Dual-pol with cross pow : [XX Re{XY} Im{XY} YY ... ] : 4 x Lfft X Nfft (xmac kernel mode 1)
            // Dual-pol with cross pow : [XX   XX ... YY   YY ...  Re{XY} Im{XY} ...] : 4 x Lfft X Nfft (xmac kernel mode 0)
            size_t nbytes = sizeof(float)*(fs->nchan+0)*fs->raw_nsubbands;
            if (fs->do_cross) {
                nbytes *= 2; // for each XX,YY we also have Re{XY},Im{XY}
            }
            CUDA_CALL( cudaMalloc( (void **)&(fs->d_powspecs[g][s]), nbytes ) );
            CUDA_CALL( cudaMallocHost( (void **)&(fs->h_powspecs[g][s]), nbytes, cudaHostAllocPortable ) );
            CUDA_CALL( cudaMallocHost( (void **)&(fs->h_crosspecs[g][s]), nbytes/2, cudaHostAllocDefault ) );
            CUDA_CALL( cudaMemsetAsync( fs->d_powspecs[g][s], 0x00, nbytes, fs->sid[g][s]) );
            memset(fs->h_powspecs[g][s], 0x00, nbytes);
            memset(fs->h_crosspecs[g][s], 0x00, nbytes/2);
            fs->spec_weight[g][s] = SPEC_INVALID;
        }
        CUDA_PRINT_MEMORY_INFO("after output spectrum alloc");

        /* FFT plans */
        // Note: cuFFT API offers a multi-GPU capability (cufftXtSetGPUs(), cufftXtExecDescriptorC2C()) in which
        //       intermediate data will be transferred GPU-to-GPU. This makes most sense for 3D FFT, not 1D FFT.
        for (s = 0; s < fs->nstreams; s++) {
            // Config that throws away Nyquist bin
            int dimn[1] = {2*fs->nchan};  // r2c DFT size
            int inembed[1] = {0};         // ignored for 1D xform
            int onembed[1] = {0};         // ignored for 1D xform
            int istride = 1;              // step between successive in elements
            int ostride = 1;              // step between successive out elements
            int idist = 2*fs->nchan;      // step between batches (cufft r2c input = real)
            int odist = fs->nchan+0;      // step between batches (cufft r2c output = 1st Nyquist only)
                                          // with +0 rather than +1 we let overwrite the N/2+1 point
            CUFFT_CALL( cufftPlanMany(&(fs->cufftplans[g][s]), 1, dimn,
                inembed, istride, idist,
                onembed, ostride, odist,
                CUFFT_R2C,
                nfft_subint * fs->raw_nsubbands)
            );
            #if defined(CUDA_VERSION) && (CUDA_VERSION < 8000)
            CUFFT_CALL( cufftSetCompatibilityMode(fs->cufftplans[g][s], CUFFT_COMPATIBILITY_NATIVE) );
            #endif
            CUFFT_CALL( cufftSetStream(fs->cufftplans[g][s], fs->sid[g][s]) );
        }
        CUDA_PRINT_MEMORY_INFO("after CuFFT plan");

#ifdef HAVE_CUFFT_CALLBACKS
        /* Special cuFFT settings : Windowing via cuFFT Callback function */
        if (fs->do_window) {
            cufftCallbackLoadR h_windowfuncCallback;
            switch (fs->window_type) {
                case WINDOW_FUNCTION_HANN:
                    CUDA_CALL( cudaMemcpyFromSymbol(&h_windowfuncCallback, cu_window_hann_cufftCallbackLoadR_ptr, sizeof(h_windowfuncCallback)) );
                    break;
                case WINDOW_FUNCTION_HAMMING:
                    CUDA_CALL( cudaMemcpyFromSymbol(&h_windowfuncCallback, cu_window_hamming_cufftCallbackLoadR_ptr, sizeof(h_windowfuncCallback)) );
                    break;
                case WINDOW_FUNCTION_HFT248D:
                    // not yet implemented
                default:
                    CUDA_CALL( cudaMemcpyFromSymbol(&h_windowfuncCallback, cu_window_hann_cufftCallbackLoadR_ptr, sizeof(h_windowfuncCallback)) );
            }
            fs->h_cufft_userparams.fftlen = 2*fs->nchan;
            CUDA_CALL( cudaMalloc( (void **)&(fs->d_cufft_userparams[g]), sizeof(cu_window_cb_params_t) ) );
            CUDA_CALL( cudaMemcpy( fs->d_cufft_userparams[g], &(fs->h_cufft_userparams), sizeof(cu_window_cb_params_t), cudaMemcpyHostToDevice ) );
            for (s = 0; s < fs->nstreams; s++) {
                CUFFT_CALL( cufftXtSetCallback( fs->cufftplans[g][s], (void **)&h_windowfuncCallback,
                                                CUFFT_CB_LD_REAL, (void**)&(fs->d_cufft_userparams[g]) ) );
            }
        }
#endif

#ifdef HAVE_CUFFT_CALLBACKS
        /* Special cuFFT settings : FFT output 2 float {Re,Im} --> 1 float {Re^2+Im^2} via cuFFT Callback */
        if (!fs->do_cross) {
            cufftCallbackStoreC h_powerCallbackPtr;
            CUDA_CALL( cudaMemcpyFromSymbol(&h_powerCallbackPtr, cu_autoPowerSpectrum_cufftCallbackStoreC_ptr, sizeof(h_powerCallbackPtr)) );
            for (s = 0; s < fs->nstreams; s++) {
                CUFFT_CALL( cufftXtSetCallback( fs->cufftplans[g][s], (void **)&h_powerCallbackPtr,
                                                CUFFT_CB_ST_COMPLEX, NULL) );
            }
        }
#endif

        CUDA_PRINT_MEMORY_INFO("after kfftspec allocations");
    }

#ifdef DEBUG
    for (int g = 0; g < fs->nGPUs; g++) {
        for (int s = 0; s < fs->nstreams; s++) {
            fprintf(stderr, "rawbufs[%d][%d] @ host %p, GPU %p\n", g,s,fs->h_rawbufs[g][s],fs->d_rawbufs[g][s]);
        }
        for (int s = 0; s < fs->nstreams; s++) {
            fprintf(stderr, "fft[%d][%d] in @ GPU %p, out @ GPU %p\n", g,s,fs->d_fft_in[g][s],fs->d_fft_out[g][s]);
        }
    }
#endif

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Print out the spectral processing setup, and memory usage estimate.
 */
void fftspec_show_config(const fftspec_config_t *fs)
{
    const double to_GB = 1/(1024*1024*1024.0);
    size_t mem_gpu, mem_cpu;

    fftspec_estimate_memory(fs, &mem_gpu, &mem_cpu);

    fprintf(stderr, "Computational setup  : %d GPUs x %d streams x %d sub-integrations, "
            #ifdef HAVE_CUFFT_CALLBACKS
            "with cuFFT Callbacks when possible.\n",
            #else
            "without cuFFT Callbacks.\n",
            #endif
            fs->nGPUs, fs->nstreams, fs->nsubints
    );
    fprintf(stderr, "Estimated memory req.: %.2f GB per GPU, %.2f GB on host\n", mem_gpu*to_GB, mem_cpu*to_GB);
    fprintf(stderr, "Spectral setup       : %d spectral points, %d subbands, %.3fs integration (wish was %.3fs) of %d spectra in %d sub-ints\n",
            fs->nchan, fs->raw_nsubbands, fs->T_int, fs->T_int_wish, fs->nfft, fs->nsubints);
    fprintf(stderr, "Spectral taper       : %s time domain windowing with %.1f%% overlap\n",
            C_windowfunc_names[fs->window_type], 100.0*fs->window_overlap);
    fprintf(stderr, "Spectral output      : %d spectra per integration, %s cross-power spectra\n",
            fs->nspecs, (fs->do_cross) ? "including" : "with no");
    if (FAKE_RECEIVE || CHECK_TIMING) {
        fprintf(stderr, "Special/debug        : ");
        if (FAKE_RECEIVE) { fprintf(stderr, "fake receive  ");  }
        if (CHECK_TIMING) { fprintf(stderr, "kernel timing  "); }
        fprintf(stderr, "\n");
    }

    for (int g = 0; g < fs->nGPUs; g++) {
        fprintf(stderr, "Card %d: CUDA device %d: %s, CC %d.%d, %d threads/block, warpsize %d\n",
                g, fs->devicemap[g], fs->devprops[g].name, fs->devprops[g].major, fs->devprops[g].minor,
                fs->devprops[g].maxThreadsPerBlock, fs->devprops[g].warpSize
        );
    }

    return;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * Write out one spectrum in some "suitable" data format.
 * Currently intended for KVN/ASTE/TRAO/... and not yet ACA-TP.
 */
int fftspec_store_results(const fftspec_config_t *fs, const int g, const int s, const int format)
{
#if REPORT_SEGMENT_RESULTS
    char  datatime[64];
    float dT_msec, R;

    /* Determine wall-clock processing time in CUDA */
    CUDA_CALL( cudaEventSynchronize(fs->process_cuda_stoptimes[g][s]) );
    CUDA_CALL( cudaEventElapsedTime(&dT_msec, fs->process_cuda_starttimes[g][s], fs->process_cuda_stoptimes[g][s]) );

    /* Get data-time timestamp */
    timeval2YYYYMMDDhhmmss(&(fs->h_rawbuf_midtimes[g][s]), datatime, sizeof(datatime));

    /* Print partial result on terminal for debug */
    const float* d = fs->h_powspecs[g][s];
    const double w = fs->spec_weight[g][s];
    R = ((fs->rawbuflen*8)/fs->raw_nbits) / (1e-3*dT_msec);
    fprintf(stderr, "Got result  [%d][%d] : mid-time %s: weight %.4f : computing %.2f Gs/s", g, s, datatime, w, R*1e-9);
    #ifdef DEBUG
    fprintf(stderr, " : [%.2f %.2f %.2f %.2f %.2f %.2f %.2f %.2f ... %.2f]\n", d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[fs->nchan-1]);
    #else
    fprintf(stderr, "\n");
    #endif
#endif

    /* Select desired output writer */
    switch (format) {
        case OUT_FORMAT_KVNDSM:
            return fftspec_store_results_KVNDSM(fs, g, s);
        case OUT_FORMAT_KFFTSPEC:
            return fftspec_store_results_KFFTSPEC(fs, g, s);
        case OUT_DISCARD:
            return 0;
        default:
            break;
    }
    return -1;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * fftspec_store_results_KVNDSM()
 *
 * Append spectra to a file in a KVN DSM -like output format.
 * Not entirely DSM -like i.e. not 100% compatible. In addition,
 * the DSM format is not quite suitable as it lacks any #channels
 * info, lacks sub-second timestamps, and does not allow arbitrary
 * integration times.
 *
 * TODO: add support for cross-power spectra and multi-subband data
 */
int fftspec_store_results_KVNDSM(const fftspec_config_t *fs, const int g, const int s)
{
    static int fudge_inited = 0;
    static kvn_dsm_writer_t* fudge_wdsm = NULL;

    /* Shorthands */
    const struct timeval* tv = &(fs->h_rawbuf_midtimes[g][s]);
    const float* d = fs->h_powspecs[g][s];
    const double w = fs->spec_weight[g][s];

    /* Init the ugly static variables */
    if (!fudge_inited) {
        char starttime[64];
        char base_file[4096];
        timeval2YYYYDDDhhmmss_fmt(tv, starttime, sizeof(starttime), "%04d%03d%02d%02d%02.0f");
        snprintf(base_file, sizeof(base_file), "%s.%s", starttime, fs->experiment);
        fudge_inited = 1;
        fudge_wdsm = create_kvn_dsm(base_file);
    }

    /* Write KVN DSM -like output file */
    append_kvn_dsm(fudge_wdsm,
        (const float**)&d, 1, NULL, 0,
        fs->nchan*fs->nfft*w,       // num. of valid samples
        fs->nchan*fs->nfft*(1.0-w), // num. of invalid samples
        fs->nchan, tv
    );

    return 0;
}

/////////////////////////////////////////////////////////////////////////////////////

/**
 * fftspec_store_results_KFFTSPEC()
 * Append spectra of one time slice to a file in a custom "KFFTSPEC" type of format.
 */
int fftspec_store_results_KFFTSPEC(const fftspec_config_t *fs, const int g, const int s)
{
    static int fudge_inited = 0;
    static char ks_filename[4096];
    int i;

    struct tm* tm_val; // gmtime() internal static buffer, does not need free()!
    double sec_of_hour, sec_of_day;

    /* Shorthands */
    const struct timeval* tv = &(fs->h_rawbuf_midtimes[g][s]);
    const float* d = fs->h_powspecs[g][s];
    const double w = fs->spec_weight[g][s];

    /* Init the ugly static variables */
    if (!fudge_inited) {
        char starttime[64];
        timeval2YYYYDDDhhmmss_fmt(tv, starttime, sizeof(starttime), "%04d%03d%02d%02d%02.0f");
        if (fs->scanname == NULL) {
            snprintf(ks_filename, sizeof(ks_filename), "%s_%s_%s.kfftspec", fs->experiment, fs->station, starttime);
        } else {
            snprintf(ks_filename, sizeof(ks_filename), "%s_%s_%s_%s.kfftspec", fs->experiment, fs->station, fs->scanname, starttime);
        }
        fudge_inited = 1;
    }

    /* Prepare standard header entries */
    kfftspec_outfile_spectrumheader_v1_t spechdr;
    spechdr.header_version = 0;
    spechdr.headerlen  = sizeof(kfftspec_outfile_spectrumheader_v1_t);
    spechdr.Tint       = fs->T_int;
    spechdr.bandwidth  = fs->bw;
    spechdr.nchan      = fs->nchan;
    spechdr.weight     = w;
    spechdr.winoverlap = fs->window_overlap;
    spechdr.GPU_device = fs->devicemap[g];
    spechdr.GPU_stream = s;
    spechdr.GPU_datasource = (fs->rx->is_file) ? 0 : 1;
    snprintf(spechdr.winfunc, sizeof(spechdr.winfunc), "%s", C_windowfunc_names[fs->window_type]);

    /* Add quantizer sample statistics (not yet fully implemented) */
    spechdr.nlevels_src_1 = 1 << fs->raw_nbits;
    spechdr.nlevels_src_2 = 1 << fs->raw_nbits;
    for (i = 0; i < (int)(sizeof(spechdr.plevels_src_1)/sizeof(double)); i++) {
        spechdr.plevels_src_1[i] = 0.0; // TODO
        spechdr.plevels_src_2[i] = 0.0; // TODO
    }

    /* Convert timestamp into several formats */
    tm_val      = gmtime((const time_t*)tv);
    sec_of_hour = tm_val->tm_sec + 1e-6*tv->tv_usec;
    sec_of_day  = (tm_val->tm_hour*60 + tm_val->tm_min)*60 + tm_val->tm_sec + 1e-6*tv->tv_usec;
    spechdr.timestamp_secondsofday = sec_of_day;
    spechdr.timestamp_tm_seconds   = sec_of_hour;
    memcpy(&spechdr.timestamp_tm, tm_val, sizeof(struct tm));
    snprintf(spechdr.timestamp_str, sizeof(spechdr.timestamp_str),
             "%04d%03d%02d%02d%09.6f", // YYYYDDDhhmmss.ffffff
             1900 + tm_val->tm_year, tm_val->tm_yday + 1, tm_val->tm_hour, tm_val->tm_min, sec_of_hour
    );

    /* Open */
    FILE* f = fopen(ks_filename, "a");
    if (f == NULL) {
        perror("Opening output file for appending");
        exit(EXIT_FAILURE);
    }

    /* Append power spectra of this time slice */
    for (int b = 0; b < fs->raw_nsubbands; b++) {

        spechdr.datalayout = KFFTSPEC_OUTFILE_DLAYOUT_RE;
        spechdr.datalen    = (fs->nchan+0)*sizeof(float);
        spechdr.signal_src_1 = b;
        spechdr.signal_src_2 = b;

        fwrite(&spechdr, 1, sizeof(spechdr), f);
        fwrite(d + b*(fs->nchan+0), 1, spechdr.datalen, f);
    }

    /* Append cross-power spectrum of this time slice */
    if (fs->do_cross) {
        assert(fs->raw_nsubbands == 2);

        spechdr.datalayout = KFFTSPEC_OUTFILE_DLAYOUT_NxREIM;
        spechdr.datalen    = 2*(fs->nchan+0)*sizeof(float);
        spechdr.signal_src_1 = 0;
        spechdr.signal_src_2 = 1;

        fwrite(&spechdr, 1, sizeof(spechdr), f);
        fwrite(d + 2*(fs->nchan+0), 1, spechdr.datalen, f);
    }

    fclose(f);

    return 0;
}


int connectshm_FS(key_t shm_key, fftspec_config_t **fs)
{
    int shmid;
    void *shared_memory=(void *)0;

    shmid=shmget(shm_key,sizeof(fftspec_config_t), 0666 | IPC_CREAT);

    if(shmid == -1){
        fprintf(stderr, "shmget failed\n");
    return shmid;
    }

    shared_memory =(void *) shmat(shmid, (void *)0, 0);

    if(shared_memory == (void *)-1){
        fprintf(stderr, "shmat failed\n");
    return -1;
    }

    printf("Memory attached.\n");

    *fs = (fftspec_config_t *)shared_memory;

    return shmid;
}

/////////////////////////////////////////////////////////////////////////////////////
