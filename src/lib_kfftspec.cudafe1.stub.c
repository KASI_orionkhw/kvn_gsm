#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
#pragma GCC diagnostic ignored "-Wcast-qual"
#define __NV_MODULE_ID _20_lib_kfftspec_cpp1_ii_350f4d54
#define __NV_CUBIN_HANDLE_STORAGE__ extern
#include "crt/host_runtime.h"
#include "lib_kfftspec.fatbin.c"
extern void __device_stub__Z24cu_decode_2bit1ch_1D_v1bPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, size_t, size_t);
extern void __device_stub__Z23cu_decode_2bit1ch_1D_v2PKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t);
extern void __device_stub__Z30cu_decode_2bit1ch_1D_mark5b_v2PKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t);
extern void __device_stub__Z23cu_decode_2bit1ch_1D_v3PKhP6float4mm(const uint8_t *, struct float4 *, size_t, size_t);
extern void __device_stub__Z23cu_decode_2bit1ch_1D_v4PKhP6float4mm(const uint8_t *, struct float4 *, size_t, size_t);
extern void __device_stub__Z17cu_decode_2bit1chPKhP6float4m(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t);
extern void __device_stub__Z17cu_decode_2bit2chPKhP6float2S2_m(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t);
extern void __device_stub__Z17cu_decode_2bit4chPKhPfS1_S1_S1_m(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t);
extern void __device_stub__Z22cu_decode_2bit1ch_HannPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z22cu_decode_2bit2ch_HannPKhP6float2S2_mm(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z22cu_decode_2bit4ch_HannPKhPfS1_S1_S1_mm(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t);
extern void __device_stub__Z25cu_decode_2bit1ch_HammingPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z25cu_decode_2bit2ch_HammingPKhP6float2S2_mm(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z25cu_decode_2bit4ch_HammingPKhPfS1_S1_S1_mm(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t);
extern void __device_stub__Z30cu_decode_2bit1ch_CustomWindowPKhP6float4mmPKS1_(const uint8_t *, struct float4 *, const size_t, const size_t, const struct float4 *__restrict__);
extern void __device_stub__Z30cu_decode_2bit2ch_CustomWindowPKhP6float2S2_mmPKS1_(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t, const struct float2 *__restrict__);
extern void __device_stub__Z30cu_decode_2bit4ch_CustomWindowPKhPfS1_S1_S1_mmPKf(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t, const float *__restrict__);
extern void __device_stub__Z23cu_decode_2bit2ch_splitPKhP6float2S2_m(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t);
extern void __device_stub__Z17cu_decode_3bit1chPKhP6float4m(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t);
extern void __device_stub__Z23cu_decode_3bit2ch_splitPKhP6float4S2_m(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t);
extern void __device_stub__Z22cu_decode_3bit1ch_HannPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z25cu_decode_3bit1ch_HammingPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z28cu_decode_3bit2ch_Hann_splitPKhP6float4S2_mm(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z31cu_decode_3bit2ch_Hamming_splitPKhP6float4S2_mm(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
extern void __device_stub__Z21cu_split_2chan_f4_2f2PK6float4P6float2S3_m(const struct float4 *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t);
extern void __device_stub__Z20cu_split_2chan_f2_2fPK6float2PfS2_m(const struct float2 *__restrict__, float *__restrict__, float *__restrict__, const size_t);
extern void __device_stub__Z18cu_accumulate_2polPK6float2S1_P6float4mm(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float4 *__restrict__, size_t, size_t);
extern void __device_stub__Z25cu_accumulate_2pol_packedPK6float4PS_mm(const struct float4 *__restrict__, struct float4 *__restrict__, size_t, size_t);
extern void __device_stub__Z27cu_accumulate_2pol_autoOnlyPK6float2S1_PS_mm(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float2 *__restrict__, size_t, size_t);
extern void __device_stub__Z34cu_accumulate_2pol_autoOnly_packedPK6float4P6float2mm(const struct float4 *__restrict__, struct float2 *__restrict__, size_t, size_t);
extern void __device_stub__Z17autoPowerSpectrumP6float2Pfmm(cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t);
extern void __device_stub__Z20autoPowerSpectrum_v2PK6float2Pfmm(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t);
extern void __device_stub__Z20autoPowerSpectrum_v3PK6float2Pfmm(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t);
extern void __device_stub__Z23autoPowerSpectrum_v3_CBPKfPfmm(const cufftReal *__restrict__, float *__restrict__, const size_t, const size_t);
extern void __device_stub__Z14cu_window_hannP6float4mm(struct float4 *, size_t, size_t);
extern void __device_stub__Z17cu_window_hammingP6float4mm(struct float4 *, size_t, size_t);
static void __nv_cudaEntityRegisterCallback(void **);
static void __sti____cudaRegisterAll_20_lib_kfftspec_cpp1_ii_350f4d54(void) __attribute__((__constructor__));
void __device_stub__Z24cu_decode_2bit1ch_1D_v1bPKhP6float4mm(const uint8_t *__restrict__ __par0, struct float4 *__restrict__ __par1, size_t __par2, size_t __par3){ const uint8_t *__T28;
 struct float4 *__T29;
__T28 = __par0;__cudaSetupArgSimple(__T28, 0UL);__T29 = __par1;__cudaSetupArgSimple(__T29, 8UL);__cudaSetupArgSimple(__par2, 16UL);__cudaSetupArgSimple(__par3, 24UL);__cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, size_t, size_t))cu_decode_2bit1ch_1D_v1b)));}
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_1D_v1b( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,size_t __cuda_2,size_t __cuda_3)
# 74 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z24cu_decode_2bit1ch_1D_v1bPKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 95 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z23cu_decode_2bit1ch_1D_v2PKhP6float4mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2,  size_t __par3) {  const uint8_t *__T210;
 struct float4 *__T211;
__T210 = __par0; __cudaSetupArgSimple(__T210, 0UL); __T211 = __par1; __cudaSetupArgSimple(__T211, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t))cu_decode_2bit1ch_1D_v2))); }
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_1D_v2( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2,size_t __cuda_3)
# 105 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z23cu_decode_2bit1ch_1D_v2PKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 122 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z30cu_decode_2bit1ch_1D_mark5b_v2PKhP6float4mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2,  size_t __par3) {  const uint8_t *__T212;
 struct float4 *__T213;
__T212 = __par0; __cudaSetupArgSimple(__T212, 0UL); __T213 = __par1; __cudaSetupArgSimple(__T213, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t))cu_decode_2bit1ch_1D_mark5b_v2))); }
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_1D_mark5b_v2( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2,size_t __cuda_3)
# 125 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z30cu_decode_2bit1ch_1D_mark5b_v2PKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 140 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z23cu_decode_2bit1ch_1D_v3PKhP6float4mm( const uint8_t *__par0,  struct float4 *__par1,  size_t __par2,  size_t __par3) {  __cudaSetupArgSimple(__par0, 0UL); __cudaSetupArgSimple(__par1, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *, struct float4 *, size_t, size_t))cu_decode_2bit1ch_1D_v3))); }
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_1D_v3( const uint8_t *__cuda_0,struct float4 *__cuda_1,size_t __cuda_2,size_t __cuda_3)
# 153 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z23cu_decode_2bit1ch_1D_v3PKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 180 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z23cu_decode_2bit1ch_1D_v4PKhP6float4mm( const uint8_t *__par0,  struct float4 *__par1,  size_t __par2,  size_t __par3) {  __cudaSetupArgSimple(__par0, 0UL); __cudaSetupArgSimple(__par1, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *, struct float4 *, size_t, size_t))cu_decode_2bit1ch_1D_v4))); }
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_1D_v4( const uint8_t *__cuda_0,struct float4 *__cuda_1,size_t __cuda_2,size_t __cuda_3)
# 193 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z23cu_decode_2bit1ch_1D_v4PKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 211 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z17cu_decode_2bit1chPKhP6float4m( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2) {  const uint8_t *__T214;
 struct float4 *__T215;
__T214 = __par0; __cudaSetupArgSimple(__T214, 0UL); __T215 = __par1; __cudaSetupArgSimple(__T215, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t))cu_decode_2bit1ch))); }
# 217 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2)
# 218 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z17cu_decode_2bit1chPKhP6float4m( __cuda_0,__cuda_1,__cuda_2);
# 230 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z17cu_decode_2bit2chPKhP6float2S2_m( const uint8_t *__restrict__ __par0,  struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  const size_t __par3) {  const uint8_t *__T216;
 struct float2 *__T217;
 struct float2 *__T218;
__T216 = __par0; __cudaSetupArgSimple(__T216, 0UL); __T217 = __par1; __cudaSetupArgSimple(__T217, 8UL); __T218 = __par2; __cudaSetupArgSimple(__T218, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t))cu_decode_2bit2ch))); }
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit2ch( const uint8_t *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,const size_t __cuda_3)
# 233 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z17cu_decode_2bit2chPKhP6float2S2_m( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 242 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z17cu_decode_2bit4chPKhPfS1_S1_S1_m( const uint8_t *__restrict__ __par0,  float *__restrict__ __par1,  float *__restrict__ __par2,  float *__restrict__ __par3,  float *__restrict__ __par4,  const size_t __par5) {  const uint8_t *__T219;
 float *__T220;
 float *__T221;
 float *__T222;
 float *__T223;
__T219 = __par0; __cudaSetupArgSimple(__T219, 0UL); __T220 = __par1; __cudaSetupArgSimple(__T220, 8UL); __T221 = __par2; __cudaSetupArgSimple(__T221, 16UL); __T222 = __par3; __cudaSetupArgSimple(__T222, 24UL); __T223 = __par4; __cudaSetupArgSimple(__T223, 32UL); __cudaSetupArgSimple(__par5, 40UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t))cu_decode_2bit4ch))); }
# 244 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit4ch( const uint8_t *__restrict__ __cuda_0,float *__restrict__ __cuda_1,float *__restrict__ __cuda_2,float *__restrict__ __cuda_3,float *__restrict__ __cuda_4,const size_t __cuda_5)
# 248 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z17cu_decode_2bit4chPKhPfS1_S1_S1_m( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4,__cuda_5);
# 261 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z22cu_decode_2bit1ch_HannPKhP6float4mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const uint8_t *__T224;
 struct float4 *__T225;
__T224 = __par0; __cudaSetupArgSimple(__T224, 0UL); __T225 = __par1; __cudaSetupArgSimple(__T225, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_2bit1ch_Hann))); }
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_Hann( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 278 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z22cu_decode_2bit1ch_HannPKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 298 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z22cu_decode_2bit2ch_HannPKhP6float2S2_mm( const uint8_t *__restrict__ __par0,  struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  const size_t __par3,  const size_t __par4) {  const uint8_t *__T226;
 struct float2 *__T227;
 struct float2 *__T228;
__T226 = __par0; __cudaSetupArgSimple(__T226, 0UL); __T227 = __par1; __cudaSetupArgSimple(__T227, 8UL); __T228 = __par2; __cudaSetupArgSimple(__T228, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t))cu_decode_2bit2ch_Hann))); }
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit2ch_Hann( const uint8_t *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,const size_t __cuda_3,const size_t __cuda_4)
# 301 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z22cu_decode_2bit2ch_HannPKhP6float2S2_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 316 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z22cu_decode_2bit4ch_HannPKhPfS1_S1_S1_mm( const uint8_t *__restrict__ __par0,  float *__restrict__ __par1,  float *__restrict__ __par2,  float *__restrict__ __par3,  float *__restrict__ __par4,  const size_t __par5,  const size_t __par6) {  const uint8_t *__T229;
 float *__T230;
 float *__T231;
 float *__T232;
 float *__T233;
__T229 = __par0; __cudaSetupArgSimple(__T229, 0UL); __T230 = __par1; __cudaSetupArgSimple(__T230, 8UL); __T231 = __par2; __cudaSetupArgSimple(__T231, 16UL); __T232 = __par3; __cudaSetupArgSimple(__T232, 24UL); __T233 = __par4; __cudaSetupArgSimple(__T233, 32UL); __cudaSetupArgSimple(__par5, 40UL); __cudaSetupArgSimple(__par6, 48UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t))cu_decode_2bit4ch_Hann))); }
# 318 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit4ch_Hann( const uint8_t *__restrict__ __cuda_0,float *__restrict__ __cuda_1,float *__restrict__ __cuda_2,float *__restrict__ __cuda_3,float *__restrict__ __cuda_4,const size_t __cuda_5,const size_t __cuda_6)
# 322 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z22cu_decode_2bit4ch_HannPKhPfS1_S1_S1_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4,__cuda_5,__cuda_6);
# 338 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z25cu_decode_2bit1ch_HammingPKhP6float4mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const uint8_t *__T234;
 struct float4 *__T235;
__T234 = __par0; __cudaSetupArgSimple(__T234, 0UL); __T235 = __par1; __cudaSetupArgSimple(__T235, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_2bit1ch_Hamming))); }
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_Hamming( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 345 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z25cu_decode_2bit1ch_HammingPKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 365 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z25cu_decode_2bit2ch_HammingPKhP6float2S2_mm( const uint8_t *__restrict__ __par0,  struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  const size_t __par3,  const size_t __par4) {  const uint8_t *__T236;
 struct float2 *__T237;
 struct float2 *__T238;
__T236 = __par0; __cudaSetupArgSimple(__T236, 0UL); __T237 = __par1; __cudaSetupArgSimple(__T237, 8UL); __T238 = __par2; __cudaSetupArgSimple(__T238, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t))cu_decode_2bit2ch_Hamming))); }
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit2ch_Hamming( const uint8_t *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,const size_t __cuda_3,const size_t __cuda_4)
# 368 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z25cu_decode_2bit2ch_HammingPKhP6float2S2_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 383 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z25cu_decode_2bit4ch_HammingPKhPfS1_S1_S1_mm( const uint8_t *__restrict__ __par0,  float *__restrict__ __par1,  float *__restrict__ __par2,  float *__restrict__ __par3,  float *__restrict__ __par4,  const size_t __par5,  const size_t __par6) {  const uint8_t *__T239;
 float *__T240;
 float *__T241;
 float *__T242;
 float *__T243;
__T239 = __par0; __cudaSetupArgSimple(__T239, 0UL); __T240 = __par1; __cudaSetupArgSimple(__T240, 8UL); __T241 = __par2; __cudaSetupArgSimple(__T241, 16UL); __T242 = __par3; __cudaSetupArgSimple(__T242, 24UL); __T243 = __par4; __cudaSetupArgSimple(__T243, 32UL); __cudaSetupArgSimple(__par5, 40UL); __cudaSetupArgSimple(__par6, 48UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t))cu_decode_2bit4ch_Hamming))); }
# 385 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit4ch_Hamming( const uint8_t *__restrict__ __cuda_0,float *__restrict__ __cuda_1,float *__restrict__ __cuda_2,float *__restrict__ __cuda_3,float *__restrict__ __cuda_4,const size_t __cuda_5,const size_t __cuda_6)
# 389 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z25cu_decode_2bit4ch_HammingPKhPfS1_S1_S1_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4,__cuda_5,__cuda_6);
# 405 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z30cu_decode_2bit1ch_CustomWindowPKhP6float4mmPKS1_( const uint8_t *__par0,  struct float4 *__par1,  const size_t __par2,  const size_t __par3,  const struct float4 *__restrict__ __par4) {  const struct float4 *__T244;
__cudaSetupArgSimple(__par0, 0UL); __cudaSetupArgSimple(__par1, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __T244 = __par4; __cudaSetupArgSimple(__T244, 32UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *, struct float4 *, const size_t, const size_t, const struct float4 *__restrict__))cu_decode_2bit1ch_CustomWindow))); }
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit1ch_CustomWindow( const uint8_t *__cuda_0,struct float4 *__cuda_1,const size_t __cuda_2,const size_t __cuda_3,const struct float4 *__restrict__ __cuda_4)
# 412 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z30cu_decode_2bit1ch_CustomWindowPKhP6float4mmPKS1_( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 425 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z30cu_decode_2bit2ch_CustomWindowPKhP6float2S2_mmPKS1_( const uint8_t *__restrict__ __par0,  struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  const size_t __par3,  const size_t __par4,  const struct float2 *__restrict__ __par5) {  const uint8_t *__T245;
 struct float2 *__T246;
 struct float2 *__T247;
 const struct float2 *__T248;
__T245 = __par0; __cudaSetupArgSimple(__T245, 0UL); __T246 = __par1; __cudaSetupArgSimple(__T246, 8UL); __T247 = __par2; __cudaSetupArgSimple(__T247, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __T248 = __par5; __cudaSetupArgSimple(__T248, 40UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t, const struct float2 *__restrict__))cu_decode_2bit2ch_CustomWindow))); }
# 427 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit2ch_CustomWindow( const uint8_t *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,const size_t __cuda_3,const size_t __cuda_4,const struct float2 *__restrict__ __cuda_5)
# 430 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z30cu_decode_2bit2ch_CustomWindowPKhP6float2S2_mmPKS1_( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4,__cuda_5);
# 440 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z30cu_decode_2bit4ch_CustomWindowPKhPfS1_S1_S1_mmPKf( const uint8_t *__restrict__ __par0,  float *__restrict__ __par1,  float *__restrict__ __par2,  float *__restrict__ __par3,  float *__restrict__ __par4,  const size_t __par5,  const size_t __par6,  const float *__restrict__ __par7) {  const uint8_t *__T249;
 float *__T250;
 float *__T251;
 float *__T252;
 float *__T253;
 const float *__T254;
__T249 = __par0; __cudaSetupArgSimple(__T249, 0UL); __T250 = __par1; __cudaSetupArgSimple(__T250, 8UL); __T251 = __par2; __cudaSetupArgSimple(__T251, 16UL); __T252 = __par3; __cudaSetupArgSimple(__T252, 24UL); __T253 = __par4; __cudaSetupArgSimple(__T253, 32UL); __cudaSetupArgSimple(__par5, 40UL); __cudaSetupArgSimple(__par6, 48UL); __T254 = __par7; __cudaSetupArgSimple(__T254, 56UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t, const float *__restrict__))cu_decode_2bit4ch_CustomWindow))); }
# 442 "../kernels/cuda/decoder_2b32f_kernels.cu"
void cu_decode_2bit4ch_CustomWindow( const uint8_t *__restrict__ __cuda_0,float *__restrict__ __cuda_1,float *__restrict__ __cuda_2,float *__restrict__ __cuda_3,float *__restrict__ __cuda_4,const size_t __cuda_5,const size_t __cuda_6,const float *__restrict__ __cuda_7)
# 446 "../kernels/cuda/decoder_2b32f_kernels.cu"
{__device_stub__Z30cu_decode_2bit4ch_CustomWindowPKhPfS1_S1_S1_mmPKf( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4,__cuda_5,__cuda_6,__cuda_7);
# 460 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z23cu_decode_2bit2ch_splitPKhP6float2S2_m( const uint8_t *__restrict__ __par0,  struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  const size_t __par3) {  const uint8_t *__T255;
 struct float2 *__T256;
 struct float2 *__T257;
__T255 = __par0; __cudaSetupArgSimple(__T255, 0UL); __T256 = __par1; __cudaSetupArgSimple(__T256, 8UL); __T257 = __par2; __cudaSetupArgSimple(__T257, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t))cu_decode_2bit2ch_split))); }
# 39 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
void cu_decode_2bit2ch_split( const uint8_t *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,const size_t __cuda_3)
# 44 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
{__device_stub__Z23cu_decode_2bit2ch_splitPKhP6float2S2_m( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 54 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z17cu_decode_3bit1chPKhP6float4m( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2) {  const uint8_t *__T258;
 struct float4 *__T259;
__T258 = __par0; __cudaSetupArgSimple(__T258, 0UL); __T259 = __par1; __cudaSetupArgSimple(__T259, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t))cu_decode_3bit1ch))); }
# 92 "../kernels/cuda/decoder_3b32f_kernels.cu"
void cu_decode_3bit1ch( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2)
# 93 "../kernels/cuda/decoder_3b32f_kernels.cu"
{__device_stub__Z17cu_decode_3bit1chPKhP6float4m( __cuda_0,__cuda_1,__cuda_2);
# 126 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z23cu_decode_3bit2ch_splitPKhP6float4S2_m( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  struct float4 *__restrict__ __par2,  const size_t __par3) {  const uint8_t *__T260;
 struct float4 *__T261;
 struct float4 *__T262;
__T260 = __par0; __cudaSetupArgSimple(__T260, 0UL); __T261 = __par1; __cudaSetupArgSimple(__T261, 8UL); __T262 = __par2; __cudaSetupArgSimple(__T262, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t))cu_decode_3bit2ch_split))); }
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
void cu_decode_3bit2ch_split( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,struct float4 *__restrict__ __cuda_2,const size_t __cuda_3)
# 141 "../kernels/cuda/decoder_3b32f_kernels.cu"
{__device_stub__Z23cu_decode_3bit2ch_splitPKhP6float4S2_m( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 174 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z22cu_decode_3bit1ch_HannPKhP6float4mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const uint8_t *__T263;
 struct float4 *__T264;
__T263 = __par0; __cudaSetupArgSimple(__T263, 0UL); __T264 = __par1; __cudaSetupArgSimple(__T264, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit1ch_Hann))); }
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
void cu_decode_3bit1ch_Hann( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 198 "../kernels/cuda/decoder_3b32f_kernels.cu"
{__device_stub__Z22cu_decode_3bit1ch_HannPKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 246 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z25cu_decode_3bit1ch_HammingPKhP6float4mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const uint8_t *__T265;
 struct float4 *__T266;
__T265 = __par0; __cudaSetupArgSimple(__T265, 0UL); __T266 = __par1; __cudaSetupArgSimple(__T266, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit1ch_Hamming))); }
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
void cu_decode_3bit1ch_Hamming( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 256 "../kernels/cuda/decoder_3b32f_kernels.cu"
{__device_stub__Z25cu_decode_3bit1ch_HammingPKhP6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 304 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z28cu_decode_3bit2ch_Hann_splitPKhP6float4S2_mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  struct float4 *__restrict__ __par2,  const size_t __par3,  const size_t __par4) {  const uint8_t *__T267;
 struct float4 *__T268;
 struct float4 *__T269;
__T267 = __par0; __cudaSetupArgSimple(__T267, 0UL); __T268 = __par1; __cudaSetupArgSimple(__T268, 8UL); __T269 = __par2; __cudaSetupArgSimple(__T269, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit2ch_Hann_split))); }
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
void cu_decode_3bit2ch_Hann_split( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,struct float4 *__restrict__ __cuda_2,const size_t __cuda_3,const size_t __cuda_4)
# 315 "../kernels/cuda/decoder_3b32f_kernels.cu"
{__device_stub__Z28cu_decode_3bit2ch_Hann_splitPKhP6float4S2_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 357 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z31cu_decode_3bit2ch_Hamming_splitPKhP6float4S2_mm( const uint8_t *__restrict__ __par0,  struct float4 *__restrict__ __par1,  struct float4 *__restrict__ __par2,  const size_t __par3,  const size_t __par4) {  const uint8_t *__T270;
 struct float4 *__T271;
 struct float4 *__T272;
__T270 = __par0; __cudaSetupArgSimple(__T270, 0UL); __T271 = __par1; __cudaSetupArgSimple(__T271, 8UL); __T272 = __par2; __cudaSetupArgSimple(__T272, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __cudaLaunch(((char *)((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit2ch_Hamming_split))); }
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
void cu_decode_3bit2ch_Hamming_split( const uint8_t *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,struct float4 *__restrict__ __cuda_2,const size_t __cuda_3,const size_t __cuda_4)
# 368 "../kernels/cuda/decoder_3b32f_kernels.cu"
{__device_stub__Z31cu_decode_3bit2ch_Hamming_splitPKhP6float4S2_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 410 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z21cu_split_2chan_f4_2f2PK6float4P6float2S3_m( const struct float4 *__restrict__ __par0,  struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  const size_t __par3) {  const struct float4 *__T273;
 struct float2 *__T274;
 struct float2 *__T275;
__T273 = __par0; __cudaSetupArgSimple(__T273, 0UL); __T274 = __par1; __cudaSetupArgSimple(__T274, 8UL); __T275 = __par2; __cudaSetupArgSimple(__T275, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const struct float4 *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t))cu_split_2chan_f4_2f2))); }
# 28 "../kernels/cuda/memory_split_kernels.cu"
void cu_split_2chan_f4_2f2( const struct float4 *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,const size_t __cuda_3)
# 29 "../kernels/cuda/memory_split_kernels.cu"
{__device_stub__Z21cu_split_2chan_f4_2f2PK6float4P6float2S3_m( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 39 "../kernels/cuda/memory_split_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z20cu_split_2chan_f2_2fPK6float2PfS2_m( const struct float2 *__restrict__ __par0,  float *__restrict__ __par1,  float *__restrict__ __par2,  const size_t __par3) {  const struct float2 *__T276;
 float *__T277;
 float *__T278;
__T276 = __par0; __cudaSetupArgSimple(__T276, 0UL); __T277 = __par1; __cudaSetupArgSimple(__T277, 8UL); __T278 = __par2; __cudaSetupArgSimple(__T278, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const struct float2 *__restrict__, float *__restrict__, float *__restrict__, const size_t))cu_split_2chan_f2_2f))); }
# 48 "../kernels/cuda/memory_split_kernels.cu"
void cu_split_2chan_f2_2f( const struct float2 *__restrict__ __cuda_0,float *__restrict__ __cuda_1,float *__restrict__ __cuda_2,const size_t __cuda_3)
# 49 "../kernels/cuda/memory_split_kernels.cu"
{__device_stub__Z20cu_split_2chan_f2_2fPK6float2PfS2_m( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 56 "../kernels/cuda/memory_split_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z18cu_accumulate_2polPK6float2S1_P6float4mm( const struct float2 *__restrict__ __par0,  const struct float2 *__restrict__ __par1,  struct float4 *__restrict__ __par2,  size_t __par3,  size_t __par4) {  const struct float2 *__T279;
 const struct float2 *__T280;
 struct float4 *__T281;
__T279 = __par0; __cudaSetupArgSimple(__T279, 0UL); __T280 = __par1; __cudaSetupArgSimple(__T280, 8UL); __T281 = __par2; __cudaSetupArgSimple(__T281, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __cudaLaunch(((char *)((void ( *)(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float4 *__restrict__, size_t, size_t))cu_accumulate_2pol))); }
# 32 "../kernels/cuda/arithmetic_xmac_kernels.cu"
void cu_accumulate_2pol( const struct float2 *__restrict__ __cuda_0,const struct float2 *__restrict__ __cuda_1,struct float4 *__restrict__ __cuda_2,size_t __cuda_3,size_t __cuda_4)
# 34 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{__device_stub__Z18cu_accumulate_2polPK6float2S1_P6float4mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 78 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z25cu_accumulate_2pol_packedPK6float4PS_mm( const struct float4 *__restrict__ __par0,  struct float4 *__restrict__ __par1,  size_t __par2,  size_t __par3) {  const struct float4 *__T282;
 struct float4 *__T283;
__T282 = __par0; __cudaSetupArgSimple(__T282, 0UL); __T283 = __par1; __cudaSetupArgSimple(__T283, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const struct float4 *__restrict__, struct float4 *__restrict__, size_t, size_t))cu_accumulate_2pol_packed))); }
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
void cu_accumulate_2pol_packed( const struct float4 *__restrict__ __cuda_0,struct float4 *__restrict__ __cuda_1,size_t __cuda_2,size_t __cuda_3)
# 81 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{__device_stub__Z25cu_accumulate_2pol_packedPK6float4PS_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 126 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z27cu_accumulate_2pol_autoOnlyPK6float2S1_PS_mm( const struct float2 *__restrict__ __par0,  const struct float2 *__restrict__ __par1,  struct float2 *__restrict__ __par2,  size_t __par3,  size_t __par4) {  const struct float2 *__T284;
 const struct float2 *__T285;
 struct float2 *__T286;
__T284 = __par0; __cudaSetupArgSimple(__T284, 0UL); __T285 = __par1; __cudaSetupArgSimple(__T285, 8UL); __T286 = __par2; __cudaSetupArgSimple(__T286, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaSetupArgSimple(__par4, 32UL); __cudaLaunch(((char *)((void ( *)(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float2 *__restrict__, size_t, size_t))cu_accumulate_2pol_autoOnly))); }
# 130 "../kernels/cuda/arithmetic_xmac_kernels.cu"
void cu_accumulate_2pol_autoOnly( const struct float2 *__restrict__ __cuda_0,const struct float2 *__restrict__ __cuda_1,struct float2 *__restrict__ __cuda_2,size_t __cuda_3,size_t __cuda_4)
# 132 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{__device_stub__Z27cu_accumulate_2pol_autoOnlyPK6float2S1_PS_mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3,__cuda_4);
# 168 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z34cu_accumulate_2pol_autoOnly_packedPK6float4P6float2mm( const struct float4 *__restrict__ __par0,  struct float2 *__restrict__ __par1,  size_t __par2,  size_t __par3) {  const struct float4 *__T287;
 struct float2 *__T288;
__T287 = __par0; __cudaSetupArgSimple(__T287, 0UL); __T288 = __par1; __cudaSetupArgSimple(__T288, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const struct float4 *__restrict__, struct float2 *__restrict__, size_t, size_t))cu_accumulate_2pol_autoOnly_packed))); }
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
void cu_accumulate_2pol_autoOnly_packed( const struct float4 *__restrict__ __cuda_0,struct float2 *__restrict__ __cuda_1,size_t __cuda_2,size_t __cuda_3)
# 171 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{__device_stub__Z34cu_accumulate_2pol_autoOnly_packedPK6float4P6float2mm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 207 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z17autoPowerSpectrumP6float2Pfmm( cufftComplex *__restrict__ __par0,  float *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  cufftComplex *__T289;
 float *__T290;
__T289 = __par0; __cudaSetupArgSimple(__T289, 0UL); __T290 = __par1; __cudaSetupArgSimple(__T290, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum))); }
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void autoPowerSpectrum( cufftComplex *__restrict__ __cuda_0,float *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 45 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{__device_stub__Z17autoPowerSpectrumP6float2Pfmm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 55 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z20autoPowerSpectrum_v2PK6float2Pfmm( const cufftComplex *__restrict__ __par0,  float *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const cufftComplex *__T291;
 float *__T292;
__T291 = __par0; __cudaSetupArgSimple(__T291, 0UL); __T292 = __par1; __cudaSetupArgSimple(__T292, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum_v2))); }
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void autoPowerSpectrum_v2( const cufftComplex *__restrict__ __cuda_0,float *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 59 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{__device_stub__Z20autoPowerSpectrum_v2PK6float2Pfmm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 75 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z20autoPowerSpectrum_v3PK6float2Pfmm( const cufftComplex *__restrict__ __par0,  float *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const cufftComplex *__T293;
 float *__T294;
__T293 = __par0; __cudaSetupArgSimple(__T293, 0UL); __T294 = __par1; __cudaSetupArgSimple(__T294, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum_v3))); }
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void autoPowerSpectrum_v3( const cufftComplex *__restrict__ __cuda_0,float *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 79 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{__device_stub__Z20autoPowerSpectrum_v3PK6float2Pfmm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 112 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z23autoPowerSpectrum_v3_CBPKfPfmm( const cufftReal *__restrict__ __par0,  float *__restrict__ __par1,  const size_t __par2,  const size_t __par3) {  const cufftReal *__T296;
 float *__T297;
__T296 = __par0; __cudaSetupArgSimple(__T296, 0UL); __T297 = __par1; __cudaSetupArgSimple(__T297, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaSetupArgSimple(__par3, 24UL); __cudaLaunch(((char *)((void ( *)(const cufftReal *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum_v3_CB))); }
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void autoPowerSpectrum_v3_CB( const cufftReal *__restrict__ __cuda_0,float *__restrict__ __cuda_1,const size_t __cuda_2,const size_t __cuda_3)
# 128 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{__device_stub__Z23autoPowerSpectrum_v3_CBPKfPfmm( __cuda_0,__cuda_1,__cuda_2,__cuda_3);
# 156 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z14cu_window_hannP6float4mm( struct float4 *__par0,  size_t __par1,  size_t __par2) {  __cudaSetupArgSimple(__par0, 0UL); __cudaSetupArgSimple(__par1, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaLaunch(((char *)((void ( *)(struct float4 *, size_t, size_t))cu_window_hann))); }
# 56 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void cu_window_hann( struct float4 *__cuda_0,size_t __cuda_1,size_t __cuda_2)
# 57 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{__device_stub__Z14cu_window_hannP6float4mm( __cuda_0,__cuda_1,__cuda_2);
# 73 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
void __device_stub__Z17cu_window_hammingP6float4mm( struct float4 *__par0,  size_t __par1,  size_t __par2) {  __cudaSetupArgSimple(__par0, 0UL); __cudaSetupArgSimple(__par1, 8UL); __cudaSetupArgSimple(__par2, 16UL); __cudaLaunch(((char *)((void ( *)(struct float4 *, size_t, size_t))cu_window_hamming))); }
# 82 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void cu_window_hamming( struct float4 *__cuda_0,size_t __cuda_1,size_t __cuda_2)
# 83 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{__device_stub__Z17cu_window_hammingP6float4mm( __cuda_0,__cuda_1,__cuda_2);
# 99 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}
# 1 "lib_kfftspec.cudafe1.stub.c"
static void __nv_cudaEntityRegisterCallback( void **__T2101) {  __nv_dummy_param_ref(__T2101); __nv_save_fatbinhandle_for_managed_rt(__T2101); __cudaRegisterEntry(__T2101, ((void ( *)(struct float4 *, size_t, size_t))cu_window_hamming), _Z17cu_window_hammingP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(struct float4 *, size_t, size_t))cu_window_hann), _Z14cu_window_hannP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const cufftReal *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum_v3_CB), _Z23autoPowerSpectrum_v3_CBPKfPfmm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum_v3), _Z20autoPowerSpectrum_v3PK6float2Pfmm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum_v2), _Z20autoPowerSpectrum_v2PK6float2Pfmm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t))autoPowerSpectrum), _Z17autoPowerSpectrumP6float2Pfmm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const struct float4 *__restrict__, struct float2 *__restrict__, size_t, size_t))cu_accumulate_2pol_autoOnly_packed), _Z34cu_accumulate_2pol_autoOnly_packedPK6float4P6float2mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float2 *__restrict__, size_t, size_t))cu_accumulate_2pol_autoOnly), _Z27cu_accumulate_2pol_autoOnlyPK6float2S1_PS_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const struct float4 *__restrict__, struct float4 *__restrict__, size_t, size_t))cu_accumulate_2pol_packed), _Z25cu_accumulate_2pol_packedPK6float4PS_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float4 *__restrict__, size_t, size_t))cu_accumulate_2pol), _Z18cu_accumulate_2polPK6float2S1_P6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const struct float2 *__restrict__, float *__restrict__, float *__restrict__, const size_t))cu_split_2chan_f2_2f), _Z20cu_split_2chan_f2_2fPK6float2PfS2_m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const struct float4 *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t))cu_split_2chan_f4_2f2), _Z21cu_split_2chan_f4_2f2PK6float4P6float2S3_m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit2ch_Hamming_split), _Z31cu_decode_3bit2ch_Hamming_splitPKhP6float4S2_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit2ch_Hann_split), _Z28cu_decode_3bit2ch_Hann_splitPKhP6float4S2_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit1ch_Hamming), _Z25cu_decode_3bit1ch_HammingPKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_3bit1ch_Hann), _Z22cu_decode_3bit1ch_HannPKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t))cu_decode_3bit2ch_split), _Z23cu_decode_3bit2ch_splitPKhP6float4S2_m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t))cu_decode_3bit1ch), _Z17cu_decode_3bit1chPKhP6float4m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t))cu_decode_2bit2ch_split), _Z23cu_decode_2bit2ch_splitPKhP6float2S2_m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t, const float *__restrict__))cu_decode_2bit4ch_CustomWindow), _Z30cu_decode_2bit4ch_CustomWindowPKhPfS1_S1_S1_mmPKf, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t, const struct float2 *__restrict__))cu_decode_2bit2ch_CustomWindow), _Z30cu_decode_2bit2ch_CustomWindowPKhP6float2S2_mmPKS1_, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *, struct float4 *, const size_t, const size_t, const struct float4 *__restrict__))cu_decode_2bit1ch_CustomWindow), _Z30cu_decode_2bit1ch_CustomWindowPKhP6float4mmPKS1_, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t))cu_decode_2bit4ch_Hamming), _Z25cu_decode_2bit4ch_HammingPKhPfS1_S1_S1_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t))cu_decode_2bit2ch_Hamming), _Z25cu_decode_2bit2ch_HammingPKhP6float2S2_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_2bit1ch_Hamming), _Z25cu_decode_2bit1ch_HammingPKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t))cu_decode_2bit4ch_Hann), _Z22cu_decode_2bit4ch_HannPKhPfS1_S1_S1_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t))cu_decode_2bit2ch_Hann), _Z22cu_decode_2bit2ch_HannPKhP6float2S2_mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t))cu_decode_2bit1ch_Hann), _Z22cu_decode_2bit1ch_HannPKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t))cu_decode_2bit4ch), _Z17cu_decode_2bit4chPKhPfS1_S1_S1_m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t))cu_decode_2bit2ch), _Z17cu_decode_2bit2chPKhP6float2S2_m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t))cu_decode_2bit1ch), _Z17cu_decode_2bit1chPKhP6float4m, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *, struct float4 *, size_t, size_t))cu_decode_2bit1ch_1D_v4), _Z23cu_decode_2bit1ch_1D_v4PKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *, struct float4 *, size_t, size_t))cu_decode_2bit1ch_1D_v3), _Z23cu_decode_2bit1ch_1D_v3PKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t))cu_decode_2bit1ch_1D_mark5b_v2), _Z30cu_decode_2bit1ch_1D_mark5b_v2PKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t))cu_decode_2bit1ch_1D_v2), _Z23cu_decode_2bit1ch_1D_v2PKhP6float4mm, (-1)); __cudaRegisterEntry(__T2101, ((void ( *)(const uint8_t *__restrict__, struct float4 *__restrict__, size_t, size_t))cu_decode_2bit1ch_1D_v1b), _Z24cu_decode_2bit1ch_1D_v1bPKhP6float4mm, (-1)); __cudaRegisterVariable(__T2101, __shadow_var(c_lookup_2bit_VDIF,::c_lookup_2bit_VDIF), 0, 4096, 1, 0); __cudaRegisterVariable(__T2101, __shadow_var(c_lut2bit_vdif,::c_lut2bit_vdif), 0, 16, 1, 0); __cudaRegisterVariable(__T2101, __shadow_var(c_lut2bit_mark5b,::c_lut2bit_mark5b), 0, 16, 1, 0); __cudaRegisterVariable(__T2101, __shadow_var(c_lut3bit,::c_lut3bit), 0, 32, 1, 0); __cudaRegisterVariable(__T2101, __shadow_var(cu_autoPowerSpectrum_cufftCallbackStoreC_ptr,::cu_autoPowerSpectrum_cufftCallbackStoreC_ptr), 0, 8, 0, 0); __cudaRegisterVariable(__T2101, __shadow_var(cu_window_hann_cufftCallbackLoadR_ptr,::cu_window_hann_cufftCallbackLoadR_ptr), 0, 8, 0, 0); __cudaRegisterVariable(__T2101, __shadow_var(cu_window_hamming_cufftCallbackLoadR_ptr,::cu_window_hamming_cufftCallbackLoadR_ptr), 0, 8, 0, 0); }
static void __sti____cudaRegisterAll_20_lib_kfftspec_cpp1_ii_350f4d54(void) {  ____cudaRegisterLinkedBinary(__nv_cudaEntityRegisterCallback);  }

#pragma GCC diagnostic pop
