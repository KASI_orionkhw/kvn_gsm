/////////////////////////////////////////////////////////////////////////////////////
//
// GPU Spectrometer v0.6.8
//
// Processes single IF data present in VDIF format. The data can come either from
// the network (UDP stream) or from an existing file on disk/RAID. Utilizes
// a single graphics card.
//
// Basic processing is quite trivial :
//  - decode VDIF, from whatever input source
//  - perform non-windowed non-overlapped FFT
//  - accumulate squared-magnitude FFTs to get time-integrated autopower spectrum
//  - output the integrated spectrum in regular intervals
//
// To Do:
//  - add server component, allow spectrometer to be configured and started
//    over the network rather than command line; use ALMA/ACA-like commands?
//
// (C) 2015 Jan Wagner, Jongsoo Kim
// --------------------------------
// 2017.6.23
// Hyunwoo Kang modifys to support GSM
/////////////////////////////////////////////////////////////////////////////////////
#ifndef KFFTSPEC_H
#define KFFTSPEC_H

#include "vdif_receiver_rb.h"
#include "kvn_dsm.h"
#include "lib_kvn_dsm.h"

#include "arithmetic_winfunc_kernels.h"
#include "device_types.h"

#include <unistd.h>
#include <stdio.h>
#include <sys/time.h>
#include <cufft.h>

#define FFTSPEC_MAX_GPUS     8   // Max nr of GPUs to use in parallel
#define FFTSPEC_MAX_STREAMS  4   // Max nr of CUDA streams to create on each GPU
#define FFTSPEC_MAX_SUBBANDS 2   // Input data, maximum nr of subbands (aka IFs aka channels) to support
#define SPEC_INVALID      -1.0

#define DEFAULT_EXPERIMENT_NAME "test"
#define DEFAULT_STATION_NAME    "KVN_Yonsei"
#define DEFAULT_OBSERVER_NAME   "Operator"
#define BACKEND_NAME            "KSPEC-1.0"
#define OUT_FORMAT_KVNDSM    1
#define OUT_FORMAT_KFFTSPEC  2
#define OUT_DISCARD          64

#define WINDOW_FUNCTION_NONE 0
#define WINDOW_FUNCTION_HANN 1
#define WINDOW_FUNCTION_HAMMING 2
#define WINDOW_FUNCTION_HFT248D 3

#define INIT_FLAG		0x01
#define READY_FLAG		0x02
#define RUN_FLAG		0x10
#define OUTDATA_FLAG	0x20

typedef struct fftspec_config_tt
{
    /* By Hyunwoo Kang for DAS */
    int dasfd;
    int idling;
    char *rx_source;
	int data_flag[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];
	int spec_flag[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];
	int rx_flag;	// 0x01: init, 0x02: data ready, 
					// 0x10: run, 0x20: out of data
	int rx_is_file;
	//Data headers for DSM format
	kvn_dsm_structure_t h;

    /* Spectral configuration */
    double T_int;     // Integration time in seconds
    double T_int_wish;// Originally requested integration time in seconds, may not fit framing & data rate
    double bw;        // Bandwidth of one band in Hz
    int    nchan;     // Number of output channels (r2c FFT length is <in>=2*nchan, <out>=nchan+1)
    int    nfft;      // Number of FFTs per subband that fit into T_int
    int    nsubints;  // Number of subintegrations to evenly split 'nfft' into.
    int    do_cross;  // 1 to form cross-power spectra, in addition to power spectra
    int    nspecs;    // total number of averaged spectra to be produced, including cross-power if enabled
    int    do_window; // 1 to apply window function prior to FFT
    int    window_type; // window function (Hann, Hanmming, flat-top, ...)
    float  window_overlap; // amount of overlap between FFTs in percent : TODO

    /* Time division multiplexing */
    int nGPUs;     // How many GPU to use in parallel
    int nstreams;  // How many CUDA Streams per GPU to create

    /* VDIF data receiver object (both file or UDP/IP) */
    vdif_rx_t *rx;
    char *raw_format;
    char *old_format;
    int raw_nsubbands;
    int raw_nbits;

    /* Raw input sample data (headerless, with a fill pattern for missing frames) */
    struct timeval h_rawbuf_midtimes[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // Timestamps (from VDIF) at midpoint of integration period
    unsigned char* h_rawbufs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // Pointers to full integration period -sized data chunks
    unsigned char* d_rawbufs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // -"- on GPU
    size_t         rawbuflen;                                        // Length in bytes for raw input data of one integration period
    unsigned char* h_random;

    /* Stream and FFT setup */
    int            devicemap[FFTSPEC_MAX_GPUS];                // Map own index to GPU device number (default is 0,1,2,3,..)
    cudaStream_t   sid[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // IDs of Streams created on the GPUs
    cufftHandle    cufftplans[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];
    cudaDeviceProp devprops[FFTSPEC_MAX_GPUS];

    /* On-GPU arrays */
    float*         d_fft_in[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // input to r2c FFT
    float*         d_fft_out[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // output of r2c FFT (type is recast cufftComplex)
    cu_window_cb_params_t  h_cufft_userparams;                       // extra user params in cuFFT Callback
    cu_window_cb_params_t* d_cufft_userparams[FFTSPEC_MAX_GPUS];     // device copy of -"-, one copy per GPU is enough

    /* Output arrays */
    float*         d_powspecs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // accumulated auto&cross power spectra (interleaved {XX,Re XY,Im XY, YY) of all FFTSPEC_MAX_SUBBANDS side-by-side
    float*         h_powspecs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // -"- power spectra
    float*         h_crosspecs[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // -"- cross-power spectra
    double         spec_weight[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // weights of each spectrum = valid data / total data; -1 if invalid spectrum

    /* Output storage (formats: SDFITS, KVN DSM, other...?) */
    char* scanname;
    char* experiment;
    char* station;
    char* observer;
    char* out_filename;

    /* Performance tracking */
    cudaEvent_t process_cuda_starttimes[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS]; // Wallclock timestamps when processing of segment started
    cudaEvent_t process_cuda_stoptimes[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // -"- finished
    cudaEvent_t process_cuda_inputdata_overwriteable[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // Event happens after raw input data area can be freely overwritten
    cudaEvent_t process_cuda_spectrum_available[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // Event happens after spectrum completely copied to host
    cudaEvent_t estart[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];  // kernel timing
    cudaEvent_t estop[FFTSPEC_MAX_GPUS][FFTSPEC_MAX_STREAMS];   // kernel timing
} fftspec_config_t;

int fftspec_parse_args(const int, char *const[], fftspec_config_t*);

int fftspec_adjust_config(fftspec_config_t*);
int fftspec_check_config(const fftspec_config_t*);
void fftspec_show_config(const fftspec_config_t*);

int fftspec_estimate_memory(const fftspec_config_t*, size_t*, size_t*);
int fftspec_allocate(fftspec_config_t*);

int fftspec_process_segment(fftspec_config_t*, const int, const int);

int fftspec_store_results(const fftspec_config_t*, const int, const int, const int);

int connectshm_FS(key_t shm_key, fftspec_config_t **fs);
void *mainprocessing(void *args);
int run_gpu(fftspec_config_t *fs);
int fftspec_init_condition(fftspec_config_t *fs);
#endif // KFFTSPEC_H
