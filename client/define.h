#ifndef DEFINE_H
#define DEFINE_H

struct CorrInfo{
	int tseg;
	int tleft;
	int tint;
	int run;
	int integ;
	int halt;
	int tick;
	int tick_per_sec;
	int dasfd;
	int das;
	int flag;
	int startflag;
	int stopflag;
	int haltflag;
	int band;

};


#endif
