# 1 "lib_kfftspec.cudafe1.gpu"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "lib_kfftspec.cudafe1.gpu"
typedef char __nv_bool;
# 1483 "/usr/local/cuda-8.0/bin/..//include/driver_types.h"
struct CUstream_st;
# 1488 "/usr/local/cuda-8.0/bin/..//include/driver_types.h"
struct CUevent_st;
# 54 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
enum cudaDataType_t {
# 56 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_16F = 2,
# 57 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_16F = 6,
# 58 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_32F = 0,
# 59 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_32F = 4,
# 60 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_64F = 1,
# 61 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_64F = 5,
# 62 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_8I = 3,
# 63 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_8I = 7,
# 64 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_8U,
# 65 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_8U,
# 66 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_32I,
# 67 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_32I,
# 68 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_32U,
# 69 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_32U};
# 73 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
enum libraryPropertyType_t {
# 75 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
MAJOR_VERSION,
# 76 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
MINOR_VERSION,
# 77 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
PATCH_LEVEL};
# 30 "/usr/include/bits/time.h" 3
struct timeval;
# 133 "/usr/include/time.h" 3
struct tm;
# 181 "/usr/include/libio.h" 3
enum __codecvt_result {
# 183 "/usr/include/libio.h" 3
__codecvt_ok,
# 184 "/usr/include/libio.h" 3
__codecvt_partial,
# 185 "/usr/include/libio.h" 3
__codecvt_error,
# 186 "/usr/include/libio.h" 3
__codecvt_noconv};
# 246 "/usr/include/libio.h" 3
struct _IO_FILE;
# 75 "/usr/include/bits/pthreadtypes.h" 3
struct __pthread_internal_list;
# 92 "/usr/include/bits/pthreadtypes.h" 3
struct _ZN15pthread_mutex_t17__pthread_mutex_sE;
# 91 "/usr/include/bits/pthreadtypes.h" 3
union pthread_mutex_t;
# 132 "/usr/include/bits/pthreadtypes.h" 3
struct _ZN14pthread_cond_tUt_E;
# 130 "/usr/include/bits/pthreadtypes.h" 3
union pthread_cond_t;
# 191 "/usr/include/math.h" 3
enum _ZUt_ {
# 192 "/usr/include/math.h" 3
FP_NAN,
# 195 "/usr/include/math.h" 3
FP_INFINITE,
# 198 "/usr/include/math.h" 3
FP_ZERO,
# 201 "/usr/include/math.h" 3
FP_SUBNORMAL,
# 204 "/usr/include/math.h" 3
FP_NORMAL};
# 289 "/usr/include/math.h" 3
enum _LIB_VERSION_TYPE {
# 290 "/usr/include/math.h" 3
_IEEE_ = (-1),
# 291 "/usr/include/math.h" 3
_SVID_,
# 292 "/usr/include/math.h" 3
_XOPEN_,
# 293 "/usr/include/math.h" 3
_POSIX_,
# 294 "/usr/include/math.h" 3
_ISOC_};
# 26 "/usr/include/bits/confname.h" 3
enum _ZUt0_ {
# 27 "/usr/include/bits/confname.h" 3
_PC_LINK_MAX,
# 29 "/usr/include/bits/confname.h" 3
_PC_MAX_CANON,
# 31 "/usr/include/bits/confname.h" 3
_PC_MAX_INPUT,
# 33 "/usr/include/bits/confname.h" 3
_PC_NAME_MAX,
# 35 "/usr/include/bits/confname.h" 3
_PC_PATH_MAX,
# 37 "/usr/include/bits/confname.h" 3
_PC_PIPE_BUF,
# 39 "/usr/include/bits/confname.h" 3
_PC_CHOWN_RESTRICTED,
# 41 "/usr/include/bits/confname.h" 3
_PC_NO_TRUNC,
# 43 "/usr/include/bits/confname.h" 3
_PC_VDISABLE,
# 45 "/usr/include/bits/confname.h" 3
_PC_SYNC_IO,
# 47 "/usr/include/bits/confname.h" 3
_PC_ASYNC_IO,
# 49 "/usr/include/bits/confname.h" 3
_PC_PRIO_IO,
# 51 "/usr/include/bits/confname.h" 3
_PC_SOCK_MAXBUF,
# 53 "/usr/include/bits/confname.h" 3
_PC_FILESIZEBITS,
# 55 "/usr/include/bits/confname.h" 3
_PC_REC_INCR_XFER_SIZE,
# 57 "/usr/include/bits/confname.h" 3
_PC_REC_MAX_XFER_SIZE,
# 59 "/usr/include/bits/confname.h" 3
_PC_REC_MIN_XFER_SIZE,
# 61 "/usr/include/bits/confname.h" 3
_PC_REC_XFER_ALIGN,
# 63 "/usr/include/bits/confname.h" 3
_PC_ALLOC_SIZE_MIN,
# 65 "/usr/include/bits/confname.h" 3
_PC_SYMLINK_MAX,
# 67 "/usr/include/bits/confname.h" 3
_PC_2_SYMLINKS};
# 73 "/usr/include/bits/confname.h" 3
enum _ZUt1_ {
# 74 "/usr/include/bits/confname.h" 3
_SC_ARG_MAX,
# 76 "/usr/include/bits/confname.h" 3
_SC_CHILD_MAX,
# 78 "/usr/include/bits/confname.h" 3
_SC_CLK_TCK,
# 80 "/usr/include/bits/confname.h" 3
_SC_NGROUPS_MAX,
# 82 "/usr/include/bits/confname.h" 3
_SC_OPEN_MAX,
# 84 "/usr/include/bits/confname.h" 3
_SC_STREAM_MAX,
# 86 "/usr/include/bits/confname.h" 3
_SC_TZNAME_MAX,
# 88 "/usr/include/bits/confname.h" 3
_SC_JOB_CONTROL,
# 90 "/usr/include/bits/confname.h" 3
_SC_SAVED_IDS,
# 92 "/usr/include/bits/confname.h" 3
_SC_REALTIME_SIGNALS,
# 94 "/usr/include/bits/confname.h" 3
_SC_PRIORITY_SCHEDULING,
# 96 "/usr/include/bits/confname.h" 3
_SC_TIMERS,
# 98 "/usr/include/bits/confname.h" 3
_SC_ASYNCHRONOUS_IO,
# 100 "/usr/include/bits/confname.h" 3
_SC_PRIORITIZED_IO,
# 102 "/usr/include/bits/confname.h" 3
_SC_SYNCHRONIZED_IO,
# 104 "/usr/include/bits/confname.h" 3
_SC_FSYNC,
# 106 "/usr/include/bits/confname.h" 3
_SC_MAPPED_FILES,
# 108 "/usr/include/bits/confname.h" 3
_SC_MEMLOCK,
# 110 "/usr/include/bits/confname.h" 3
_SC_MEMLOCK_RANGE,
# 112 "/usr/include/bits/confname.h" 3
_SC_MEMORY_PROTECTION,
# 114 "/usr/include/bits/confname.h" 3
_SC_MESSAGE_PASSING,
# 116 "/usr/include/bits/confname.h" 3
_SC_SEMAPHORES,
# 118 "/usr/include/bits/confname.h" 3
_SC_SHARED_MEMORY_OBJECTS,
# 120 "/usr/include/bits/confname.h" 3
_SC_AIO_LISTIO_MAX,
# 122 "/usr/include/bits/confname.h" 3
_SC_AIO_MAX,
# 124 "/usr/include/bits/confname.h" 3
_SC_AIO_PRIO_DELTA_MAX,
# 126 "/usr/include/bits/confname.h" 3
_SC_DELAYTIMER_MAX,
# 128 "/usr/include/bits/confname.h" 3
_SC_MQ_OPEN_MAX,
# 130 "/usr/include/bits/confname.h" 3
_SC_MQ_PRIO_MAX,
# 132 "/usr/include/bits/confname.h" 3
_SC_VERSION,
# 134 "/usr/include/bits/confname.h" 3
_SC_PAGESIZE,
# 137 "/usr/include/bits/confname.h" 3
_SC_RTSIG_MAX,
# 139 "/usr/include/bits/confname.h" 3
_SC_SEM_NSEMS_MAX,
# 141 "/usr/include/bits/confname.h" 3
_SC_SEM_VALUE_MAX,
# 143 "/usr/include/bits/confname.h" 3
_SC_SIGQUEUE_MAX,
# 145 "/usr/include/bits/confname.h" 3
_SC_TIMER_MAX,
# 150 "/usr/include/bits/confname.h" 3
_SC_BC_BASE_MAX,
# 152 "/usr/include/bits/confname.h" 3
_SC_BC_DIM_MAX,
# 154 "/usr/include/bits/confname.h" 3
_SC_BC_SCALE_MAX,
# 156 "/usr/include/bits/confname.h" 3
_SC_BC_STRING_MAX,
# 158 "/usr/include/bits/confname.h" 3
_SC_COLL_WEIGHTS_MAX,
# 160 "/usr/include/bits/confname.h" 3
_SC_EQUIV_CLASS_MAX,
# 162 "/usr/include/bits/confname.h" 3
_SC_EXPR_NEST_MAX,
# 164 "/usr/include/bits/confname.h" 3
_SC_LINE_MAX,
# 166 "/usr/include/bits/confname.h" 3
_SC_RE_DUP_MAX,
# 168 "/usr/include/bits/confname.h" 3
_SC_CHARCLASS_NAME_MAX,
# 171 "/usr/include/bits/confname.h" 3
_SC_2_VERSION,
# 173 "/usr/include/bits/confname.h" 3
_SC_2_C_BIND,
# 175 "/usr/include/bits/confname.h" 3
_SC_2_C_DEV,
# 177 "/usr/include/bits/confname.h" 3
_SC_2_FORT_DEV,
# 179 "/usr/include/bits/confname.h" 3
_SC_2_FORT_RUN,
# 181 "/usr/include/bits/confname.h" 3
_SC_2_SW_DEV,
# 183 "/usr/include/bits/confname.h" 3
_SC_2_LOCALEDEF,
# 186 "/usr/include/bits/confname.h" 3
_SC_PII,
# 188 "/usr/include/bits/confname.h" 3
_SC_PII_XTI,
# 190 "/usr/include/bits/confname.h" 3
_SC_PII_SOCKET,
# 192 "/usr/include/bits/confname.h" 3
_SC_PII_INTERNET,
# 194 "/usr/include/bits/confname.h" 3
_SC_PII_OSI,
# 196 "/usr/include/bits/confname.h" 3
_SC_POLL,
# 198 "/usr/include/bits/confname.h" 3
_SC_SELECT,
# 200 "/usr/include/bits/confname.h" 3
_SC_UIO_MAXIOV,
# 202 "/usr/include/bits/confname.h" 3
_SC_IOV_MAX = 60,
# 204 "/usr/include/bits/confname.h" 3
_SC_PII_INTERNET_STREAM,
# 206 "/usr/include/bits/confname.h" 3
_SC_PII_INTERNET_DGRAM,
# 208 "/usr/include/bits/confname.h" 3
_SC_PII_OSI_COTS,
# 210 "/usr/include/bits/confname.h" 3
_SC_PII_OSI_CLTS,
# 212 "/usr/include/bits/confname.h" 3
_SC_PII_OSI_M,
# 214 "/usr/include/bits/confname.h" 3
_SC_T_IOV_MAX,
# 218 "/usr/include/bits/confname.h" 3
_SC_THREADS,
# 220 "/usr/include/bits/confname.h" 3
_SC_THREAD_SAFE_FUNCTIONS,
# 222 "/usr/include/bits/confname.h" 3
_SC_GETGR_R_SIZE_MAX,
# 224 "/usr/include/bits/confname.h" 3
_SC_GETPW_R_SIZE_MAX,
# 226 "/usr/include/bits/confname.h" 3
_SC_LOGIN_NAME_MAX,
# 228 "/usr/include/bits/confname.h" 3
_SC_TTY_NAME_MAX,
# 230 "/usr/include/bits/confname.h" 3
_SC_THREAD_DESTRUCTOR_ITERATIONS,
# 232 "/usr/include/bits/confname.h" 3
_SC_THREAD_KEYS_MAX,
# 234 "/usr/include/bits/confname.h" 3
_SC_THREAD_STACK_MIN,
# 236 "/usr/include/bits/confname.h" 3
_SC_THREAD_THREADS_MAX,
# 238 "/usr/include/bits/confname.h" 3
_SC_THREAD_ATTR_STACKADDR,
# 240 "/usr/include/bits/confname.h" 3
_SC_THREAD_ATTR_STACKSIZE,
# 242 "/usr/include/bits/confname.h" 3
_SC_THREAD_PRIORITY_SCHEDULING,
# 244 "/usr/include/bits/confname.h" 3
_SC_THREAD_PRIO_INHERIT,
# 246 "/usr/include/bits/confname.h" 3
_SC_THREAD_PRIO_PROTECT,
# 248 "/usr/include/bits/confname.h" 3
_SC_THREAD_PROCESS_SHARED,
# 251 "/usr/include/bits/confname.h" 3
_SC_NPROCESSORS_CONF,
# 253 "/usr/include/bits/confname.h" 3
_SC_NPROCESSORS_ONLN,
# 255 "/usr/include/bits/confname.h" 3
_SC_PHYS_PAGES,
# 257 "/usr/include/bits/confname.h" 3
_SC_AVPHYS_PAGES,
# 259 "/usr/include/bits/confname.h" 3
_SC_ATEXIT_MAX,
# 261 "/usr/include/bits/confname.h" 3
_SC_PASS_MAX,
# 264 "/usr/include/bits/confname.h" 3
_SC_XOPEN_VERSION,
# 266 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XCU_VERSION,
# 268 "/usr/include/bits/confname.h" 3
_SC_XOPEN_UNIX,
# 270 "/usr/include/bits/confname.h" 3
_SC_XOPEN_CRYPT,
# 272 "/usr/include/bits/confname.h" 3
_SC_XOPEN_ENH_I18N,
# 274 "/usr/include/bits/confname.h" 3
_SC_XOPEN_SHM,
# 277 "/usr/include/bits/confname.h" 3
_SC_2_CHAR_TERM,
# 279 "/usr/include/bits/confname.h" 3
_SC_2_C_VERSION,
# 281 "/usr/include/bits/confname.h" 3
_SC_2_UPE,
# 284 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XPG2,
# 286 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XPG3,
# 288 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XPG4,
# 291 "/usr/include/bits/confname.h" 3
_SC_CHAR_BIT,
# 293 "/usr/include/bits/confname.h" 3
_SC_CHAR_MAX,
# 295 "/usr/include/bits/confname.h" 3
_SC_CHAR_MIN,
# 297 "/usr/include/bits/confname.h" 3
_SC_INT_MAX,
# 299 "/usr/include/bits/confname.h" 3
_SC_INT_MIN,
# 301 "/usr/include/bits/confname.h" 3
_SC_LONG_BIT,
# 303 "/usr/include/bits/confname.h" 3
_SC_WORD_BIT,
# 305 "/usr/include/bits/confname.h" 3
_SC_MB_LEN_MAX,
# 307 "/usr/include/bits/confname.h" 3
_SC_NZERO,
# 309 "/usr/include/bits/confname.h" 3
_SC_SSIZE_MAX,
# 311 "/usr/include/bits/confname.h" 3
_SC_SCHAR_MAX,
# 313 "/usr/include/bits/confname.h" 3
_SC_SCHAR_MIN,
# 315 "/usr/include/bits/confname.h" 3
_SC_SHRT_MAX,
# 317 "/usr/include/bits/confname.h" 3
_SC_SHRT_MIN,
# 319 "/usr/include/bits/confname.h" 3
_SC_UCHAR_MAX,
# 321 "/usr/include/bits/confname.h" 3
_SC_UINT_MAX,
# 323 "/usr/include/bits/confname.h" 3
_SC_ULONG_MAX,
# 325 "/usr/include/bits/confname.h" 3
_SC_USHRT_MAX,
# 328 "/usr/include/bits/confname.h" 3
_SC_NL_ARGMAX,
# 330 "/usr/include/bits/confname.h" 3
_SC_NL_LANGMAX,
# 332 "/usr/include/bits/confname.h" 3
_SC_NL_MSGMAX,
# 334 "/usr/include/bits/confname.h" 3
_SC_NL_NMAX,
# 336 "/usr/include/bits/confname.h" 3
_SC_NL_SETMAX,
# 338 "/usr/include/bits/confname.h" 3
_SC_NL_TEXTMAX,
# 341 "/usr/include/bits/confname.h" 3
_SC_XBS5_ILP32_OFF32,
# 343 "/usr/include/bits/confname.h" 3
_SC_XBS5_ILP32_OFFBIG,
# 345 "/usr/include/bits/confname.h" 3
_SC_XBS5_LP64_OFF64,
# 347 "/usr/include/bits/confname.h" 3
_SC_XBS5_LPBIG_OFFBIG,
# 350 "/usr/include/bits/confname.h" 3
_SC_XOPEN_LEGACY,
# 352 "/usr/include/bits/confname.h" 3
_SC_XOPEN_REALTIME,
# 354 "/usr/include/bits/confname.h" 3
_SC_XOPEN_REALTIME_THREADS,
# 357 "/usr/include/bits/confname.h" 3
_SC_ADVISORY_INFO,
# 359 "/usr/include/bits/confname.h" 3
_SC_BARRIERS,
# 361 "/usr/include/bits/confname.h" 3
_SC_BASE,
# 363 "/usr/include/bits/confname.h" 3
_SC_C_LANG_SUPPORT,
# 365 "/usr/include/bits/confname.h" 3
_SC_C_LANG_SUPPORT_R,
# 367 "/usr/include/bits/confname.h" 3
_SC_CLOCK_SELECTION,
# 369 "/usr/include/bits/confname.h" 3
_SC_CPUTIME,
# 371 "/usr/include/bits/confname.h" 3
_SC_THREAD_CPUTIME,
# 373 "/usr/include/bits/confname.h" 3
_SC_DEVICE_IO,
# 375 "/usr/include/bits/confname.h" 3
_SC_DEVICE_SPECIFIC,
# 377 "/usr/include/bits/confname.h" 3
_SC_DEVICE_SPECIFIC_R,
# 379 "/usr/include/bits/confname.h" 3
_SC_FD_MGMT,
# 381 "/usr/include/bits/confname.h" 3
_SC_FIFO,
# 383 "/usr/include/bits/confname.h" 3
_SC_PIPE,
# 385 "/usr/include/bits/confname.h" 3
_SC_FILE_ATTRIBUTES,
# 387 "/usr/include/bits/confname.h" 3
_SC_FILE_LOCKING,
# 389 "/usr/include/bits/confname.h" 3
_SC_FILE_SYSTEM,
# 391 "/usr/include/bits/confname.h" 3
_SC_MONOTONIC_CLOCK,
# 393 "/usr/include/bits/confname.h" 3
_SC_MULTI_PROCESS,
# 395 "/usr/include/bits/confname.h" 3
_SC_SINGLE_PROCESS,
# 397 "/usr/include/bits/confname.h" 3
_SC_NETWORKING,
# 399 "/usr/include/bits/confname.h" 3
_SC_READER_WRITER_LOCKS,
# 401 "/usr/include/bits/confname.h" 3
_SC_SPIN_LOCKS,
# 403 "/usr/include/bits/confname.h" 3
_SC_REGEXP,
# 405 "/usr/include/bits/confname.h" 3
_SC_REGEX_VERSION,
# 407 "/usr/include/bits/confname.h" 3
_SC_SHELL,
# 409 "/usr/include/bits/confname.h" 3
_SC_SIGNALS,
# 411 "/usr/include/bits/confname.h" 3
_SC_SPAWN,
# 413 "/usr/include/bits/confname.h" 3
_SC_SPORADIC_SERVER,
# 415 "/usr/include/bits/confname.h" 3
_SC_THREAD_SPORADIC_SERVER,
# 417 "/usr/include/bits/confname.h" 3
_SC_SYSTEM_DATABASE,
# 419 "/usr/include/bits/confname.h" 3
_SC_SYSTEM_DATABASE_R,
# 421 "/usr/include/bits/confname.h" 3
_SC_TIMEOUTS,
# 423 "/usr/include/bits/confname.h" 3
_SC_TYPED_MEMORY_OBJECTS,
# 425 "/usr/include/bits/confname.h" 3
_SC_USER_GROUPS,
# 427 "/usr/include/bits/confname.h" 3
_SC_USER_GROUPS_R,
# 429 "/usr/include/bits/confname.h" 3
_SC_2_PBS,
# 431 "/usr/include/bits/confname.h" 3
_SC_2_PBS_ACCOUNTING,
# 433 "/usr/include/bits/confname.h" 3
_SC_2_PBS_LOCATE,
# 435 "/usr/include/bits/confname.h" 3
_SC_2_PBS_MESSAGE,
# 437 "/usr/include/bits/confname.h" 3
_SC_2_PBS_TRACK,
# 439 "/usr/include/bits/confname.h" 3
_SC_SYMLOOP_MAX,
# 441 "/usr/include/bits/confname.h" 3
_SC_STREAMS,
# 443 "/usr/include/bits/confname.h" 3
_SC_2_PBS_CHECKPOINT,
# 446 "/usr/include/bits/confname.h" 3
_SC_V6_ILP32_OFF32,
# 448 "/usr/include/bits/confname.h" 3
_SC_V6_ILP32_OFFBIG,
# 450 "/usr/include/bits/confname.h" 3
_SC_V6_LP64_OFF64,
# 452 "/usr/include/bits/confname.h" 3
_SC_V6_LPBIG_OFFBIG,
# 455 "/usr/include/bits/confname.h" 3
_SC_HOST_NAME_MAX,
# 457 "/usr/include/bits/confname.h" 3
_SC_TRACE,
# 459 "/usr/include/bits/confname.h" 3
_SC_TRACE_EVENT_FILTER,
# 461 "/usr/include/bits/confname.h" 3
_SC_TRACE_INHERIT,
# 463 "/usr/include/bits/confname.h" 3
_SC_TRACE_LOG,
# 466 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_ICACHE_SIZE,
# 468 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_ICACHE_ASSOC,
# 470 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_ICACHE_LINESIZE,
# 472 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_DCACHE_SIZE,
# 474 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_DCACHE_ASSOC,
# 476 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_DCACHE_LINESIZE,
# 478 "/usr/include/bits/confname.h" 3
_SC_LEVEL2_CACHE_SIZE,
# 480 "/usr/include/bits/confname.h" 3
_SC_LEVEL2_CACHE_ASSOC,
# 482 "/usr/include/bits/confname.h" 3
_SC_LEVEL2_CACHE_LINESIZE,
# 484 "/usr/include/bits/confname.h" 3
_SC_LEVEL3_CACHE_SIZE,
# 486 "/usr/include/bits/confname.h" 3
_SC_LEVEL3_CACHE_ASSOC,
# 488 "/usr/include/bits/confname.h" 3
_SC_LEVEL3_CACHE_LINESIZE,
# 490 "/usr/include/bits/confname.h" 3
_SC_LEVEL4_CACHE_SIZE,
# 492 "/usr/include/bits/confname.h" 3
_SC_LEVEL4_CACHE_ASSOC,
# 494 "/usr/include/bits/confname.h" 3
_SC_LEVEL4_CACHE_LINESIZE,
# 498 "/usr/include/bits/confname.h" 3
_SC_IPV6 = 235,
# 500 "/usr/include/bits/confname.h" 3
_SC_RAW_SOCKETS,
# 503 "/usr/include/bits/confname.h" 3
_SC_V7_ILP32_OFF32,
# 505 "/usr/include/bits/confname.h" 3
_SC_V7_ILP32_OFFBIG,
# 507 "/usr/include/bits/confname.h" 3
_SC_V7_LP64_OFF64,
# 509 "/usr/include/bits/confname.h" 3
_SC_V7_LPBIG_OFFBIG,
# 512 "/usr/include/bits/confname.h" 3
_SC_SS_REPL_MAX,
# 515 "/usr/include/bits/confname.h" 3
_SC_TRACE_EVENT_NAME_MAX,
# 517 "/usr/include/bits/confname.h" 3
_SC_TRACE_NAME_MAX,
# 519 "/usr/include/bits/confname.h" 3
_SC_TRACE_SYS_MAX,
# 521 "/usr/include/bits/confname.h" 3
_SC_TRACE_USER_EVENT_MAX,
# 524 "/usr/include/bits/confname.h" 3
_SC_XOPEN_STREAMS,
# 527 "/usr/include/bits/confname.h" 3
_SC_THREAD_ROBUST_PRIO_INHERIT,
# 529 "/usr/include/bits/confname.h" 3
_SC_THREAD_ROBUST_PRIO_PROTECT};
# 535 "/usr/include/bits/confname.h" 3
enum _ZUt2_ {
# 536 "/usr/include/bits/confname.h" 3
_CS_PATH,
# 539 "/usr/include/bits/confname.h" 3
_CS_V6_WIDTH_RESTRICTED_ENVS,
# 543 "/usr/include/bits/confname.h" 3
_CS_GNU_LIBC_VERSION,
# 545 "/usr/include/bits/confname.h" 3
_CS_GNU_LIBPTHREAD_VERSION,
# 548 "/usr/include/bits/confname.h" 3
_CS_V5_WIDTH_RESTRICTED_ENVS,
# 552 "/usr/include/bits/confname.h" 3
_CS_V7_WIDTH_RESTRICTED_ENVS,
# 556 "/usr/include/bits/confname.h" 3
_CS_LFS_CFLAGS = 1000,
# 558 "/usr/include/bits/confname.h" 3
_CS_LFS_LDFLAGS,
# 560 "/usr/include/bits/confname.h" 3
_CS_LFS_LIBS,
# 562 "/usr/include/bits/confname.h" 3
_CS_LFS_LINTFLAGS,
# 564 "/usr/include/bits/confname.h" 3
_CS_LFS64_CFLAGS,
# 566 "/usr/include/bits/confname.h" 3
_CS_LFS64_LDFLAGS,
# 568 "/usr/include/bits/confname.h" 3
_CS_LFS64_LIBS,
# 570 "/usr/include/bits/confname.h" 3
_CS_LFS64_LINTFLAGS,
# 573 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_CFLAGS = 1100,
# 575 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_LDFLAGS,
# 577 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_LIBS,
# 579 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_LINTFLAGS,
# 581 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_CFLAGS,
# 583 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_LDFLAGS,
# 585 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_LIBS,
# 587 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_LINTFLAGS,
# 589 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_CFLAGS,
# 591 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_LDFLAGS,
# 593 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_LIBS,
# 595 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_LINTFLAGS,
# 597 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_CFLAGS,
# 599 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_LDFLAGS,
# 601 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_LIBS,
# 603 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_LINTFLAGS,
# 606 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_CFLAGS,
# 608 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_LDFLAGS,
# 610 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_LIBS,
# 612 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_LINTFLAGS,
# 614 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS,
# 616 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS,
# 618 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_LIBS,
# 620 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS,
# 622 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_CFLAGS,
# 624 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_LDFLAGS,
# 626 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_LIBS,
# 628 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_LINTFLAGS,
# 630 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS,
# 632 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS,
# 634 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_LIBS,
# 636 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS,
# 639 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_CFLAGS,
# 641 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_LDFLAGS,
# 643 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_LIBS,
# 645 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_LINTFLAGS,
# 647 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS,
# 649 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS,
# 651 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_LIBS,
# 653 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS,
# 655 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_CFLAGS,
# 657 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_LDFLAGS,
# 659 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_LIBS,
# 661 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_LINTFLAGS,
# 663 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS,
# 665 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS,
# 667 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_LIBS,
# 669 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS,
# 672 "/usr/include/bits/confname.h" 3
_CS_V6_ENV,
# 674 "/usr/include/bits/confname.h" 3
_CS_V7_ENV};
# 24 "/usr/include/bits/socket_type.h" 3
enum __socket_type {
# 26 "/usr/include/bits/socket_type.h" 3
SOCK_STREAM = 1,
# 29 "/usr/include/bits/socket_type.h" 3
SOCK_DGRAM,
# 32 "/usr/include/bits/socket_type.h" 3
SOCK_RAW,
# 34 "/usr/include/bits/socket_type.h" 3
SOCK_RDM,
# 36 "/usr/include/bits/socket_type.h" 3
SOCK_SEQPACKET,
# 39 "/usr/include/bits/socket_type.h" 3
SOCK_DCCP,
# 41 "/usr/include/bits/socket_type.h" 3
SOCK_PACKET = 10,
# 49 "/usr/include/bits/socket_type.h" 3
SOCK_CLOEXEC = 524288,
# 52 "/usr/include/bits/socket_type.h" 3
SOCK_NONBLOCK = 2048};
# 171 "/usr/include/bits/socket.h" 3
enum _ZUt3_ {
# 172 "/usr/include/bits/socket.h" 3
MSG_OOB = 1,
# 174 "/usr/include/bits/socket.h" 3
MSG_PEEK,
# 176 "/usr/include/bits/socket.h" 3
MSG_DONTROUTE = 4,
# 180 "/usr/include/bits/socket.h" 3
MSG_TRYHARD = 4,
# 183 "/usr/include/bits/socket.h" 3
MSG_CTRUNC = 8,
# 185 "/usr/include/bits/socket.h" 3
MSG_PROXY = 16,
# 187 "/usr/include/bits/socket.h" 3
MSG_TRUNC = 32,
# 189 "/usr/include/bits/socket.h" 3
MSG_DONTWAIT = 64,
# 191 "/usr/include/bits/socket.h" 3
MSG_EOR = 128,
# 193 "/usr/include/bits/socket.h" 3
MSG_WAITALL = 256,
# 195 "/usr/include/bits/socket.h" 3
MSG_FIN = 512,
# 197 "/usr/include/bits/socket.h" 3
MSG_SYN = 1024,
# 199 "/usr/include/bits/socket.h" 3
MSG_CONFIRM = 2048,
# 201 "/usr/include/bits/socket.h" 3
MSG_RST = 4096,
# 203 "/usr/include/bits/socket.h" 3
MSG_ERRQUEUE = 8192,
# 205 "/usr/include/bits/socket.h" 3
MSG_NOSIGNAL = 16384,
# 207 "/usr/include/bits/socket.h" 3
MSG_MORE = 32768,
# 209 "/usr/include/bits/socket.h" 3
MSG_WAITFORONE = 65536,
# 212 "/usr/include/bits/socket.h" 3
MSG_CMSG_CLOEXEC = 1073741824};
# 297 "/usr/include/bits/socket.h" 3
enum _ZUt4_ {
# 298 "/usr/include/bits/socket.h" 3
SCM_RIGHTS = 1,
# 301 "/usr/include/bits/socket.h" 3
SCM_CREDENTIALS};
# 54 "/usr/include/sys/socket.h" 3
enum _ZUt5_ {
# 55 "/usr/include/sys/socket.h" 3
SHUT_RD,
# 57 "/usr/include/sys/socket.h" 3
SHUT_WR,
# 59 "/usr/include/sys/socket.h" 3
SHUT_RDWR};
# 34 "../ringbuffer/RingBuffer.h"
struct RingBuffer_t;
# 33 "/usr/include/pthread.h" 3
enum _ZUt6_ {
# 34 "/usr/include/pthread.h" 3
PTHREAD_CREATE_JOINABLE,
# 36 "/usr/include/pthread.h" 3
PTHREAD_CREATE_DETACHED};
# 43 "/usr/include/pthread.h" 3
enum _ZUt7_ {
# 44 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_TIMED_NP,
# 45 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_RECURSIVE_NP,
# 46 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ERRORCHECK_NP,
# 47 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ADAPTIVE_NP,
# 50 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_NORMAL = 0,
# 51 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_RECURSIVE,
# 52 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ERRORCHECK,
# 53 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_DEFAULT = 0,
# 57 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_FAST_NP = 0};
# 65 "/usr/include/pthread.h" 3
enum _ZUt8_ {
# 66 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_STALLED,
# 67 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_STALLED_NP = 0,
# 68 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ROBUST,
# 69 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ROBUST_NP = 1};
# 77 "/usr/include/pthread.h" 3
enum _ZUt9_ {
# 78 "/usr/include/pthread.h" 3
PTHREAD_PRIO_NONE,
# 79 "/usr/include/pthread.h" 3
PTHREAD_PRIO_INHERIT,
# 80 "/usr/include/pthread.h" 3
PTHREAD_PRIO_PROTECT};
# 114 "/usr/include/pthread.h" 3
enum _ZUt10_ {
# 115 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_PREFER_READER_NP,
# 116 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_PREFER_WRITER_NP,
# 117 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP,
# 118 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_DEFAULT_NP = 0};
# 155 "/usr/include/pthread.h" 3
enum _ZUt11_ {
# 156 "/usr/include/pthread.h" 3
PTHREAD_INHERIT_SCHED,
# 158 "/usr/include/pthread.h" 3
PTHREAD_EXPLICIT_SCHED};
# 165 "/usr/include/pthread.h" 3
enum _ZUt12_ {
# 166 "/usr/include/pthread.h" 3
PTHREAD_SCOPE_SYSTEM,
# 168 "/usr/include/pthread.h" 3
PTHREAD_SCOPE_PROCESS};
# 175 "/usr/include/pthread.h" 3
enum _ZUt13_ {
# 176 "/usr/include/pthread.h" 3
PTHREAD_PROCESS_PRIVATE,
# 178 "/usr/include/pthread.h" 3
PTHREAD_PROCESS_SHARED};
# 199 "/usr/include/pthread.h" 3
enum _ZUt14_ {
# 200 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_ENABLE,
# 202 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_DISABLE};
# 206 "/usr/include/pthread.h" 3
enum _ZUt15_ {
# 207 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_DEFERRED,
# 209 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_ASYNCHRONOUS};
# 53 "../network/vdif_receiver_rb.h"
struct vdif_rx_tt;
# 92 "/usr/include/sys/time.h" 3
enum __itimer_which {
# 95 "/usr/include/sys/time.h" 3
ITIMER_REAL,
# 98 "/usr/include/sys/time.h" 3
ITIMER_VIRTUAL,
# 102 "/usr/include/sys/time.h" 3
ITIMER_PROF};
# 28 "../helpers/kvn_dsm.h"
struct kvn_dsm_dataheader_tt;
# 40 "../helpers/kvn_dsm.h"
struct kvn_dsm_writer_tt;
# 78 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
enum cufftResult_t {
# 79 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_SUCCESS,
# 80 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_PLAN,
# 81 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_ALLOC_FAILED,
# 82 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_TYPE,
# 83 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_VALUE,
# 84 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INTERNAL_ERROR,
# 85 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_EXEC_FAILED,
# 86 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_SETUP_FAILED,
# 87 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_SIZE,
# 88 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_UNALIGNED_DATA,
# 89 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INCOMPLETE_PARAMETER_LIST,
# 90 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_DEVICE,
# 91 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_PARSE_ERROR,
# 92 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_NO_WORKSPACE,
# 93 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_NOT_IMPLEMENTED,
# 94 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_LICENSE_ERROR,
# 95 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_NOT_SUPPORTED};
# 121 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
enum cufftType_t {
# 122 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_R2C = 42,
# 123 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_C2R = 44,
# 124 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_C2C = 41,
# 125 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_D2Z = 106,
# 126 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_Z2D = 108,
# 127 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_Z2Z = 105};
# 131 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
enum cufftCompatibility_t {
# 132 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_COMPATIBILITY_FFTW_PADDING = 1};
# 60 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
enum cudaXtCopyType_t {
# 61 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_XT_COPY_HOST_TO_DEVICE,
# 62 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_XT_COPY_DEVICE_TO_HOST,
# 63 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_XT_COPY_DEVICE_TO_DEVICE};
# 67 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
enum libFormat_t {
# 68 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_FORMAT_CUFFT,
# 69 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_FORMAT_UNDEFINED};
# 79 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtSubFormat_t {
# 80 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_INPUT,
# 81 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_OUTPUT,
# 82 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_INPLACE,
# 83 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_INPLACE_SHUFFLED,
# 84 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_1D_INPUT_SHUFFLED,
# 85 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_FORMAT_UNDEFINED};
# 91 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtCopyType_t {
# 92 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_HOST_TO_DEVICE,
# 93 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_DEVICE_TO_HOST,
# 94 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_DEVICE_TO_DEVICE,
# 95 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_UNDEFINED};
# 101 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtQueryType_t {
# 102 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_QUERY_1D_FACTORS,
# 103 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_QUERY_UNDEFINED};
# 173 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtCallbackType_t {
# 174 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_COMPLEX,
# 175 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_COMPLEX_DOUBLE,
# 176 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_REAL,
# 177 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_REAL_DOUBLE,
# 178 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_COMPLEX,
# 179 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_COMPLEX_DOUBLE,
# 180 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_REAL,
# 181 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_REAL_DOUBLE,
# 182 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_UNDEFINED};
# 17 "../kernels/cuda/arithmetic_winfunc_kernels.h"
struct cu_window_cb_params_tt;
# 55 "kfftspec.h"
struct fftspec_config_tt;
# 37 "../helpers/kfftspec_outfile.h"
struct kfftspec_outfile_spectrumheader_v1_tt;
# 48 "/usr/include/ctype.h" 3
enum _ZUt16_ {
# 49 "/usr/include/ctype.h" 3
_ISupper = 256,
# 50 "/usr/include/ctype.h" 3
_ISlower = 512,
# 51 "/usr/include/ctype.h" 3
_ISalpha = 1024,
# 52 "/usr/include/ctype.h" 3
_ISdigit = 2048,
# 53 "/usr/include/ctype.h" 3
_ISxdigit = 4096,
# 54 "/usr/include/ctype.h" 3
_ISspace = 8192,
# 55 "/usr/include/ctype.h" 3
_ISprint = 16384,
# 56 "/usr/include/ctype.h" 3
_ISgraph = 32768,
# 57 "/usr/include/ctype.h" 3
_ISblank = 1,
# 58 "/usr/include/ctype.h" 3
_IScntrl,
# 59 "/usr/include/ctype.h" 3
_ISpunct = 4,
# 60 "/usr/include/ctype.h" 3
_ISalnum = 8};
# 32 "../helpers/kvn_dsm.c"
struct _ZN22kvn_dsm_out_streams_tt6bit_ttE;
# 30 "../helpers/kvn_dsm.c"
union kvn_dsm_out_streams_tt;
# 152 "/usr/include/bits/siginfo.h" 3
enum _ZUt17_ {
# 153 "/usr/include/bits/siginfo.h" 3
SI_ASYNCNL = (-60),
# 155 "/usr/include/bits/siginfo.h" 3
SI_TKILL = (-6),
# 157 "/usr/include/bits/siginfo.h" 3
SI_SIGIO,
# 159 "/usr/include/bits/siginfo.h" 3
SI_ASYNCIO,
# 161 "/usr/include/bits/siginfo.h" 3
SI_MESGQ,
# 163 "/usr/include/bits/siginfo.h" 3
SI_TIMER,
# 165 "/usr/include/bits/siginfo.h" 3
SI_QUEUE,
# 167 "/usr/include/bits/siginfo.h" 3
SI_USER,
# 169 "/usr/include/bits/siginfo.h" 3
SI_KERNEL = 128};
# 176 "/usr/include/bits/siginfo.h" 3
enum _ZUt18_ {
# 177 "/usr/include/bits/siginfo.h" 3
ILL_ILLOPC = 1,
# 179 "/usr/include/bits/siginfo.h" 3
ILL_ILLOPN,
# 181 "/usr/include/bits/siginfo.h" 3
ILL_ILLADR,
# 183 "/usr/include/bits/siginfo.h" 3
ILL_ILLTRP,
# 185 "/usr/include/bits/siginfo.h" 3
ILL_PRVOPC,
# 187 "/usr/include/bits/siginfo.h" 3
ILL_PRVREG,
# 189 "/usr/include/bits/siginfo.h" 3
ILL_COPROC,
# 191 "/usr/include/bits/siginfo.h" 3
ILL_BADSTK};
# 197 "/usr/include/bits/siginfo.h" 3
enum _ZUt19_ {
# 198 "/usr/include/bits/siginfo.h" 3
FPE_INTDIV = 1,
# 200 "/usr/include/bits/siginfo.h" 3
FPE_INTOVF,
# 202 "/usr/include/bits/siginfo.h" 3
FPE_FLTDIV,
# 204 "/usr/include/bits/siginfo.h" 3
FPE_FLTOVF,
# 206 "/usr/include/bits/siginfo.h" 3
FPE_FLTUND,
# 208 "/usr/include/bits/siginfo.h" 3
FPE_FLTRES,
# 210 "/usr/include/bits/siginfo.h" 3
FPE_FLTINV,
# 212 "/usr/include/bits/siginfo.h" 3
FPE_FLTSUB};
# 218 "/usr/include/bits/siginfo.h" 3
enum _ZUt20_ {
# 219 "/usr/include/bits/siginfo.h" 3
SEGV_MAPERR = 1,
# 221 "/usr/include/bits/siginfo.h" 3
SEGV_ACCERR};
# 227 "/usr/include/bits/siginfo.h" 3
enum _ZUt21_ {
# 228 "/usr/include/bits/siginfo.h" 3
BUS_ADRALN = 1,
# 230 "/usr/include/bits/siginfo.h" 3
BUS_ADRERR,
# 232 "/usr/include/bits/siginfo.h" 3
BUS_OBJERR};
# 238 "/usr/include/bits/siginfo.h" 3
enum _ZUt22_ {
# 239 "/usr/include/bits/siginfo.h" 3
TRAP_BRKPT = 1,
# 241 "/usr/include/bits/siginfo.h" 3
TRAP_TRACE};
# 247 "/usr/include/bits/siginfo.h" 3
enum _ZUt23_ {
# 248 "/usr/include/bits/siginfo.h" 3
CLD_EXITED = 1,
# 250 "/usr/include/bits/siginfo.h" 3
CLD_KILLED,
# 252 "/usr/include/bits/siginfo.h" 3
CLD_DUMPED,
# 254 "/usr/include/bits/siginfo.h" 3
CLD_TRAPPED,
# 256 "/usr/include/bits/siginfo.h" 3
CLD_STOPPED,
# 258 "/usr/include/bits/siginfo.h" 3
CLD_CONTINUED};
# 264 "/usr/include/bits/siginfo.h" 3
enum _ZUt24_ {
# 265 "/usr/include/bits/siginfo.h" 3
POLL_IN = 1,
# 267 "/usr/include/bits/siginfo.h" 3
POLL_OUT,
# 269 "/usr/include/bits/siginfo.h" 3
POLL_MSG,
# 271 "/usr/include/bits/siginfo.h" 3
POLL_ERR,
# 273 "/usr/include/bits/siginfo.h" 3
POLL_PRI,
# 275 "/usr/include/bits/siginfo.h" 3
POLL_HUP};
# 329 "/usr/include/bits/siginfo.h" 3
enum _ZUt25_ {
# 330 "/usr/include/bits/siginfo.h" 3
SIGEV_SIGNAL,
# 332 "/usr/include/bits/siginfo.h" 3
SIGEV_NONE,
# 334 "/usr/include/bits/siginfo.h" 3
SIGEV_THREAD,
# 337 "/usr/include/bits/siginfo.h" 3
SIGEV_THREAD_ID = 4};
# 34 "/usr/include/bits/sigstack.h" 3
enum _ZUt26_ {
# 35 "/usr/include/bits/sigstack.h" 3
SS_ONSTACK = 1,
# 37 "/usr/include/bits/sigstack.h" 3
SS_DISABLE};
# 42 "/usr/include/sys/ucontext.h" 3
enum _ZUt27_ {
# 43 "/usr/include/sys/ucontext.h" 3
REG_R8,
# 45 "/usr/include/sys/ucontext.h" 3
REG_R9,
# 47 "/usr/include/sys/ucontext.h" 3
REG_R10,
# 49 "/usr/include/sys/ucontext.h" 3
REG_R11,
# 51 "/usr/include/sys/ucontext.h" 3
REG_R12,
# 53 "/usr/include/sys/ucontext.h" 3
REG_R13,
# 55 "/usr/include/sys/ucontext.h" 3
REG_R14,
# 57 "/usr/include/sys/ucontext.h" 3
REG_R15,
# 59 "/usr/include/sys/ucontext.h" 3
REG_RDI,
# 61 "/usr/include/sys/ucontext.h" 3
REG_RSI,
# 63 "/usr/include/sys/ucontext.h" 3
REG_RBP,
# 65 "/usr/include/sys/ucontext.h" 3
REG_RBX,
# 67 "/usr/include/sys/ucontext.h" 3
REG_RDX,
# 69 "/usr/include/sys/ucontext.h" 3
REG_RAX,
# 71 "/usr/include/sys/ucontext.h" 3
REG_RCX,
# 73 "/usr/include/sys/ucontext.h" 3
REG_RSP,
# 75 "/usr/include/sys/ucontext.h" 3
REG_RIP,
# 77 "/usr/include/sys/ucontext.h" 3
REG_EFL,
# 79 "/usr/include/sys/ucontext.h" 3
REG_CSGSFS,
# 81 "/usr/include/sys/ucontext.h" 3
REG_ERR,
# 83 "/usr/include/sys/ucontext.h" 3
REG_TRAPNO,
# 85 "/usr/include/sys/ucontext.h" 3
REG_OLDMASK,
# 87 "/usr/include/sys/ucontext.h" 3
REG_CR2};
# 271 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUipcMem_flags_enum {
# 272 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_IPC_MEM_LAZY_ENABLE_PEER_ACCESS = 1};
# 280 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUmemAttach_flags_enum {
# 281 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ATTACH_GLOBAL = 1,
# 282 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ATTACH_HOST,
# 283 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ATTACH_SINGLE = 4};
# 289 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUctx_flags_enum {
# 290 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_SCHED_AUTO,
# 291 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_SCHED_SPIN,
# 292 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_SCHED_YIELD,
# 293 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_SCHED_BLOCKING_SYNC = 4,
# 294 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_BLOCKING_SYNC = 4,
# 297 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_SCHED_MASK = 7,
# 298 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_MAP_HOST,
# 299 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_LMEM_RESIZE_TO_MAX = 16,
# 300 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CTX_FLAGS_MASK = 31};
# 306 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUstream_flags_enum {
# 307 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_DEFAULT,
# 308 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_NON_BLOCKING};
# 334 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUevent_flags_enum {
# 335 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_EVENT_DEFAULT,
# 336 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_EVENT_BLOCKING_SYNC,
# 337 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_EVENT_DISABLE_TIMING,
# 338 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_EVENT_INTERPROCESS = 4};
# 345 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUstreamWaitValue_flags_enum {
# 346 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_WAIT_VALUE_GEQ,
# 348 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_WAIT_VALUE_EQ,
# 349 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_WAIT_VALUE_AND,
# 350 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_WAIT_VALUE_FLUSH = 1073741824};
# 362 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUstreamWriteValue_flags_enum {
# 363 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_WRITE_VALUE_DEFAULT,
# 364 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_WRITE_VALUE_NO_MEMORY_BARRIER};
# 375 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUstreamBatchMemOpType_enum {
# 376 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_MEM_OP_WAIT_VALUE_32 = 1,
# 377 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_MEM_OP_WRITE_VALUE_32,
# 378 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_STREAM_MEM_OP_FLUSH_REMOTE_WRITES};
# 418 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUoccupancy_flags_enum {
# 419 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_OCCUPANCY_DEFAULT,
# 420 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_OCCUPANCY_DISABLE_CACHING_OVERRIDE};
# 426 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUarray_format_enum {
# 427 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_UNSIGNED_INT8 = 1,
# 428 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_UNSIGNED_INT16,
# 429 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_UNSIGNED_INT32,
# 430 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_SIGNED_INT8 = 8,
# 431 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_SIGNED_INT16,
# 432 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_SIGNED_INT32,
# 433 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_HALF = 16,
# 434 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_AD_FORMAT_FLOAT = 32};
# 440 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUaddress_mode_enum {
# 441 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TR_ADDRESS_MODE_WRAP,
# 442 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TR_ADDRESS_MODE_CLAMP,
# 443 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TR_ADDRESS_MODE_MIRROR,
# 444 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TR_ADDRESS_MODE_BORDER};
# 450 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUfilter_mode_enum {
# 451 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TR_FILTER_MODE_POINT,
# 452 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TR_FILTER_MODE_LINEAR};
# 458 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUdevice_attribute_enum {
# 459 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK = 1,
# 460 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_X,
# 461 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Y,
# 462 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_BLOCK_DIM_Z,
# 463 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_X,
# 464 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Y,
# 465 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_GRID_DIM_Z,
# 466 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK,
# 467 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_SHARED_MEMORY_PER_BLOCK = 8,
# 468 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_TOTAL_CONSTANT_MEMORY,
# 469 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_WARP_SIZE,
# 470 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_PITCH,
# 471 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_BLOCK,
# 472 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_REGISTERS_PER_BLOCK = 12,
# 473 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_CLOCK_RATE,
# 474 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_TEXTURE_ALIGNMENT,
# 475 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_GPU_OVERLAP,
# 476 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT,
# 477 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_KERNEL_EXEC_TIMEOUT,
# 478 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_INTEGRATED,
# 479 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_CAN_MAP_HOST_MEMORY,
# 480 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_COMPUTE_MODE,
# 481 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_WIDTH,
# 482 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_WIDTH,
# 483 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_HEIGHT,
# 484 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH,
# 485 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT,
# 486 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH,
# 487 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_WIDTH,
# 488 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_HEIGHT,
# 489 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LAYERED_LAYERS,
# 490 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_WIDTH = 27,
# 491 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_HEIGHT,
# 492 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_ARRAY_NUMSLICES,
# 493 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_SURFACE_ALIGNMENT,
# 494 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_CONCURRENT_KERNELS,
# 495 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_ECC_ENABLED,
# 496 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_PCI_BUS_ID,
# 497 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_PCI_DEVICE_ID,
# 498 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_TCC_DRIVER,
# 499 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MEMORY_CLOCK_RATE,
# 500 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_GLOBAL_MEMORY_BUS_WIDTH,
# 501 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_L2_CACHE_SIZE,
# 502 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR,
# 503 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_ASYNC_ENGINE_COUNT,
# 504 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_UNIFIED_ADDRESSING,
# 505 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_WIDTH,
# 506 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LAYERED_LAYERS,
# 507 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_CAN_TEX2D_GATHER,
# 508 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_GATHER_WIDTH,
# 509 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_GATHER_HEIGHT,
# 510 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_WIDTH_ALTERNATE,
# 511 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_HEIGHT_ALTERNATE,
# 512 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE3D_DEPTH_ALTERNATE,
# 513 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_PCI_DOMAIN_ID,
# 514 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_TEXTURE_PITCH_ALIGNMENT,
# 515 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_WIDTH,
# 516 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_LAYERED_WIDTH,
# 517 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURECUBEMAP_LAYERED_LAYERS,
# 518 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_WIDTH,
# 519 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_WIDTH,
# 520 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_HEIGHT,
# 521 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_WIDTH,
# 522 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_HEIGHT,
# 523 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE3D_DEPTH,
# 524 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_LAYERED_WIDTH,
# 525 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE1D_LAYERED_LAYERS,
# 526 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_WIDTH,
# 527 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_HEIGHT,
# 528 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACE2D_LAYERED_LAYERS,
# 529 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_WIDTH,
# 530 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_LAYERED_WIDTH,
# 531 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_SURFACECUBEMAP_LAYERED_LAYERS,
# 532 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_LINEAR_WIDTH,
# 533 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_WIDTH,
# 534 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_HEIGHT,
# 535 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_LINEAR_PITCH,
# 536 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_MIPMAPPED_WIDTH,
# 537 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE2D_MIPMAPPED_HEIGHT,
# 538 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MAJOR,
# 539 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_COMPUTE_CAPABILITY_MINOR,
# 540 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAXIMUM_TEXTURE1D_MIPMAPPED_WIDTH,
# 541 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_STREAM_PRIORITIES_SUPPORTED,
# 542 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_GLOBAL_L1_CACHE_SUPPORTED,
# 543 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_LOCAL_L1_CACHE_SUPPORTED,
# 544 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_MULTIPROCESSOR,
# 545 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX_REGISTERS_PER_MULTIPROCESSOR,
# 546 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MANAGED_MEMORY,
# 547 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MULTI_GPU_BOARD,
# 548 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MULTI_GPU_BOARD_GROUP_ID,
# 549 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_HOST_NATIVE_ATOMIC_SUPPORTED,
# 550 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_SINGLE_TO_DOUBLE_PRECISION_PERF_RATIO,
# 551 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_PAGEABLE_MEMORY_ACCESS,
# 552 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_CONCURRENT_MANAGED_ACCESS,
# 553 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_COMPUTE_PREEMPTION_SUPPORTED,
# 554 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_CAN_USE_HOST_POINTER_FOR_REGISTERED_MEM,
# 555 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_ATTRIBUTE_MAX};
# 577 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUpointer_attribute_enum {
# 578 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_CONTEXT = 1,
# 579 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_MEMORY_TYPE,
# 580 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_DEVICE_POINTER,
# 581 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_HOST_POINTER,
# 582 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_P2P_TOKENS,
# 583 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_SYNC_MEMOPS,
# 584 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_BUFFER_ID,
# 585 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_POINTER_ATTRIBUTE_IS_MANAGED};
# 591 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUfunction_attribute_enum {
# 597 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK,
# 604 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_SHARED_SIZE_BYTES,
# 610 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_CONST_SIZE_BYTES,
# 615 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_LOCAL_SIZE_BYTES,
# 620 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_NUM_REGS,
# 629 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_PTX_VERSION,
# 638 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_BINARY_VERSION,
# 644 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_CACHE_MODE_CA,
# 646 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_ATTRIBUTE_MAX};
# 652 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUfunc_cache_enum {
# 653 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_CACHE_PREFER_NONE,
# 654 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_CACHE_PREFER_SHARED,
# 655 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_CACHE_PREFER_L1,
# 656 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_FUNC_CACHE_PREFER_EQUAL};
# 662 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUsharedconfig_enum {
# 663 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_SHARED_MEM_CONFIG_DEFAULT_BANK_SIZE,
# 664 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_SHARED_MEM_CONFIG_FOUR_BYTE_BANK_SIZE,
# 665 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_SHARED_MEM_CONFIG_EIGHT_BYTE_BANK_SIZE};
# 671 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUmemorytype_enum {
# 672 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEMORYTYPE_HOST = 1,
# 673 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEMORYTYPE_DEVICE,
# 674 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEMORYTYPE_ARRAY,
# 675 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEMORYTYPE_UNIFIED};
# 681 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUcomputemode_enum {
# 682 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_COMPUTEMODE_DEFAULT,
# 683 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_COMPUTEMODE_PROHIBITED = 2,
# 684 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_COMPUTEMODE_EXCLUSIVE_PROCESS};
# 690 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUmem_advise_enum {
# 691 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ADVISE_SET_READ_MOSTLY = 1,
# 692 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ADVISE_UNSET_READ_MOSTLY,
# 693 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ADVISE_SET_PREFERRED_LOCATION,
# 694 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ADVISE_UNSET_PREFERRED_LOCATION,
# 695 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ADVISE_SET_ACCESSED_BY,
# 696 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_ADVISE_UNSET_ACCESSED_BY};
# 699 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUmem_range_attribute_enum {
# 700 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_RANGE_ATTRIBUTE_READ_MOSTLY = 1,
# 701 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_RANGE_ATTRIBUTE_PREFERRED_LOCATION,
# 702 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_RANGE_ATTRIBUTE_ACCESSED_BY,
# 703 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_MEM_RANGE_ATTRIBUTE_LAST_PREFETCH_LOCATION};
# 709 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUjit_option_enum {
# 716 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_MAX_REGISTERS,
# 731 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_THREADS_PER_BLOCK,
# 739 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_WALL_TIME,
# 748 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INFO_LOG_BUFFER,
# 757 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INFO_LOG_BUFFER_SIZE_BYTES,
# 766 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_ERROR_LOG_BUFFER,
# 775 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_ERROR_LOG_BUFFER_SIZE_BYTES,
# 783 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_OPTIMIZATION_LEVEL,
# 791 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_TARGET_FROM_CUCONTEXT,
# 799 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_TARGET,
# 808 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_FALLBACK_STRATEGY,
# 816 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_GENERATE_DEBUG_INFO,
# 823 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_LOG_VERBOSE,
# 830 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_GENERATE_LINE_INFO,
# 838 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_CACHE_MODE,
# 843 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_NEW_SM3X_OPT,
# 844 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_FAST_COMPILE,
# 846 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_NUM_OPTIONS};
# 853 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUjit_target_enum {
# 855 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_10 = 10,
# 856 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_11,
# 857 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_12,
# 858 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_13,
# 859 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_20 = 20,
# 860 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_21,
# 861 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_30 = 30,
# 862 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_32 = 32,
# 863 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_35 = 35,
# 864 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_37 = 37,
# 865 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_50 = 50,
# 866 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_52 = 52,
# 867 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_53,
# 868 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_60 = 60,
# 869 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_61,
# 870 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_TARGET_COMPUTE_62};
# 876 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUjit_fallback_enum {
# 878 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_PREFER_PTX,
# 880 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_PREFER_BINARY};
# 887 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUjit_cacheMode_enum {
# 889 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_CACHE_OPTION_NONE,
# 890 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_CACHE_OPTION_CG,
# 891 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_CACHE_OPTION_CA};
# 897 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUjitInputType_enum {
# 903 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INPUT_CUBIN,
# 909 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INPUT_PTX,
# 915 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INPUT_FATBINARY,
# 921 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INPUT_OBJECT,
# 927 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_INPUT_LIBRARY,
# 929 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_JIT_NUM_INPUT_TYPES};
# 939 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUgraphicsRegisterFlags_enum {
# 940 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_REGISTER_FLAGS_NONE,
# 941 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_REGISTER_FLAGS_READ_ONLY,
# 942 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_REGISTER_FLAGS_WRITE_DISCARD,
# 943 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_REGISTER_FLAGS_SURFACE_LDST = 4,
# 944 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_REGISTER_FLAGS_TEXTURE_GATHER = 8};
# 950 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUgraphicsMapResourceFlags_enum {
# 951 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_MAP_RESOURCE_FLAGS_NONE,
# 952 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_MAP_RESOURCE_FLAGS_READ_ONLY,
# 953 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_GRAPHICS_MAP_RESOURCE_FLAGS_WRITE_DISCARD};
# 959 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUarray_cubemap_face_enum {
# 960 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CUBEMAP_FACE_POSITIVE_X,
# 961 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CUBEMAP_FACE_NEGATIVE_X,
# 962 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CUBEMAP_FACE_POSITIVE_Y,
# 963 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CUBEMAP_FACE_NEGATIVE_Y,
# 964 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CUBEMAP_FACE_POSITIVE_Z,
# 965 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_CUBEMAP_FACE_NEGATIVE_Z};
# 971 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUlimit_enum {
# 972 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_LIMIT_STACK_SIZE,
# 973 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_LIMIT_PRINTF_FIFO_SIZE,
# 974 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_LIMIT_MALLOC_HEAP_SIZE,
# 975 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_LIMIT_DEV_RUNTIME_SYNC_DEPTH,
# 976 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_LIMIT_DEV_RUNTIME_PENDING_LAUNCH_COUNT,
# 977 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_LIMIT_MAX};
# 983 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUresourcetype_enum {
# 984 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RESOURCE_TYPE_ARRAY,
# 985 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RESOURCE_TYPE_MIPMAPPED_ARRAY,
# 986 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RESOURCE_TYPE_LINEAR,
# 987 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RESOURCE_TYPE_PITCH2D};
# 993 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum cudaError_enum {
# 999 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_SUCCESS,
# 1005 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_VALUE,
# 1011 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_OUT_OF_MEMORY,
# 1017 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_INITIALIZED,
# 1022 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_DEINITIALIZED,
# 1029 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PROFILER_DISABLED,
# 1037 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PROFILER_NOT_INITIALIZED,
# 1044 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PROFILER_ALREADY_STARTED,
# 1051 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PROFILER_ALREADY_STOPPED,
# 1057 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NO_DEVICE = 100,
# 1063 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_DEVICE,
# 1070 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_IMAGE = 200,
# 1080 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_CONTEXT,
# 1089 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_CONTEXT_ALREADY_CURRENT,
# 1094 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_MAP_FAILED = 205,
# 1099 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_UNMAP_FAILED,
# 1105 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ARRAY_IS_MAPPED,
# 1110 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ALREADY_MAPPED,
# 1118 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NO_BINARY_FOR_GPU,
# 1123 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ALREADY_ACQUIRED,
# 1128 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_MAPPED,
# 1134 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_MAPPED_AS_ARRAY,
# 1140 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_MAPPED_AS_POINTER,
# 1146 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ECC_UNCORRECTABLE,
# 1152 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_UNSUPPORTED_LIMIT,
# 1159 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_CONTEXT_ALREADY_IN_USE,
# 1165 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PEER_ACCESS_UNSUPPORTED,
# 1170 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_PTX,
# 1175 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_GRAPHICS_CONTEXT,
# 1181 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NVLINK_UNCORRECTABLE,
# 1186 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_SOURCE = 300,
# 1191 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_FILE_NOT_FOUND,
# 1196 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_SHARED_OBJECT_SYMBOL_NOT_FOUND,
# 1201 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_SHARED_OBJECT_INIT_FAILED,
# 1206 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_OPERATING_SYSTEM,
# 1212 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_HANDLE = 400,
# 1218 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_FOUND = 500,
# 1226 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_READY = 600,
# 1235 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ILLEGAL_ADDRESS = 700,
# 1246 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_LAUNCH_OUT_OF_RESOURCES,
# 1256 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_LAUNCH_TIMEOUT,
# 1262 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_LAUNCH_INCOMPATIBLE_TEXTURING,
# 1269 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PEER_ACCESS_ALREADY_ENABLED,
# 1276 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PEER_ACCESS_NOT_ENABLED,
# 1282 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_PRIMARY_CONTEXT_ACTIVE = 708,
# 1289 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_CONTEXT_IS_DESTROYED,
# 1297 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ASSERT,
# 1304 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_TOO_MANY_PEERS,
# 1310 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_HOST_MEMORY_ALREADY_REGISTERED,
# 1316 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_HOST_MEMORY_NOT_REGISTERED,
# 1325 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_HARDWARE_STACK_ERROR,
# 1333 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_ILLEGAL_INSTRUCTION,
# 1342 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_MISALIGNED_ADDRESS,
# 1353 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_ADDRESS_SPACE,
# 1361 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_INVALID_PC,
# 1371 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_LAUNCH_FAILED,
# 1377 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_PERMITTED = 800,
# 1383 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_NOT_SUPPORTED,
# 1388 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CUDA_ERROR_UNKNOWN = 999};
# 1394 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUdevice_P2PAttribute_enum {
# 1395 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_P2P_ATTRIBUTE_PERFORMANCE_RANK = 1,
# 1396 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_P2P_ATTRIBUTE_ACCESS_SUPPORTED,
# 1397 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_DEVICE_P2P_ATTRIBUTE_NATIVE_ATOMIC_SUPPORTED};
# 1649 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
enum CUresourceViewFormat_enum {
# 1651 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_NONE,
# 1652 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_1X8,
# 1653 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_2X8,
# 1654 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_4X8,
# 1655 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_1X8,
# 1656 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_2X8,
# 1657 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_4X8,
# 1658 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_1X16,
# 1659 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_2X16,
# 1660 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_4X16,
# 1661 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_1X16,
# 1662 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_2X16,
# 1663 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_4X16,
# 1664 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_1X32,
# 1665 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_2X32,
# 1666 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UINT_4X32,
# 1667 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_1X32,
# 1668 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_2X32,
# 1669 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SINT_4X32,
# 1670 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_FLOAT_1X16,
# 1671 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_FLOAT_2X16,
# 1672 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_FLOAT_4X16,
# 1673 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_FLOAT_1X32,
# 1674 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_FLOAT_2X32,
# 1675 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_FLOAT_4X32,
# 1676 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC1,
# 1677 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC2,
# 1678 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC3,
# 1679 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC4,
# 1680 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SIGNED_BC4,
# 1681 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC5,
# 1682 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SIGNED_BC5,
# 1683 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC6H,
# 1684 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_SIGNED_BC6H,
# 1685 "/usr/local/cuda-8.0/bin/..//include/cuda.h"
CU_RES_VIEW_FORMAT_UNSIGNED_BC7};
# 128 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_voidIvEUt_E {
# 128 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_voidIvE7__valueE = 1};
# 148 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIbEUt_E {
# 148 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIbE7__valueE = 1};
# 155 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIcEUt_E {
# 155 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIcE7__valueE = 1};
# 162 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIaEUt_E {
# 162 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIaE7__valueE = 1};
# 169 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIhEUt_E {
# 169 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIhE7__valueE = 1};
# 177 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIwEUt_E {
# 177 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIwE7__valueE = 1};
# 201 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIsEUt_E {
# 201 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIsE7__valueE = 1};
# 208 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerItEUt_E {
# 208 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerItE7__valueE = 1};
# 215 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIiEUt_E {
# 215 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIiE7__valueE = 1};
# 222 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIjEUt_E {
# 222 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIjE7__valueE = 1};
# 229 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIlEUt_E {
# 229 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIlE7__valueE = 1};
# 236 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerImEUt_E {
# 236 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerImE7__valueE = 1};
# 243 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIxEUt_E {
# 243 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIxE7__valueE = 1};
# 250 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIyEUt_E {
# 250 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIyE7__valueE = 1};
# 268 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt13__is_floatingIfEUt_E {
# 268 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt13__is_floatingIfE7__valueE = 1};
# 275 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt13__is_floatingIdEUt_E {
# 275 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt13__is_floatingIdE7__valueE = 1};
# 282 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt13__is_floatingIeEUt_E {
# 282 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt13__is_floatingIeE7__valueE = 1};
# 358 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_charIcEUt_E {
# 358 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_charIcE7__valueE = 1};
# 366 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_charIwEUt_E {
# 366 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_charIwE7__valueE = 1};
# 381 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_byteIcEUt_E {
# 381 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_byteIcE7__valueE = 1};
# 388 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_byteIaEUt_E {
# 388 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_byteIaE7__valueE = 1};
# 395 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_byteIhEUt_E {
# 395 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_byteIhE7__valueE = 1};
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIeEUt_E {
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIeE7__valueE};
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIdEUt_E {
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIdE7__valueE};
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIfEUt_E {
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIfE7__valueE};
# 153 "/usr/include/bits/mathinline.h" 3
union _ZZ10__signbitlEUt_;
# 212 "/usr/lib/gcc/x86_64-redhat-linux/4.8.5/include/stddef.h" 3
typedef unsigned long size_t;
# 1 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 1 3
# 38 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
# 1 "/usr/local/cuda-8.0/bin/..//include/host_defines.h" 1 3
# 39 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3





typedef __attribute__((device_builtin_texture_type)) unsigned long long __texture_type__;
typedef __attribute__((device_builtin_surface_type)) unsigned long long __surface_type__;
# 196 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
extern __attribute__((device)) __attribute__((used)) void* malloc(size_t);
extern __attribute__((device)) __attribute__((used)) void free(void*);


static __attribute__((device)) void __nv_sized_free(void *p, size_t sz) { free(p); }
static __attribute__((device)) void __nv_sized_array_free(void *p, size_t sz) { free(p); }


extern __attribute__((device)) void __assertfail(
  const void *message,
  const void *file,
  unsigned int line,
  const void *function,
  size_t charsize);
# 254 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
static __attribute__((device)) void __assert_fail(
  const char *__assertion,
  const char *__file,
  unsigned int __line,
  const char *__function)
{
  __assertfail(
    (const void *)__assertion,
    (const void *)__file,
                  __line,
    (const void *)__function,
    sizeof(char));
}
# 284 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
# 1 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 1 3
# 56 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 3
# 1 "/usr/local/cuda-8.0/include/device_types.h" 1 3
# 53 "/usr/local/cuda-8.0/include/device_types.h" 3
# 1 "/usr/local/cuda-8.0/include/host_defines.h" 1 3
# 54 "/usr/local/cuda-8.0/include/device_types.h" 2 3







enum __attribute__((device_builtin)) cudaRoundMode
{
    cudaRoundNearest,
    cudaRoundZero,
    cudaRoundPosInf,
    cudaRoundMinInf
};
# 57 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3


# 1 "/usr/local/cuda-8.0/include/driver_types.h" 1 3
# 156 "/usr/local/cuda-8.0/include/driver_types.h" 3
enum __attribute__((device_builtin)) cudaError
{





    cudaSuccess = 0,





    cudaErrorMissingConfiguration = 1,





    cudaErrorMemoryAllocation = 2,





    cudaErrorInitializationError = 3,
# 191 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchFailure = 4,
# 200 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorPriorLaunchFailure = 5,
# 211 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchTimeout = 6,
# 220 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchOutOfResources = 7,





    cudaErrorInvalidDeviceFunction = 8,
# 235 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorInvalidConfiguration = 9,





    cudaErrorInvalidDevice = 10,





    cudaErrorInvalidValue = 11,





    cudaErrorInvalidPitchValue = 12,





    cudaErrorInvalidSymbol = 13,




    cudaErrorMapBufferObjectFailed = 14,




    cudaErrorUnmapBufferObjectFailed = 15,





    cudaErrorInvalidHostPointer = 16,





    cudaErrorInvalidDevicePointer = 17,





    cudaErrorInvalidTexture = 18,





    cudaErrorInvalidTextureBinding = 19,






    cudaErrorInvalidChannelDescriptor = 20,





    cudaErrorInvalidMemcpyDirection = 21,
# 316 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorAddressOfConstant = 22,
# 325 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorTextureFetchFailed = 23,
# 334 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorTextureNotBound = 24,
# 343 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorSynchronizationError = 25,





    cudaErrorInvalidFilterSetting = 26,





    cudaErrorInvalidNormSetting = 27,







    cudaErrorMixedDeviceExecution = 28,






    cudaErrorCudartUnloading = 29,




    cudaErrorUnknown = 30,







    cudaErrorNotYetImplemented = 31,
# 392 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorMemoryValueTooLarge = 32,






    cudaErrorInvalidResourceHandle = 33,







    cudaErrorNotReady = 34,






    cudaErrorInsufficientDriver = 35,
# 427 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorSetOnActiveProcess = 36,





    cudaErrorInvalidSurface = 37,





    cudaErrorNoDevice = 38,





    cudaErrorECCUncorrectable = 39,




    cudaErrorSharedObjectSymbolNotFound = 40,




    cudaErrorSharedObjectInitFailed = 41,





    cudaErrorUnsupportedLimit = 42,





    cudaErrorDuplicateVariableName = 43,





    cudaErrorDuplicateTextureName = 44,





    cudaErrorDuplicateSurfaceName = 45,
# 489 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorDevicesUnavailable = 46,




    cudaErrorInvalidKernelImage = 47,







    cudaErrorNoKernelImageForDevice = 48,
# 515 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorIncompatibleDriverContext = 49,






    cudaErrorPeerAccessAlreadyEnabled = 50,






    cudaErrorPeerAccessNotEnabled = 51,





    cudaErrorDeviceAlreadyInUse = 54,






    cudaErrorProfilerDisabled = 55,







    cudaErrorProfilerNotInitialized = 56,






    cudaErrorProfilerAlreadyStarted = 57,






     cudaErrorProfilerAlreadyStopped = 58,







    cudaErrorAssert = 59,






    cudaErrorTooManyPeers = 60,





    cudaErrorHostMemoryAlreadyRegistered = 61,





    cudaErrorHostMemoryNotRegistered = 62,




    cudaErrorOperatingSystem = 63,





    cudaErrorPeerAccessUnsupported = 64,






    cudaErrorLaunchMaxDepthExceeded = 65,







    cudaErrorLaunchFileScopedTex = 66,







    cudaErrorLaunchFileScopedSurf = 67,
# 640 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorSyncDepthExceeded = 68,
# 652 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchPendingCountExceeded = 69,




    cudaErrorNotPermitted = 70,





    cudaErrorNotSupported = 71,
# 672 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorHardwareStackError = 72,







    cudaErrorIllegalInstruction = 73,
# 689 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorMisalignedAddress = 74,
# 700 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorInvalidAddressSpace = 75,







    cudaErrorInvalidPc = 76,







    cudaErrorIllegalAddress = 77,





    cudaErrorInvalidPtx = 78,




    cudaErrorInvalidGraphicsContext = 79,





    cudaErrorNvlinkUncorrectable = 80,




    cudaErrorStartupFailure = 0x7f,







    cudaErrorApiFailureBase = 10000
};




enum __attribute__((device_builtin)) cudaChannelFormatKind
{
    cudaChannelFormatKindSigned = 0,
    cudaChannelFormatKindUnsigned = 1,
    cudaChannelFormatKindFloat = 2,
    cudaChannelFormatKindNone = 3
};




struct __attribute__((device_builtin)) cudaChannelFormatDesc
{
    int x;
    int y;
    int z;
    int w;
    enum cudaChannelFormatKind f;
};




typedef struct cudaArray *cudaArray_t;




typedef const struct cudaArray *cudaArray_const_t;

struct cudaArray;




typedef struct cudaMipmappedArray *cudaMipmappedArray_t;




typedef const struct cudaMipmappedArray *cudaMipmappedArray_const_t;

struct cudaMipmappedArray;




enum __attribute__((device_builtin)) cudaMemoryType
{
    cudaMemoryTypeHost = 1,
    cudaMemoryTypeDevice = 2
};




enum __attribute__((device_builtin)) cudaMemcpyKind
{
    cudaMemcpyHostToHost = 0,
    cudaMemcpyHostToDevice = 1,
    cudaMemcpyDeviceToHost = 2,
    cudaMemcpyDeviceToDevice = 3,
    cudaMemcpyDefault = 4
};






struct __attribute__((device_builtin)) cudaPitchedPtr
{
    void *ptr;
    size_t pitch;
    size_t xsize;
    size_t ysize;
};






struct __attribute__((device_builtin)) cudaExtent
{
    size_t width;
    size_t height;
    size_t depth;
};






struct __attribute__((device_builtin)) cudaPos
{
    size_t x;
    size_t y;
    size_t z;
};




struct __attribute__((device_builtin)) cudaMemcpy3DParms
{
    cudaArray_t srcArray;
    struct cudaPos srcPos;
    struct cudaPitchedPtr srcPtr;

    cudaArray_t dstArray;
    struct cudaPos dstPos;
    struct cudaPitchedPtr dstPtr;

    struct cudaExtent extent;
    enum cudaMemcpyKind kind;
};




struct __attribute__((device_builtin)) cudaMemcpy3DPeerParms
{
    cudaArray_t srcArray;
    struct cudaPos srcPos;
    struct cudaPitchedPtr srcPtr;
    int srcDevice;

    cudaArray_t dstArray;
    struct cudaPos dstPos;
    struct cudaPitchedPtr dstPtr;
    int dstDevice;

    struct cudaExtent extent;
};




struct cudaGraphicsResource;




enum __attribute__((device_builtin)) cudaGraphicsRegisterFlags
{
    cudaGraphicsRegisterFlagsNone = 0,
    cudaGraphicsRegisterFlagsReadOnly = 1,
    cudaGraphicsRegisterFlagsWriteDiscard = 2,
    cudaGraphicsRegisterFlagsSurfaceLoadStore = 4,
    cudaGraphicsRegisterFlagsTextureGather = 8
};




enum __attribute__((device_builtin)) cudaGraphicsMapFlags
{
    cudaGraphicsMapFlagsNone = 0,
    cudaGraphicsMapFlagsReadOnly = 1,
    cudaGraphicsMapFlagsWriteDiscard = 2
};




enum __attribute__((device_builtin)) cudaGraphicsCubeFace
{
    cudaGraphicsCubeFacePositiveX = 0x00,
    cudaGraphicsCubeFaceNegativeX = 0x01,
    cudaGraphicsCubeFacePositiveY = 0x02,
    cudaGraphicsCubeFaceNegativeY = 0x03,
    cudaGraphicsCubeFacePositiveZ = 0x04,
    cudaGraphicsCubeFaceNegativeZ = 0x05
};




enum __attribute__((device_builtin)) cudaResourceType
{
    cudaResourceTypeArray = 0x00,
    cudaResourceTypeMipmappedArray = 0x01,
    cudaResourceTypeLinear = 0x02,
    cudaResourceTypePitch2D = 0x03
};




enum __attribute__((device_builtin)) cudaResourceViewFormat
{
    cudaResViewFormatNone = 0x00,
    cudaResViewFormatUnsignedChar1 = 0x01,
    cudaResViewFormatUnsignedChar2 = 0x02,
    cudaResViewFormatUnsignedChar4 = 0x03,
    cudaResViewFormatSignedChar1 = 0x04,
    cudaResViewFormatSignedChar2 = 0x05,
    cudaResViewFormatSignedChar4 = 0x06,
    cudaResViewFormatUnsignedShort1 = 0x07,
    cudaResViewFormatUnsignedShort2 = 0x08,
    cudaResViewFormatUnsignedShort4 = 0x09,
    cudaResViewFormatSignedShort1 = 0x0a,
    cudaResViewFormatSignedShort2 = 0x0b,
    cudaResViewFormatSignedShort4 = 0x0c,
    cudaResViewFormatUnsignedInt1 = 0x0d,
    cudaResViewFormatUnsignedInt2 = 0x0e,
    cudaResViewFormatUnsignedInt4 = 0x0f,
    cudaResViewFormatSignedInt1 = 0x10,
    cudaResViewFormatSignedInt2 = 0x11,
    cudaResViewFormatSignedInt4 = 0x12,
    cudaResViewFormatHalf1 = 0x13,
    cudaResViewFormatHalf2 = 0x14,
    cudaResViewFormatHalf4 = 0x15,
    cudaResViewFormatFloat1 = 0x16,
    cudaResViewFormatFloat2 = 0x17,
    cudaResViewFormatFloat4 = 0x18,
    cudaResViewFormatUnsignedBlockCompressed1 = 0x19,
    cudaResViewFormatUnsignedBlockCompressed2 = 0x1a,
    cudaResViewFormatUnsignedBlockCompressed3 = 0x1b,
    cudaResViewFormatUnsignedBlockCompressed4 = 0x1c,
    cudaResViewFormatSignedBlockCompressed4 = 0x1d,
    cudaResViewFormatUnsignedBlockCompressed5 = 0x1e,
    cudaResViewFormatSignedBlockCompressed5 = 0x1f,
    cudaResViewFormatUnsignedBlockCompressed6H = 0x20,
    cudaResViewFormatSignedBlockCompressed6H = 0x21,
    cudaResViewFormatUnsignedBlockCompressed7 = 0x22
};




struct __attribute__((device_builtin)) cudaResourceDesc {
 enum cudaResourceType resType;

 union {
  struct {
   cudaArray_t array;
  } array;
        struct {
            cudaMipmappedArray_t mipmap;
        } mipmap;
  struct {
   void *devPtr;
   struct cudaChannelFormatDesc desc;
   size_t sizeInBytes;
  } linear;
  struct {
   void *devPtr;
   struct cudaChannelFormatDesc desc;
   size_t width;
   size_t height;
   size_t pitchInBytes;
  } pitch2D;
 } res;
};




struct __attribute__((device_builtin)) cudaResourceViewDesc
{
    enum cudaResourceViewFormat format;
    size_t width;
    size_t height;
    size_t depth;
    unsigned int firstMipmapLevel;
    unsigned int lastMipmapLevel;
    unsigned int firstLayer;
    unsigned int lastLayer;
};




struct __attribute__((device_builtin)) cudaPointerAttributes
{




    enum cudaMemoryType memoryType;
# 1045 "/usr/local/cuda-8.0/include/driver_types.h" 3
    int device;





    void *devicePointer;





    void *hostPointer;




    int isManaged;
};




struct __attribute__((device_builtin)) cudaFuncAttributes
{





   size_t sharedSizeBytes;





   size_t constSizeBytes;




   size_t localSizeBytes;






   int maxThreadsPerBlock;




   int numRegs;






   int ptxVersion;






   int binaryVersion;





   int cacheModeCA;
};




enum __attribute__((device_builtin)) cudaFuncCache
{
    cudaFuncCachePreferNone = 0,
    cudaFuncCachePreferShared = 1,
    cudaFuncCachePreferL1 = 2,
    cudaFuncCachePreferEqual = 3
};





enum __attribute__((device_builtin)) cudaSharedMemConfig
{
    cudaSharedMemBankSizeDefault = 0,
    cudaSharedMemBankSizeFourByte = 1,
    cudaSharedMemBankSizeEightByte = 2
};




enum __attribute__((device_builtin)) cudaComputeMode
{
    cudaComputeModeDefault = 0,
    cudaComputeModeExclusive = 1,
    cudaComputeModeProhibited = 2,
    cudaComputeModeExclusiveProcess = 3
};




enum __attribute__((device_builtin)) cudaLimit
{
    cudaLimitStackSize = 0x00,
    cudaLimitPrintfFifoSize = 0x01,
    cudaLimitMallocHeapSize = 0x02,
    cudaLimitDevRuntimeSyncDepth = 0x03,
    cudaLimitDevRuntimePendingLaunchCount = 0x04
};




enum __attribute__((device_builtin)) cudaMemoryAdvise
{
    cudaMemAdviseSetReadMostly = 1,
    cudaMemAdviseUnsetReadMostly = 2,
    cudaMemAdviseSetPreferredLocation = 3,
    cudaMemAdviseUnsetPreferredLocation = 4,
    cudaMemAdviseSetAccessedBy = 5,
    cudaMemAdviseUnsetAccessedBy = 6
};




enum __attribute__((device_builtin)) cudaMemRangeAttribute
{
    cudaMemRangeAttributeReadMostly = 1,
    cudaMemRangeAttributePreferredLocation = 2,
    cudaMemRangeAttributeAccessedBy = 3,
    cudaMemRangeAttributeLastPrefetchLocation = 4
};




enum __attribute__((device_builtin)) cudaOutputMode
{
    cudaKeyValuePair = 0x00,
    cudaCSV = 0x01
};




enum __attribute__((device_builtin)) cudaDeviceAttr
{
    cudaDevAttrMaxThreadsPerBlock = 1,
    cudaDevAttrMaxBlockDimX = 2,
    cudaDevAttrMaxBlockDimY = 3,
    cudaDevAttrMaxBlockDimZ = 4,
    cudaDevAttrMaxGridDimX = 5,
    cudaDevAttrMaxGridDimY = 6,
    cudaDevAttrMaxGridDimZ = 7,
    cudaDevAttrMaxSharedMemoryPerBlock = 8,
    cudaDevAttrTotalConstantMemory = 9,
    cudaDevAttrWarpSize = 10,
    cudaDevAttrMaxPitch = 11,
    cudaDevAttrMaxRegistersPerBlock = 12,
    cudaDevAttrClockRate = 13,
    cudaDevAttrTextureAlignment = 14,
    cudaDevAttrGpuOverlap = 15,
    cudaDevAttrMultiProcessorCount = 16,
    cudaDevAttrKernelExecTimeout = 17,
    cudaDevAttrIntegrated = 18,
    cudaDevAttrCanMapHostMemory = 19,
    cudaDevAttrComputeMode = 20,
    cudaDevAttrMaxTexture1DWidth = 21,
    cudaDevAttrMaxTexture2DWidth = 22,
    cudaDevAttrMaxTexture2DHeight = 23,
    cudaDevAttrMaxTexture3DWidth = 24,
    cudaDevAttrMaxTexture3DHeight = 25,
    cudaDevAttrMaxTexture3DDepth = 26,
    cudaDevAttrMaxTexture2DLayeredWidth = 27,
    cudaDevAttrMaxTexture2DLayeredHeight = 28,
    cudaDevAttrMaxTexture2DLayeredLayers = 29,
    cudaDevAttrSurfaceAlignment = 30,
    cudaDevAttrConcurrentKernels = 31,
    cudaDevAttrEccEnabled = 32,
    cudaDevAttrPciBusId = 33,
    cudaDevAttrPciDeviceId = 34,
    cudaDevAttrTccDriver = 35,
    cudaDevAttrMemoryClockRate = 36,
    cudaDevAttrGlobalMemoryBusWidth = 37,
    cudaDevAttrL2CacheSize = 38,
    cudaDevAttrMaxThreadsPerMultiProcessor = 39,
    cudaDevAttrAsyncEngineCount = 40,
    cudaDevAttrUnifiedAddressing = 41,
    cudaDevAttrMaxTexture1DLayeredWidth = 42,
    cudaDevAttrMaxTexture1DLayeredLayers = 43,
    cudaDevAttrMaxTexture2DGatherWidth = 45,
    cudaDevAttrMaxTexture2DGatherHeight = 46,
    cudaDevAttrMaxTexture3DWidthAlt = 47,
    cudaDevAttrMaxTexture3DHeightAlt = 48,
    cudaDevAttrMaxTexture3DDepthAlt = 49,
    cudaDevAttrPciDomainId = 50,
    cudaDevAttrTexturePitchAlignment = 51,
    cudaDevAttrMaxTextureCubemapWidth = 52,
    cudaDevAttrMaxTextureCubemapLayeredWidth = 53,
    cudaDevAttrMaxTextureCubemapLayeredLayers = 54,
    cudaDevAttrMaxSurface1DWidth = 55,
    cudaDevAttrMaxSurface2DWidth = 56,
    cudaDevAttrMaxSurface2DHeight = 57,
    cudaDevAttrMaxSurface3DWidth = 58,
    cudaDevAttrMaxSurface3DHeight = 59,
    cudaDevAttrMaxSurface3DDepth = 60,
    cudaDevAttrMaxSurface1DLayeredWidth = 61,
    cudaDevAttrMaxSurface1DLayeredLayers = 62,
    cudaDevAttrMaxSurface2DLayeredWidth = 63,
    cudaDevAttrMaxSurface2DLayeredHeight = 64,
    cudaDevAttrMaxSurface2DLayeredLayers = 65,
    cudaDevAttrMaxSurfaceCubemapWidth = 66,
    cudaDevAttrMaxSurfaceCubemapLayeredWidth = 67,
    cudaDevAttrMaxSurfaceCubemapLayeredLayers = 68,
    cudaDevAttrMaxTexture1DLinearWidth = 69,
    cudaDevAttrMaxTexture2DLinearWidth = 70,
    cudaDevAttrMaxTexture2DLinearHeight = 71,
    cudaDevAttrMaxTexture2DLinearPitch = 72,
    cudaDevAttrMaxTexture2DMipmappedWidth = 73,
    cudaDevAttrMaxTexture2DMipmappedHeight = 74,
    cudaDevAttrComputeCapabilityMajor = 75,
    cudaDevAttrComputeCapabilityMinor = 76,
    cudaDevAttrMaxTexture1DMipmappedWidth = 77,
    cudaDevAttrStreamPrioritiesSupported = 78,
    cudaDevAttrGlobalL1CacheSupported = 79,
    cudaDevAttrLocalL1CacheSupported = 80,
    cudaDevAttrMaxSharedMemoryPerMultiprocessor = 81,
    cudaDevAttrMaxRegistersPerMultiprocessor = 82,
    cudaDevAttrManagedMemory = 83,
    cudaDevAttrIsMultiGpuBoard = 84,
    cudaDevAttrMultiGpuBoardGroupID = 85,
    cudaDevAttrHostNativeAtomicSupported = 86,
    cudaDevAttrSingleToDoublePrecisionPerfRatio = 87,
    cudaDevAttrPageableMemoryAccess = 88,
    cudaDevAttrConcurrentManagedAccess = 89,
    cudaDevAttrComputePreemptionSupported = 90,
    cudaDevAttrCanUseHostPointerForRegisteredMem = 91
};





enum __attribute__((device_builtin)) cudaDeviceP2PAttr {
    cudaDevP2PAttrPerformanceRank = 1,
    cudaDevP2PAttrAccessSupported = 2,
    cudaDevP2PAttrNativeAtomicSupported = 3
};



struct __attribute__((device_builtin)) cudaDeviceProp
{
    char name[256];
    size_t totalGlobalMem;
    size_t sharedMemPerBlock;
    int regsPerBlock;
    int warpSize;
    size_t memPitch;
    int maxThreadsPerBlock;
    int maxThreadsDim[3];
    int maxGridSize[3];
    int clockRate;
    size_t totalConstMem;
    int major;
    int minor;
    size_t textureAlignment;
    size_t texturePitchAlignment;
    int deviceOverlap;
    int multiProcessorCount;
    int kernelExecTimeoutEnabled;
    int integrated;
    int canMapHostMemory;
    int computeMode;
    int maxTexture1D;
    int maxTexture1DMipmap;
    int maxTexture1DLinear;
    int maxTexture2D[2];
    int maxTexture2DMipmap[2];
    int maxTexture2DLinear[3];
    int maxTexture2DGather[2];
    int maxTexture3D[3];
    int maxTexture3DAlt[3];
    int maxTextureCubemap;
    int maxTexture1DLayered[2];
    int maxTexture2DLayered[3];
    int maxTextureCubemapLayered[2];
    int maxSurface1D;
    int maxSurface2D[2];
    int maxSurface3D[3];
    int maxSurface1DLayered[2];
    int maxSurface2DLayered[3];
    int maxSurfaceCubemap;
    int maxSurfaceCubemapLayered[2];
    size_t surfaceAlignment;
    int concurrentKernels;
    int ECCEnabled;
    int pciBusID;
    int pciDeviceID;
    int pciDomainID;
    int tccDriver;
    int asyncEngineCount;
    int unifiedAddressing;
    int memoryClockRate;
    int memoryBusWidth;
    int l2CacheSize;
    int maxThreadsPerMultiProcessor;
    int streamPrioritiesSupported;
    int globalL1CacheSupported;
    int localL1CacheSupported;
    size_t sharedMemPerMultiprocessor;
    int regsPerMultiprocessor;
    int managedMemory;
    int isMultiGpuBoard;
    int multiGpuBoardGroupID;
    int hostNativeAtomicSupported;
    int singleToDoublePrecisionPerfRatio;
    int pageableMemoryAccess;
    int concurrentManagedAccess;
};
# 1456 "/usr/local/cuda-8.0/include/driver_types.h" 3
typedef __attribute__((device_builtin)) struct __attribute__((device_builtin)) cudaIpcEventHandle_st
{
    char reserved[64];
}cudaIpcEventHandle_t;




typedef __attribute__((device_builtin)) struct __attribute__((device_builtin)) cudaIpcMemHandle_st
{
    char reserved[64];
}cudaIpcMemHandle_t;
# 1478 "/usr/local/cuda-8.0/include/driver_types.h" 3
typedef __attribute__((device_builtin)) enum cudaError cudaError_t;




typedef __attribute__((device_builtin)) struct CUstream_st *cudaStream_t;




typedef __attribute__((device_builtin)) struct CUevent_st *cudaEvent_t;




typedef __attribute__((device_builtin)) struct cudaGraphicsResource *cudaGraphicsResource_t;




typedef __attribute__((device_builtin)) struct CUuuid_st cudaUUID_t;




typedef __attribute__((device_builtin)) enum cudaOutputMode cudaOutputMode_t;
# 60 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3


# 1 "/usr/local/cuda-8.0/include/surface_types.h" 1 3
# 59 "/usr/local/cuda-8.0/include/surface_types.h" 3
# 1 "/usr/local/cuda-8.0/include/driver_types.h" 1 3
# 60 "/usr/local/cuda-8.0/include/surface_types.h" 2 3
# 84 "/usr/local/cuda-8.0/include/surface_types.h" 3
enum __attribute__((device_builtin)) cudaSurfaceBoundaryMode
{
    cudaBoundaryModeZero = 0,
    cudaBoundaryModeClamp = 1,
    cudaBoundaryModeTrap = 2
};




enum __attribute__((device_builtin)) cudaSurfaceFormatMode
{
    cudaFormatModeForced = 0,
    cudaFormatModeAuto = 1
};




struct __attribute__((device_builtin)) surfaceReference
{



    struct cudaChannelFormatDesc channelDesc;
};




typedef __attribute__((device_builtin)) unsigned long long cudaSurfaceObject_t;
# 63 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/texture_types.h" 1 3
# 84 "/usr/local/cuda-8.0/include/texture_types.h" 3
enum __attribute__((device_builtin)) cudaTextureAddressMode
{
    cudaAddressModeWrap = 0,
    cudaAddressModeClamp = 1,
    cudaAddressModeMirror = 2,
    cudaAddressModeBorder = 3
};




enum __attribute__((device_builtin)) cudaTextureFilterMode
{
    cudaFilterModePoint = 0,
    cudaFilterModeLinear = 1
};




enum __attribute__((device_builtin)) cudaTextureReadMode
{
    cudaReadModeElementType = 0,
    cudaReadModeNormalizedFloat = 1
};




struct __attribute__((device_builtin)) textureReference
{



    int normalized;



    enum cudaTextureFilterMode filterMode;



    enum cudaTextureAddressMode addressMode[3];



    struct cudaChannelFormatDesc channelDesc;



    int sRGB;



    unsigned int maxAnisotropy;



    enum cudaTextureFilterMode mipmapFilterMode;



    float mipmapLevelBias;



    float minMipmapLevelClamp;



    float maxMipmapLevelClamp;
    int __cudaReserved[15];
};




struct __attribute__((device_builtin)) cudaTextureDesc
{



    enum cudaTextureAddressMode addressMode[3];



    enum cudaTextureFilterMode filterMode;



    enum cudaTextureReadMode readMode;



    int sRGB;



    float borderColor[4];



    int normalizedCoords;



    unsigned int maxAnisotropy;



    enum cudaTextureFilterMode mipmapFilterMode;



    float mipmapLevelBias;



    float minMipmapLevelClamp;



    float maxMipmapLevelClamp;
};




typedef __attribute__((device_builtin)) unsigned long long cudaTextureObject_t;
# 64 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/vector_types.h" 1 3
# 61 "/usr/local/cuda-8.0/include/vector_types.h" 3
# 1 "/usr/local/cuda-8.0/include/builtin_types.h" 1 3
# 56 "/usr/local/cuda-8.0/include/builtin_types.h" 3
# 1 "/usr/local/cuda-8.0/include/device_types.h" 1 3
# 57 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3





# 1 "/usr/local/cuda-8.0/include/surface_types.h" 1 3
# 63 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/texture_types.h" 1 3
# 64 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/vector_types.h" 1 3
# 64 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3
# 62 "/usr/local/cuda-8.0/include/vector_types.h" 2 3
# 98 "/usr/local/cuda-8.0/include/vector_types.h" 3
struct __attribute__((device_builtin)) char1
{
    signed char x;
};

struct __attribute__((device_builtin)) uchar1
{
    unsigned char x;
};


struct __attribute__((device_builtin)) __attribute__((aligned(2))) char2
{
    signed char x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(2))) uchar2
{
    unsigned char x, y;
};

struct __attribute__((device_builtin)) char3
{
    signed char x, y, z;
};

struct __attribute__((device_builtin)) uchar3
{
    unsigned char x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) char4
{
    signed char x, y, z, w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) uchar4
{
    unsigned char x, y, z, w;
};

struct __attribute__((device_builtin)) short1
{
    short x;
};

struct __attribute__((device_builtin)) ushort1
{
    unsigned short x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) short2
{
    short x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) ushort2
{
    unsigned short x, y;
};

struct __attribute__((device_builtin)) short3
{
    short x, y, z;
};

struct __attribute__((device_builtin)) ushort3
{
    unsigned short x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(8))) short4 { short x; short y; short z; short w; };
struct __attribute__((device_builtin)) __attribute__((aligned(8))) ushort4 { unsigned short x; unsigned short y; unsigned short z; unsigned short w; };

struct __attribute__((device_builtin)) int1
{
    int x;
};

struct __attribute__((device_builtin)) uint1
{
    unsigned int x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(8))) int2 { int x; int y; };
struct __attribute__((device_builtin)) __attribute__((aligned(8))) uint2 { unsigned int x; unsigned int y; };

struct __attribute__((device_builtin)) int3
{
    int x, y, z;
};

struct __attribute__((device_builtin)) uint3
{
    unsigned int x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) int4
{
    int x, y, z, w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) uint4
{
    unsigned int x, y, z, w;
};

struct __attribute__((device_builtin)) long1
{
    long int x;
};

struct __attribute__((device_builtin)) ulong1
{
    unsigned long x;
};






struct __attribute__((device_builtin)) __attribute__((aligned(2*sizeof(long int)))) long2
{
    long int x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(2*sizeof(unsigned long int)))) ulong2
{
    unsigned long int x, y;
};



struct __attribute__((device_builtin)) long3
{
    long int x, y, z;
};

struct __attribute__((device_builtin)) ulong3
{
    unsigned long int x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) long4
{
    long int x, y, z, w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) ulong4
{
    unsigned long int x, y, z, w;
};

struct __attribute__((device_builtin)) float1
{
    float x;
};
# 274 "/usr/local/cuda-8.0/include/vector_types.h" 3
struct __attribute__((device_builtin)) __attribute__((aligned(8))) float2 { float x; float y; };




struct __attribute__((device_builtin)) float3
{
    float x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) float4
{
    float x, y, z, w;
};

struct __attribute__((device_builtin)) longlong1
{
    long long int x;
};

struct __attribute__((device_builtin)) ulonglong1
{
    unsigned long long int x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) longlong2
{
    long long int x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) ulonglong2
{
    unsigned long long int x, y;
};

struct __attribute__((device_builtin)) longlong3
{
    long long int x, y, z;
};

struct __attribute__((device_builtin)) ulonglong3
{
    unsigned long long int x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) longlong4
{
    long long int x, y, z ,w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) ulonglong4
{
    unsigned long long int x, y, z, w;
};

struct __attribute__((device_builtin)) double1
{
    double x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) double2
{
    double x, y;
};

struct __attribute__((device_builtin)) double3
{
    double x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) double4
{
    double x, y, z, w;
};
# 362 "/usr/local/cuda-8.0/include/vector_types.h" 3
typedef __attribute__((device_builtin)) struct char1 char1;
typedef __attribute__((device_builtin)) struct uchar1 uchar1;
typedef __attribute__((device_builtin)) struct char2 char2;
typedef __attribute__((device_builtin)) struct uchar2 uchar2;
typedef __attribute__((device_builtin)) struct char3 char3;
typedef __attribute__((device_builtin)) struct uchar3 uchar3;
typedef __attribute__((device_builtin)) struct char4 char4;
typedef __attribute__((device_builtin)) struct uchar4 uchar4;
typedef __attribute__((device_builtin)) struct short1 short1;
typedef __attribute__((device_builtin)) struct ushort1 ushort1;
typedef __attribute__((device_builtin)) struct short2 short2;
typedef __attribute__((device_builtin)) struct ushort2 ushort2;
typedef __attribute__((device_builtin)) struct short3 short3;
typedef __attribute__((device_builtin)) struct ushort3 ushort3;
typedef __attribute__((device_builtin)) struct short4 short4;
typedef __attribute__((device_builtin)) struct ushort4 ushort4;
typedef __attribute__((device_builtin)) struct int1 int1;
typedef __attribute__((device_builtin)) struct uint1 uint1;
typedef __attribute__((device_builtin)) struct int2 int2;
typedef __attribute__((device_builtin)) struct uint2 uint2;
typedef __attribute__((device_builtin)) struct int3 int3;
typedef __attribute__((device_builtin)) struct uint3 uint3;
typedef __attribute__((device_builtin)) struct int4 int4;
typedef __attribute__((device_builtin)) struct uint4 uint4;
typedef __attribute__((device_builtin)) struct long1 long1;
typedef __attribute__((device_builtin)) struct ulong1 ulong1;
typedef __attribute__((device_builtin)) struct long2 long2;
typedef __attribute__((device_builtin)) struct ulong2 ulong2;
typedef __attribute__((device_builtin)) struct long3 long3;
typedef __attribute__((device_builtin)) struct ulong3 ulong3;
typedef __attribute__((device_builtin)) struct long4 long4;
typedef __attribute__((device_builtin)) struct ulong4 ulong4;
typedef __attribute__((device_builtin)) struct float1 float1;
typedef __attribute__((device_builtin)) struct float2 float2;
typedef __attribute__((device_builtin)) struct float3 float3;
typedef __attribute__((device_builtin)) struct float4 float4;
typedef __attribute__((device_builtin)) struct longlong1 longlong1;
typedef __attribute__((device_builtin)) struct ulonglong1 ulonglong1;
typedef __attribute__((device_builtin)) struct longlong2 longlong2;
typedef __attribute__((device_builtin)) struct ulonglong2 ulonglong2;
typedef __attribute__((device_builtin)) struct longlong3 longlong3;
typedef __attribute__((device_builtin)) struct ulonglong3 ulonglong3;
typedef __attribute__((device_builtin)) struct longlong4 longlong4;
typedef __attribute__((device_builtin)) struct ulonglong4 ulonglong4;
typedef __attribute__((device_builtin)) struct double1 double1;
typedef __attribute__((device_builtin)) struct double2 double2;
typedef __attribute__((device_builtin)) struct double3 double3;
typedef __attribute__((device_builtin)) struct double4 double4;







struct __attribute__((device_builtin)) dim3
{
    unsigned int x, y, z;





};

typedef __attribute__((device_builtin)) struct dim3 dim3;
# 64 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3
# 285 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3
# 1 "/usr/local/cuda-8.0/bin/..//include/device_launch_parameters.h" 1 3
# 71 "/usr/local/cuda-8.0/bin/..//include/device_launch_parameters.h" 3
uint3 __attribute__((device_builtin)) extern const threadIdx;
uint3 __attribute__((device_builtin)) extern const blockIdx;
dim3 __attribute__((device_builtin)) extern const blockDim;
dim3 __attribute__((device_builtin)) extern const gridDim;
int __attribute__((device_builtin)) extern const warpSize;
# 286 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3
# 1 "/usr/local/cuda-8.0/include/crt/storage_class.h" 1 3
# 286 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3
# 214 "/usr/lib/gcc/x86_64-redhat-linux/4.8.5/include/stddef.h" 2 3
# 141 "/usr/include/bits/types.h" 3
typedef long __off64_t;
# 148 "/usr/include/bits/types.h" 3
typedef long __time_t;
# 150 "/usr/include/bits/types.h" 3
typedef long __suseconds_t;
# 153 "/usr/include/bits/types.h" 3
typedef int __key_t;
# 30 "/usr/include/bits/time.h" 3
struct timeval {
# 32 "/usr/include/bits/time.h" 3
__time_t tv_sec;
# 33 "/usr/include/bits/time.h" 3
__suseconds_t tv_usec;};
# 75 "/usr/include/time.h" 3
typedef __time_t time_t;
# 133 "/usr/include/time.h" 3
struct tm {
# 135 "/usr/include/time.h" 3
int tm_sec;
# 136 "/usr/include/time.h" 3
int tm_min;
# 137 "/usr/include/time.h" 3
int tm_hour;
# 138 "/usr/include/time.h" 3
int tm_mday;
# 139 "/usr/include/time.h" 3
int tm_mon;
# 140 "/usr/include/time.h" 3
int tm_year;
# 141 "/usr/include/time.h" 3
int tm_wday;
# 142 "/usr/include/time.h" 3
int tm_yday;
# 143 "/usr/include/time.h" 3
int tm_isdst;
# 146 "/usr/include/time.h" 3
long tm_gmtoff;
# 147 "/usr/include/time.h" 3
const char *tm_zone;};
# 48 "/usr/include/stdio.h" 3
typedef struct _IO_FILE FILE;
# 92 "/usr/include/stdio.h" 3
typedef __off64_t off_t;
# 97 "/usr/include/stdio.h" 3
typedef __off64_t off64_t;
# 122 "/usr/include/sys/types.h" 3
typedef __key_t key_t;
# 60 "/usr/include/bits/pthreadtypes.h" 3
typedef unsigned long pthread_t;
# 75 "/usr/include/bits/pthreadtypes.h" 3
struct __pthread_internal_list {
# 77 "/usr/include/bits/pthreadtypes.h" 3
struct __pthread_internal_list *__prev;
# 78 "/usr/include/bits/pthreadtypes.h" 3
struct __pthread_internal_list *__next;};
# 79 "/usr/include/bits/pthreadtypes.h" 3
typedef struct __pthread_internal_list __pthread_list_t;
# 92 "/usr/include/bits/pthreadtypes.h" 3
struct _ZN15pthread_mutex_t17__pthread_mutex_sE {
# 94 "/usr/include/bits/pthreadtypes.h" 3
int __lock;
# 95 "/usr/include/bits/pthreadtypes.h" 3
unsigned __count;
# 96 "/usr/include/bits/pthreadtypes.h" 3
int __owner;
# 98 "/usr/include/bits/pthreadtypes.h" 3
unsigned __nusers;
# 102 "/usr/include/bits/pthreadtypes.h" 3
int __kind;
# 104 "/usr/include/bits/pthreadtypes.h" 3
int __spins;
# 105 "/usr/include/bits/pthreadtypes.h" 3
__pthread_list_t __list;};
# 91 "/usr/include/bits/pthreadtypes.h" 3
union pthread_mutex_t {
# 115 "/usr/include/bits/pthreadtypes.h" 3
struct _ZN15pthread_mutex_t17__pthread_mutex_sE __data;
# 116 "/usr/include/bits/pthreadtypes.h" 3
char __size[40];
# 117 "/usr/include/bits/pthreadtypes.h" 3
long __align;};
# 118 "/usr/include/bits/pthreadtypes.h" 3
typedef union pthread_mutex_t pthread_mutex_t;
# 132 "/usr/include/bits/pthreadtypes.h" 3
struct _ZN14pthread_cond_tUt_E {
# 133 "/usr/include/bits/pthreadtypes.h" 3
int __lock;
# 134 "/usr/include/bits/pthreadtypes.h" 3
unsigned __futex;
# 135 "/usr/include/bits/pthreadtypes.h" 3
unsigned long long __total_seq;
# 136 "/usr/include/bits/pthreadtypes.h" 3
unsigned long long __wakeup_seq;
# 137 "/usr/include/bits/pthreadtypes.h" 3
unsigned long long __woken_seq;
# 138 "/usr/include/bits/pthreadtypes.h" 3
void *__mutex;
# 139 "/usr/include/bits/pthreadtypes.h" 3
unsigned __nwaiters;
# 140 "/usr/include/bits/pthreadtypes.h" 3
unsigned __broadcast_seq;};
# 130 "/usr/include/bits/pthreadtypes.h" 3
union pthread_cond_t {
# 141 "/usr/include/bits/pthreadtypes.h" 3
struct _ZN14pthread_cond_tUt_E __data;
# 142 "/usr/include/bits/pthreadtypes.h" 3
char __size[48];
# 143 "/usr/include/bits/pthreadtypes.h" 3
long long __align;};
# 144 "/usr/include/bits/pthreadtypes.h" 3
typedef union pthread_cond_t pthread_cond_t;
# 34 "../ringbuffer/RingBuffer.h"
struct RingBuffer_t {
# 35 "../ringbuffer/RingBuffer.h"
void *addr;
# 36 "../ringbuffer/RingBuffer.h"
size_t size;
# 37 "../ringbuffer/RingBuffer.h"
size_t woff;
# 38 "../ringbuffer/RingBuffer.h"
size_t roff;
# 40 "../ringbuffer/RingBuffer.h"
char name[80];
# 41 "../ringbuffer/RingBuffer.h"
size_t full;
# 42 "../ringbuffer/RingBuffer.h"
size_t drop;
# 43 "../ringbuffer/RingBuffer.h"
size_t blnk;
# 44 "../ringbuffer/RingBuffer.h"
size_t max_exist;
# 46 "../ringbuffer/RingBuffer.h"
void *rxaddr;};
# 47 "../ringbuffer/RingBuffer.h"
typedef struct RingBuffer_t RingBuffer;
# 48 "/usr/include/stdint.h" 3
typedef unsigned char uint8_t;
# 49 "/usr/include/stdint.h" 3
typedef unsigned short uint16_t;
# 51 "/usr/include/stdint.h" 3
typedef unsigned uint32_t;
# 53 "../network/vdif_receiver_rb.h"
struct vdif_rx_tt {
# 54 "../network/vdif_receiver_rb.h"
pthread_t tid;
# 55 "../network/vdif_receiver_rb.h"
pthread_mutex_t mtx;
# 56 "../network/vdif_receiver_rb.h"
pthread_cond_t go;
# 57 "../network/vdif_receiver_rb.h"
int bind_cpu;
# 58 "../network/vdif_receiver_rb.h"
int sd;
# 58 "../network/vdif_receiver_rb.h"
int fd;
# 59 "../network/vdif_receiver_rb.h"
RingBuffer *rb;
# 60 "../network/vdif_receiver_rb.h"
size_t frame_size;
# 61 "../network/vdif_receiver_rb.h"
size_t frame_offset;
# 62 "../network/vdif_receiver_rb.h"
size_t bufsize_bytes;
# 63 "../network/vdif_receiver_rb.h"
size_t bufsize_frames;
# 64 "../network/vdif_receiver_rb.h"
size_t rate_Mbps;
# 65 "../network/vdif_receiver_rb.h"
size_t rate_fps;
# 66 "../network/vdif_receiver_rb.h"
int is_file;
# 67 "../network/vdif_receiver_rb.h"
int is_network;
# 68 "../network/vdif_receiver_rb.h"
volatile int at_eof;
# 69 "../network/vdif_receiver_rb.h"
void *workbuf;
# 70 "../network/vdif_receiver_rb.h"
void *dlhandle;};
# 71 "../network/vdif_receiver_rb.h"
typedef struct vdif_rx_tt vdif_rx_t;
# 28 "../helpers/kvn_dsm.h"
struct kvn_dsm_dataheader_tt {
# 29 "../helpers/kvn_dsm.h"
uint32_t sequence_number;
# 30 "../helpers/kvn_dsm.h"
char start_time[7];
# 31 "../helpers/kvn_dsm.h"
char input_mode[1];
# 32 "../helpers/kvn_dsm.h"
uint16_t output_streams;
# 33 "../helpers/kvn_dsm.h"
uint32_t valid_samples[8];
# 34 "../helpers/kvn_dsm.h"
uint32_t invalid_samples[8];
# 35 "../helpers/kvn_dsm.h"
char integration_length;
# 36 "../helpers/kvn_dsm.h"
char output_mode;
# 37 "../helpers/kvn_dsm.h"
uint16_t start_channel_index;};
# 38 "../helpers/kvn_dsm.h"
typedef struct kvn_dsm_dataheader_tt kvn_dsm_dataheader_t;
# 40 "../helpers/kvn_dsm.h"
struct kvn_dsm_writer_tt {
# 41 "../helpers/kvn_dsm.h"
const char *base_filename;
# 42 "../helpers/kvn_dsm.h"
size_t serialnum;
# 43 "../helpers/kvn_dsm.h"
size_t ipnum;};
# 44 "../helpers/kvn_dsm.h"
typedef struct kvn_dsm_writer_tt kvn_dsm_writer_t;
# 67 "/usr/local/cuda-8.0/bin/..//include/cuComplex.h"
typedef struct float2 cuFloatComplex;
# 289 "/usr/local/cuda-8.0/bin/..//include/cuComplex.h"
typedef cuFloatComplex cuComplex;
# 97 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
typedef enum cufftResult_t cufftResult;
# 107 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
typedef float cufftReal;
# 113 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
typedef cuComplex cufftComplex;
# 128 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
typedef enum cufftType_t cufftType;
# 143 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
typedef int cufftHandle;
# 184 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
typedef enum cufftXtCallbackType_t cufftXtCallbackType;
# 188 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
typedef cufftReal (*cufftCallbackLoadR)(void *, size_t, void *, void *);
# 191 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
typedef void (*cufftCallbackStoreC)(void *, size_t, cufftComplex, void *, void *);
# 17 "../kernels/cuda/arithmetic_winfunc_kernels.h"
struct cu_window_cb_params_tt {
# 18 "../kernels/cuda/arithmetic_winfunc_kernels.h"
size_t fftlen;};
# 19 "../kernels/cuda/arithmetic_winfunc_kernels.h"
typedef struct cu_window_cb_params_tt cu_window_cb_params_t;
# 55 "kfftspec.h"
struct fftspec_config_tt {
# 58 "kfftspec.h"
int dasfd;
# 60 "kfftspec.h"
double T_int;
# 61 "kfftspec.h"
double T_int_wish;
# 62 "kfftspec.h"
double bw;
# 63 "kfftspec.h"
int nchan;
# 64 "kfftspec.h"
int nfft;
# 65 "kfftspec.h"
int nsubints;
# 66 "kfftspec.h"
int do_cross;
# 67 "kfftspec.h"
int nspecs;
# 68 "kfftspec.h"
int do_window;
# 69 "kfftspec.h"
int window_type;
# 70 "kfftspec.h"
float window_overlap;
# 73 "kfftspec.h"
int nGPUs;
# 74 "kfftspec.h"
int nstreams;
# 77 "kfftspec.h"
vdif_rx_t *rx;
# 78 "kfftspec.h"
char *raw_format;
# 79 "kfftspec.h"
int raw_nsubbands;
# 80 "kfftspec.h"
int raw_nbits;
# 83 "kfftspec.h"
struct timeval h_rawbuf_midtimes[8][4];
# 84 "kfftspec.h"
unsigned char *h_rawbufs[8][4];
# 85 "kfftspec.h"
unsigned char *d_rawbufs[8][4];
# 86 "kfftspec.h"
size_t rawbuflen;
# 87 "kfftspec.h"
unsigned char *h_random;
# 90 "kfftspec.h"
int devicemap[8];
# 91 "kfftspec.h"
struct CUstream_st *sid[8][4];
# 92 "kfftspec.h"
cufftHandle cufftplans[8][4];
# 93 "kfftspec.h"
struct cudaDeviceProp devprops[8];
# 96 "kfftspec.h"
float *d_fft_in[8][4];
# 97 "kfftspec.h"
float *d_fft_out[8][4];
# 98 "kfftspec.h"
cu_window_cb_params_t h_cufft_userparams;
# 99 "kfftspec.h"
cu_window_cb_params_t *d_cufft_userparams[8];
# 102 "kfftspec.h"
float *d_powspecs[8][4];
# 103 "kfftspec.h"
float *h_powspecs[8][4];
# 104 "kfftspec.h"
float *h_crosspecs[8][4];
# 105 "kfftspec.h"
double spec_weight[8][4];
# 108 "kfftspec.h"
const char *scanname;
# 109 "kfftspec.h"
const char *experiment;
# 110 "kfftspec.h"
const char *station;
# 111 "kfftspec.h"
const char *observer;
# 112 "kfftspec.h"
char *out_filename;
# 115 "kfftspec.h"
struct CUevent_st *process_cuda_starttimes[8][4];
# 116 "kfftspec.h"
struct CUevent_st *process_cuda_stoptimes[8][4];
# 117 "kfftspec.h"
struct CUevent_st *process_cuda_inputdata_overwriteable[8][4];
# 118 "kfftspec.h"
struct CUevent_st *process_cuda_spectrum_available[8][4];
# 119 "kfftspec.h"
struct CUevent_st *estart[8][4];
# 120 "kfftspec.h"
struct CUevent_st *estop[8][4];};
# 121 "kfftspec.h"
typedef struct fftspec_config_tt fftspec_config_t;
# 37 "../helpers/kfftspec_outfile.h"
struct kfftspec_outfile_spectrumheader_v1_tt {
# 40 "../helpers/kfftspec_outfile.h"
uint32_t headerlen;
# 43 "../helpers/kfftspec_outfile.h"
uint32_t datalen;
# 44 "../helpers/kfftspec_outfile.h"
uint32_t datalayout;
# 47 "../helpers/kfftspec_outfile.h"
uint32_t header_version;
# 50 "../helpers/kfftspec_outfile.h"
uint32_t nchan;
# 51 "../helpers/kfftspec_outfile.h"
uint32_t signal_src_1;
# 52 "../helpers/kfftspec_outfile.h"
uint32_t signal_src_2;
# 53 "../helpers/kfftspec_outfile.h"
double Tint;
# 54 "../helpers/kfftspec_outfile.h"
double bandwidth;
# 55 "../helpers/kfftspec_outfile.h"
double weight;
# 56 "../helpers/kfftspec_outfile.h"
char winfunc[8];
# 57 "../helpers/kfftspec_outfile.h"
double winoverlap;
# 60 "../helpers/kfftspec_outfile.h"
uint32_t nlevels_src_1;
# 61 "../helpers/kfftspec_outfile.h"
uint32_t nlevels_src_2;
# 62 "../helpers/kfftspec_outfile.h"
double plevels_src_1[16];
# 63 "../helpers/kfftspec_outfile.h"
double plevels_src_2[16];
# 66 "../helpers/kfftspec_outfile.h"
uint8_t GPU_device;
# 67 "../helpers/kfftspec_outfile.h"
uint8_t GPU_stream;
# 68 "../helpers/kfftspec_outfile.h"
uint8_t GPU_datasource;
# 69 "../helpers/kfftspec_outfile.h"
uint8_t _struct_padding_1;
# 72 "../helpers/kfftspec_outfile.h"
double timestamp_secondsofday;
# 73 "../helpers/kfftspec_outfile.h"
double timestamp_tm_seconds;
# 74 "../helpers/kfftspec_outfile.h"
char timestamp_str[20];
# 75 "../helpers/kfftspec_outfile.h"
struct tm timestamp_tm;};
# 77 "../helpers/kfftspec_outfile.h"
typedef struct kfftspec_outfile_spectrumheader_v1_tt kfftspec_outfile_spectrumheader_v1_t;
# 32 "../helpers/kvn_dsm.c"
struct _ZN22kvn_dsm_out_streams_tt6bit_ttE {
# 34 "../helpers/kvn_dsm.c"
unsigned char auto_f1: 1;
# 35 "../helpers/kvn_dsm.c"
unsigned char auto_f2: 1;
# 36 "../helpers/kvn_dsm.c"
unsigned char cross_f1f2_re: 1;
# 37 "../helpers/kvn_dsm.c"
unsigned char cross_f1f2_im: 1;
# 38 "../helpers/kvn_dsm.c"
unsigned char auto_f3: 1;
# 39 "../helpers/kvn_dsm.c"
unsigned char auto_f4: 1;
# 40 "../helpers/kvn_dsm.c"
unsigned char cross_f3f4_re: 1;
# 41 "../helpers/kvn_dsm.c"
unsigned char cross_f3f4_im: 1;
# 43 "../helpers/kvn_dsm.c"
unsigned char auto_f5: 1;
# 44 "../helpers/kvn_dsm.c"
unsigned char auto_f6: 1;
# 45 "../helpers/kvn_dsm.c"
unsigned char cross_f5f6_re: 1;
# 46 "../helpers/kvn_dsm.c"
unsigned char cross_f5f6_im: 1;
# 47 "../helpers/kvn_dsm.c"
unsigned char auto_f7: 1;
# 48 "../helpers/kvn_dsm.c"
unsigned char auto_f8: 1;
# 49 "../helpers/kvn_dsm.c"
unsigned char cross_f7f8_re: 1;
# 50 "../helpers/kvn_dsm.c"
unsigned char cross_f7f8_im: 1;};
# 30 "../helpers/kvn_dsm.c"
union kvn_dsm_out_streams_tt {
# 31 "../helpers/kvn_dsm.c"
uint16_t word16;
# 51 "../helpers/kvn_dsm.c"
struct _ZN22kvn_dsm_out_streams_tt6bit_ttE bit;};
# 52 "../helpers/kvn_dsm.c"
typedef union kvn_dsm_out_streams_tt kvn_dsm_out_streams_t;
# 153 "/usr/include/bits/mathinline.h" 3
union _ZZ10__signbitlEUt_ {
# 153 "/usr/include/bits/mathinline.h" 3
long double __l;
# 153 "/usr/include/bits/mathinline.h" 3
int __i[3];};
# 73 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) __attribute__((__nothrow__)) void *memcpy(void *__restrict__, const void *__restrict__, size_t);
# 72 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) __attribute__((__nothrow__)) void *memset(void *, int, size_t);
# 135 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) int fprintf(FILE *__restrict__, const char *__restrict__, ...);
# 129 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) int printf(const char *__restrict__, ...);
# 138 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
extern __attribute__((device)) __attribute__((__malloc__)) __attribute__((__nothrow__)) void *malloc(size_t);
# 171 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
extern __attribute__((device)) __attribute__((__nothrow__)) __attribute__((__noreturn__)) void __assert_fail(const char *, const char *, unsigned, const char *);
# 315 "/usr/local/cuda-8.0/bin/..//include/math_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) unsigned umax(unsigned, unsigned);
# 317 "/usr/local/cuda-8.0/bin/..//include/math_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) unsigned long long ullmax(unsigned long long, unsigned long long);
# 466 "/usr/local/cuda-8.0/bin/..//include/device_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) __attribute__((__nothrow__)) float __cosf(float);
# 127 "/usr/include/bits/mathinline.h" 3
 __attribute__((device_builtin)) extern __attribute__((device)) __inline__ __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__nothrow__)) __attribute__((__const__)) int __signbitf(float);
# 139 "/usr/include/bits/mathinline.h" 3
 __attribute__((device_builtin)) extern __attribute__((device)) __inline__ __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__nothrow__)) __attribute__((__const__)) int __signbit(double);
# 151 "/usr/include/bits/mathinline.h" 3
 __attribute__((device_builtin)) extern __attribute__((device)) __inline__ __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__nothrow__)) __attribute__((__const__)) int __signbitl(long double);
# 939 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
static __attribute__((device)) __inline__ unsigned long _Z3maxmm(unsigned long, unsigned long);
# 168 "/usr/local/cuda-8.0/bin/..//include/device_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) void __syncthreads(void);
# 82 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) float __fAtomicAdd(float *, float);
# 76 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
static __attribute__((device)) __inline__ float _Z9atomicAddPff(float *, float);
# 34 "../kernels/cuda/cuda_utils.cu"
extern __attribute__((device)) struct float4 _Zpl6float4S_(const struct float4, const struct float4);
# 40 "../kernels/cuda/cuda_utils.cu"
extern __attribute__((device)) struct float4 _Zmi6float4S_(const struct float4, const struct float4);
# 46 "../kernels/cuda/cuda_utils.cu"
extern __attribute__((device)) struct float4 _Zml6float4S_(const struct float4, const struct float4);
# 20 "../kernels/cuda/arithmetic_xmac_kernels.cu"
extern __attribute__((device)) struct float4 *_ZdVR6float4f(struct float4 *, const float);
# 26 "../kernels/cuda/arithmetic_xmac_kernels.cu"
extern __attribute__((device)) struct float2 *_ZdVR6float2f(struct float2 *, const float);
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
extern __attribute__((device)) void _Z40cu_autoPowerSpectrum_cufftCallbackStoreCPvm6float2S_S_(void *, size_t, cufftComplex, void *, void *);
# 108 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
extern __attribute__((device)) cufftReal _Z33cu_window_hann_cufftCallbackLoadRPvmS_S_(void *, size_t, void *, void *);
# 120 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
extern __attribute__((device)) cufftReal _Z36cu_window_hamming_cufftCallbackLoadRPvmS_S_(void *, size_t, void *, void *);
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z24cu_decode_2bit1ch_1D_v1bPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, size_t, size_t);
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z23cu_decode_2bit1ch_1D_v2PKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t);
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z30cu_decode_2bit1ch_1D_mark5b_v2PKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, size_t);
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z23cu_decode_2bit1ch_1D_v3PKhP6float4mm(const uint8_t *, struct float4 *, size_t, size_t);
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z23cu_decode_2bit1ch_1D_v4PKhP6float4mm(const uint8_t *, struct float4 *, size_t, size_t);
# 217 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z17cu_decode_2bit1chPKhP6float4m(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t);
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z17cu_decode_2bit2chPKhP6float2S2_m(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t);
# 244 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z17cu_decode_2bit4chPKhPfS1_S1_S1_m(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t);
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z22cu_decode_2bit1ch_HannPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z22cu_decode_2bit2ch_HannPKhP6float2S2_mm(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t);
# 318 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z22cu_decode_2bit4ch_HannPKhPfS1_S1_S1_mm(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t);
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z25cu_decode_2bit1ch_HammingPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z25cu_decode_2bit2ch_HammingPKhP6float2S2_mm(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t);
# 385 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z25cu_decode_2bit4ch_HammingPKhPfS1_S1_S1_mm(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t);
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z30cu_decode_2bit1ch_CustomWindowPKhP6float4mmPKS1_(const uint8_t *, struct float4 *, const size_t, const size_t, const struct float4 *__restrict__);
# 427 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z30cu_decode_2bit2ch_CustomWindowPKhP6float2S2_mmPKS1_(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t, const size_t, const struct float2 *__restrict__);
# 442 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) extern void _Z30cu_decode_2bit4ch_CustomWindowPKhPfS1_S1_S1_mmPKf(const uint8_t *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, float *__restrict__, const size_t, const size_t, const float *__restrict__);
# 39 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
__attribute__((global)) extern void _Z23cu_decode_2bit2ch_splitPKhP6float2S2_m(const uint8_t *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t);
# 92 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) extern void _Z17cu_decode_3bit1chPKhP6float4m(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t);
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) extern void _Z23cu_decode_3bit2ch_splitPKhP6float4S2_m(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t);
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) extern void _Z22cu_decode_3bit1ch_HannPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) extern void _Z25cu_decode_3bit1ch_HammingPKhP6float4mm(const uint8_t *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) extern void _Z28cu_decode_3bit2ch_Hann_splitPKhP6float4S2_mm(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) extern void _Z31cu_decode_3bit2ch_Hamming_splitPKhP6float4S2_mm(const uint8_t *__restrict__, struct float4 *__restrict__, struct float4 *__restrict__, const size_t, const size_t);
# 28 "../kernels/cuda/memory_split_kernels.cu"
__attribute__((global)) extern void _Z21cu_split_2chan_f4_2f2PK6float4P6float2S3_m(const struct float4 *__restrict__, struct float2 *__restrict__, struct float2 *__restrict__, const size_t);
# 48 "../kernels/cuda/memory_split_kernels.cu"
__attribute__((global)) extern void _Z20cu_split_2chan_f2_2fPK6float2PfS2_m(const struct float2 *__restrict__, float *__restrict__, float *__restrict__, const size_t);
# 32 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) extern void _Z18cu_accumulate_2polPK6float2S1_P6float4mm(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float4 *__restrict__, size_t, size_t);
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) extern void _Z25cu_accumulate_2pol_packedPK6float4PS_mm(const struct float4 *__restrict__, struct float4 *__restrict__, size_t, size_t);
# 130 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) extern void _Z27cu_accumulate_2pol_autoOnlyPK6float2S1_PS_mm(const struct float2 *__restrict__, const struct float2 *__restrict__, struct float2 *__restrict__, size_t, size_t);
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) extern void _Z34cu_accumulate_2pol_autoOnly_packedPK6float4P6float2mm(const struct float4 *__restrict__, struct float2 *__restrict__, size_t, size_t);
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) extern void _Z17autoPowerSpectrumP6float2Pfmm(cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t);
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) extern void _Z20autoPowerSpectrum_v2PK6float2Pfmm(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t);
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) extern void _Z20autoPowerSpectrum_v3PK6float2Pfmm(const cufftComplex *__restrict__, float *__restrict__, const size_t, const size_t);
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) extern void _Z23autoPowerSpectrum_v3_CBPKfPfmm(const cufftReal *__restrict__, float *__restrict__, const size_t, const size_t);
# 56 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__attribute__((global)) extern void _Z14cu_window_hannP6float4mm(struct float4 *, size_t, size_t);
# 82 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__attribute__((global)) extern void _Z17cu_window_hammingP6float4mm(struct float4 *, size_t, size_t);
# 13 "../kernels/cuda/decoder_2b32f_large_lookup.cu"
extern __attribute__((constant)) struct float4 c_lookup_2bit_VDIF[256];
# 32 "../kernels/cuda/decoder_2b32f_kernels.cu"
extern __attribute__((constant)) float c_lut2bit_vdif[4];
# 33 "../kernels/cuda/decoder_2b32f_kernels.cu"
extern __attribute__((constant)) float c_lut2bit_mark5b[4];
# 68 "../kernels/cuda/decoder_3b32f_kernels.cu"
extern __attribute__((constant)) float c_lut3bit[8];
# 123 "../kernels/cuda/arithmetic_autospec_kernels.cu"
extern __attribute__((device)) cufftCallbackStoreC cu_autoPowerSpectrum_cufftCallbackStoreC_ptr;
# 132 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
extern __attribute__((device)) cufftCallbackLoadR cu_window_hann_cufftCallbackLoadR_ptr;
# 133 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
extern __attribute__((device)) cufftCallbackLoadR cu_window_hamming_cufftCallbackLoadR_ptr;
# 1 "/usr/local/cuda-8.0/bin/..//include/common_functions.h" 1
# 249 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
# 1 "/usr/local/cuda-8.0/include/math_functions.h" 1 3
# 10327 "/usr/local/cuda-8.0/include/math_functions.h" 3
# 1 "/usr/local/cuda-8.0/include/math_functions.hpp" 1 3
# 10328 "/usr/local/cuda-8.0/include/math_functions.h" 2 3



# 1 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.h" 1 3
# 270 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.h" 3
# 1 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.hpp" 1 3
# 271 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.h" 2 3
# 10332 "/usr/local/cuda-8.0/include/math_functions.h" 2 3
# 250 "/usr/local/cuda-8.0/bin/..//include/common_functions.h" 2
# 135 "../kernels/cuda/arithmetic_winfunc_kernels.cu" 2
# 13 "../kernels/cuda/decoder_2b32f_large_lookup.cu"
 __attribute__((constant)) struct float4 c_lookup_2bit_VDIF[256] = {{(-3.335900068F),(-3.335900068F),(-3.335900068F),(-3.335900068F)},{(-1.0F),(-3.335900068F),(-3.335900068F),(-3.335900068F)},{(1.0F),(-3.335900068F),(-3.335900068F),(-3.335900068F)},{(3.335900068F),(-3.335900068F),(-3.335900068F),(-3.335900068F)},{(-3.335900068F),(-1.0F),(-3.335900068F),(-3.335900068F)},{(-1.0F),(-1.0F),(-3.335900068F),(-3.335900068F)},{(1.0F),(-1.0F),(-3.335900068F),(-3.335900068F)},{(3.335900068F),(-1.0F),(-3.335900068F),(-3.335900068F)},{(-3.335900068F),(1.0F),(-3.335900068F),(-3.335900068F)},{(-1.0F),(1.0F),(-3.335900068F),(-3.335900068F)},{(1.0F),(1.0F),(-3.335900068F),(-3.335900068F)},{(3.335900068F),(1.0F),(-3.335900068F),(-3.335900068F)},{(-3.335900068F),(3.335900068F),(-3.335900068F),(-3.335900068F)},{(-1.0F),(3.335900068F),(-3.335900068F),(-3.335900068F)},{(1.0F),(3.335900068F),(-3.335900068F),(-3.335900068F)},{(3.335900068F),(3.335900068F),(-3.335900068F),(-3.335900068F)},{(-3.335900068F),(-3.335900068F),(-1.0F),(-3.335900068F)},{(-1.0F),(-3.335900068F),(-1.0F),(-3.335900068F)},{(1.0F),(-3.335900068F),(-1.0F),(-3.335900068F)},{(3.335900068F),(-3.335900068F),(-1.0F),(-3.335900068F)},{(-3.335900068F),(-1.0F),(-1.0F),(-3.335900068F)},{(-1.0F),(-1.0F),(-1.0F),(-3.335900068F)},{(1.0F),(-1.0F),(-1.0F),(-3.335900068F)},{(3.335900068F),(-1.0F),(-1.0F),(-3.335900068F)},{(-3.335900068F),(1.0F),(-1.0F),(-3.335900068F)},{(-1.0F),(1.0F),(-1.0F),(-3.335900068F)},{(1.0F),(1.0F),(-1.0F),(-3.335900068F)},{(3.335900068F),(1.0F),(-1.0F),(-3.335900068F)},{(-3.335900068F),(3.335900068F),(-1.0F),(-3.335900068F)},{(-1.0F),(3.335900068F),(-1.0F),(-3.335900068F)},{(1.0F),(3.335900068F),(-1.0F),(-3.335900068F)},{(3.335900068F),(3.335900068F),(-1.0F),(-3.335900068F)},{(-3.335900068F),(-3.335900068F),(1.0F),(-3.335900068F)},{(-1.0F),(-3.335900068F),(1.0F),(-3.335900068F)},{(1.0F),(-3.335900068F),(1.0F),(-3.335900068F)},{(3.335900068F),(-3.335900068F),(1.0F),(-3.335900068F)},{(-3.335900068F),(-1.0F),(1.0F),(-3.335900068F)},{(-1.0F),(-1.0F),(1.0F),(-3.335900068F)},{(1.0F),(-1.0F),(1.0F),(-3.335900068F)},{(3.335900068F),(-1.0F),(1.0F),(-3.335900068F)},{(-3.335900068F),(1.0F),(1.0F),(-3.335900068F)},{(-1.0F),(1.0F),(1.0F),(-3.335900068F)},{(1.0F),(1.0F),(1.0F),(-3.335900068F)},{(3.335900068F),(1.0F),(1.0F),(-3.335900068F)},{(-3.335900068F),(3.335900068F),(1.0F),(-3.335900068F)},{(-1.0F),(3.335900068F),(1.0F),(-3.335900068F)},{(1.0F),(3.335900068F),(1.0F),(-3.335900068F)},{(3.335900068F),(3.335900068F),(1.0F),(-3.335900068F)},{(-3.335900068F),(-3.335900068F),(3.335900068F),(-3.335900068F)},{(-1.0F),(-3.335900068F),(3.335900068F),(-3.335900068F)},{(1.0F),(-3.335900068F),(3.335900068F),(-3.335900068F)},{(3.335900068F),(-3.335900068F),(3.335900068F),(-3.335900068F)},{(-3.335900068F),(-1.0F),(3.335900068F),(-3.335900068F)},{(-1.0F),(-1.0F),(3.335900068F),(-3.335900068F)},{(1.0F),(-1.0F),(3.335900068F),(-3.335900068F)},{(3.335900068F),(-1.0F),(3.335900068F),(-3.335900068F)},{(-3.335900068F),(1.0F),(3.335900068F),(-3.335900068F)},{(-1.0F),(1.0F),(3.335900068F),(-3.335900068F)},{(1.0F),(1.0F),(3.335900068F),(-3.335900068F)},{(3.335900068F),(1.0F),(3.335900068F),(-3.335900068F)},{(-3.335900068F),(3.335900068F),(3.335900068F),(-3.335900068F)},{(-1.0F),(3.335900068F),(3.335900068F),(-3.335900068F)},{(1.0F),(3.335900068F),(3.335900068F),(-3.335900068F)},{(3.335900068F),(3.335900068F),(3.335900068F),(-3.335900068F)},{(-3.335900068F),(-3.335900068F),(-3.335900068F),(-1.0F)},{(-1.0F),(-3.335900068F),(-3.335900068F),(-1.0F)},{(1.0F),(-3.335900068F),(-3.335900068F),(-1.0F)},{(3.335900068F),(-3.335900068F),(-3.335900068F),(-1.0F)},{(-3.335900068F),(-1.0F),(-3.335900068F),(-1.0F)},{(-1.0F),(-1.0F),(-3.335900068F),(-1.0F)},{(1.0F),(-1.0F),(-3.335900068F),(-1.0F)},{(3.335900068F),(-1.0F),(-3.335900068F),(-1.0F)},{(-3.335900068F),(1.0F),(-3.335900068F),(-1.0F)},{(-1.0F),(1.0F),(-3.335900068F),(-1.0F)},{(1.0F),(1.0F),(-3.335900068F),(-1.0F)},{(3.335900068F),(1.0F),(-3.335900068F),(-1.0F)},{(-3.335900068F),(3.335900068F),(-3.335900068F),(-1.0F)},{(-1.0F),(3.335900068F),(-3.335900068F),(-1.0F)},{(1.0F),(3.335900068F),(-3.335900068F),(-1.0F)},{(3.335900068F),(3.335900068F),(-3.335900068F),(-1.0F)},{(-3.335900068F),(-3.335900068F),(-1.0F),(-1.0F)},{(-1.0F),(-3.335900068F),(-1.0F),(-1.0F)},{(1.0F),(-3.335900068F),(-1.0F),(-1.0F)},{(3.335900068F),(-3.335900068F),(-1.0F),(-1.0F)},{(-3.335900068F),(-1.0F),(-1.0F),(-1.0F)},{(-1.0F),(-1.0F),(-1.0F),(-1.0F)},{(1.0F),(-1.0F),(-1.0F),(-1.0F)},{(3.335900068F),(-1.0F),(-1.0F),(-1.0F)},{(-3.335900068F),(1.0F),(-1.0F),(-1.0F)},{(-1.0F),(1.0F),(-1.0F),(-1.0F)},{(1.0F),(1.0F),(-1.0F),(-1.0F)},{(3.335900068F),(1.0F),(-1.0F),(-1.0F)},{(-3.335900068F),(3.335900068F),(-1.0F),(-1.0F)},{(-1.0F),(3.335900068F),(-1.0F),(-1.0F)},{(1.0F),(3.335900068F),(-1.0F),(-1.0F)},{(3.335900068F),(3.335900068F),(-1.0F),(-1.0F)},{(-3.335900068F),(-3.335900068F),(1.0F),(-1.0F)},{(-1.0F),(-3.335900068F),(1.0F),(-1.0F)},{(1.0F),(-3.335900068F),(1.0F),(-1.0F)},{(3.335900068F),(-3.335900068F),(1.0F),(-1.0F)},{(-3.335900068F),(-1.0F),(1.0F),(-1.0F)},{(-1.0F),(-1.0F),(1.0F),(-1.0F)},{(1.0F),(-1.0F),(1.0F),(-1.0F)},{(3.335900068F),(-1.0F),(1.0F),(-1.0F)},{(-3.335900068F),(1.0F),(1.0F),(-1.0F)},{(-1.0F),(1.0F),(1.0F),(-1.0F)},{(1.0F),(1.0F),(1.0F),(-1.0F)},{(3.335900068F),(1.0F),(1.0F),(-1.0F)},{(-3.335900068F),(3.335900068F),(1.0F),(-1.0F)},{(-1.0F),(3.335900068F),(1.0F),(-1.0F)},{(1.0F),(3.335900068F),(1.0F),(-1.0F)},{(3.335900068F),(3.335900068F),(1.0F),(-1.0F)},{(-3.335900068F),(-3.335900068F),(3.335900068F),(-1.0F)},{(-1.0F),(-3.335900068F),(3.335900068F),(-1.0F)},{(1.0F),(-3.335900068F),(3.335900068F),(-1.0F)},{(3.335900068F),(-3.335900068F),(3.335900068F),(-1.0F)},{(-3.335900068F),(-1.0F),(3.335900068F),(-1.0F)},{(-1.0F),(-1.0F),(3.335900068F),(-1.0F)},{(1.0F),(-1.0F),(3.335900068F),(-1.0F)},{(3.335900068F),(-1.0F),(3.335900068F),(-1.0F)},{(-3.335900068F),(1.0F),(3.335900068F),(-1.0F)},{(-1.0F),(1.0F),(3.335900068F),(-1.0F)},{(1.0F),(1.0F),(3.335900068F),(-1.0F)},{(3.335900068F),(1.0F),(3.335900068F),(-1.0F)},{(-3.335900068F),(3.335900068F),(3.335900068F),(-1.0F)},{(-1.0F),(3.335900068F),(3.335900068F),(-1.0F)},{(1.0F),(3.335900068F),(3.335900068F),(-1.0F)},{(3.335900068F),(3.335900068F),(3.335900068F),(-1.0F)},{(-3.335900068F),(-3.335900068F),(-3.335900068F),(1.0F)},{(-1.0F),(-3.335900068F),(-3.335900068F),(1.0F)},{(1.0F),(-3.335900068F),(-3.335900068F),(1.0F)},{(3.335900068F),(-3.335900068F),(-3.335900068F),(1.0F)},{(-3.335900068F),(-1.0F),(-3.335900068F),(1.0F)},{(-1.0F),(-1.0F),(-3.335900068F),(1.0F)},{(1.0F),(-1.0F),(-3.335900068F),(1.0F)},{(3.335900068F),(-1.0F),(-3.335900068F),(1.0F)},{(-3.335900068F),(1.0F),(-3.335900068F),(1.0F)},{(-1.0F),(1.0F),(-3.335900068F),(1.0F)},{(1.0F),(1.0F),(-3.335900068F),(1.0F)},{(3.335900068F),(1.0F),(-3.335900068F),(1.0F)},{(-3.335900068F),(3.335900068F),(-3.335900068F),(1.0F)},{(-1.0F),(3.335900068F),(-3.335900068F),(1.0F)},{(1.0F),(3.335900068F),(-3.335900068F),(1.0F)},{(3.335900068F),(3.335900068F),(-3.335900068F),(1.0F)},{(-3.335900068F),(-3.335900068F),(-1.0F),(1.0F)},{(-1.0F),(-3.335900068F),(-1.0F),(1.0F)},{(1.0F),(-3.335900068F),(-1.0F),(1.0F)},{(3.335900068F),(-3.335900068F),(-1.0F),(1.0F)},{(-3.335900068F),(-1.0F),(-1.0F),(1.0F)},{(-1.0F),(-1.0F),(-1.0F),(1.0F)},{(1.0F),(-1.0F),(-1.0F),(1.0F)},{(3.335900068F),(-1.0F),(-1.0F),(1.0F)},{(-3.335900068F),(1.0F),(-1.0F),(1.0F)},{(-1.0F),(1.0F),(-1.0F),(1.0F)},{(1.0F),(1.0F),(-1.0F),(1.0F)},{(3.335900068F),(1.0F),(-1.0F),(1.0F)},{(-3.335900068F),(3.335900068F),(-1.0F),(1.0F)},{(-1.0F),(3.335900068F),(-1.0F),(1.0F)},{(1.0F),(3.335900068F),(-1.0F),(1.0F)},{(3.335900068F),(3.335900068F),(-1.0F),(1.0F)},{(-3.335900068F),(-3.335900068F),(1.0F),(1.0F)},{(-1.0F),(-3.335900068F),(1.0F),(1.0F)},{(1.0F),(-3.335900068F),(1.0F),(1.0F)},{(3.335900068F),(-3.335900068F),(1.0F),(1.0F)},{(-3.335900068F),(-1.0F),(1.0F),(1.0F)},{(-1.0F),(-1.0F),(1.0F),(1.0F)},{(1.0F),(-1.0F),(1.0F),(1.0F)},{(3.335900068F),(-1.0F),(1.0F),(1.0F)},{(-3.335900068F),(1.0F),(1.0F),(1.0F)},{(-1.0F),(1.0F),(1.0F),(1.0F)},{(1.0F),(1.0F),(1.0F),(1.0F)},{(3.335900068F),(1.0F),(1.0F),(1.0F)},{(-3.335900068F),(3.335900068F),(1.0F),(1.0F)},{(-1.0F),(3.335900068F),(1.0F),(1.0F)},{(1.0F),(3.335900068F),(1.0F),(1.0F)},{(3.335900068F),(3.335900068F),(1.0F),(1.0F)},{(-3.335900068F),(-3.335900068F),(3.335900068F),(1.0F)},{(-1.0F),(-3.335900068F),(3.335900068F),(1.0F)},{(1.0F),(-3.335900068F),(3.335900068F),(1.0F)},{(3.335900068F),(-3.335900068F),(3.335900068F),(1.0F)},{(-3.335900068F),(-1.0F),(3.335900068F),(1.0F)},{(-1.0F),(-1.0F),(3.335900068F),(1.0F)},{(1.0F),(-1.0F),(3.335900068F),(1.0F)},{(3.335900068F),(-1.0F),(3.335900068F),(1.0F)},{(-3.335900068F),(1.0F),(3.335900068F),(1.0F)},{(-1.0F),(1.0F),(3.335900068F),(1.0F)},{(1.0F),(1.0F),(3.335900068F),(1.0F)},{(3.335900068F),(1.0F),(3.335900068F),(1.0F)},{(-3.335900068F),(3.335900068F),(3.335900068F),(1.0F)},{(-1.0F),(3.335900068F),(3.335900068F),(1.0F)},{(1.0F),(3.335900068F),(3.335900068F),(1.0F)},{(3.335900068F),(3.335900068F),(3.335900068F),(1.0F)},{(-3.335900068F),(-3.335900068F),(-3.335900068F),(3.335900068F)},{(-1.0F),(-3.335900068F),(-3.335900068F),(3.335900068F)},{(1.0F),(-3.335900068F),(-3.335900068F),(3.335900068F)},{(3.335900068F),(-3.335900068F),(-3.335900068F),(3.335900068F)},{(-3.335900068F),(-1.0F),(-3.335900068F),(3.335900068F)},{(-1.0F),(-1.0F),(-3.335900068F),(3.335900068F)},{(1.0F),(-1.0F),(-3.335900068F),(3.335900068F)},{(3.335900068F),(-1.0F),(-3.335900068F),(3.335900068F)},{(-3.335900068F),(1.0F),(-3.335900068F),(3.335900068F)},{(-1.0F),(1.0F),(-3.335900068F),(3.335900068F)},{(1.0F),(1.0F),(-3.335900068F),(3.335900068F)},{(3.335900068F),(1.0F),(-3.335900068F),(3.335900068F)},{(-3.335900068F),(3.335900068F),(-3.335900068F),(3.335900068F)},{(-1.0F),(3.335900068F),(-3.335900068F),(3.335900068F)},{(1.0F),(3.335900068F),(-3.335900068F),(3.335900068F)},{(3.335900068F),(3.335900068F),(-3.335900068F),(3.335900068F)},{(-3.335900068F),(-3.335900068F),(-1.0F),(3.335900068F)},{(-1.0F),(-3.335900068F),(-1.0F),(3.335900068F)},{(1.0F),(-3.335900068F),(-1.0F),(3.335900068F)},{(3.335900068F),(-3.335900068F),(-1.0F),(3.335900068F)},{(-3.335900068F),(-1.0F),(-1.0F),(3.335900068F)},{(-1.0F),(-1.0F),(-1.0F),(3.335900068F)},{(1.0F),(-1.0F),(-1.0F),(3.335900068F)},{(3.335900068F),(-1.0F),(-1.0F),(3.335900068F)},{(-3.335900068F),(1.0F),(-1.0F),(3.335900068F)},{(-1.0F),(1.0F),(-1.0F),(3.335900068F)},{(1.0F),(1.0F),(-1.0F),(3.335900068F)},{(3.335900068F),(1.0F),(-1.0F),(3.335900068F)},{(-3.335900068F),(3.335900068F),(-1.0F),(3.335900068F)},{(-1.0F),(3.335900068F),(-1.0F),(3.335900068F)},{(1.0F),(3.335900068F),(-1.0F),(3.335900068F)},{(3.335900068F),(3.335900068F),(-1.0F),(3.335900068F)},{(-3.335900068F),(-3.335900068F),(1.0F),(3.335900068F)},{(-1.0F),(-3.335900068F),(1.0F),(3.335900068F)},{(1.0F),(-3.335900068F),(1.0F),(3.335900068F)},{(3.335900068F),(-3.335900068F),(1.0F),(3.335900068F)},{(-3.335900068F),(-1.0F),(1.0F),(3.335900068F)},{(-1.0F),(-1.0F),(1.0F),(3.335900068F)},{(1.0F),(-1.0F),(1.0F),(3.335900068F)},{(3.335900068F),(-1.0F),(1.0F),(3.335900068F)},{(-3.335900068F),(1.0F),(1.0F),(3.335900068F)},{(-1.0F),(1.0F),(1.0F),(3.335900068F)},{(1.0F),(1.0F),(1.0F),(3.335900068F)},{(3.335900068F),(1.0F),(1.0F),(3.335900068F)},{(-3.335900068F),(3.335900068F),(1.0F),(3.335900068F)},{(-1.0F),(3.335900068F),(1.0F),(3.335900068F)},{(1.0F),(3.335900068F),(1.0F),(3.335900068F)},{(3.335900068F),(3.335900068F),(1.0F),(3.335900068F)},{(-3.335900068F),(-3.335900068F),(3.335900068F),(3.335900068F)},{(-1.0F),(-3.335900068F),(3.335900068F),(3.335900068F)},{(1.0F),(-3.335900068F),(3.335900068F),(3.335900068F)},{(3.335900068F),(-3.335900068F),(3.335900068F),(3.335900068F)},{(-3.335900068F),(-1.0F),(3.335900068F),(3.335900068F)},{(-1.0F),(-1.0F),(3.335900068F),(3.335900068F)},{(1.0F),(-1.0F),(3.335900068F),(3.335900068F)},{(3.335900068F),(-1.0F),(3.335900068F),(3.335900068F)},{(-3.335900068F),(1.0F),(3.335900068F),(3.335900068F)},{(-1.0F),(1.0F),(3.335900068F),(3.335900068F)},{(1.0F),(1.0F),(3.335900068F),(3.335900068F)},{(3.335900068F),(1.0F),(3.335900068F),(3.335900068F)},{(-3.335900068F),(3.335900068F),(3.335900068F),(3.335900068F)},{(-1.0F),(3.335900068F),(3.335900068F),(3.335900068F)},{(1.0F),(3.335900068F),(3.335900068F),(3.335900068F)},{(3.335900068F),(3.335900068F),(3.335900068F),(3.335900068F)}};
# 32 "../kernels/cuda/decoder_2b32f_kernels.cu"
 __attribute__((constant)) float c_lut2bit_vdif[4] = {(-3.335900068F),(-1.0F),(1.0F),(3.335900068F)};
# 33 "../kernels/cuda/decoder_2b32f_kernels.cu"
 __attribute__((constant)) float c_lut2bit_mark5b[4] = {(-3.335900068F),(1.0F),(-1.0F),(3.335900068F)};
# 68 "../kernels/cuda/decoder_3b32f_kernels.cu"
 __attribute__((constant)) float c_lut3bit[8] = {(-7.0F),(-5.0F),(-1.0F),(-3.0F),(7.0F),(5.0F),(1.0F),(3.0F)};
# 123 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 __attribute__((device)) cufftCallbackStoreC cu_autoPowerSpectrum_cufftCallbackStoreC_ptr = _Z40cu_autoPowerSpectrum_cufftCallbackStoreCPvm6float2S_S_;
# 132 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 __attribute__((device)) cufftCallbackLoadR cu_window_hann_cufftCallbackLoadR_ptr = _Z33cu_window_hann_cufftCallbackLoadRPvmS_S_;
# 133 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 __attribute__((device)) cufftCallbackLoadR cu_window_hamming_cufftCallbackLoadR_ptr = _Z36cu_window_hamming_cufftCallbackLoadRPvmS_S_;
# 939 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
static __attribute__((device)) __inline__ unsigned long _Z3maxmm(
# 939 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
unsigned long a,
# 939 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
unsigned long b){
# 940 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
{
# 944 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
if (0)
# 944 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
{
# 948 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
return (unsigned long)(umax(((unsigned)a), ((unsigned)b)));
# 949 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
}
# 949 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
else
# 949 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
{
# 950 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
return (unsigned long)(ullmax(((unsigned long long)a), ((unsigned long long)b)));
# 951 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
}
# 952 "/usr/local/cuda-8.0/bin/..//include/math_functions.hpp"
}}
# 76 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
static __attribute__((device)) __inline__ float _Z9atomicAddPff(
# 76 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
float *address,
# 76 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
float val){
# 77 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
{
# 78 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
return __fAtomicAdd(address, val);
# 79 "/usr/local/cuda-8.0/bin/..//include/sm_20_atomic_functions.hpp"
}}
# 34 "../kernels/cuda/cuda_utils.cu"
 __attribute__((device)) struct float4 _Zpl6float4S_(
# 34 "../kernels/cuda/cuda_utils.cu"
const struct float4 lhs,
# 34 "../kernels/cuda/cuda_utils.cu"
const struct float4 rhs){
# 35 "../kernels/cuda/cuda_utils.cu"
{
# 36 "../kernels/cuda/cuda_utils.cu"
 struct float4 __cuda_local_var_19459_12_non_const_res;
# 36 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19459_12_non_const_res.x) = ((lhs.x) + (rhs.x));
# 36 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19459_12_non_const_res.y) = ((lhs.y) + (rhs.y));
# 36 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19459_12_non_const_res.z) = ((lhs.z) + (rhs.z));
# 36 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19459_12_non_const_res.w) = ((lhs.w) + (rhs.w));
# 37 "../kernels/cuda/cuda_utils.cu"
return __cuda_local_var_19459_12_non_const_res;
# 38 "../kernels/cuda/cuda_utils.cu"
}}
# 40 "../kernels/cuda/cuda_utils.cu"
 __attribute__((device)) struct float4 _Zmi6float4S_(
# 40 "../kernels/cuda/cuda_utils.cu"
const struct float4 lhs,
# 40 "../kernels/cuda/cuda_utils.cu"
const struct float4 rhs){
# 41 "../kernels/cuda/cuda_utils.cu"
{
# 42 "../kernels/cuda/cuda_utils.cu"
 struct float4 __cuda_local_var_19465_12_non_const_res;
# 42 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19465_12_non_const_res.x) = ((lhs.x) - (rhs.x));
# 42 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19465_12_non_const_res.y) = ((lhs.y) - (rhs.y));
# 42 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19465_12_non_const_res.z) = ((lhs.z) - (rhs.z));
# 42 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19465_12_non_const_res.w) = ((lhs.w) - (rhs.w));
# 43 "../kernels/cuda/cuda_utils.cu"
return __cuda_local_var_19465_12_non_const_res;
# 44 "../kernels/cuda/cuda_utils.cu"
}}
# 46 "../kernels/cuda/cuda_utils.cu"
 __attribute__((device)) struct float4 _Zml6float4S_(
# 46 "../kernels/cuda/cuda_utils.cu"
const struct float4 lhs,
# 46 "../kernels/cuda/cuda_utils.cu"
const struct float4 rhs){
# 47 "../kernels/cuda/cuda_utils.cu"
{
# 48 "../kernels/cuda/cuda_utils.cu"
 struct float4 __cuda_local_var_19471_12_non_const_res;
# 48 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19471_12_non_const_res.x) = ((lhs.x) * (rhs.x));
# 48 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19471_12_non_const_res.y) = ((lhs.y) * (rhs.y));
# 48 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19471_12_non_const_res.z) = ((lhs.z) * (rhs.z));
# 48 "../kernels/cuda/cuda_utils.cu"
(__cuda_local_var_19471_12_non_const_res.w) = ((lhs.w) * (rhs.w));
# 49 "../kernels/cuda/cuda_utils.cu"
return __cuda_local_var_19471_12_non_const_res;
# 50 "../kernels/cuda/cuda_utils.cu"
}}
# 20 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 __attribute__((device)) struct float4 *_ZdVR6float4f(
# 20 "../kernels/cuda/arithmetic_xmac_kernels.cu"
struct float4 *a,
# 20 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const float f){
# 21 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 22 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(a->x) = ( fdividef((a->x) , f));
# 22 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(a->y) = ( fdividef((a->y) , f));
# 22 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(a->z) = ( fdividef((a->z) , f));
# 22 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(a->w) = ( fdividef((a->w) , f));
# 23 "../kernels/cuda/arithmetic_xmac_kernels.cu"
return a;
# 24 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}}
# 26 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 __attribute__((device)) struct float2 *_ZdVR6float2f(
# 26 "../kernels/cuda/arithmetic_xmac_kernels.cu"
struct float2 *a,
# 26 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const float f){
# 27 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 28 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(a->x) = ( fdividef((a->x) , f));
# 28 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(a->y) = ( fdividef((a->y) , f));
# 29 "../kernels/cuda/arithmetic_xmac_kernels.cu"
return a;
# 30 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}}
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 __attribute__((device)) void _Z40cu_autoPowerSpectrum_cufftCallbackStoreCPvm6float2S_S_(
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void *dataOut,
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
size_t offset,
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
cufftComplex element,
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void *callerInfo,
# 118 "../kernels/cuda/arithmetic_autospec_kernels.cu"
void *sharedPtr){
# 119 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{ cufftComplex __T295;
# 120 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 cufftReal *__cuda_local_var_20900_16_non_const_P;
# 120 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20900_16_non_const_P = ((cufftReal *)dataOut);
# 121 "../kernels/cuda/arithmetic_autospec_kernels.cu"
(__cuda_local_var_20900_16_non_const_P[offset]) = ((__T295 = element) , (((__T295.x) * (__T295.x)) + ((__T295.y) * (__T295.y))));
# 122 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}}
# 108 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 __attribute__((device)) cufftReal _Z33cu_window_hann_cufftCallbackLoadRPvmS_S_(
# 108 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void *dataIn,
# 108 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
size_t offset,
# 108 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void *callerInfo,
# 108 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void *sharedPointer){
# 109 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{
# 110 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 cu_window_cb_params_t *__cuda_local_var_20975_32_non_const_my_params;
# 111 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 size_t __cuda_local_var_20976_16_non_const_n;
# 112 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 cufftReal __cuda_local_var_20977_19_non_const_omega;
# 113 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 cufftReal __cuda_local_var_20978_19_non_const_w;
# 110 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20975_32_non_const_my_params = ((cu_window_cb_params_t *)callerInfo);
# 111 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20976_16_non_const_n = (offset % (__cuda_local_var_20975_32_non_const_my_params->fftlen));
# 112 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20977_19_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)(((__cuda_local_var_20975_32_non_const_my_params->fftlen) - 1UL))))));
# 113 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20978_19_non_const_w = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20977_19_non_const_omega * ((float)__ull2float_rn((unsigned long long)(__cuda_local_var_20976_16_non_const_n))))))) , (2.0F)));
# 117 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
return __cuda_local_var_20978_19_non_const_w * (((cufftReal *)dataIn)[offset]);
# 118 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}}
# 120 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 __attribute__((device)) cufftReal _Z36cu_window_hamming_cufftCallbackLoadRPvmS_S_(
# 120 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void *dataIn,
# 120 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
size_t offset,
# 120 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void *callerInfo,
# 120 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
void *sharedPointer){
# 121 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{
# 122 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 cu_window_cb_params_t *__cuda_local_var_20987_32_non_const_my_params;
# 123 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 size_t __cuda_local_var_20988_16_non_const_n;
# 124 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 cufftReal __cuda_local_var_20989_19_non_const_omega;
# 125 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 cufftReal __cuda_local_var_20990_19_non_const_w;
# 122 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20987_32_non_const_my_params = ((cu_window_cb_params_t *)callerInfo);
# 123 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20988_16_non_const_n = (offset % (__cuda_local_var_20987_32_non_const_my_params->fftlen));
# 124 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20989_19_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)(((__cuda_local_var_20987_32_non_const_my_params->fftlen) - 1UL))))));
# 125 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20990_19_non_const_w = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20989_19_non_const_omega * ((float)__ull2float_rn((unsigned long long)(__cuda_local_var_20988_16_non_const_n))))))));
# 129 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
return __cuda_local_var_20990_19_non_const_w * (((cufftReal *)dataIn)[offset]);
# 130 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}}
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z24cu_decode_2bit1ch_1D_v1bPKhP6float4mm(
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t Nbytes,
# 73 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t bytes_per_thread){
# 74 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 75 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20000_16_non_const_i;
# 76 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20001_16_non_const_nthreads;
# 78 "../kernels/cuda/decoder_2b32f_kernels.cu"
  __attribute__((shared)) float __nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit[4];
# 75 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20000_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 76 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20001_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 79 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[0]) = (-3.335900068F);
# 79 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[1]) = (-1.0F);
# 80 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[2]) = (1.0F);
# 80 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[3]) = (3.335900068F); {
# 83 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t nbyte;
# 83 "../kernels/cuda/decoder_2b32f_kernels.cu"
nbyte = 0UL;
# 83 "../kernels/cuda/decoder_2b32f_kernels.cu"
for (; ((nbyte < bytes_per_thread) && (__cuda_local_var_20000_16_non_const_i < Nbytes)); nbyte++)
# 83 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 84 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20009_25_non_const_v;
# 85 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20010_24_non_const_tmp;
# 84 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20009_25_non_const_v = (src[__cuda_local_var_20000_16_non_const_i]);
# 85 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20010_24_non_const_tmp.x) = ((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[((((int)__cuda_local_var_20009_25_non_const_v) >> 0) & 3)]);
# 85 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20010_24_non_const_tmp.y) = ((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[((((int)__cuda_local_var_20009_25_non_const_v) >> 2) & 3)]);
# 85 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20010_24_non_const_tmp.z) = ((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[((((int)__cuda_local_var_20009_25_non_const_v) >> 4) & 3)]);
# 85 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20010_24_non_const_tmp.w) = ((__nv_cuda_svar_55__ZZ24cu_decode_2bit1ch_1D_v1bPKhP6float4mmE10sh_lut2bit_10_sh_lut2bit)[((((int)__cuda_local_var_20009_25_non_const_v) >> 6) & 3)]);
# 91 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20000_16_non_const_i]) = __cuda_local_var_20010_24_non_const_tmp;
# 92 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20000_16_non_const_i += __cuda_local_var_20001_16_non_const_nthreads;
# 93 "../kernels/cuda/decoder_2b32f_kernels.cu"
} }
# 94 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 95 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z23cu_decode_2bit1ch_1D_v2PKhP6float4mm(
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 104 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t dummy){
# 105 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 106 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20024_16_non_const_i;
# 112 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20030_17_non_const_v;
# 113 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20031_9_non_const_tmp;
# 106 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20024_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 110 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20024_16_non_const_i >= Nbytes)
# 110 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 110 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 110 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 112 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20030_17_non_const_v = (src[__cuda_local_var_20024_16_non_const_i]);
# 113 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20031_9_non_const_tmp.x) = (c_lut2bit_vdif[((((int)__cuda_local_var_20030_17_non_const_v) >> 0) & 3)]);
# 113 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20031_9_non_const_tmp.y) = (c_lut2bit_vdif[((((int)__cuda_local_var_20030_17_non_const_v) >> 2) & 3)]);
# 113 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20031_9_non_const_tmp.z) = (c_lut2bit_vdif[((((int)__cuda_local_var_20030_17_non_const_v) >> 4) & 3)]);
# 113 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20031_9_non_const_tmp.w) = (c_lut2bit_vdif[((((int)__cuda_local_var_20030_17_non_const_v) >> 6) & 3)]);
# 119 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20024_16_non_const_i]) = __cuda_local_var_20031_9_non_const_tmp;
# 121 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 122 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z30cu_decode_2bit1ch_1D_mark5b_v2PKhP6float4mm(
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 124 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t dummy){
# 125 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 126 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20044_16_non_const_i;
# 130 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20048_17_non_const_v;
# 131 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20049_9_non_const_tmp;
# 126 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20044_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 128 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20044_16_non_const_i >= Nbytes)
# 128 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 128 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 128 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 130 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20048_17_non_const_v = (src[__cuda_local_var_20044_16_non_const_i]);
# 131 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20049_9_non_const_tmp.x) = (c_lut2bit_mark5b[((((int)__cuda_local_var_20048_17_non_const_v) >> 0) & 3)]);
# 131 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20049_9_non_const_tmp.y) = (c_lut2bit_mark5b[((((int)__cuda_local_var_20048_17_non_const_v) >> 2) & 3)]);
# 131 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20049_9_non_const_tmp.z) = (c_lut2bit_mark5b[((((int)__cuda_local_var_20048_17_non_const_v) >> 4) & 3)]);
# 131 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20049_9_non_const_tmp.w) = (c_lut2bit_mark5b[((((int)__cuda_local_var_20048_17_non_const_v) >> 6) & 3)]);
# 137 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20044_16_non_const_i]) = __cuda_local_var_20049_9_non_const_tmp;
# 139 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 140 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z23cu_decode_2bit1ch_1D_v3PKhP6float4mm(
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *src,
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *dst,
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t Nbytes,
# 152 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t bytes_per_thread){
# 153 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 154 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20062_16_non_const_i;
# 155 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20063_16_non_const_nthreads;
# 157 "../kernels/cuda/decoder_2b32f_kernels.cu"
  __attribute__((shared)) struct float4 __nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit[256];
# 162 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20070_16_non_const_sh_i;
# 154 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20062_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 155 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20063_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 162 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20070_16_non_const_sh_i = ((size_t)(8U * (threadIdx.x)));
# 163 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 0UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 0UL)]);
# 164 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 1UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 1UL)]);
# 165 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 2UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 2UL)]);
# 166 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 3UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 3UL)]);
# 167 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 4UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 4UL)]);
# 168 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 5UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 5UL)]);
# 169 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 6UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 6UL)]);
# 170 "../kernels/cuda/decoder_2b32f_kernels.cu"
((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[(__cuda_local_var_20070_16_non_const_sh_i + 7UL)]) = ((c_lookup_2bit_VDIF)[(__cuda_local_var_20070_16_non_const_sh_i + 7UL)]);
# 171 "../kernels/cuda/decoder_2b32f_kernels.cu"
__syncthreads(); {
# 173 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t nbyte;
# 173 "../kernels/cuda/decoder_2b32f_kernels.cu"
nbyte = 0UL;
# 173 "../kernels/cuda/decoder_2b32f_kernels.cu"
for (; ((nbyte < bytes_per_thread) && (__cuda_local_var_20062_16_non_const_i < Nbytes)); nbyte++)
# 173 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 174 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20082_25_non_const_v;
# 174 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20082_25_non_const_v = (src[__cuda_local_var_20062_16_non_const_i]);
# 176 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20062_16_non_const_i]) = ((__nv_cuda_svar_58__ZZ23cu_decode_2bit1ch_1D_v3PKhP6float4mmE14sh_lookup_2bit_14_sh_lookup_2bit)[__cuda_local_var_20082_25_non_const_v]);
# 177 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20062_16_non_const_i += __cuda_local_var_20063_16_non_const_nthreads;
# 178 "../kernels/cuda/decoder_2b32f_kernels.cu"
} }
# 179 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 180 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z23cu_decode_2bit1ch_1D_v4PKhP6float4mm(
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *src,
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *dst,
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t Nbytes,
# 192 "../kernels/cuda/decoder_2b32f_kernels.cu"
size_t bytes_per_thread){
# 193 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 194 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20092_16_non_const_i;
# 195 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20093_16_non_const_nthreads;
# 197 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20095_22_const_c1_c4;
# 198 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20096_22_const_c2_c3;
# 194 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20092_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 195 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20093_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 197 "../kernels/cuda/decoder_2b32f_kernels.cu"
memset((char *)&__cuda_local_var_20095_22_const_c1_c4, 0,sizeof(__cuda_local_var_20095_22_const_c1_c4));
# 197 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20095_22_const_c1_c4.x = (0.555983305F);
# 197 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20095_22_const_c1_c4.y = (0.555983305F);
# 197 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20095_22_const_c1_c4.z = (0.555983305F);
# 197 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20095_22_const_c1_c4.w = (0.555983305F);
# 198 "../kernels/cuda/decoder_2b32f_kernels.cu"
memset((char *)&__cuda_local_var_20096_22_const_c2_c3, 0,sizeof(__cuda_local_var_20096_22_const_c2_c3));
# 198 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20096_22_const_c2_c3.x = (-0.5F);
# 198 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20096_22_const_c2_c3.y = (-0.5F);
# 198 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20096_22_const_c2_c3.z = (-0.5F);
# 198 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20096_22_const_c2_c3.w = (-0.5F); {
# 200 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t nbyte;
# 200 "../kernels/cuda/decoder_2b32f_kernels.cu"
nbyte = 0UL;
# 200 "../kernels/cuda/decoder_2b32f_kernels.cu"
for (; ((nbyte < bytes_per_thread) && (__cuda_local_var_20092_16_non_const_i < Nbytes)); nbyte++)
# 200 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 201 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20099_25_non_const_v;
# 202 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20100_24_non_const_xx;
# 203 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20101_24_non_const_xm1;
# 204 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20102_24_non_const_xm2;
# 205 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20103_24_non_const_xm3;
# 206 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20104_24_non_const_xm2xm3;
# 201 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20099_25_non_const_v = (src[__cuda_local_var_20092_16_non_const_i]);
# 202 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20100_24_non_const_xx.x) = ((float)((((int)__cuda_local_var_20099_25_non_const_v) >> 0) & 3));
# 202 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20100_24_non_const_xx.y) = ((float)((((int)__cuda_local_var_20099_25_non_const_v) >> 2) & 3));
# 202 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20100_24_non_const_xx.z) = ((float)((((int)__cuda_local_var_20099_25_non_const_v) >> 4) & 3));
# 202 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20100_24_non_const_xx.w) = ((float)((((int)__cuda_local_var_20099_25_non_const_v) >> 6) & 3));
# 203 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20101_24_non_const_xm1.x) = ((__cuda_local_var_20100_24_non_const_xx.x) - (1.0F));
# 203 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20101_24_non_const_xm1.y) = ((__cuda_local_var_20100_24_non_const_xx.y) - (1.0F));
# 203 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20101_24_non_const_xm1.z) = ((__cuda_local_var_20100_24_non_const_xx.z) - (1.0F));
# 203 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20101_24_non_const_xm1.w) = ((__cuda_local_var_20100_24_non_const_xx.w) - (1.0F));
# 204 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20102_24_non_const_xm2.x) = ((__cuda_local_var_20100_24_non_const_xx.x) - (2.0F));
# 204 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20102_24_non_const_xm2.y) = ((__cuda_local_var_20100_24_non_const_xx.y) - (2.0F));
# 204 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20102_24_non_const_xm2.z) = ((__cuda_local_var_20100_24_non_const_xx.z) - (2.0F));
# 204 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20102_24_non_const_xm2.w) = ((__cuda_local_var_20100_24_non_const_xx.w) - (2.0F));
# 205 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20103_24_non_const_xm3.x) = ((__cuda_local_var_20100_24_non_const_xx.x) - (3.0F));
# 205 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20103_24_non_const_xm3.y) = ((__cuda_local_var_20100_24_non_const_xx.y) - (3.0F));
# 205 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20103_24_non_const_xm3.z) = ((__cuda_local_var_20100_24_non_const_xx.z) - (3.0F));
# 205 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20103_24_non_const_xm3.w) = ((__cuda_local_var_20100_24_non_const_xx.w) - (3.0F));
# 206 "../kernels/cuda/decoder_2b32f_kernels.cu"
memset((char *)&__cuda_local_var_20104_24_non_const_xm2xm3, 0,sizeof(__cuda_local_var_20104_24_non_const_xm2xm3));
# 206 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20104_24_non_const_xm2xm3 = (_Zml6float4S_(__cuda_local_var_20102_24_non_const_xm2, __cuda_local_var_20103_24_non_const_xm3));
# 207 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20092_16_non_const_i]) = (_Zpl6float4S_((_Zml6float4S_((_Zml6float4S_(((*(struct float4 *)&__cuda_local_var_20095_22_const_c1_c4)), __cuda_local_var_20101_24_non_const_xm1)), __cuda_local_var_20104_24_non_const_xm2xm3)), (_Zml6float4S_(__cuda_local_var_20100_24_non_const_xx, (_Zpl6float4S_((_Zml6float4S_(((*(struct float4 *)&__cuda_local_var_20096_22_const_c2_c3)), __cuda_local_var_20104_24_non_const_xm2xm3)), (_Zml6float4S_(__cuda_local_var_20101_24_non_const_xm1, (_Zpl6float4S_((_Zml6float4S_(((*(struct float4 *)&__cuda_local_var_20095_22_const_c1_c4)), __cuda_local_var_20102_24_non_const_xm2)), (_Zml6float4S_(((*(struct float4 *)&__cuda_local_var_20096_22_const_c2_c3)), __cuda_local_var_20103_24_non_const_xm3))))))))))));
# 208 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20092_16_non_const_i += __cuda_local_var_20093_16_non_const_nthreads;
# 209 "../kernels/cuda/decoder_2b32f_kernels.cu"
} }
# 210 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 211 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 217 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z17cu_decode_2bit1chPKhP6float4m(
# 217 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 217 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 217 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes){
# 218 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 219 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20117_16_non_const_i;
# 221 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20119_17_non_const_v;
# 222 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20120_9_non_const_tmp;
# 219 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20117_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 220 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20117_16_non_const_i >= Nbytes)
# 220 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 220 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 220 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 221 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20119_17_non_const_v = (src[__cuda_local_var_20117_16_non_const_i]);
# 222 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20120_9_non_const_tmp.x) = (c_lut2bit_vdif[((((int)__cuda_local_var_20119_17_non_const_v) >> 0) & 3)]);
# 222 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20120_9_non_const_tmp.y) = (c_lut2bit_vdif[((((int)__cuda_local_var_20119_17_non_const_v) >> 2) & 3)]);
# 222 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20120_9_non_const_tmp.z) = (c_lut2bit_vdif[((((int)__cuda_local_var_20119_17_non_const_v) >> 4) & 3)]);
# 222 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20120_9_non_const_tmp.w) = (c_lut2bit_vdif[((((int)__cuda_local_var_20119_17_non_const_v) >> 6) & 3)]);
# 228 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20117_16_non_const_i]) = __cuda_local_var_20120_9_non_const_tmp;
# 229 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 230 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z17cu_decode_2bit2chPKhP6float2S2_m(
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch0,
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch1,
# 232 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes){
# 233 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 234 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20132_16_non_const_i;
# 236 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20134_17_non_const_v;
# 237 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20135_16_non_const_ch0;
# 238 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20136_16_non_const_ch1;
# 234 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20132_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 235 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20132_16_non_const_i >= Nbytes)
# 235 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 235 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 235 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 236 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20134_17_non_const_v = (src[__cuda_local_var_20132_16_non_const_i]);
# 237 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20135_16_non_const_ch0.x) = (c_lut2bit_vdif[((((int)__cuda_local_var_20134_17_non_const_v) >> 0) & 3)]);
# 237 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20135_16_non_const_ch0.y) = (c_lut2bit_vdif[((((int)__cuda_local_var_20134_17_non_const_v) >> 4) & 3)]);
# 238 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20136_16_non_const_ch1.x) = (c_lut2bit_vdif[((((int)__cuda_local_var_20134_17_non_const_v) >> 2) & 3)]);
# 238 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20136_16_non_const_ch1.y) = (c_lut2bit_vdif[((((int)__cuda_local_var_20134_17_non_const_v) >> 6) & 3)]);
# 239 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20132_16_non_const_i]) = __cuda_local_var_20135_16_non_const_ch0;
# 240 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20132_16_non_const_i]) = __cuda_local_var_20136_16_non_const_ch1;
# 241 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 242 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 244 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z17cu_decode_2bit4chPKhPfS1_S1_S1_m(
# 244 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 245 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch0,
# 245 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch1,
# 246 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch2,
# 246 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch3,
# 247 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes){
# 248 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 249 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20147_16_non_const_i;
# 251 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20149_17_non_const_v;
# 252 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20150_15_non_const_ch0;
# 253 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20151_15_non_const_ch1;
# 254 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20152_15_non_const_ch2;
# 255 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20153_15_non_const_ch3;
# 249 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20147_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 250 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20147_16_non_const_i >= Nbytes)
# 250 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 250 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 250 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 251 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20149_17_non_const_v = (src[__cuda_local_var_20147_16_non_const_i]);
# 252 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20150_15_non_const_ch0 = (c_lut2bit_vdif[((((int)__cuda_local_var_20149_17_non_const_v) >> 0) & 3)]);
# 253 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20151_15_non_const_ch1 = (c_lut2bit_vdif[((((int)__cuda_local_var_20149_17_non_const_v) >> 4) & 3)]);
# 254 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20152_15_non_const_ch2 = (c_lut2bit_vdif[((((int)__cuda_local_var_20149_17_non_const_v) >> 2) & 3)]);
# 255 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20153_15_non_const_ch3 = (c_lut2bit_vdif[((((int)__cuda_local_var_20149_17_non_const_v) >> 6) & 3)]);
# 256 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20147_16_non_const_i]) = __cuda_local_var_20150_15_non_const_ch0;
# 257 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20147_16_non_const_i]) = __cuda_local_var_20151_15_non_const_ch1;
# 258 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch2[__cuda_local_var_20147_16_non_const_i]) = __cuda_local_var_20152_15_non_const_ch2;
# 259 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch3[__cuda_local_var_20147_16_non_const_i]) = __cuda_local_var_20153_15_non_const_ch3;
# 260 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 261 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z22cu_decode_2bit1ch_HannPKhP6float4mm(
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 277 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft){
# 278 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 279 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20163_16_non_const_i;
# 281 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20165_15_non_const_omega;
# 282 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20166_15_non_const_n_win;
# 283 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20167_16_non_const_wf;
# 289 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20173_17_non_const_v;
# 290 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20174_9_non_const_tmp;
# 279 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20163_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 280 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20163_16_non_const_i >= Nbytes)
# 280 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 280 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 280 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 281 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20165_15_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 282 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20166_15_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((4UL * __cuda_local_var_20163_16_non_const_i) % Lfft))));
# 283 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20167_16_non_const_wf.x) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20165_15_non_const_omega * (__cuda_local_var_20166_15_non_const_n_win + (0.0F)))))) , (2.0F)));
# 283 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20167_16_non_const_wf.y) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20165_15_non_const_omega * (__cuda_local_var_20166_15_non_const_n_win + (1.0F)))))) , (2.0F)));
# 283 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20167_16_non_const_wf.z) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20165_15_non_const_omega * (__cuda_local_var_20166_15_non_const_n_win + (2.0F)))))) , (2.0F)));
# 283 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20167_16_non_const_wf.w) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20165_15_non_const_omega * (__cuda_local_var_20166_15_non_const_n_win + (3.0F)))))) , (2.0F)));
# 289 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20173_17_non_const_v = (src[__cuda_local_var_20163_16_non_const_i]);
# 290 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20174_9_non_const_tmp.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20173_17_non_const_v) >> 0) & 3)]) * (__cuda_local_var_20167_16_non_const_wf.x));
# 290 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20174_9_non_const_tmp.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20173_17_non_const_v) >> 2) & 3)]) * (__cuda_local_var_20167_16_non_const_wf.y));
# 290 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20174_9_non_const_tmp.z) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20173_17_non_const_v) >> 4) & 3)]) * (__cuda_local_var_20167_16_non_const_wf.z));
# 290 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20174_9_non_const_tmp.w) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20173_17_non_const_v) >> 6) & 3)]) * (__cuda_local_var_20167_16_non_const_wf.w));
# 296 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20163_16_non_const_i]) = __cuda_local_var_20174_9_non_const_tmp;
# 297 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 298 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z22cu_decode_2bit2ch_HannPKhP6float2S2_mm(
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch0,
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch1,
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 300 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft){
# 301 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 302 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20186_16_non_const_i;
# 304 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20188_15_non_const_omega;
# 305 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20189_15_non_const_n_win;
# 306 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20190_16_non_const_wf;
# 310 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20194_17_non_const_v;
# 311 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20195_16_non_const_ch0;
# 312 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20196_16_non_const_ch1;
# 302 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20186_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 303 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20186_16_non_const_i >= Nbytes)
# 303 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 303 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 303 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 304 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20188_15_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 305 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20189_15_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((2UL * __cuda_local_var_20186_16_non_const_i) % Lfft))));
# 306 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20190_16_non_const_wf.x) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20188_15_non_const_omega * (__cuda_local_var_20189_15_non_const_n_win + (0.0F)))))) , (2.0F)));
# 306 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20190_16_non_const_wf.y) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20188_15_non_const_omega * (__cuda_local_var_20189_15_non_const_n_win + (1.0F)))))) , (2.0F)));
# 310 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20194_17_non_const_v = (src[__cuda_local_var_20186_16_non_const_i]);
# 311 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20195_16_non_const_ch0.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20194_17_non_const_v) >> 0) & 3)]) * (__cuda_local_var_20190_16_non_const_wf.x));
# 311 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20195_16_non_const_ch0.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20194_17_non_const_v) >> 4) & 3)]) * (__cuda_local_var_20190_16_non_const_wf.y));
# 312 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20196_16_non_const_ch1.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20194_17_non_const_v) >> 2) & 3)]) * (__cuda_local_var_20190_16_non_const_wf.x));
# 312 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20196_16_non_const_ch1.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20194_17_non_const_v) >> 6) & 3)]) * (__cuda_local_var_20190_16_non_const_wf.y));
# 313 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20186_16_non_const_i]) = __cuda_local_var_20195_16_non_const_ch0;
# 314 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20186_16_non_const_i]) = __cuda_local_var_20196_16_non_const_ch1;
# 315 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 316 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 318 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z22cu_decode_2bit4ch_HannPKhPfS1_S1_S1_mm(
# 318 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 319 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch0,
# 319 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch1,
# 320 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch2,
# 320 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch3,
# 321 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 321 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft){
# 322 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 323 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20207_16_non_const_i;
# 325 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20209_15_non_const_omega;
# 326 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20210_15_non_const_n_win;
# 327 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20211_15_non_const_wf;
# 328 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20212_17_non_const_v;
# 329 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20213_15_non_const_ch0;
# 330 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20214_15_non_const_ch1;
# 331 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20215_15_non_const_ch2;
# 332 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20216_15_non_const_ch3;
# 323 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20207_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 324 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20207_16_non_const_i >= Nbytes)
# 324 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 324 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 324 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 325 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20209_15_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 326 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20210_15_non_const_n_win = ((float)__ull2float_rn((unsigned long long)((__cuda_local_var_20207_16_non_const_i % Lfft))));
# 327 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20211_15_non_const_wf = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20209_15_non_const_omega * __cuda_local_var_20210_15_non_const_n_win)))) , (2.0F)));
# 328 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20212_17_non_const_v = (src[__cuda_local_var_20207_16_non_const_i]);
# 329 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20213_15_non_const_ch0 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20212_17_non_const_v) >> 0) & 3)]) * __cuda_local_var_20211_15_non_const_wf);
# 330 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20214_15_non_const_ch1 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20212_17_non_const_v) >> 4) & 3)]) * __cuda_local_var_20211_15_non_const_wf);
# 331 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20215_15_non_const_ch2 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20212_17_non_const_v) >> 2) & 3)]) * __cuda_local_var_20211_15_non_const_wf);
# 332 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20216_15_non_const_ch3 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20212_17_non_const_v) >> 6) & 3)]) * __cuda_local_var_20211_15_non_const_wf);
# 333 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20207_16_non_const_i]) = __cuda_local_var_20213_15_non_const_ch0;
# 334 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20207_16_non_const_i]) = __cuda_local_var_20214_15_non_const_ch1;
# 335 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch2[__cuda_local_var_20207_16_non_const_i]) = __cuda_local_var_20215_15_non_const_ch2;
# 336 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch3[__cuda_local_var_20207_16_non_const_i]) = __cuda_local_var_20216_15_non_const_ch3;
# 337 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 338 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z25cu_decode_2bit1ch_HammingPKhP6float4mm(
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 344 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft){
# 345 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 346 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20230_16_non_const_i;
# 348 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20232_15_non_const_omega;
# 349 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20233_15_non_const_n_win;
# 350 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20234_16_non_const_wf;
# 356 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20240_17_non_const_v;
# 357 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20241_9_non_const_tmp;
# 346 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20230_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 347 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20230_16_non_const_i >= Nbytes)
# 347 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 347 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 347 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 348 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20232_15_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 349 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20233_15_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((4UL * __cuda_local_var_20230_16_non_const_i) % Lfft))));
# 350 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20234_16_non_const_wf.x) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20232_15_non_const_omega * (__cuda_local_var_20233_15_non_const_n_win + (0.0F)))))));
# 350 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20234_16_non_const_wf.y) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20232_15_non_const_omega * (__cuda_local_var_20233_15_non_const_n_win + (1.0F)))))));
# 350 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20234_16_non_const_wf.z) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20232_15_non_const_omega * (__cuda_local_var_20233_15_non_const_n_win + (2.0F)))))));
# 350 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20234_16_non_const_wf.w) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20232_15_non_const_omega * (__cuda_local_var_20233_15_non_const_n_win + (3.0F)))))));
# 356 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20240_17_non_const_v = (src[__cuda_local_var_20230_16_non_const_i]);
# 357 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20241_9_non_const_tmp.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20240_17_non_const_v) >> 0) & 3)]) * (__cuda_local_var_20234_16_non_const_wf.x));
# 357 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20241_9_non_const_tmp.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20240_17_non_const_v) >> 2) & 3)]) * (__cuda_local_var_20234_16_non_const_wf.y));
# 357 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20241_9_non_const_tmp.z) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20240_17_non_const_v) >> 4) & 3)]) * (__cuda_local_var_20234_16_non_const_wf.z));
# 357 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20241_9_non_const_tmp.w) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20240_17_non_const_v) >> 6) & 3)]) * (__cuda_local_var_20234_16_non_const_wf.w));
# 363 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20230_16_non_const_i]) = __cuda_local_var_20241_9_non_const_tmp;
# 364 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 365 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z25cu_decode_2bit2ch_HammingPKhP6float2S2_mm(
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch0,
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch1,
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 367 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft){
# 368 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 369 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20253_16_non_const_i;
# 371 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20255_15_non_const_omega;
# 372 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20256_15_non_const_n_win;
# 373 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20257_16_non_const_wf;
# 377 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20261_17_non_const_v;
# 378 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20262_16_non_const_ch0;
# 379 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20263_16_non_const_ch1;
# 369 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20253_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 370 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20253_16_non_const_i >= Nbytes)
# 370 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 370 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 370 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 371 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20255_15_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 372 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20256_15_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((2UL * __cuda_local_var_20253_16_non_const_i) % Lfft))));
# 373 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20257_16_non_const_wf.x) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20255_15_non_const_omega * (__cuda_local_var_20256_15_non_const_n_win + (0.0F)))))));
# 373 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20257_16_non_const_wf.y) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20255_15_non_const_omega * (__cuda_local_var_20256_15_non_const_n_win + (1.0F)))))));
# 377 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20261_17_non_const_v = (src[__cuda_local_var_20253_16_non_const_i]);
# 378 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20262_16_non_const_ch0.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20261_17_non_const_v) >> 0) & 3)]) * (__cuda_local_var_20257_16_non_const_wf.x));
# 378 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20262_16_non_const_ch0.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20261_17_non_const_v) >> 4) & 3)]) * (__cuda_local_var_20257_16_non_const_wf.y));
# 379 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20263_16_non_const_ch1.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20261_17_non_const_v) >> 2) & 3)]) * (__cuda_local_var_20257_16_non_const_wf.x));
# 379 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20263_16_non_const_ch1.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20261_17_non_const_v) >> 6) & 3)]) * (__cuda_local_var_20257_16_non_const_wf.y));
# 380 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20253_16_non_const_i]) = __cuda_local_var_20262_16_non_const_ch0;
# 381 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20253_16_non_const_i]) = __cuda_local_var_20263_16_non_const_ch1;
# 382 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 383 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 385 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z25cu_decode_2bit4ch_HammingPKhPfS1_S1_S1_mm(
# 385 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 386 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch0,
# 386 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch1,
# 387 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch2,
# 387 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch3,
# 388 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 388 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft){
# 389 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 390 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20274_16_non_const_i;
# 392 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20276_15_non_const_omega;
# 393 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20277_15_non_const_n_win;
# 394 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20278_15_non_const_wf;
# 395 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20279_17_non_const_v;
# 396 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20280_15_non_const_ch0;
# 397 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20281_15_non_const_ch1;
# 398 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20282_15_non_const_ch2;
# 399 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20283_15_non_const_ch3;
# 390 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20274_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 391 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20274_16_non_const_i >= Nbytes)
# 391 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 391 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 391 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 392 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20276_15_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 393 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20277_15_non_const_n_win = ((float)__ull2float_rn((unsigned long long)((__cuda_local_var_20274_16_non_const_i % Lfft))));
# 394 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20278_15_non_const_wf = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20276_15_non_const_omega * __cuda_local_var_20277_15_non_const_n_win)))));
# 395 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20279_17_non_const_v = (src[__cuda_local_var_20274_16_non_const_i]);
# 396 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20280_15_non_const_ch0 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20279_17_non_const_v) >> 0) & 3)]) * __cuda_local_var_20278_15_non_const_wf);
# 397 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20281_15_non_const_ch1 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20279_17_non_const_v) >> 4) & 3)]) * __cuda_local_var_20278_15_non_const_wf);
# 398 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20282_15_non_const_ch2 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20279_17_non_const_v) >> 2) & 3)]) * __cuda_local_var_20278_15_non_const_wf);
# 399 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20283_15_non_const_ch3 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20279_17_non_const_v) >> 6) & 3)]) * __cuda_local_var_20278_15_non_const_wf);
# 400 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20274_16_non_const_i]) = __cuda_local_var_20280_15_non_const_ch0;
# 401 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20274_16_non_const_i]) = __cuda_local_var_20281_15_non_const_ch1;
# 402 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch2[__cuda_local_var_20274_16_non_const_i]) = __cuda_local_var_20282_15_non_const_ch2;
# 403 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch3[__cuda_local_var_20274_16_non_const_i]) = __cuda_local_var_20283_15_non_const_ch3;
# 404 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 405 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z30cu_decode_2bit1ch_CustomWindowPKhP6float4mmPKS1_(
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *src,
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float4 *dst,
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft,
# 411 "../kernels/cuda/decoder_2b32f_kernels.cu"
const struct float4 *__restrict__ wf){
# 412 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 413 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20297_16_non_const_i;
# 415 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20299_16_non_const_w;
# 416 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20300_17_non_const_v;
# 417 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float4 __cuda_local_var_20301_9_non_const_tmp;
# 413 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20297_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 414 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20297_16_non_const_i >= Nbytes)
# 414 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 414 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 414 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 415 "../kernels/cuda/decoder_2b32f_kernels.cu"
memset((char *)&__cuda_local_var_20299_16_non_const_w, 0,sizeof(__cuda_local_var_20299_16_non_const_w));
# 415 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20299_16_non_const_w = ((*(struct float4 *)&(wf[(__cuda_local_var_20297_16_non_const_i % (Lfft / 4UL))])));
# 416 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20300_17_non_const_v = (src[__cuda_local_var_20297_16_non_const_i]);
# 417 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20301_9_non_const_tmp.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20300_17_non_const_v) >> 0) & 3)]) * (__cuda_local_var_20299_16_non_const_w.x));
# 417 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20301_9_non_const_tmp.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20300_17_non_const_v) >> 2) & 3)]) * (__cuda_local_var_20299_16_non_const_w.y));
# 417 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20301_9_non_const_tmp.z) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20300_17_non_const_v) >> 4) & 3)]) * (__cuda_local_var_20299_16_non_const_w.z));
# 417 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20301_9_non_const_tmp.w) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20300_17_non_const_v) >> 6) & 3)]) * (__cuda_local_var_20299_16_non_const_w.w));
# 423 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst[__cuda_local_var_20297_16_non_const_i]) = __cuda_local_var_20301_9_non_const_tmp;
# 424 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 425 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 427 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z30cu_decode_2bit2ch_CustomWindowPKhP6float2S2_mmPKS1_(
# 427 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 428 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch0,
# 428 "../kernels/cuda/decoder_2b32f_kernels.cu"
struct float2 *__restrict__ dst_ch1,
# 429 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 429 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft,
# 429 "../kernels/cuda/decoder_2b32f_kernels.cu"
const struct float2 *__restrict__ wf){
# 430 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 431 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20315_16_non_const_i;
# 433 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20317_16_non_const_w;
# 434 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20318_17_non_const_v;
# 435 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20319_16_non_const_ch0;
# 436 "../kernels/cuda/decoder_2b32f_kernels.cu"
 struct float2 __cuda_local_var_20320_16_non_const_ch1;
# 431 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20315_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 432 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20315_16_non_const_i >= Nbytes)
# 432 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 432 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 432 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 433 "../kernels/cuda/decoder_2b32f_kernels.cu"
memset((char *)&__cuda_local_var_20317_16_non_const_w, 0,sizeof(__cuda_local_var_20317_16_non_const_w));
# 433 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20317_16_non_const_w = ((*(struct float2 *)&(wf[(__cuda_local_var_20315_16_non_const_i % (Lfft / 2UL))])));
# 434 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20318_17_non_const_v = (src[__cuda_local_var_20315_16_non_const_i]);
# 435 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20319_16_non_const_ch0.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20318_17_non_const_v) >> 0) & 3)]) * (__cuda_local_var_20317_16_non_const_w.x));
# 435 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20319_16_non_const_ch0.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20318_17_non_const_v) >> 4) & 3)]) * (__cuda_local_var_20317_16_non_const_w.y));
# 436 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20320_16_non_const_ch1.x) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20318_17_non_const_v) >> 2) & 3)]) * (__cuda_local_var_20317_16_non_const_w.x));
# 436 "../kernels/cuda/decoder_2b32f_kernels.cu"
(__cuda_local_var_20320_16_non_const_ch1.y) = ((c_lut2bit_vdif[((((int)__cuda_local_var_20318_17_non_const_v) >> 6) & 3)]) * (__cuda_local_var_20317_16_non_const_w.y));
# 437 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20315_16_non_const_i]) = __cuda_local_var_20319_16_non_const_ch0;
# 438 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20315_16_non_const_i]) = __cuda_local_var_20320_16_non_const_ch1;
# 439 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 440 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 442 "../kernels/cuda/decoder_2b32f_kernels.cu"
__attribute__((global)) void _Z30cu_decode_2bit4ch_CustomWindowPKhPfS1_S1_S1_mmPKf(
# 442 "../kernels/cuda/decoder_2b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 443 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch0,
# 443 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch1,
# 444 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch2,
# 444 "../kernels/cuda/decoder_2b32f_kernels.cu"
float *__restrict__ dst_ch3,
# 445 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Nbytes,
# 445 "../kernels/cuda/decoder_2b32f_kernels.cu"
const size_t Lfft,
# 445 "../kernels/cuda/decoder_2b32f_kernels.cu"
const float *__restrict__ wf){
# 446 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 447 "../kernels/cuda/decoder_2b32f_kernels.cu"
 size_t __cuda_local_var_20331_16_non_const_i;
# 449 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20333_15_non_const_w;
# 450 "../kernels/cuda/decoder_2b32f_kernels.cu"
 uint8_t __cuda_local_var_20334_17_non_const_v;
# 451 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20335_15_non_const_ch0;
# 452 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20336_15_non_const_ch1;
# 453 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20337_15_non_const_ch2;
# 454 "../kernels/cuda/decoder_2b32f_kernels.cu"
 float __cuda_local_var_20338_15_non_const_ch3;
# 447 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20331_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 448 "../kernels/cuda/decoder_2b32f_kernels.cu"
if (__cuda_local_var_20331_16_non_const_i >= Nbytes)
# 448 "../kernels/cuda/decoder_2b32f_kernels.cu"
{
# 448 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 448 "../kernels/cuda/decoder_2b32f_kernels.cu"
}
# 449 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20333_15_non_const_w = (wf[(__cuda_local_var_20331_16_non_const_i % Lfft)]);
# 450 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20334_17_non_const_v = (src[__cuda_local_var_20331_16_non_const_i]);
# 451 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20335_15_non_const_ch0 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20334_17_non_const_v) >> 0) & 3)]) * __cuda_local_var_20333_15_non_const_w);
# 452 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20336_15_non_const_ch1 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20334_17_non_const_v) >> 4) & 3)]) * __cuda_local_var_20333_15_non_const_w);
# 453 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20337_15_non_const_ch2 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20334_17_non_const_v) >> 2) & 3)]) * __cuda_local_var_20333_15_non_const_w);
# 454 "../kernels/cuda/decoder_2b32f_kernels.cu"
__cuda_local_var_20338_15_non_const_ch3 = ((c_lut2bit_vdif[((((int)__cuda_local_var_20334_17_non_const_v) >> 6) & 3)]) * __cuda_local_var_20333_15_non_const_w);
# 455 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch0[__cuda_local_var_20331_16_non_const_i]) = __cuda_local_var_20335_15_non_const_ch0;
# 456 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch1[__cuda_local_var_20331_16_non_const_i]) = __cuda_local_var_20336_15_non_const_ch1;
# 457 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch2[__cuda_local_var_20331_16_non_const_i]) = __cuda_local_var_20337_15_non_const_ch2;
# 458 "../kernels/cuda/decoder_2b32f_kernels.cu"
(dst_ch3[__cuda_local_var_20331_16_non_const_i]) = __cuda_local_var_20338_15_non_const_ch3;
# 459 "../kernels/cuda/decoder_2b32f_kernels.cu"
return;
# 460 "../kernels/cuda/decoder_2b32f_kernels.cu"
}}
# 39 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
__attribute__((global)) void _Z23cu_decode_2bit2ch_splitPKhP6float2S2_m(
# 40 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
const uint8_t *__restrict__ src,
# 41 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
struct float2 *__restrict__ dst_ch0,
# 41 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
struct float2 *__restrict__ dst_ch1,
# 42 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
const size_t Nbytes){
# 44 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
{
# 45 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
 size_t __cuda_local_var_20354_16_non_const_i;
# 48 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
 uint8_t __cuda_local_var_20357_17_non_const_v;
# 49 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
 struct float2 __cuda_local_var_20358_16_non_const_ch0;
# 50 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
 struct float2 __cuda_local_var_20359_16_non_const_ch1;
# 45 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
__cuda_local_var_20354_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 47 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
if (__cuda_local_var_20354_16_non_const_i >= Nbytes)
# 47 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
{
# 47 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
return;
# 47 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
}
# 48 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
__cuda_local_var_20357_17_non_const_v = (src[__cuda_local_var_20354_16_non_const_i]);
# 49 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
(__cuda_local_var_20358_16_non_const_ch0.x) = (c_lut2bit_vdif[((((int)__cuda_local_var_20357_17_non_const_v) >> 0) & 3)]);
# 49 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
(__cuda_local_var_20358_16_non_const_ch0.y) = (c_lut2bit_vdif[((((int)__cuda_local_var_20357_17_non_const_v) >> 4) & 3)]);
# 50 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
(__cuda_local_var_20359_16_non_const_ch1.x) = (c_lut2bit_vdif[((((int)__cuda_local_var_20357_17_non_const_v) >> 2) & 3)]);
# 50 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
(__cuda_local_var_20359_16_non_const_ch1.y) = (c_lut2bit_vdif[((((int)__cuda_local_var_20357_17_non_const_v) >> 6) & 3)]);
# 51 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
(dst_ch0[__cuda_local_var_20354_16_non_const_i]) = __cuda_local_var_20358_16_non_const_ch0;
# 52 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
(dst_ch1[__cuda_local_var_20354_16_non_const_i]) = __cuda_local_var_20359_16_non_const_ch1;
# 53 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
return;
# 54 "../kernels/cuda/decoder_2b32f_split_kernels.cu"
}}
# 92 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) void _Z17cu_decode_3bit1chPKhP6float4m(
# 92 "../kernels/cuda/decoder_3b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 92 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 92 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Nbytes){
# 93 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 94 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20382_18_non_const_i;
# 95 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20383_18_non_const_rawpos;
# 100 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20388_13_non_const_b0;
# 101 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20389_13_non_const_b1;
# 102 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20390_13_non_const_b2;
# 105 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint32_t __cuda_local_var_20393_14_non_const_w;
# 108 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20396_12_non_const_out0;
# 114 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20402_12_non_const_out1;
# 94 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20382_18_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 95 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20383_18_non_const_rawpos = (3UL * __cuda_local_var_20382_18_non_const_i);
# 97 "../kernels/cuda/decoder_3b32f_kernels.cu"
if ((__cuda_local_var_20383_18_non_const_rawpos + 2UL) >= Nbytes)
# 97 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 97 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 97 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 100 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20388_13_non_const_b0 = (src[(__cuda_local_var_20383_18_non_const_rawpos + 0UL)]);
# 101 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20389_13_non_const_b1 = (src[(__cuda_local_var_20383_18_non_const_rawpos + 1UL)]);
# 102 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20390_13_non_const_b2 = (src[(__cuda_local_var_20383_18_non_const_rawpos + 2UL)]);
# 105 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20393_14_non_const_w = (((((uint32_t)__cuda_local_var_20390_13_non_const_b2) << 16) | (((uint32_t)__cuda_local_var_20389_13_non_const_b1) << 8)) | ((uint32_t)__cuda_local_var_20388_13_non_const_b0));
# 108 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20396_12_non_const_out0.x) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 0) & 7U)]);
# 108 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20396_12_non_const_out0.y) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 3) & 7U)]);
# 108 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20396_12_non_const_out0.z) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 6) & 7U)]);
# 108 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20396_12_non_const_out0.w) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 9) & 7U)]);
# 114 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20402_12_non_const_out1.x) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 12) & 7U)]);
# 114 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20402_12_non_const_out1.y) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 15) & 7U)]);
# 114 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20402_12_non_const_out1.z) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 18) & 7U)]);
# 114 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20402_12_non_const_out1.w) = (c_lut3bit[((__cuda_local_var_20393_14_non_const_w >> 21) & 7U)]);
# 122 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dst[((2UL * __cuda_local_var_20382_18_non_const_i) + 0UL)]) = __cuda_local_var_20396_12_non_const_out0;
# 123 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dst[((2UL * __cuda_local_var_20382_18_non_const_i) + 1UL)]) = __cuda_local_var_20402_12_non_const_out1;
# 125 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 126 "../kernels/cuda/decoder_3b32f_kernels.cu"
}}
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) void _Z23cu_decode_3bit2ch_splitPKhP6float4S2_m(
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dstX,
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dstY,
# 140 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Nbytes){
# 141 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 142 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20418_18_non_const_i;
# 143 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20419_18_non_const_rawpos;
# 148 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20424_13_non_const_b0;
# 149 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20425_13_non_const_b1;
# 150 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20426_13_non_const_b2;
# 153 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint32_t __cuda_local_var_20429_14_non_const_w;
# 156 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20432_12_non_const_X;
# 162 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20438_12_non_const_Y;
# 142 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20418_18_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 143 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20419_18_non_const_rawpos = (3UL * __cuda_local_var_20418_18_non_const_i);
# 145 "../kernels/cuda/decoder_3b32f_kernels.cu"
if ((__cuda_local_var_20419_18_non_const_rawpos + 2UL) >= Nbytes)
# 145 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 145 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 145 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 148 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20424_13_non_const_b0 = (src[(__cuda_local_var_20419_18_non_const_rawpos + 0UL)]);
# 149 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20425_13_non_const_b1 = (src[(__cuda_local_var_20419_18_non_const_rawpos + 1UL)]);
# 150 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20426_13_non_const_b2 = (src[(__cuda_local_var_20419_18_non_const_rawpos + 2UL)]);
# 153 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20429_14_non_const_w = (((((uint32_t)__cuda_local_var_20426_13_non_const_b2) << 16) | (((uint32_t)__cuda_local_var_20425_13_non_const_b1) << 8)) | ((uint32_t)__cuda_local_var_20424_13_non_const_b0));
# 156 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20432_12_non_const_X.x) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 0) & 7U)]);
# 156 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20432_12_non_const_X.y) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 6) & 7U)]);
# 156 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20432_12_non_const_X.z) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 12) & 7U)]);
# 156 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20432_12_non_const_X.w) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 18) & 7U)]);
# 162 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20438_12_non_const_Y.x) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 3) & 7U)]);
# 162 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20438_12_non_const_Y.y) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 9) & 7U)]);
# 162 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20438_12_non_const_Y.z) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 15) & 7U)]);
# 162 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20438_12_non_const_Y.w) = (c_lut3bit[((__cuda_local_var_20429_14_non_const_w >> 21) & 7U)]);
# 170 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dstX[__cuda_local_var_20418_18_non_const_i]) = __cuda_local_var_20432_12_non_const_X;
# 171 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dstY[__cuda_local_var_20418_18_non_const_i]) = __cuda_local_var_20438_12_non_const_Y;
# 173 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 174 "../kernels/cuda/decoder_3b32f_kernels.cu"
}}
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) void _Z22cu_decode_3bit1ch_HannPKhP6float4mm(
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Nbytes,
# 197 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Lfft){
# 198 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 199 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20454_18_non_const_i;
# 200 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20455_18_non_const_rawpos;
# 204 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20459_11_non_const_omega;
# 205 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20460_11_non_const_n_win;
# 206 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20461_12_non_const_wf0;
# 212 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20467_12_non_const_wf1;
# 220 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20475_13_non_const_b0;
# 221 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20476_13_non_const_b1;
# 222 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20477_13_non_const_b2;
# 225 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint32_t __cuda_local_var_20480_14_non_const_w;
# 228 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20483_12_non_const_out0;
# 234 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20489_12_non_const_out1;
# 199 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20454_18_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 200 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20455_18_non_const_rawpos = (3UL * __cuda_local_var_20454_18_non_const_i);
# 202 "../kernels/cuda/decoder_3b32f_kernels.cu"
if ((__cuda_local_var_20455_18_non_const_rawpos + 2UL) >= Nbytes)
# 202 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 202 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 202 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 204 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20459_11_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 205 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20460_11_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((8UL * __cuda_local_var_20454_18_non_const_i) % Lfft))));
# 206 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20461_12_non_const_wf0.x) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (0.0F)))))) , (2.0F)));
# 206 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20461_12_non_const_wf0.y) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (1.0F)))))) , (2.0F)));
# 206 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20461_12_non_const_wf0.z) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (2.0F)))))) , (2.0F)));
# 206 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20461_12_non_const_wf0.w) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (3.0F)))))) , (2.0F)));
# 212 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20467_12_non_const_wf1.x) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (4.0F)))))) , (2.0F)));
# 212 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20467_12_non_const_wf1.y) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (5.0F)))))) , (2.0F)));
# 212 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20467_12_non_const_wf1.z) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (6.0F)))))) , (2.0F)));
# 212 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20467_12_non_const_wf1.w) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20459_11_non_const_omega * (__cuda_local_var_20460_11_non_const_n_win + (7.0F)))))) , (2.0F)));
# 220 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20475_13_non_const_b0 = (src[(__cuda_local_var_20455_18_non_const_rawpos + 0UL)]);
# 221 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20476_13_non_const_b1 = (src[(__cuda_local_var_20455_18_non_const_rawpos + 1UL)]);
# 222 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20477_13_non_const_b2 = (src[(__cuda_local_var_20455_18_non_const_rawpos + 2UL)]);
# 225 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20480_14_non_const_w = (((((uint32_t)__cuda_local_var_20477_13_non_const_b2) << 16) | (((uint32_t)__cuda_local_var_20476_13_non_const_b1) << 8)) | ((uint32_t)__cuda_local_var_20475_13_non_const_b0));
# 228 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20483_12_non_const_out0.x) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 0) & 7U)]) * (__cuda_local_var_20461_12_non_const_wf0.x));
# 228 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20483_12_non_const_out0.y) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 3) & 7U)]) * (__cuda_local_var_20461_12_non_const_wf0.y));
# 228 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20483_12_non_const_out0.z) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 6) & 7U)]) * (__cuda_local_var_20461_12_non_const_wf0.z));
# 228 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20483_12_non_const_out0.w) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 9) & 7U)]) * (__cuda_local_var_20461_12_non_const_wf0.w));
# 234 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20489_12_non_const_out1.x) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 12) & 7U)]) * (__cuda_local_var_20467_12_non_const_wf1.x));
# 234 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20489_12_non_const_out1.y) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 15) & 7U)]) * (__cuda_local_var_20467_12_non_const_wf1.y));
# 234 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20489_12_non_const_out1.z) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 18) & 7U)]) * (__cuda_local_var_20467_12_non_const_wf1.z));
# 234 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20489_12_non_const_out1.w) = ((c_lut3bit[((__cuda_local_var_20480_14_non_const_w >> 21) & 7U)]) * (__cuda_local_var_20467_12_non_const_wf1.w));
# 242 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dst[((2UL * __cuda_local_var_20454_18_non_const_i) + 0UL)]) = __cuda_local_var_20483_12_non_const_out0;
# 243 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dst[((2UL * __cuda_local_var_20454_18_non_const_i) + 1UL)]) = __cuda_local_var_20489_12_non_const_out1;
# 245 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 246 "../kernels/cuda/decoder_3b32f_kernels.cu"
}}
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) void _Z25cu_decode_3bit1ch_HammingPKhP6float4mm(
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dst,
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Nbytes,
# 255 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Lfft){
# 256 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 257 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20505_18_non_const_i;
# 258 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20506_18_non_const_rawpos;
# 262 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20510_11_non_const_omega;
# 263 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20511_11_non_const_n_win;
# 264 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20512_12_non_const_wf0;
# 270 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20518_12_non_const_wf1;
# 278 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20526_13_non_const_b0;
# 279 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20527_13_non_const_b1;
# 280 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20528_13_non_const_b2;
# 283 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint32_t __cuda_local_var_20531_14_non_const_w;
# 286 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20534_12_non_const_out0;
# 292 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20540_12_non_const_out1;
# 257 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20505_18_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 258 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20506_18_non_const_rawpos = (3UL * __cuda_local_var_20505_18_non_const_i);
# 260 "../kernels/cuda/decoder_3b32f_kernels.cu"
if ((__cuda_local_var_20506_18_non_const_rawpos + 2UL) >= Nbytes)
# 260 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 260 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 260 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 262 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20510_11_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 263 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20511_11_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((8UL * __cuda_local_var_20505_18_non_const_i) % Lfft))));
# 264 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20512_12_non_const_wf0.x) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (0.0F)))))));
# 264 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20512_12_non_const_wf0.y) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (1.0F)))))));
# 264 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20512_12_non_const_wf0.z) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (2.0F)))))));
# 264 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20512_12_non_const_wf0.w) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (3.0F)))))));
# 270 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20518_12_non_const_wf1.x) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (4.0F)))))));
# 270 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20518_12_non_const_wf1.y) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (5.0F)))))));
# 270 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20518_12_non_const_wf1.z) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (6.0F)))))));
# 270 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20518_12_non_const_wf1.w) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20510_11_non_const_omega * (__cuda_local_var_20511_11_non_const_n_win + (7.0F)))))));
# 278 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20526_13_non_const_b0 = (src[(__cuda_local_var_20506_18_non_const_rawpos + 0UL)]);
# 279 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20527_13_non_const_b1 = (src[(__cuda_local_var_20506_18_non_const_rawpos + 1UL)]);
# 280 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20528_13_non_const_b2 = (src[(__cuda_local_var_20506_18_non_const_rawpos + 2UL)]);
# 283 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20531_14_non_const_w = (((((uint32_t)__cuda_local_var_20528_13_non_const_b2) << 16) | (((uint32_t)__cuda_local_var_20527_13_non_const_b1) << 8)) | ((uint32_t)__cuda_local_var_20526_13_non_const_b0));
# 286 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20534_12_non_const_out0.x) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 0) & 7U)]) * (__cuda_local_var_20512_12_non_const_wf0.x));
# 286 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20534_12_non_const_out0.y) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 3) & 7U)]) * (__cuda_local_var_20512_12_non_const_wf0.y));
# 286 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20534_12_non_const_out0.z) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 6) & 7U)]) * (__cuda_local_var_20512_12_non_const_wf0.z));
# 286 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20534_12_non_const_out0.w) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 9) & 7U)]) * (__cuda_local_var_20512_12_non_const_wf0.w));
# 292 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20540_12_non_const_out1.x) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 12) & 7U)]) * (__cuda_local_var_20518_12_non_const_wf1.x));
# 292 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20540_12_non_const_out1.y) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 15) & 7U)]) * (__cuda_local_var_20518_12_non_const_wf1.y));
# 292 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20540_12_non_const_out1.z) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 18) & 7U)]) * (__cuda_local_var_20518_12_non_const_wf1.z));
# 292 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20540_12_non_const_out1.w) = ((c_lut3bit[((__cuda_local_var_20531_14_non_const_w >> 21) & 7U)]) * (__cuda_local_var_20518_12_non_const_wf1.w));
# 300 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dst[((2UL * __cuda_local_var_20505_18_non_const_i) + 0UL)]) = __cuda_local_var_20534_12_non_const_out0;
# 301 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dst[((2UL * __cuda_local_var_20505_18_non_const_i) + 1UL)]) = __cuda_local_var_20540_12_non_const_out1;
# 303 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 304 "../kernels/cuda/decoder_3b32f_kernels.cu"
}}
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) void _Z28cu_decode_3bit2ch_Hann_splitPKhP6float4S2_mm(
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dstX,
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dstY,
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Nbytes,
# 314 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Lfft){
# 315 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 316 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20556_18_non_const_i;
# 317 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20557_18_non_const_rawpos;
# 321 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20561_11_non_const_omega;
# 322 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20562_11_non_const_n_win;
# 323 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20563_12_non_const_wf;
# 331 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20571_13_non_const_b0;
# 332 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20572_13_non_const_b1;
# 333 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20573_13_non_const_b2;
# 336 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint32_t __cuda_local_var_20576_14_non_const_w;
# 339 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20579_12_non_const_X;
# 345 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20585_12_non_const_Y;
# 316 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20556_18_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 317 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20557_18_non_const_rawpos = (3UL * __cuda_local_var_20556_18_non_const_i);
# 319 "../kernels/cuda/decoder_3b32f_kernels.cu"
if ((__cuda_local_var_20557_18_non_const_rawpos + 2UL) >= Nbytes)
# 319 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 319 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 319 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 321 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20561_11_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 322 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20562_11_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((4UL * __cuda_local_var_20556_18_non_const_i) % Lfft))));
# 323 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20563_12_non_const_wf.x) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20561_11_non_const_omega * (__cuda_local_var_20562_11_non_const_n_win + (0.0F)))))) , (2.0F)));
# 323 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20563_12_non_const_wf.y) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20561_11_non_const_omega * (__cuda_local_var_20562_11_non_const_n_win + (1.0F)))))) , (2.0F)));
# 323 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20563_12_non_const_wf.z) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20561_11_non_const_omega * (__cuda_local_var_20562_11_non_const_n_win + (2.0F)))))) , (2.0F)));
# 323 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20563_12_non_const_wf.w) = ( fdividef(((1.0F) - (__cosf((__cuda_local_var_20561_11_non_const_omega * (__cuda_local_var_20562_11_non_const_n_win + (3.0F)))))) , (2.0F)));
# 331 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20571_13_non_const_b0 = (src[(__cuda_local_var_20557_18_non_const_rawpos + 0UL)]);
# 332 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20572_13_non_const_b1 = (src[(__cuda_local_var_20557_18_non_const_rawpos + 1UL)]);
# 333 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20573_13_non_const_b2 = (src[(__cuda_local_var_20557_18_non_const_rawpos + 2UL)]);
# 336 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20576_14_non_const_w = (((((uint32_t)__cuda_local_var_20573_13_non_const_b2) << 16) | (((uint32_t)__cuda_local_var_20572_13_non_const_b1) << 8)) | ((uint32_t)__cuda_local_var_20571_13_non_const_b0));
# 339 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20579_12_non_const_X.x) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 0) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.x));
# 339 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20579_12_non_const_X.y) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 6) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.y));
# 339 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20579_12_non_const_X.z) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 12) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.z));
# 339 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20579_12_non_const_X.w) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 18) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.w));
# 345 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20585_12_non_const_Y.x) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 3) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.x));
# 345 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20585_12_non_const_Y.y) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 9) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.y));
# 345 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20585_12_non_const_Y.z) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 15) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.z));
# 345 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20585_12_non_const_Y.w) = ((c_lut3bit[((__cuda_local_var_20576_14_non_const_w >> 21) & 7U)]) * (__cuda_local_var_20563_12_non_const_wf.w));
# 353 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dstX[__cuda_local_var_20556_18_non_const_i]) = __cuda_local_var_20579_12_non_const_X;
# 354 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dstY[__cuda_local_var_20556_18_non_const_i]) = __cuda_local_var_20585_12_non_const_Y;
# 356 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 357 "../kernels/cuda/decoder_3b32f_kernels.cu"
}}
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
__attribute__((global)) void _Z31cu_decode_3bit2ch_Hamming_splitPKhP6float4S2_mm(
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
const uint8_t *__restrict__ src,
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dstX,
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
struct float4 *__restrict__ dstY,
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Nbytes,
# 367 "../kernels/cuda/decoder_3b32f_kernels.cu"
const size_t Lfft){
# 368 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 369 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20601_18_non_const_i;
# 370 "../kernels/cuda/decoder_3b32f_kernels.cu"
 size_t __cuda_local_var_20602_18_non_const_rawpos;
# 374 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20606_11_non_const_omega;
# 375 "../kernels/cuda/decoder_3b32f_kernels.cu"
 float __cuda_local_var_20607_11_non_const_n_win;
# 376 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20608_12_non_const_wf;
# 384 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20616_13_non_const_b0;
# 385 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20617_13_non_const_b1;
# 386 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint8_t __cuda_local_var_20618_13_non_const_b2;
# 389 "../kernels/cuda/decoder_3b32f_kernels.cu"
 uint32_t __cuda_local_var_20621_14_non_const_w;
# 392 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20624_12_non_const_X;
# 398 "../kernels/cuda/decoder_3b32f_kernels.cu"
 struct float4 __cuda_local_var_20630_12_non_const_Y;
# 369 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20601_18_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 370 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20602_18_non_const_rawpos = (3UL * __cuda_local_var_20601_18_non_const_i);
# 372 "../kernels/cuda/decoder_3b32f_kernels.cu"
if ((__cuda_local_var_20602_18_non_const_rawpos + 2UL) >= Nbytes)
# 372 "../kernels/cuda/decoder_3b32f_kernels.cu"
{
# 372 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 372 "../kernels/cuda/decoder_3b32f_kernels.cu"
}
# 374 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20606_11_non_const_omega = ( fdividef((6.283185482F) , ((float)__ull2float_rn((unsigned long long)((Lfft - 1UL))))));
# 375 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20607_11_non_const_n_win = ((float)__ull2float_rn((unsigned long long)(((4UL * __cuda_local_var_20601_18_non_const_i) % Lfft))));
# 376 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20608_12_non_const_wf.x) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20606_11_non_const_omega * (__cuda_local_var_20607_11_non_const_n_win + (0.0F)))))));
# 376 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20608_12_non_const_wf.y) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20606_11_non_const_omega * (__cuda_local_var_20607_11_non_const_n_win + (1.0F)))))));
# 376 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20608_12_non_const_wf.z) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20606_11_non_const_omega * (__cuda_local_var_20607_11_non_const_n_win + (2.0F)))))));
# 376 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20608_12_non_const_wf.w) = ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20606_11_non_const_omega * (__cuda_local_var_20607_11_non_const_n_win + (3.0F)))))));
# 384 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20616_13_non_const_b0 = (src[(__cuda_local_var_20602_18_non_const_rawpos + 0UL)]);
# 385 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20617_13_non_const_b1 = (src[(__cuda_local_var_20602_18_non_const_rawpos + 1UL)]);
# 386 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20618_13_non_const_b2 = (src[(__cuda_local_var_20602_18_non_const_rawpos + 2UL)]);
# 389 "../kernels/cuda/decoder_3b32f_kernels.cu"
__cuda_local_var_20621_14_non_const_w = (((((uint32_t)__cuda_local_var_20618_13_non_const_b2) << 16) | (((uint32_t)__cuda_local_var_20617_13_non_const_b1) << 8)) | ((uint32_t)__cuda_local_var_20616_13_non_const_b0));
# 392 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20624_12_non_const_X.x) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 0) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.x));
# 392 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20624_12_non_const_X.y) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 6) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.y));
# 392 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20624_12_non_const_X.z) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 12) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.z));
# 392 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20624_12_non_const_X.w) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 18) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.w));
# 398 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20630_12_non_const_Y.x) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 3) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.x));
# 398 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20630_12_non_const_Y.y) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 9) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.y));
# 398 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20630_12_non_const_Y.z) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 15) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.z));
# 398 "../kernels/cuda/decoder_3b32f_kernels.cu"
(__cuda_local_var_20630_12_non_const_Y.w) = ((c_lut3bit[((__cuda_local_var_20621_14_non_const_w >> 21) & 7U)]) * (__cuda_local_var_20608_12_non_const_wf.w));
# 406 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dstX[__cuda_local_var_20601_18_non_const_i]) = __cuda_local_var_20624_12_non_const_X;
# 407 "../kernels/cuda/decoder_3b32f_kernels.cu"
(dstY[__cuda_local_var_20601_18_non_const_i]) = __cuda_local_var_20630_12_non_const_Y;
# 409 "../kernels/cuda/decoder_3b32f_kernels.cu"
return;
# 410 "../kernels/cuda/decoder_3b32f_kernels.cu"
}}
# 28 "../kernels/cuda/memory_split_kernels.cu"
__attribute__((global)) void _Z21cu_split_2chan_f4_2f2PK6float4P6float2S3_m(
# 28 "../kernels/cuda/memory_split_kernels.cu"
const struct float4 *__restrict__ src,
# 28 "../kernels/cuda/memory_split_kernels.cu"
struct float2 *__restrict__ dst_x,
# 28 "../kernels/cuda/memory_split_kernels.cu"
struct float2 *__restrict__ dst_y,
# 28 "../kernels/cuda/memory_split_kernels.cu"
const size_t N){
# 29 "../kernels/cuda/memory_split_kernels.cu"
{
# 30 "../kernels/cuda/memory_split_kernels.cu"
 size_t __cuda_local_var_20648_16_non_const_i;
# 32 "../kernels/cuda/memory_split_kernels.cu"
 struct float4 __cuda_local_var_20650_16_non_const_v;
# 33 "../kernels/cuda/memory_split_kernels.cu"
 struct float2 __cuda_local_var_20651_16_non_const_x;
# 34 "../kernels/cuda/memory_split_kernels.cu"
 struct float2 __cuda_local_var_20652_16_non_const_y;
# 30 "../kernels/cuda/memory_split_kernels.cu"
__cuda_local_var_20648_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 31 "../kernels/cuda/memory_split_kernels.cu"
if (__cuda_local_var_20648_16_non_const_i >= (N / 2UL))
# 31 "../kernels/cuda/memory_split_kernels.cu"
{
# 31 "../kernels/cuda/memory_split_kernels.cu"
return;
# 31 "../kernels/cuda/memory_split_kernels.cu"
}
# 32 "../kernels/cuda/memory_split_kernels.cu"
memset((char *)&__cuda_local_var_20650_16_non_const_v, 0,sizeof(__cuda_local_var_20650_16_non_const_v));
# 32 "../kernels/cuda/memory_split_kernels.cu"
__cuda_local_var_20650_16_non_const_v = ((*(struct float4 *)&(src[__cuda_local_var_20648_16_non_const_i])));
# 33 "../kernels/cuda/memory_split_kernels.cu"
(__cuda_local_var_20651_16_non_const_x.x) = (__cuda_local_var_20650_16_non_const_v.x);
# 33 "../kernels/cuda/memory_split_kernels.cu"
(__cuda_local_var_20651_16_non_const_x.y) = (__cuda_local_var_20650_16_non_const_v.z);
# 34 "../kernels/cuda/memory_split_kernels.cu"
(__cuda_local_var_20652_16_non_const_y.x) = (__cuda_local_var_20650_16_non_const_v.y);
# 34 "../kernels/cuda/memory_split_kernels.cu"
(__cuda_local_var_20652_16_non_const_y.y) = (__cuda_local_var_20650_16_non_const_v.w);
# 35 "../kernels/cuda/memory_split_kernels.cu"
__syncthreads();
# 36 "../kernels/cuda/memory_split_kernels.cu"
(dst_x[__cuda_local_var_20648_16_non_const_i]) = __cuda_local_var_20651_16_non_const_x;
# 37 "../kernels/cuda/memory_split_kernels.cu"
(dst_y[__cuda_local_var_20648_16_non_const_i]) = __cuda_local_var_20652_16_non_const_y;
# 38 "../kernels/cuda/memory_split_kernels.cu"
return;
# 39 "../kernels/cuda/memory_split_kernels.cu"
}}
# 48 "../kernels/cuda/memory_split_kernels.cu"
__attribute__((global)) void _Z20cu_split_2chan_f2_2fPK6float2PfS2_m(
# 48 "../kernels/cuda/memory_split_kernels.cu"
const struct float2 *__restrict__ src,
# 48 "../kernels/cuda/memory_split_kernels.cu"
float *__restrict__ dst_x,
# 48 "../kernels/cuda/memory_split_kernels.cu"
float *__restrict__ dst_y,
# 48 "../kernels/cuda/memory_split_kernels.cu"
const size_t N){
# 49 "../kernels/cuda/memory_split_kernels.cu"
{
# 50 "../kernels/cuda/memory_split_kernels.cu"
 size_t __cuda_local_var_20661_16_non_const_i;
# 52 "../kernels/cuda/memory_split_kernels.cu"
 struct float2 __cuda_local_var_20663_16_non_const_v;
# 50 "../kernels/cuda/memory_split_kernels.cu"
__cuda_local_var_20661_16_non_const_i = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 51 "../kernels/cuda/memory_split_kernels.cu"
if (__cuda_local_var_20661_16_non_const_i >= N)
# 51 "../kernels/cuda/memory_split_kernels.cu"
{
# 51 "../kernels/cuda/memory_split_kernels.cu"
return;
# 51 "../kernels/cuda/memory_split_kernels.cu"
}
# 52 "../kernels/cuda/memory_split_kernels.cu"
memset((char *)&__cuda_local_var_20663_16_non_const_v, 0,sizeof(__cuda_local_var_20663_16_non_const_v));
# 52 "../kernels/cuda/memory_split_kernels.cu"
__cuda_local_var_20663_16_non_const_v = ((*(struct float2 *)&(src[__cuda_local_var_20661_16_non_const_i])));
# 53 "../kernels/cuda/memory_split_kernels.cu"
(dst_x[__cuda_local_var_20661_16_non_const_i]) = (__cuda_local_var_20663_16_non_const_v.x);
# 54 "../kernels/cuda/memory_split_kernels.cu"
(dst_y[__cuda_local_var_20661_16_non_const_i]) = (__cuda_local_var_20663_16_non_const_v.y);
# 55 "../kernels/cuda/memory_split_kernels.cu"
return;
# 56 "../kernels/cuda/memory_split_kernels.cu"
}}
# 32 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) void _Z18cu_accumulate_2polPK6float2S1_P6float4mm(
# 32 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const struct float2 *__restrict__ x,
# 32 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const struct float2 *__restrict__ y,
# 33 "../kernels/cuda/arithmetic_xmac_kernels.cu"
struct float4 *__restrict__ acc,
# 33 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Lfft,
# 33 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Nfft){
# 34 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 35 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20686_16_non_const_nthreads;
# 36 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20687_16_non_const_idx;
# 37 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20688_16_non_const_last_idx;
# 38 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20689_16_non_const_bin;
# 43 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float4 __cuda_local_var_20694_16_non_const_a;
# 70 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 float *__cuda_local_var_20711_16_non_const_acc1f;
# 35 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20686_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 36 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20687_16_non_const_idx = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 37 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20688_16_non_const_last_idx = (Lfft * Nfft);
# 38 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20689_16_non_const_bin = (__cuda_local_var_20687_16_non_const_idx % Lfft);
# 43 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20694_16_non_const_a, 0,sizeof(__cuda_local_var_20694_16_non_const_a));
# 43 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20694_16_non_const_a.x = (0.0F);
# 43 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20694_16_non_const_a.y = (0.0F);
# 43 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20694_16_non_const_a.z = (0.0F);
# 43 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20694_16_non_const_a.w = (0.0F);
# 45 "../kernels/cuda/arithmetic_xmac_kernels.cu"
while (__cuda_local_var_20687_16_non_const_idx < __cuda_local_var_20688_16_non_const_last_idx)
# 45 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 47 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20698_21_non_const_P1;
# 48 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20699_21_non_const_P2;
# 47 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20698_21_non_const_P1, 0,sizeof(__cuda_local_var_20698_21_non_const_P1));
# 47 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20698_21_non_const_P1 = ((*(struct float2 *)&(x[__cuda_local_var_20687_16_non_const_idx])));
# 48 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20699_21_non_const_P2, 0,sizeof(__cuda_local_var_20699_21_non_const_P2));
# 48 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20699_21_non_const_P2 = ((*(struct float2 *)&(y[__cuda_local_var_20687_16_non_const_idx])));
# 52 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20694_16_non_const_a.x) += (((__cuda_local_var_20698_21_non_const_P1.x) * (__cuda_local_var_20698_21_non_const_P1.x)) + ((__cuda_local_var_20698_21_non_const_P1.y) * (__cuda_local_var_20698_21_non_const_P1.y)));
# 53 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20694_16_non_const_a.y) += (((__cuda_local_var_20698_21_non_const_P1.x) * (__cuda_local_var_20699_21_non_const_P2.x)) + ((__cuda_local_var_20698_21_non_const_P1.y) * (__cuda_local_var_20699_21_non_const_P2.y)));
# 54 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20694_16_non_const_a.z) += (((-(__cuda_local_var_20698_21_non_const_P1.x)) * (__cuda_local_var_20699_21_non_const_P2.y)) + ((__cuda_local_var_20698_21_non_const_P1.y) * (__cuda_local_var_20699_21_non_const_P2.x)));
# 55 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20694_16_non_const_a.w) += (((__cuda_local_var_20699_21_non_const_P2.x) * (__cuda_local_var_20699_21_non_const_P2.x)) + ((__cuda_local_var_20699_21_non_const_P2.y) * (__cuda_local_var_20699_21_non_const_P2.y)));
# 57 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20687_16_non_const_idx += __cuda_local_var_20686_16_non_const_nthreads;
# 58 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 70 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20711_16_non_const_acc1f = ((float *)acc);
# 71 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20711_16_non_const_acc1f + __cuda_local_var_20689_16_non_const_bin), (__cuda_local_var_20694_16_non_const_a.x));
# 72 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20711_16_non_const_acc1f + (Lfft + __cuda_local_var_20689_16_non_const_bin)), (__cuda_local_var_20694_16_non_const_a.w));
# 73 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20711_16_non_const_acc1f + (((2UL * Lfft) + (2UL * __cuda_local_var_20689_16_non_const_bin)) + 0UL)), (__cuda_local_var_20694_16_non_const_a.y));
# 74 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20711_16_non_const_acc1f + (((2UL * Lfft) + (2UL * __cuda_local_var_20689_16_non_const_bin)) + 1UL)), (__cuda_local_var_20694_16_non_const_a.z));
# 77 "../kernels/cuda/arithmetic_xmac_kernels.cu"
return;
# 78 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}}
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) void _Z25cu_accumulate_2pol_packedPK6float4PS_mm(
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const struct float4 *__restrict__ xy,
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
struct float4 *__restrict__ acc,
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Lfft,
# 80 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Nfft){
# 81 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 82 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20723_16_non_const_nthreads;
# 83 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20724_16_non_const_idx;
# 84 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20725_16_non_const_last_idx;
# 85 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20726_16_non_const_bin;
# 90 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float4 __cuda_local_var_20731_16_non_const_a;
# 117 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 float *__cuda_local_var_20748_16_non_const_acc1f;
# 82 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20723_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 83 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20724_16_non_const_idx = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 84 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20725_16_non_const_last_idx = (Lfft * Nfft);
# 85 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20726_16_non_const_bin = (__cuda_local_var_20724_16_non_const_idx % Lfft);
# 90 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20731_16_non_const_a, 0,sizeof(__cuda_local_var_20731_16_non_const_a));
# 90 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20731_16_non_const_a.x = (0.0F);
# 90 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20731_16_non_const_a.y = (0.0F);
# 90 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20731_16_non_const_a.z = (0.0F);
# 90 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20731_16_non_const_a.w = (0.0F);
# 92 "../kernels/cuda/arithmetic_xmac_kernels.cu"
while (__cuda_local_var_20724_16_non_const_idx < __cuda_local_var_20725_16_non_const_last_idx)
# 92 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 94 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20735_21_non_const_P1;
# 95 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20736_21_non_const_P2;
# 94 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20735_21_non_const_P1.x) = ((xy[__cuda_local_var_20724_16_non_const_idx]).x);
# 94 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20735_21_non_const_P1.y) = ((xy[__cuda_local_var_20724_16_non_const_idx]).y);
# 95 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20736_21_non_const_P2.x) = ((xy[__cuda_local_var_20724_16_non_const_idx]).z);
# 95 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20736_21_non_const_P2.y) = ((xy[__cuda_local_var_20724_16_non_const_idx]).w);
# 99 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20731_16_non_const_a.x) += (((__cuda_local_var_20735_21_non_const_P1.x) * (__cuda_local_var_20735_21_non_const_P1.x)) + ((__cuda_local_var_20735_21_non_const_P1.y) * (__cuda_local_var_20735_21_non_const_P1.y)));
# 100 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20731_16_non_const_a.y) += (((__cuda_local_var_20735_21_non_const_P1.x) * (__cuda_local_var_20736_21_non_const_P2.x)) + ((__cuda_local_var_20735_21_non_const_P1.y) * (__cuda_local_var_20736_21_non_const_P2.y)));
# 101 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20731_16_non_const_a.z) += (((-(__cuda_local_var_20735_21_non_const_P1.x)) * (__cuda_local_var_20736_21_non_const_P2.y)) + ((__cuda_local_var_20735_21_non_const_P1.y) * (__cuda_local_var_20736_21_non_const_P2.x)));
# 102 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20731_16_non_const_a.w) += (((__cuda_local_var_20736_21_non_const_P2.x) * (__cuda_local_var_20736_21_non_const_P2.x)) + ((__cuda_local_var_20736_21_non_const_P2.y) * (__cuda_local_var_20736_21_non_const_P2.y)));
# 104 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20724_16_non_const_idx += __cuda_local_var_20723_16_non_const_nthreads;
# 105 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 117 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20748_16_non_const_acc1f = ((float *)acc);
# 118 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20748_16_non_const_acc1f + __cuda_local_var_20726_16_non_const_bin), (__cuda_local_var_20731_16_non_const_a.x));
# 119 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20748_16_non_const_acc1f + (Lfft + __cuda_local_var_20726_16_non_const_bin)), (__cuda_local_var_20731_16_non_const_a.w));
# 120 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20748_16_non_const_acc1f + (((2UL * Lfft) + (2UL * __cuda_local_var_20726_16_non_const_bin)) + 0UL)), (__cuda_local_var_20731_16_non_const_a.y));
# 121 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20748_16_non_const_acc1f + (((2UL * Lfft) + (2UL * __cuda_local_var_20726_16_non_const_bin)) + 1UL)), (__cuda_local_var_20731_16_non_const_a.z));
# 124 "../kernels/cuda/arithmetic_xmac_kernels.cu"
return;
# 126 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}}
# 130 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) void _Z27cu_accumulate_2pol_autoOnlyPK6float2S1_PS_mm(
# 130 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const struct float2 *__restrict__ x,
# 130 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const struct float2 *__restrict__ y,
# 131 "../kernels/cuda/arithmetic_xmac_kernels.cu"
struct float2 *__restrict__ acc,
# 131 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Lfft,
# 131 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Nfft){
# 132 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 133 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20764_16_non_const_nthreads;
# 134 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20765_16_non_const_idx;
# 135 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20766_16_non_const_last_idx;
# 136 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20767_16_non_const_bin;
# 138 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20769_16_non_const_a;
# 162 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 float *__cuda_local_var_20785_16_non_const_acc1f;
# 133 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20764_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 134 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20765_16_non_const_idx = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 135 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20766_16_non_const_last_idx = (Lfft * Nfft);
# 136 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20767_16_non_const_bin = (__cuda_local_var_20765_16_non_const_idx % Lfft);
# 138 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20769_16_non_const_a, 0,sizeof(__cuda_local_var_20769_16_non_const_a));
# 138 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20769_16_non_const_a.x = (0.0F);
# 138 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20769_16_non_const_a.y = (0.0F);
# 140 "../kernels/cuda/arithmetic_xmac_kernels.cu"
while (__cuda_local_var_20765_16_non_const_idx < __cuda_local_var_20766_16_non_const_last_idx)
# 140 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 142 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20773_21_non_const_P1;
# 143 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20774_21_non_const_P2;
# 142 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20773_21_non_const_P1, 0,sizeof(__cuda_local_var_20773_21_non_const_P1));
# 142 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20773_21_non_const_P1 = ((*(struct float2 *)&(x[__cuda_local_var_20765_16_non_const_idx])));
# 143 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20774_21_non_const_P2, 0,sizeof(__cuda_local_var_20774_21_non_const_P2));
# 143 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20774_21_non_const_P2 = ((*(struct float2 *)&(y[__cuda_local_var_20765_16_non_const_idx])));
# 148 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20769_16_non_const_a.x) += (((__cuda_local_var_20773_21_non_const_P1.x) * (__cuda_local_var_20773_21_non_const_P1.x)) + ((__cuda_local_var_20773_21_non_const_P1.y) * (__cuda_local_var_20773_21_non_const_P1.y)));
# 149 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20769_16_non_const_a.y) += (((__cuda_local_var_20774_21_non_const_P2.x) * (__cuda_local_var_20774_21_non_const_P2.x)) + ((__cuda_local_var_20774_21_non_const_P2.y) * (__cuda_local_var_20774_21_non_const_P2.y)));
# 151 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20765_16_non_const_idx += __cuda_local_var_20764_16_non_const_nthreads;
# 152 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 162 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20785_16_non_const_acc1f = ((float *)acc);
# 163 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20785_16_non_const_acc1f + __cuda_local_var_20767_16_non_const_bin), (__cuda_local_var_20769_16_non_const_a.x));
# 164 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20785_16_non_const_acc1f + (Lfft + __cuda_local_var_20767_16_non_const_bin)), (__cuda_local_var_20769_16_non_const_a.y));
# 167 "../kernels/cuda/arithmetic_xmac_kernels.cu"
return;
# 168 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}}
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__attribute__((global)) void _Z34cu_accumulate_2pol_autoOnly_packedPK6float4P6float2mm(
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
const struct float4 *__restrict__ xy,
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
struct float2 *__restrict__ acc,
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Lfft,
# 170 "../kernels/cuda/arithmetic_xmac_kernels.cu"
size_t Nfft){
# 171 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 172 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20795_16_non_const_nthreads;
# 173 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20796_16_non_const_idx;
# 174 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20797_16_non_const_last_idx;
# 175 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 size_t __cuda_local_var_20798_16_non_const_bin;
# 177 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20800_16_non_const_a;
# 200 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 float *__cuda_local_var_20815_16_non_const_acc1f;
# 172 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20795_16_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 173 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20796_16_non_const_idx = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 174 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20797_16_non_const_last_idx = (Lfft * Nfft);
# 175 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20798_16_non_const_bin = (__cuda_local_var_20796_16_non_const_idx % Lfft);
# 177 "../kernels/cuda/arithmetic_xmac_kernels.cu"
memset((char *)&__cuda_local_var_20800_16_non_const_a, 0,sizeof(__cuda_local_var_20800_16_non_const_a));
# 177 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20800_16_non_const_a.x = (0.0F);
# 177 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20800_16_non_const_a.y = (0.0F);
# 179 "../kernels/cuda/arithmetic_xmac_kernels.cu"
while (__cuda_local_var_20796_16_non_const_idx < __cuda_local_var_20797_16_non_const_last_idx)
# 179 "../kernels/cuda/arithmetic_xmac_kernels.cu"
{
# 181 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20804_21_non_const_P1;
# 182 "../kernels/cuda/arithmetic_xmac_kernels.cu"
 struct float2 __cuda_local_var_20805_21_non_const_P2;
# 181 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20804_21_non_const_P1.x) = ((xy[__cuda_local_var_20796_16_non_const_idx]).x);
# 181 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20804_21_non_const_P1.y) = ((xy[__cuda_local_var_20796_16_non_const_idx]).y);
# 182 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20805_21_non_const_P2.x) = ((xy[__cuda_local_var_20796_16_non_const_idx]).z);
# 182 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20805_21_non_const_P2.y) = ((xy[__cuda_local_var_20796_16_non_const_idx]).w);
# 186 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20800_16_non_const_a.x) += (((__cuda_local_var_20804_21_non_const_P1.x) * (__cuda_local_var_20804_21_non_const_P1.x)) + ((__cuda_local_var_20804_21_non_const_P1.y) * (__cuda_local_var_20804_21_non_const_P1.y)));
# 187 "../kernels/cuda/arithmetic_xmac_kernels.cu"
(__cuda_local_var_20800_16_non_const_a.y) += (((__cuda_local_var_20805_21_non_const_P2.x) * (__cuda_local_var_20805_21_non_const_P2.x)) + ((__cuda_local_var_20805_21_non_const_P2.y) * (__cuda_local_var_20805_21_non_const_P2.y)));
# 189 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20796_16_non_const_idx += __cuda_local_var_20795_16_non_const_nthreads;
# 190 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}
# 200 "../kernels/cuda/arithmetic_xmac_kernels.cu"
__cuda_local_var_20815_16_non_const_acc1f = ((float *)acc);
# 201 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20815_16_non_const_acc1f + __cuda_local_var_20798_16_non_const_bin), (__cuda_local_var_20800_16_non_const_a.x));
# 202 "../kernels/cuda/arithmetic_xmac_kernels.cu"
_Z9atomicAddPff((__cuda_local_var_20815_16_non_const_acc1f + (Lfft + __cuda_local_var_20798_16_non_const_bin)), (__cuda_local_var_20800_16_non_const_a.y));
# 205 "../kernels/cuda/arithmetic_xmac_kernels.cu"
return;
# 207 "../kernels/cuda/arithmetic_xmac_kernels.cu"
}}
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) void _Z17autoPowerSpectrumP6float2Pfmm(
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
cufftComplex *__restrict__ c,
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
float *__restrict__ a,
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t output_size,
# 44 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t batch_size){
# 45 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{
# 46 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20838_18_non_const_numThreads;
# 47 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20839_18_non_const_threadID;
# 46 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20838_18_non_const_numThreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 47 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20839_18_non_const_threadID = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x))); {
# 49 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t i;
# 49 "../kernels/cuda/arithmetic_autospec_kernels.cu"
i = __cuda_local_var_20839_18_non_const_threadID;
# 49 "../kernels/cuda/arithmetic_autospec_kernels.cu"
for (; (i < (output_size * batch_size)); i += __cuda_local_var_20838_18_non_const_numThreads)
# 50 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{ cufftComplex __T298;
# 51 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20843_15_non_const_j;
# 51 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20843_15_non_const_j = (i % output_size);
# 52 "../kernels/cuda/arithmetic_autospec_kernels.cu"
_Z9atomicAddPff((a + __cuda_local_var_20843_15_non_const_j), ((__T298 = (c[i])) , (((__T298.x) * (__T298.x)) + ((__T298.y) * (__T298.y)))));
# 53 "../kernels/cuda/arithmetic_autospec_kernels.cu"
} }
# 55 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}}
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) void _Z20autoPowerSpectrum_v2PK6float2Pfmm(
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const cufftComplex *__restrict__ c,
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
float *__restrict__ a,
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t Lfftout,
# 58 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t Nfft){
# 59 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{
# 60 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20852_12_non_const_i_start;
# 60 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20852_12_non_const_i_start = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 61 "../kernels/cuda/arithmetic_autospec_kernels.cu"
if (__cuda_local_var_20852_12_non_const_i_start < Lfftout)
# 62 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{
# 63 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20855_15_non_const_acc;
# 64 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20856_16_non_const_i;
# 63 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20855_15_non_const_acc = (a[__cuda_local_var_20852_12_non_const_i_start]);
# 64 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20856_16_non_const_i = __cuda_local_var_20852_12_non_const_i_start; {
# 65 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t j;
# 65 "../kernels/cuda/arithmetic_autospec_kernels.cu"
j = 0UL;
# 65 "../kernels/cuda/arithmetic_autospec_kernels.cu"
for (; (j < Nfft); (j++) , (void)(__cuda_local_var_20856_16_non_const_i += Lfftout))
# 66 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{ cufftComplex __T299;
# 67 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20855_15_non_const_acc += ((__T299 = ((*(cufftComplex *)&(c[__cuda_local_var_20856_16_non_const_i])))) , (((__T299.x) * (__T299.x)) + ((__T299.y) * (__T299.y))));
# 69 "../kernels/cuda/arithmetic_autospec_kernels.cu"
} }
# 73 "../kernels/cuda/arithmetic_autospec_kernels.cu"
(a[__cuda_local_var_20852_12_non_const_i_start]) = __cuda_local_var_20855_15_non_const_acc;
# 74 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 75 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}}
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) void _Z20autoPowerSpectrum_v3PK6float2Pfmm(
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const cufftComplex *__restrict__ c,
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
float *__restrict__ a,
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t Lfftout,
# 78 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t Nfft){
# 79 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{
# 80 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20872_12_non_const_nthreads;
# 81 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20873_12_non_const_idx;
# 82 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20874_12_non_const_bin;
# 83 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20875_12_non_const_last_idx;
# 97 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20877_11_non_const_acc;
# 98 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20878_11_non_const_comp;
# 80 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20872_12_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 81 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20873_12_non_const_idx = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 82 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20874_12_non_const_bin = (__cuda_local_var_20873_12_non_const_idx % Lfftout);
# 83 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20875_12_non_const_last_idx = (Lfftout * Nfft);
# 97 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20877_11_non_const_acc = (0.0F);
# 98 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20878_11_non_const_comp = (0.0F);
# 99 "../kernels/cuda/arithmetic_autospec_kernels.cu"
while (__cuda_local_var_20873_12_non_const_idx < __cuda_local_var_20875_12_non_const_last_idx)
# 100 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{ cufftComplex __T2100;
# 101 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20881_15_non_const_P;
# 102 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20882_15_non_const_y;
# 103 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20883_15_non_const_t;
# 101 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20881_15_non_const_P = ((__T2100 = ((*(cufftComplex *)&(c[__cuda_local_var_20873_12_non_const_idx])))) , (((__T2100.x) * (__T2100.x)) + ((__T2100.y) * (__T2100.y))));
# 102 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20882_15_non_const_y = (__cuda_local_var_20881_15_non_const_P - __cuda_local_var_20878_11_non_const_comp);
# 103 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20883_15_non_const_t = (__cuda_local_var_20877_11_non_const_acc + __cuda_local_var_20882_15_non_const_y);
# 104 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20878_11_non_const_comp = ((__cuda_local_var_20883_15_non_const_t - __cuda_local_var_20877_11_non_const_acc) - __cuda_local_var_20882_15_non_const_y);
# 105 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20877_11_non_const_acc = __cuda_local_var_20883_15_non_const_t;
# 106 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20873_12_non_const_idx += __cuda_local_var_20872_12_non_const_nthreads;
# 107 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 111 "../kernels/cuda/arithmetic_autospec_kernels.cu"
_Z9atomicAddPff((a + __cuda_local_var_20874_12_non_const_bin), __cuda_local_var_20877_11_non_const_acc);
# 112 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}}
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__attribute__((global)) void _Z23autoPowerSpectrum_v3_CBPKfPfmm(
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const cufftReal *__restrict__ P,
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
float *__restrict__ a,
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t Lfftout,
# 127 "../kernels/cuda/arithmetic_autospec_kernels.cu"
const size_t Nfft){
# 128 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{
# 129 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20909_12_non_const_nthreads;
# 130 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20910_12_non_const_idx;
# 131 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20911_12_non_const_bin;
# 132 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 size_t __cuda_local_var_20912_12_non_const_last_idx;
# 142 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20914_11_non_const_acc;
# 143 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20915_11_non_const_comp;
# 129 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20909_12_non_const_nthreads = ((size_t)((blockDim.x) * (gridDim.x)));
# 130 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20910_12_non_const_idx = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 131 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20911_12_non_const_bin = (__cuda_local_var_20910_12_non_const_idx % Lfftout);
# 132 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20912_12_non_const_last_idx = (Lfftout * Nfft);
# 142 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20914_11_non_const_acc = (0.0F);
# 143 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20915_11_non_const_comp = (0.0F);
# 144 "../kernels/cuda/arithmetic_autospec_kernels.cu"
while (__cuda_local_var_20910_12_non_const_idx < __cuda_local_var_20912_12_non_const_last_idx)
# 145 "../kernels/cuda/arithmetic_autospec_kernels.cu"
{
# 146 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20918_15_non_const_y;
# 147 "../kernels/cuda/arithmetic_autospec_kernels.cu"
 float __cuda_local_var_20919_15_non_const_t;
# 146 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20918_15_non_const_y = ((P[__cuda_local_var_20910_12_non_const_idx]) - __cuda_local_var_20915_11_non_const_comp);
# 147 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20919_15_non_const_t = (__cuda_local_var_20914_11_non_const_acc + __cuda_local_var_20918_15_non_const_y);
# 148 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20915_11_non_const_comp = ((__cuda_local_var_20919_15_non_const_t - __cuda_local_var_20914_11_non_const_acc) - __cuda_local_var_20918_15_non_const_y);
# 149 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20914_11_non_const_acc = __cuda_local_var_20919_15_non_const_t;
# 150 "../kernels/cuda/arithmetic_autospec_kernels.cu"
__cuda_local_var_20910_12_non_const_idx += __cuda_local_var_20909_12_non_const_nthreads;
# 151 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}
# 155 "../kernels/cuda/arithmetic_autospec_kernels.cu"
_Z9atomicAddPff((a + __cuda_local_var_20911_12_non_const_bin), __cuda_local_var_20914_11_non_const_acc);
# 156 "../kernels/cuda/arithmetic_autospec_kernels.cu"
}}
# 56 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__attribute__((global)) void _Z14cu_window_hannP6float4mm(
# 56 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
struct float4 *inout,
# 56 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
size_t fftlen_f4,
# 56 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
size_t datalen_f4){
# 57 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{
# 58 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 size_t __cuda_local_var_20937_16_non_const_i_f4;
# 59 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 float __cuda_local_var_20938_15_non_const_N;
# 60 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 float __cuda_local_var_20939_15_non_const_n;
# 62 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 float __cuda_local_var_20941_15_non_const_omega;
# 58 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20937_16_non_const_i_f4 = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 59 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20938_15_non_const_N = ((float)((4.0) * ((double)fftlen_f4)));
# 60 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20939_15_non_const_n = ((float)((4.0) * ((double)(__cuda_local_var_20937_16_non_const_i_f4 % fftlen_f4))));
# 62 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20941_15_non_const_omega = ( fdividef((6.283185482F) , (__cuda_local_var_20938_15_non_const_N - (1.0F))));
# 63 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
if (__cuda_local_var_20937_16_non_const_i_f4 < datalen_f4)
# 63 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{
# 64 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 struct float4 __cuda_local_var_20943_24_non_const_v;
# 65 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 struct float4 __cuda_local_var_20944_24_non_const_wv;
# 64 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
memset((char *)&__cuda_local_var_20943_24_non_const_v, 0,sizeof(__cuda_local_var_20943_24_non_const_v));
# 64 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20943_24_non_const_v = (inout[__cuda_local_var_20937_16_non_const_i_f4]);
# 65 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20944_24_non_const_wv.x) = ( fdividef(((__cuda_local_var_20943_24_non_const_v.x) * ((1.0F) - (__cosf((__cuda_local_var_20941_15_non_const_omega * __cuda_local_var_20939_15_non_const_n))))) , (2.0F)));
# 65 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20944_24_non_const_wv.y) = ( fdividef(((__cuda_local_var_20943_24_non_const_v.y) * ((1.0F) - (__cosf((__cuda_local_var_20941_15_non_const_omega * (__cuda_local_var_20939_15_non_const_n + (1.0F))))))) , (2.0F)));
# 65 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20944_24_non_const_wv.z) = ( fdividef(((__cuda_local_var_20943_24_non_const_v.z) * ((1.0F) - (__cosf((__cuda_local_var_20941_15_non_const_omega * (__cuda_local_var_20939_15_non_const_n + (2.0F))))))) , (2.0F)));
# 65 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20944_24_non_const_wv.w) = ( fdividef(((__cuda_local_var_20943_24_non_const_v.w) * ((1.0F) - (__cosf((__cuda_local_var_20941_15_non_const_omega * (__cuda_local_var_20939_15_non_const_n + (3.0F))))))) , (2.0F)));
# 71 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(inout[__cuda_local_var_20937_16_non_const_i_f4]) = __cuda_local_var_20944_24_non_const_wv;
# 72 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}
# 73 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}}
# 82 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__attribute__((global)) void _Z17cu_window_hammingP6float4mm(
# 82 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
struct float4 *inout,
# 82 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
size_t fftlen_f4,
# 82 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
size_t datalen_f4){
# 83 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{
# 84 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 size_t __cuda_local_var_20956_16_non_const_i_f4;
# 85 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 float __cuda_local_var_20957_15_non_const_N;
# 86 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 float __cuda_local_var_20958_15_non_const_n;
# 88 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 float __cuda_local_var_20960_15_non_const_omega;
# 84 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20956_16_non_const_i_f4 = ((size_t)(((blockIdx.x) * (blockDim.x)) + (threadIdx.x)));
# 85 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20957_15_non_const_N = ((float)((4.0) * ((double)fftlen_f4)));
# 86 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20958_15_non_const_n = ((float)((4.0) * ((double)(__cuda_local_var_20956_16_non_const_i_f4 % fftlen_f4))));
# 88 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20960_15_non_const_omega = ( fdividef((6.283185482F) , (__cuda_local_var_20957_15_non_const_N - (1.0F))));
# 89 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
if (__cuda_local_var_20956_16_non_const_i_f4 < datalen_f4)
# 89 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
{
# 90 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 struct float4 __cuda_local_var_20962_24_non_const_v;
# 91 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
 struct float4 __cuda_local_var_20963_24_non_const_wv;
# 90 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
memset((char *)&__cuda_local_var_20962_24_non_const_v, 0,sizeof(__cuda_local_var_20962_24_non_const_v));
# 90 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
__cuda_local_var_20962_24_non_const_v = (inout[__cuda_local_var_20956_16_non_const_i_f4]);
# 91 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20963_24_non_const_wv.x) = ((__cuda_local_var_20962_24_non_const_v.x) * ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20960_15_non_const_omega * __cuda_local_var_20958_15_non_const_n))))));
# 91 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20963_24_non_const_wv.y) = ((__cuda_local_var_20962_24_non_const_v.y) * ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20960_15_non_const_omega * (__cuda_local_var_20958_15_non_const_n + (1.0F))))))));
# 91 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20963_24_non_const_wv.z) = ((__cuda_local_var_20962_24_non_const_v.z) * ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20960_15_non_const_omega * (__cuda_local_var_20958_15_non_const_n + (2.0F))))))));
# 91 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(__cuda_local_var_20963_24_non_const_wv.w) = ((__cuda_local_var_20962_24_non_const_v.w) * ((0.5400000215F) - ((0.4600000083F) * (__cosf((__cuda_local_var_20960_15_non_const_omega * (__cuda_local_var_20958_15_non_const_n + (3.0F))))))));
# 97 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
(inout[__cuda_local_var_20956_16_non_const_i_f4]) = __cuda_local_var_20963_24_non_const_wv;
# 98 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}
# 99 "../kernels/cuda/arithmetic_winfunc_kernels.cu"
}}
