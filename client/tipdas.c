// temporary interface program to test corr.c 

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <time.h>

#include "socket.h"
//#include "network.h"
#include "main.h"

#define		STR_LEN 	256
//#define         SOCKET_PORT     8001
//#define         SERVER_ADDRESS  "127.0.0.1"
#define		SLEEP_TIME	100000
#define RCVDATASIZE	20*4+4096*8+1

void printRcv(char * rcvarray);
void saveRcv(char * rcvarray);

int main(int argc, char *argv[])
{
    int sockfd;
    char buf[STR_LEN]; 
    int running = 0;
    u_int i;
	int nbytes;
	int maxbyte,i_byte;
	char rcvbuf[RCVDATASIZE];
	char fullbuf[RCVDATASIZE];
	int new_byte;
    
	maxbyte=RCVDATASIZE;
	i_byte=0;
	memset(rcvbuf,'0',sizeof(rcvbuf));
	memset(fullbuf,'0',sizeof(fullbuf));

    	sockfd=connect_server(SPECSRV_ADDRESS,DAS_PORT);
	i=sockfd;
	if(sockfd) running=1;
    while(running){
		if((nbytes = recv(sockfd,buf, sizeof(buf),0)) <= 0)
		{
			/* got error or connection closed by client */
			if(nbytes == 0)
			 	/* connection closed */
	 			printf("%s: socket %d hung up\n", argv[0], i);
 			else
				perror("recv() error lol!");
			/* close it... */
			close(sockfd);
			running=0;
		}
		else
		{
			printf("I got some data with a size of %d\n",nbytes);
			memcpy(&new_byte,buf,sizeof(int));
			fprintf(stderr,"First int is %d\n",new_byte);
			/* we got some data from a client*/
			memcpy(rcvbuf+(i_byte),buf,nbytes);
			i_byte+=nbytes;
			while(i_byte<maxbyte)
			{
				nbytes=recv(sockfd,buf,sizeof(buf),0);
				memcpy(rcvbuf+i_byte,buf,nbytes);
				i_byte+=nbytes;
		//		if((lastbuf=(char *)memchr(rcvbuf,'\r',i_byte))!=NULL) {printf("Found a endmark\n");break;}
			}
			memcpy(fullbuf,rcvbuf,i_byte);
			i_byte=0;
			printRcv(fullbuf);
			saveRcv(fullbuf);
		}
	}
	return 0;
}

void printRcv(char * rcvarray)
{
	int rcvI;
	int i=4; //,d=4;
	
	memcpy(&rcvI,rcvarray,sizeof(int));
	printf("DATE %4d/",(rcvI));
	memcpy(&rcvI,rcvarray+i,sizeof(int));
	printf("%02d/",(rcvI));
	i+=4;
	memcpy(&rcvI,rcvarray+i,sizeof(int));
	printf("%02d\t",(rcvI));
	i+=4;
	memcpy(&rcvI,rcvarray+i,sizeof(int));
	printf("Time %02d:",(rcvI));
	i+=4;
	memcpy(&rcvI,rcvarray+i,sizeof(int));
	printf("%02d:",(rcvI));
	i+=4;
	memcpy(&rcvI,rcvarray+i,sizeof(int));
	printf("%02d\n",(rcvI));

	return;
}

void saveRcv(char * rcvarray)
{
	FILE *fp;
	static int cnt=1;
	char fname[256];
	
	sprintf(fname,"seg%d.bin",cnt);
	fp=fopen(fname,"wb");
	cnt++;
	fwrite(rcvarray,RCVDATASIZE,1,fp);
	fclose(fp);

	return;
}
