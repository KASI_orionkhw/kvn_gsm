#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <signal.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>

//#include "define.h"		//Corr structure included
//#include "ansicosCal.h"
#include "main.h"
#include "socket.h"
#include "kfftspec.h"
//#include "cmdfromspecom.h"

#define SHM_KEY_FS	7
#define READY	1
#define INTEG	2

#define DATAMAX	4096
#define CORDATASIZE	(20*4+DATAMAX*8)	// 20*integer + max 4096*double

void *mainprocessing(void *);
void *dasprocessing(void *);
void doprocessing(int sock);
void SetTickVal(fftspec_config_t* fs);
//void HaltCorr(void);
void CheckFork(fftspec_config_t* fs);
int CheckHalt(int flag);

int old_tick=0;
int integ_flag=0, halt_flag=0;
int result;
int server_fd;
int running=1;


int main()
{

   int newsockfd;
   
   struct sockaddr_in client_address;
   int client_len;
   char cmdbuf[256];
   int pid, th_id;
   int th_das_id;

   pthread_t thread_t;
   pthread_t thread_t_das;
   
   fftspec_config_t* fs;
   int nattach;
   connectshm_FS(SHM_KEY_FS,&fs);
//   corr->das=-1;
//   corr->dasfd=-1;
   
   //
   // Below is command server
   // 
   server_fd=create_server(FS_PORT);
   client_len=sizeof(struct sockaddr_in);
   if(server_fd<0) return -1;
   
   fftspec_setup(fs);

//   OpenCal();
//   InitSys(1);
//   DataInit();
//   AccReset();
   
//   ClrIntegFlag();
//   corr->integ=0;
//   corr->band=1;
//   corr->tseg=1000;
//   corr->tick=0;
//   corr->tick_per_sec=0;
//   corr->startflag=0;
//   corr->stopflag=0;
//   corr->haltflag=0;

   th_das_id = pthread_create(&thread_t_das,NULL,&dasprocessing,NULL);
   th_id = pthread_create(&thread_t,NULL,&mainprocessing,NULL);
   memset(cmdbuf,'\0',sizeof(cmdbuf));


   	while (running) {
		newsockfd= accept(server_fd, (struct sockaddr *)&client_address,(socklen_t*) &client_len);
		if (newsockfd <0)
        {
            perror("ERROR on accept");
            exit(1);
        }
        /* Create child process */
        pid = fork();
        if (pid < 0)
        {
            perror("ERROR on fork");
        exit(1);
        }
        else if (pid >= 0)
        {
            // This is the client process 

            close(server_fd);
            doprocessing(newsockfd);
            exit(0);
        }
        else
        {
            close(newsockfd);
        }
      	running = CheckHalt(running);
//		if(running==0) close(server_fd);
//Doing managing correlator
    } /* end of while */
	if((nattach=shmdt(fs))==-1) fprintf(stderr,"shmdt failed\n");
	usleep(1000);
	if(nattach==0) {
		fprintf(stderr,"OK, any process is not attached to the memory area\n");
		usleep(1000);
		if(shmctl(SHM_KEY_FS,IPC_RMID,0)==-1) {
			   	fprintf(stderr,"Share memory cannot be closed\n");
				fprintf(stderr,"I tried to close the memory, but I couldn't\n");
				fprintf(stderr,"Please use ipcs and ipcrm -m to eliminate the memory\n");
		}
	}
	else {
		fprintf(stderr,"Trying to kill the memory...\n");
		if(shmctl(SHM_KEY_FS,IPC_RMID,0)==-1) {
				fprintf(stderr,"Share memory cannot be closed\n");
				fprintf(stderr,"Please use ipcs and ipcrm -m to eliminate the memory\n");
		}
	}
	pthread_detach(thread_t);
	pthread_detach(thread_t_das);
	fprintf(stderr,"Good bye\n");
   return 0;
}

void *dasprocessing(void *args)
{
	fftspec_config_t* fs;
	int das_fd;
	int client_len;
	int das_running=1;
	int newsockfd;
	int now_fd=-1;
   	struct sockaddr_in client_address;
	//char readbuf[256];

   	das_fd=create_server(DAS_PORT);
   	client_len=sizeof(struct sockaddr_in);

	if(das_fd<0) {
		fprintf(stderr,"Cannot make das server\n");
		exit(1);
	}
	connectshm_FS(SHM_KEY_FS, &fs);
	
   	while (das_running) {
		newsockfd= accept(das_fd, (struct sockaddr *)&client_address,(socklen_t *) &client_len);
		if (newsockfd <0)
        {
            perror("ERROR on accept");
            exit(1);
        }
		else if (now_fd!=newsockfd) {
			if (now_fd>0) {
				printf("Another das client is trying connection\n");
				if (fs->dasfd<0) {
						close(now_fd);
						fs->dasfd=newsockfd;
						now_fd=newsockfd;
				}

			}
			else if(now_fd<0) {
				printf("Das client is newly connected at %d\n",newsockfd);
				fs->dasfd=newsockfd;
				now_fd=newsockfd;
			}
			else {
			}
		}

      	das_running = CheckHalt(das_running);
    } /* end of while */

	close(das_fd);
	return NULL;
}

void doprocessing(int sock)
{
    int n;
	static int thread_running=1;
    char buffer[256];
    fftspec_config_t* fs;

   connectshm_FS(SHM_KEY_FS,&fs);

    bzero(buffer,256);

	while(thread_running) {
	    n = read(sock,buffer,255);
	    if (n < 0)
	    {
	        perror("ERROR reading from socket");
			thread_running=0;
	        exit(1);
	    }
	    printf("CMD: %s\n",buffer);
//	    n = write(sock,"I got your message",18);
	    if (n < 0)
	    {
	        perror("ERROR writing to socket");
	        thread_running=0;
			exit(1);
  		}
//		result= CheckCommandFromRealSpecom(buffer,corr);

		switch(result){
			case START_INTEG:
				break;
									
			case STOP_INTEG:
				break;
											
			case HALT_CORR:
				break;
											
			default:
				fprintf(stderr,"Noise signal came\n");
				result=0;
				break;
									
		}

	}
	close(sock);	
//	kill(getppid(),SIGTERM);
	if(shmdt(fs)==-1) fprintf(stderr,"shmdt failed\n");
	if(shmctl(SHM_KEY_FS,IPC_RMID,0)==-1) fprintf(stderr,"FORK:I tried to kill the momoey, but...\n");
	return ;
}

void CheckFork(fftspec_config_t* fs)
{
	static int old_band=0;	// old_band = 0 for original version
/*	
	if (corr->startflag) {
		fprintf(stderr,"init corr integ\n");
		corr->startflag=0;
		//These are setted only at start
		ResetErrorFlag();
		//	SetProgressTime();
		SetTickVal(corr);
		WRITESTATUS(INTEG);
		DataInit();
		AccReset();
		SetIntegFlag();
		corr->integ=1;
	}
	if (corr->stopflag) {
		fprintf(stderr,"stop corr integ\n");
		corr->stopflag=0;
		WRITESTATUS(READY);
		SetTickVal(corr);
	}
	if (corr->haltflag) {
		corr->haltflag=0;
		WRITESTATUS(READY);
		ClrIntegFlag();
		corr->integ=0;
		HaltCorr();
	}

	if(corr->band!=old_band) {
			ModeSet(corr->band);
			old_band=corr->band;
			fprintf(stderr,"Band mode is %d\n",old_band);
	}
*/
	return;
}
void SetTickVal(fftspec_config_t* fs)
{
//	corr->tick_per_sec=(int)(1000/corr->tseg);
//	corr->tick=0;
	return;
}
/*
void HaltCorr(void)
{
	halt_flag=1;
	return;
}
*/
int CheckHalt(int flag)
{
	if (halt_flag) return 0;
	return flag;
}

