/////////////////////////////////////////////////////////////////////////////
// Name:        cosCanvas.h
// Purpose:     Correlator Operating System
// Author:      Young-Zoo Yoon
// Modified by:
// Created:     06/27/2003
// Copyright:   (c); CubeX
// Licence:     wxWindows licence
/////////////////////////////////////////////////////////////////////////////

#ifndef _COSCAL_H_
#define _COSCAL_H_

#include "def.h"
void WRITERAWDATA(void);
void SENDRAWDATA(int sock_fd,int status, int tick, char buf[]);
void SEGWRITERAWDATA(int tick);
void WRITESTATUS(int corr_status);
void OpenCal(void);
void CloseCal(void);

void InitDIO96(void);
void InitCorr(void);
void ModeSet(int Mode);
void DataInit(void);
void AccReset(void);
int Integration(int IntegTime);
int IntegSegForCorr(int tseg);
void FindError(void);
void SetProgressTime(void);
void ClrProgressTime(void);
int IntegFlag(void);
void SetIntegFlag(void);
void ClrIntegFlag(void);
void SetObsFlag(void);
void ClrObsFlag(void);

void ResetErrorFlag(void);

void DataAcquisition1(void);
void DataAcquisition2(void);
void DataAcquisition3(void);
void DataAcquisition4(void);
void DataProcessing1(void);
void DataProcessing2(void);
void DataProcessing3(void);
void DataProcessing4(void);
void DataProcessingAll(int IntegTime);
void DataAverage(int IntegTime, double Data[]);
void SegDataProcessingAll(double IntegTime);
void SegDataAverage(double IntegTime, double Data[]);
void DataNormalize(void);
void PrintData(void);
void InitSys(int Mode);
void StartObs(int IntegTime);
void StopObs(void);
void QuitSys(void);
void ApplyWinFunc(int Select, double Data[]);
void RunFFT(void);
void cFileSave(const char* Header, double Data[], const char* FileName, int Range);
void MakeAC(int first, int second, double dData[]);

void SendStopSignal(void);
int GetBandMode(void);
void CommandFileSave(char *header, char *filename);

typedef struct COSDATA {
unsigned long nDataLSB[ASIZE], nDataMSB[ASIZE];
double dAcrData1[ASIZE], dAcrData2[ASIZE], dAcrData3[ASIZE], dAcrData4[ASIZE];
double dCalCoeff;
unsigned short cBitMask;

double dAcrData100[SIZE100];
double dAcrData50[SIZE50];
double dFFTData100[SIZE100];
double dFFTData50[SIZE50];
double dPlotData;
	
double dMax;
double dMin;
double dMid;
int nMaxIndex;

int nChannel;
int nWinFuncSel;
int nOpenFlag;
int nIntegFlag;
int nObsFlag;
int nFFTFlag;
int nIntegTime;
int nProgressTime;
int nSerLoopFlag;

int nSerConnectFlag;
int nCliConnectFlag;
int nBandMode;
int nMarkerFlag;
int nChipErrorFlag1;	// 0: Normal, 1: Abnormal
int nChipErrorFlag2;
int nChipErrorFlag3;
int nChipErrorFlag4;
int nAcrCorrect;

} cosData;

//static cosData pcosData;

#endif
