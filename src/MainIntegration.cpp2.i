# 1 "MainIntegration.cudafe1.gpu"
# 1 "<built-in>"
# 1 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 1 "<command-line>" 2
# 1 "MainIntegration.cudafe1.gpu"
typedef char __nv_bool;
# 1483 "/usr/local/cuda-8.0/bin/..//include/driver_types.h"
struct CUstream_st;
# 1488 "/usr/local/cuda-8.0/bin/..//include/driver_types.h"
struct CUevent_st;
# 54 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
enum cudaDataType_t {
# 56 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_16F = 2,
# 57 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_16F = 6,
# 58 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_32F = 0,
# 59 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_32F = 4,
# 60 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_64F = 1,
# 61 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_64F = 5,
# 62 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_8I = 3,
# 63 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_8I = 7,
# 64 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_8U,
# 65 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_8U,
# 66 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_32I,
# 67 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_32I,
# 68 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_R_32U,
# 69 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
CUDA_C_32U};
# 73 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
enum libraryPropertyType_t {
# 75 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
MAJOR_VERSION,
# 76 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
MINOR_VERSION,
# 77 "/usr/local/cuda-8.0/bin/..//include/library_types.h"
PATCH_LEVEL};
# 30 "/usr/include/bits/time.h" 3
struct timeval;
# 181 "/usr/include/libio.h" 3
enum __codecvt_result {
# 183 "/usr/include/libio.h" 3
__codecvt_ok,
# 184 "/usr/include/libio.h" 3
__codecvt_partial,
# 185 "/usr/include/libio.h" 3
__codecvt_error,
# 186 "/usr/include/libio.h" 3
__codecvt_noconv};
# 246 "/usr/include/libio.h" 3
struct _IO_FILE;
# 63 "/usr/include/bits/pthreadtypes.h" 3
union pthread_attr_t;
# 191 "/usr/include/math.h" 3
enum _ZUt_ {
# 192 "/usr/include/math.h" 3
FP_NAN,
# 195 "/usr/include/math.h" 3
FP_INFINITE,
# 198 "/usr/include/math.h" 3
FP_ZERO,
# 201 "/usr/include/math.h" 3
FP_SUBNORMAL,
# 204 "/usr/include/math.h" 3
FP_NORMAL};
# 289 "/usr/include/math.h" 3
enum _LIB_VERSION_TYPE {
# 290 "/usr/include/math.h" 3
_IEEE_ = (-1),
# 291 "/usr/include/math.h" 3
_SVID_,
# 292 "/usr/include/math.h" 3
_XOPEN_,
# 293 "/usr/include/math.h" 3
_POSIX_,
# 294 "/usr/include/math.h" 3
_ISOC_};
# 49 "/usr/include/bits/shm.h" 3
struct shmid_ds;
# 24 "/usr/include/bits/socket_type.h" 3
enum __socket_type {
# 26 "/usr/include/bits/socket_type.h" 3
SOCK_STREAM = 1,
# 29 "/usr/include/bits/socket_type.h" 3
SOCK_DGRAM,
# 32 "/usr/include/bits/socket_type.h" 3
SOCK_RAW,
# 34 "/usr/include/bits/socket_type.h" 3
SOCK_RDM,
# 36 "/usr/include/bits/socket_type.h" 3
SOCK_SEQPACKET,
# 39 "/usr/include/bits/socket_type.h" 3
SOCK_DCCP,
# 41 "/usr/include/bits/socket_type.h" 3
SOCK_PACKET = 10,
# 49 "/usr/include/bits/socket_type.h" 3
SOCK_CLOEXEC = 524288,
# 52 "/usr/include/bits/socket_type.h" 3
SOCK_NONBLOCK = 2048};
# 148 "/usr/include/bits/socket.h" 3
struct sockaddr;
# 171 "/usr/include/bits/socket.h" 3
enum _ZUt0_ {
# 172 "/usr/include/bits/socket.h" 3
MSG_OOB = 1,
# 174 "/usr/include/bits/socket.h" 3
MSG_PEEK,
# 176 "/usr/include/bits/socket.h" 3
MSG_DONTROUTE = 4,
# 180 "/usr/include/bits/socket.h" 3
MSG_TRYHARD = 4,
# 183 "/usr/include/bits/socket.h" 3
MSG_CTRUNC = 8,
# 185 "/usr/include/bits/socket.h" 3
MSG_PROXY = 16,
# 187 "/usr/include/bits/socket.h" 3
MSG_TRUNC = 32,
# 189 "/usr/include/bits/socket.h" 3
MSG_DONTWAIT = 64,
# 191 "/usr/include/bits/socket.h" 3
MSG_EOR = 128,
# 193 "/usr/include/bits/socket.h" 3
MSG_WAITALL = 256,
# 195 "/usr/include/bits/socket.h" 3
MSG_FIN = 512,
# 197 "/usr/include/bits/socket.h" 3
MSG_SYN = 1024,
# 199 "/usr/include/bits/socket.h" 3
MSG_CONFIRM = 2048,
# 201 "/usr/include/bits/socket.h" 3
MSG_RST = 4096,
# 203 "/usr/include/bits/socket.h" 3
MSG_ERRQUEUE = 8192,
# 205 "/usr/include/bits/socket.h" 3
MSG_NOSIGNAL = 16384,
# 207 "/usr/include/bits/socket.h" 3
MSG_MORE = 32768,
# 209 "/usr/include/bits/socket.h" 3
MSG_WAITFORONE = 65536,
# 212 "/usr/include/bits/socket.h" 3
MSG_CMSG_CLOEXEC = 1073741824};
# 297 "/usr/include/bits/socket.h" 3
enum _ZUt1_ {
# 298 "/usr/include/bits/socket.h" 3
SCM_RIGHTS = 1,
# 301 "/usr/include/bits/socket.h" 3
SCM_CREDENTIALS};
# 54 "/usr/include/sys/socket.h" 3
enum _ZUt2_ {
# 55 "/usr/include/sys/socket.h" 3
SHUT_RD,
# 57 "/usr/include/sys/socket.h" 3
SHUT_WR,
# 59 "/usr/include/sys/socket.h" 3
SHUT_RDWR};
# 152 "/usr/include/bits/siginfo.h" 3
enum _ZUt3_ {
# 153 "/usr/include/bits/siginfo.h" 3
SI_ASYNCNL = (-60),
# 155 "/usr/include/bits/siginfo.h" 3
SI_TKILL = (-6),
# 157 "/usr/include/bits/siginfo.h" 3
SI_SIGIO,
# 159 "/usr/include/bits/siginfo.h" 3
SI_ASYNCIO,
# 161 "/usr/include/bits/siginfo.h" 3
SI_MESGQ,
# 163 "/usr/include/bits/siginfo.h" 3
SI_TIMER,
# 165 "/usr/include/bits/siginfo.h" 3
SI_QUEUE,
# 167 "/usr/include/bits/siginfo.h" 3
SI_USER,
# 169 "/usr/include/bits/siginfo.h" 3
SI_KERNEL = 128};
# 176 "/usr/include/bits/siginfo.h" 3
enum _ZUt4_ {
# 177 "/usr/include/bits/siginfo.h" 3
ILL_ILLOPC = 1,
# 179 "/usr/include/bits/siginfo.h" 3
ILL_ILLOPN,
# 181 "/usr/include/bits/siginfo.h" 3
ILL_ILLADR,
# 183 "/usr/include/bits/siginfo.h" 3
ILL_ILLTRP,
# 185 "/usr/include/bits/siginfo.h" 3
ILL_PRVOPC,
# 187 "/usr/include/bits/siginfo.h" 3
ILL_PRVREG,
# 189 "/usr/include/bits/siginfo.h" 3
ILL_COPROC,
# 191 "/usr/include/bits/siginfo.h" 3
ILL_BADSTK};
# 197 "/usr/include/bits/siginfo.h" 3
enum _ZUt5_ {
# 198 "/usr/include/bits/siginfo.h" 3
FPE_INTDIV = 1,
# 200 "/usr/include/bits/siginfo.h" 3
FPE_INTOVF,
# 202 "/usr/include/bits/siginfo.h" 3
FPE_FLTDIV,
# 204 "/usr/include/bits/siginfo.h" 3
FPE_FLTOVF,
# 206 "/usr/include/bits/siginfo.h" 3
FPE_FLTUND,
# 208 "/usr/include/bits/siginfo.h" 3
FPE_FLTRES,
# 210 "/usr/include/bits/siginfo.h" 3
FPE_FLTINV,
# 212 "/usr/include/bits/siginfo.h" 3
FPE_FLTSUB};
# 218 "/usr/include/bits/siginfo.h" 3
enum _ZUt6_ {
# 219 "/usr/include/bits/siginfo.h" 3
SEGV_MAPERR = 1,
# 221 "/usr/include/bits/siginfo.h" 3
SEGV_ACCERR};
# 227 "/usr/include/bits/siginfo.h" 3
enum _ZUt7_ {
# 228 "/usr/include/bits/siginfo.h" 3
BUS_ADRALN = 1,
# 230 "/usr/include/bits/siginfo.h" 3
BUS_ADRERR,
# 232 "/usr/include/bits/siginfo.h" 3
BUS_OBJERR};
# 238 "/usr/include/bits/siginfo.h" 3
enum _ZUt8_ {
# 239 "/usr/include/bits/siginfo.h" 3
TRAP_BRKPT = 1,
# 241 "/usr/include/bits/siginfo.h" 3
TRAP_TRACE};
# 247 "/usr/include/bits/siginfo.h" 3
enum _ZUt9_ {
# 248 "/usr/include/bits/siginfo.h" 3
CLD_EXITED = 1,
# 250 "/usr/include/bits/siginfo.h" 3
CLD_KILLED,
# 252 "/usr/include/bits/siginfo.h" 3
CLD_DUMPED,
# 254 "/usr/include/bits/siginfo.h" 3
CLD_TRAPPED,
# 256 "/usr/include/bits/siginfo.h" 3
CLD_STOPPED,
# 258 "/usr/include/bits/siginfo.h" 3
CLD_CONTINUED};
# 264 "/usr/include/bits/siginfo.h" 3
enum _ZUt10_ {
# 265 "/usr/include/bits/siginfo.h" 3
POLL_IN = 1,
# 267 "/usr/include/bits/siginfo.h" 3
POLL_OUT,
# 269 "/usr/include/bits/siginfo.h" 3
POLL_MSG,
# 271 "/usr/include/bits/siginfo.h" 3
POLL_ERR,
# 273 "/usr/include/bits/siginfo.h" 3
POLL_PRI,
# 275 "/usr/include/bits/siginfo.h" 3
POLL_HUP};
# 329 "/usr/include/bits/siginfo.h" 3
enum _ZUt11_ {
# 330 "/usr/include/bits/siginfo.h" 3
SIGEV_SIGNAL,
# 332 "/usr/include/bits/siginfo.h" 3
SIGEV_NONE,
# 334 "/usr/include/bits/siginfo.h" 3
SIGEV_THREAD,
# 337 "/usr/include/bits/siginfo.h" 3
SIGEV_THREAD_ID = 4};
# 34 "/usr/include/bits/sigstack.h" 3
enum _ZUt12_ {
# 35 "/usr/include/bits/sigstack.h" 3
SS_ONSTACK = 1,
# 37 "/usr/include/bits/sigstack.h" 3
SS_DISABLE};
# 42 "/usr/include/sys/ucontext.h" 3
enum _ZUt13_ {
# 43 "/usr/include/sys/ucontext.h" 3
REG_R8,
# 45 "/usr/include/sys/ucontext.h" 3
REG_R9,
# 47 "/usr/include/sys/ucontext.h" 3
REG_R10,
# 49 "/usr/include/sys/ucontext.h" 3
REG_R11,
# 51 "/usr/include/sys/ucontext.h" 3
REG_R12,
# 53 "/usr/include/sys/ucontext.h" 3
REG_R13,
# 55 "/usr/include/sys/ucontext.h" 3
REG_R14,
# 57 "/usr/include/sys/ucontext.h" 3
REG_R15,
# 59 "/usr/include/sys/ucontext.h" 3
REG_RDI,
# 61 "/usr/include/sys/ucontext.h" 3
REG_RSI,
# 63 "/usr/include/sys/ucontext.h" 3
REG_RBP,
# 65 "/usr/include/sys/ucontext.h" 3
REG_RBX,
# 67 "/usr/include/sys/ucontext.h" 3
REG_RDX,
# 69 "/usr/include/sys/ucontext.h" 3
REG_RAX,
# 71 "/usr/include/sys/ucontext.h" 3
REG_RCX,
# 73 "/usr/include/sys/ucontext.h" 3
REG_RSP,
# 75 "/usr/include/sys/ucontext.h" 3
REG_RIP,
# 77 "/usr/include/sys/ucontext.h" 3
REG_EFL,
# 79 "/usr/include/sys/ucontext.h" 3
REG_CSGSFS,
# 81 "/usr/include/sys/ucontext.h" 3
REG_ERR,
# 83 "/usr/include/sys/ucontext.h" 3
REG_TRAPNO,
# 85 "/usr/include/sys/ucontext.h" 3
REG_OLDMASK,
# 87 "/usr/include/sys/ucontext.h" 3
REG_CR2};
# 32 "/usr/include/netinet/in.h" 3
struct in_addr;
# 42 "/usr/include/netinet/in.h" 3
enum _ZUt14_ {
# 43 "/usr/include/netinet/in.h" 3
IPPROTO_IP,
# 45 "/usr/include/netinet/in.h" 3
IPPROTO_ICMP,
# 47 "/usr/include/netinet/in.h" 3
IPPROTO_IGMP,
# 49 "/usr/include/netinet/in.h" 3
IPPROTO_IPIP = 4,
# 51 "/usr/include/netinet/in.h" 3
IPPROTO_TCP = 6,
# 53 "/usr/include/netinet/in.h" 3
IPPROTO_EGP = 8,
# 55 "/usr/include/netinet/in.h" 3
IPPROTO_PUP = 12,
# 57 "/usr/include/netinet/in.h" 3
IPPROTO_UDP = 17,
# 59 "/usr/include/netinet/in.h" 3
IPPROTO_IDP = 22,
# 61 "/usr/include/netinet/in.h" 3
IPPROTO_TP = 29,
# 63 "/usr/include/netinet/in.h" 3
IPPROTO_DCCP = 33,
# 65 "/usr/include/netinet/in.h" 3
IPPROTO_IPV6 = 41,
# 67 "/usr/include/netinet/in.h" 3
IPPROTO_RSVP = 46,
# 69 "/usr/include/netinet/in.h" 3
IPPROTO_GRE,
# 71 "/usr/include/netinet/in.h" 3
IPPROTO_ESP = 50,
# 73 "/usr/include/netinet/in.h" 3
IPPROTO_AH,
# 75 "/usr/include/netinet/in.h" 3
IPPROTO_MTP = 92,
# 77 "/usr/include/netinet/in.h" 3
IPPROTO_BEETPH = 94,
# 79 "/usr/include/netinet/in.h" 3
IPPROTO_ENCAP = 98,
# 81 "/usr/include/netinet/in.h" 3
IPPROTO_PIM = 103,
# 83 "/usr/include/netinet/in.h" 3
IPPROTO_COMP = 108,
# 85 "/usr/include/netinet/in.h" 3
IPPROTO_SCTP = 132,
# 87 "/usr/include/netinet/in.h" 3
IPPROTO_UDPLITE = 136,
# 89 "/usr/include/netinet/in.h" 3
IPPROTO_RAW = 255,
# 91 "/usr/include/netinet/in.h" 3
IPPROTO_MAX};
# 99 "/usr/include/netinet/in.h" 3
enum _ZUt15_ {
# 100 "/usr/include/netinet/in.h" 3
IPPROTO_HOPOPTS,
# 102 "/usr/include/netinet/in.h" 3
IPPROTO_ROUTING = 43,
# 104 "/usr/include/netinet/in.h" 3
IPPROTO_FRAGMENT,
# 106 "/usr/include/netinet/in.h" 3
IPPROTO_ICMPV6 = 58,
# 108 "/usr/include/netinet/in.h" 3
IPPROTO_NONE,
# 110 "/usr/include/netinet/in.h" 3
IPPROTO_DSTOPTS,
# 112 "/usr/include/netinet/in.h" 3
IPPROTO_MH = 135};
# 122 "/usr/include/netinet/in.h" 3
enum _ZUt16_ {
# 123 "/usr/include/netinet/in.h" 3
IPPORT_ECHO = 7,
# 124 "/usr/include/netinet/in.h" 3
IPPORT_DISCARD = 9,
# 125 "/usr/include/netinet/in.h" 3
IPPORT_SYSTAT = 11,
# 126 "/usr/include/netinet/in.h" 3
IPPORT_DAYTIME = 13,
# 127 "/usr/include/netinet/in.h" 3
IPPORT_NETSTAT = 15,
# 128 "/usr/include/netinet/in.h" 3
IPPORT_FTP = 21,
# 129 "/usr/include/netinet/in.h" 3
IPPORT_TELNET = 23,
# 130 "/usr/include/netinet/in.h" 3
IPPORT_SMTP = 25,
# 131 "/usr/include/netinet/in.h" 3
IPPORT_TIMESERVER = 37,
# 132 "/usr/include/netinet/in.h" 3
IPPORT_NAMESERVER = 42,
# 133 "/usr/include/netinet/in.h" 3
IPPORT_WHOIS,
# 134 "/usr/include/netinet/in.h" 3
IPPORT_MTP = 57,
# 136 "/usr/include/netinet/in.h" 3
IPPORT_TFTP = 69,
# 137 "/usr/include/netinet/in.h" 3
IPPORT_RJE = 77,
# 138 "/usr/include/netinet/in.h" 3
IPPORT_FINGER = 79,
# 139 "/usr/include/netinet/in.h" 3
IPPORT_TTYLINK = 87,
# 140 "/usr/include/netinet/in.h" 3
IPPORT_SUPDUP = 95,
# 143 "/usr/include/netinet/in.h" 3
IPPORT_EXECSERVER = 512,
# 144 "/usr/include/netinet/in.h" 3
IPPORT_LOGINSERVER,
# 145 "/usr/include/netinet/in.h" 3
IPPORT_CMDSERVER,
# 146 "/usr/include/netinet/in.h" 3
IPPORT_EFSSERVER = 520,
# 149 "/usr/include/netinet/in.h" 3
IPPORT_BIFFUDP = 512,
# 150 "/usr/include/netinet/in.h" 3
IPPORT_WHOSERVER,
# 151 "/usr/include/netinet/in.h" 3
IPPORT_ROUTESERVER = 520,
# 154 "/usr/include/netinet/in.h" 3
IPPORT_RESERVED = 1024,
# 157 "/usr/include/netinet/in.h" 3
IPPORT_USERRESERVED = 5000};
# 238 "/usr/include/netinet/in.h" 3
struct sockaddr_in;
# 237 "/usr/include/bits/fcntl-linux.h" 3
enum __pid_type {
# 239 "/usr/include/bits/fcntl-linux.h" 3
F_OWNER_TID,
# 240 "/usr/include/bits/fcntl-linux.h" 3
F_OWNER_PID,
# 241 "/usr/include/bits/fcntl-linux.h" 3
F_OWNER_PGRP,
# 242 "/usr/include/bits/fcntl-linux.h" 3
F_OWNER_GID = 2};
# 26 "/usr/include/bits/confname.h" 3
enum _ZUt17_ {
# 27 "/usr/include/bits/confname.h" 3
_PC_LINK_MAX,
# 29 "/usr/include/bits/confname.h" 3
_PC_MAX_CANON,
# 31 "/usr/include/bits/confname.h" 3
_PC_MAX_INPUT,
# 33 "/usr/include/bits/confname.h" 3
_PC_NAME_MAX,
# 35 "/usr/include/bits/confname.h" 3
_PC_PATH_MAX,
# 37 "/usr/include/bits/confname.h" 3
_PC_PIPE_BUF,
# 39 "/usr/include/bits/confname.h" 3
_PC_CHOWN_RESTRICTED,
# 41 "/usr/include/bits/confname.h" 3
_PC_NO_TRUNC,
# 43 "/usr/include/bits/confname.h" 3
_PC_VDISABLE,
# 45 "/usr/include/bits/confname.h" 3
_PC_SYNC_IO,
# 47 "/usr/include/bits/confname.h" 3
_PC_ASYNC_IO,
# 49 "/usr/include/bits/confname.h" 3
_PC_PRIO_IO,
# 51 "/usr/include/bits/confname.h" 3
_PC_SOCK_MAXBUF,
# 53 "/usr/include/bits/confname.h" 3
_PC_FILESIZEBITS,
# 55 "/usr/include/bits/confname.h" 3
_PC_REC_INCR_XFER_SIZE,
# 57 "/usr/include/bits/confname.h" 3
_PC_REC_MAX_XFER_SIZE,
# 59 "/usr/include/bits/confname.h" 3
_PC_REC_MIN_XFER_SIZE,
# 61 "/usr/include/bits/confname.h" 3
_PC_REC_XFER_ALIGN,
# 63 "/usr/include/bits/confname.h" 3
_PC_ALLOC_SIZE_MIN,
# 65 "/usr/include/bits/confname.h" 3
_PC_SYMLINK_MAX,
# 67 "/usr/include/bits/confname.h" 3
_PC_2_SYMLINKS};
# 73 "/usr/include/bits/confname.h" 3
enum _ZUt18_ {
# 74 "/usr/include/bits/confname.h" 3
_SC_ARG_MAX,
# 76 "/usr/include/bits/confname.h" 3
_SC_CHILD_MAX,
# 78 "/usr/include/bits/confname.h" 3
_SC_CLK_TCK,
# 80 "/usr/include/bits/confname.h" 3
_SC_NGROUPS_MAX,
# 82 "/usr/include/bits/confname.h" 3
_SC_OPEN_MAX,
# 84 "/usr/include/bits/confname.h" 3
_SC_STREAM_MAX,
# 86 "/usr/include/bits/confname.h" 3
_SC_TZNAME_MAX,
# 88 "/usr/include/bits/confname.h" 3
_SC_JOB_CONTROL,
# 90 "/usr/include/bits/confname.h" 3
_SC_SAVED_IDS,
# 92 "/usr/include/bits/confname.h" 3
_SC_REALTIME_SIGNALS,
# 94 "/usr/include/bits/confname.h" 3
_SC_PRIORITY_SCHEDULING,
# 96 "/usr/include/bits/confname.h" 3
_SC_TIMERS,
# 98 "/usr/include/bits/confname.h" 3
_SC_ASYNCHRONOUS_IO,
# 100 "/usr/include/bits/confname.h" 3
_SC_PRIORITIZED_IO,
# 102 "/usr/include/bits/confname.h" 3
_SC_SYNCHRONIZED_IO,
# 104 "/usr/include/bits/confname.h" 3
_SC_FSYNC,
# 106 "/usr/include/bits/confname.h" 3
_SC_MAPPED_FILES,
# 108 "/usr/include/bits/confname.h" 3
_SC_MEMLOCK,
# 110 "/usr/include/bits/confname.h" 3
_SC_MEMLOCK_RANGE,
# 112 "/usr/include/bits/confname.h" 3
_SC_MEMORY_PROTECTION,
# 114 "/usr/include/bits/confname.h" 3
_SC_MESSAGE_PASSING,
# 116 "/usr/include/bits/confname.h" 3
_SC_SEMAPHORES,
# 118 "/usr/include/bits/confname.h" 3
_SC_SHARED_MEMORY_OBJECTS,
# 120 "/usr/include/bits/confname.h" 3
_SC_AIO_LISTIO_MAX,
# 122 "/usr/include/bits/confname.h" 3
_SC_AIO_MAX,
# 124 "/usr/include/bits/confname.h" 3
_SC_AIO_PRIO_DELTA_MAX,
# 126 "/usr/include/bits/confname.h" 3
_SC_DELAYTIMER_MAX,
# 128 "/usr/include/bits/confname.h" 3
_SC_MQ_OPEN_MAX,
# 130 "/usr/include/bits/confname.h" 3
_SC_MQ_PRIO_MAX,
# 132 "/usr/include/bits/confname.h" 3
_SC_VERSION,
# 134 "/usr/include/bits/confname.h" 3
_SC_PAGESIZE,
# 137 "/usr/include/bits/confname.h" 3
_SC_RTSIG_MAX,
# 139 "/usr/include/bits/confname.h" 3
_SC_SEM_NSEMS_MAX,
# 141 "/usr/include/bits/confname.h" 3
_SC_SEM_VALUE_MAX,
# 143 "/usr/include/bits/confname.h" 3
_SC_SIGQUEUE_MAX,
# 145 "/usr/include/bits/confname.h" 3
_SC_TIMER_MAX,
# 150 "/usr/include/bits/confname.h" 3
_SC_BC_BASE_MAX,
# 152 "/usr/include/bits/confname.h" 3
_SC_BC_DIM_MAX,
# 154 "/usr/include/bits/confname.h" 3
_SC_BC_SCALE_MAX,
# 156 "/usr/include/bits/confname.h" 3
_SC_BC_STRING_MAX,
# 158 "/usr/include/bits/confname.h" 3
_SC_COLL_WEIGHTS_MAX,
# 160 "/usr/include/bits/confname.h" 3
_SC_EQUIV_CLASS_MAX,
# 162 "/usr/include/bits/confname.h" 3
_SC_EXPR_NEST_MAX,
# 164 "/usr/include/bits/confname.h" 3
_SC_LINE_MAX,
# 166 "/usr/include/bits/confname.h" 3
_SC_RE_DUP_MAX,
# 168 "/usr/include/bits/confname.h" 3
_SC_CHARCLASS_NAME_MAX,
# 171 "/usr/include/bits/confname.h" 3
_SC_2_VERSION,
# 173 "/usr/include/bits/confname.h" 3
_SC_2_C_BIND,
# 175 "/usr/include/bits/confname.h" 3
_SC_2_C_DEV,
# 177 "/usr/include/bits/confname.h" 3
_SC_2_FORT_DEV,
# 179 "/usr/include/bits/confname.h" 3
_SC_2_FORT_RUN,
# 181 "/usr/include/bits/confname.h" 3
_SC_2_SW_DEV,
# 183 "/usr/include/bits/confname.h" 3
_SC_2_LOCALEDEF,
# 186 "/usr/include/bits/confname.h" 3
_SC_PII,
# 188 "/usr/include/bits/confname.h" 3
_SC_PII_XTI,
# 190 "/usr/include/bits/confname.h" 3
_SC_PII_SOCKET,
# 192 "/usr/include/bits/confname.h" 3
_SC_PII_INTERNET,
# 194 "/usr/include/bits/confname.h" 3
_SC_PII_OSI,
# 196 "/usr/include/bits/confname.h" 3
_SC_POLL,
# 198 "/usr/include/bits/confname.h" 3
_SC_SELECT,
# 200 "/usr/include/bits/confname.h" 3
_SC_UIO_MAXIOV,
# 202 "/usr/include/bits/confname.h" 3
_SC_IOV_MAX = 60,
# 204 "/usr/include/bits/confname.h" 3
_SC_PII_INTERNET_STREAM,
# 206 "/usr/include/bits/confname.h" 3
_SC_PII_INTERNET_DGRAM,
# 208 "/usr/include/bits/confname.h" 3
_SC_PII_OSI_COTS,
# 210 "/usr/include/bits/confname.h" 3
_SC_PII_OSI_CLTS,
# 212 "/usr/include/bits/confname.h" 3
_SC_PII_OSI_M,
# 214 "/usr/include/bits/confname.h" 3
_SC_T_IOV_MAX,
# 218 "/usr/include/bits/confname.h" 3
_SC_THREADS,
# 220 "/usr/include/bits/confname.h" 3
_SC_THREAD_SAFE_FUNCTIONS,
# 222 "/usr/include/bits/confname.h" 3
_SC_GETGR_R_SIZE_MAX,
# 224 "/usr/include/bits/confname.h" 3
_SC_GETPW_R_SIZE_MAX,
# 226 "/usr/include/bits/confname.h" 3
_SC_LOGIN_NAME_MAX,
# 228 "/usr/include/bits/confname.h" 3
_SC_TTY_NAME_MAX,
# 230 "/usr/include/bits/confname.h" 3
_SC_THREAD_DESTRUCTOR_ITERATIONS,
# 232 "/usr/include/bits/confname.h" 3
_SC_THREAD_KEYS_MAX,
# 234 "/usr/include/bits/confname.h" 3
_SC_THREAD_STACK_MIN,
# 236 "/usr/include/bits/confname.h" 3
_SC_THREAD_THREADS_MAX,
# 238 "/usr/include/bits/confname.h" 3
_SC_THREAD_ATTR_STACKADDR,
# 240 "/usr/include/bits/confname.h" 3
_SC_THREAD_ATTR_STACKSIZE,
# 242 "/usr/include/bits/confname.h" 3
_SC_THREAD_PRIORITY_SCHEDULING,
# 244 "/usr/include/bits/confname.h" 3
_SC_THREAD_PRIO_INHERIT,
# 246 "/usr/include/bits/confname.h" 3
_SC_THREAD_PRIO_PROTECT,
# 248 "/usr/include/bits/confname.h" 3
_SC_THREAD_PROCESS_SHARED,
# 251 "/usr/include/bits/confname.h" 3
_SC_NPROCESSORS_CONF,
# 253 "/usr/include/bits/confname.h" 3
_SC_NPROCESSORS_ONLN,
# 255 "/usr/include/bits/confname.h" 3
_SC_PHYS_PAGES,
# 257 "/usr/include/bits/confname.h" 3
_SC_AVPHYS_PAGES,
# 259 "/usr/include/bits/confname.h" 3
_SC_ATEXIT_MAX,
# 261 "/usr/include/bits/confname.h" 3
_SC_PASS_MAX,
# 264 "/usr/include/bits/confname.h" 3
_SC_XOPEN_VERSION,
# 266 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XCU_VERSION,
# 268 "/usr/include/bits/confname.h" 3
_SC_XOPEN_UNIX,
# 270 "/usr/include/bits/confname.h" 3
_SC_XOPEN_CRYPT,
# 272 "/usr/include/bits/confname.h" 3
_SC_XOPEN_ENH_I18N,
# 274 "/usr/include/bits/confname.h" 3
_SC_XOPEN_SHM,
# 277 "/usr/include/bits/confname.h" 3
_SC_2_CHAR_TERM,
# 279 "/usr/include/bits/confname.h" 3
_SC_2_C_VERSION,
# 281 "/usr/include/bits/confname.h" 3
_SC_2_UPE,
# 284 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XPG2,
# 286 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XPG3,
# 288 "/usr/include/bits/confname.h" 3
_SC_XOPEN_XPG4,
# 291 "/usr/include/bits/confname.h" 3
_SC_CHAR_BIT,
# 293 "/usr/include/bits/confname.h" 3
_SC_CHAR_MAX,
# 295 "/usr/include/bits/confname.h" 3
_SC_CHAR_MIN,
# 297 "/usr/include/bits/confname.h" 3
_SC_INT_MAX,
# 299 "/usr/include/bits/confname.h" 3
_SC_INT_MIN,
# 301 "/usr/include/bits/confname.h" 3
_SC_LONG_BIT,
# 303 "/usr/include/bits/confname.h" 3
_SC_WORD_BIT,
# 305 "/usr/include/bits/confname.h" 3
_SC_MB_LEN_MAX,
# 307 "/usr/include/bits/confname.h" 3
_SC_NZERO,
# 309 "/usr/include/bits/confname.h" 3
_SC_SSIZE_MAX,
# 311 "/usr/include/bits/confname.h" 3
_SC_SCHAR_MAX,
# 313 "/usr/include/bits/confname.h" 3
_SC_SCHAR_MIN,
# 315 "/usr/include/bits/confname.h" 3
_SC_SHRT_MAX,
# 317 "/usr/include/bits/confname.h" 3
_SC_SHRT_MIN,
# 319 "/usr/include/bits/confname.h" 3
_SC_UCHAR_MAX,
# 321 "/usr/include/bits/confname.h" 3
_SC_UINT_MAX,
# 323 "/usr/include/bits/confname.h" 3
_SC_ULONG_MAX,
# 325 "/usr/include/bits/confname.h" 3
_SC_USHRT_MAX,
# 328 "/usr/include/bits/confname.h" 3
_SC_NL_ARGMAX,
# 330 "/usr/include/bits/confname.h" 3
_SC_NL_LANGMAX,
# 332 "/usr/include/bits/confname.h" 3
_SC_NL_MSGMAX,
# 334 "/usr/include/bits/confname.h" 3
_SC_NL_NMAX,
# 336 "/usr/include/bits/confname.h" 3
_SC_NL_SETMAX,
# 338 "/usr/include/bits/confname.h" 3
_SC_NL_TEXTMAX,
# 341 "/usr/include/bits/confname.h" 3
_SC_XBS5_ILP32_OFF32,
# 343 "/usr/include/bits/confname.h" 3
_SC_XBS5_ILP32_OFFBIG,
# 345 "/usr/include/bits/confname.h" 3
_SC_XBS5_LP64_OFF64,
# 347 "/usr/include/bits/confname.h" 3
_SC_XBS5_LPBIG_OFFBIG,
# 350 "/usr/include/bits/confname.h" 3
_SC_XOPEN_LEGACY,
# 352 "/usr/include/bits/confname.h" 3
_SC_XOPEN_REALTIME,
# 354 "/usr/include/bits/confname.h" 3
_SC_XOPEN_REALTIME_THREADS,
# 357 "/usr/include/bits/confname.h" 3
_SC_ADVISORY_INFO,
# 359 "/usr/include/bits/confname.h" 3
_SC_BARRIERS,
# 361 "/usr/include/bits/confname.h" 3
_SC_BASE,
# 363 "/usr/include/bits/confname.h" 3
_SC_C_LANG_SUPPORT,
# 365 "/usr/include/bits/confname.h" 3
_SC_C_LANG_SUPPORT_R,
# 367 "/usr/include/bits/confname.h" 3
_SC_CLOCK_SELECTION,
# 369 "/usr/include/bits/confname.h" 3
_SC_CPUTIME,
# 371 "/usr/include/bits/confname.h" 3
_SC_THREAD_CPUTIME,
# 373 "/usr/include/bits/confname.h" 3
_SC_DEVICE_IO,
# 375 "/usr/include/bits/confname.h" 3
_SC_DEVICE_SPECIFIC,
# 377 "/usr/include/bits/confname.h" 3
_SC_DEVICE_SPECIFIC_R,
# 379 "/usr/include/bits/confname.h" 3
_SC_FD_MGMT,
# 381 "/usr/include/bits/confname.h" 3
_SC_FIFO,
# 383 "/usr/include/bits/confname.h" 3
_SC_PIPE,
# 385 "/usr/include/bits/confname.h" 3
_SC_FILE_ATTRIBUTES,
# 387 "/usr/include/bits/confname.h" 3
_SC_FILE_LOCKING,
# 389 "/usr/include/bits/confname.h" 3
_SC_FILE_SYSTEM,
# 391 "/usr/include/bits/confname.h" 3
_SC_MONOTONIC_CLOCK,
# 393 "/usr/include/bits/confname.h" 3
_SC_MULTI_PROCESS,
# 395 "/usr/include/bits/confname.h" 3
_SC_SINGLE_PROCESS,
# 397 "/usr/include/bits/confname.h" 3
_SC_NETWORKING,
# 399 "/usr/include/bits/confname.h" 3
_SC_READER_WRITER_LOCKS,
# 401 "/usr/include/bits/confname.h" 3
_SC_SPIN_LOCKS,
# 403 "/usr/include/bits/confname.h" 3
_SC_REGEXP,
# 405 "/usr/include/bits/confname.h" 3
_SC_REGEX_VERSION,
# 407 "/usr/include/bits/confname.h" 3
_SC_SHELL,
# 409 "/usr/include/bits/confname.h" 3
_SC_SIGNALS,
# 411 "/usr/include/bits/confname.h" 3
_SC_SPAWN,
# 413 "/usr/include/bits/confname.h" 3
_SC_SPORADIC_SERVER,
# 415 "/usr/include/bits/confname.h" 3
_SC_THREAD_SPORADIC_SERVER,
# 417 "/usr/include/bits/confname.h" 3
_SC_SYSTEM_DATABASE,
# 419 "/usr/include/bits/confname.h" 3
_SC_SYSTEM_DATABASE_R,
# 421 "/usr/include/bits/confname.h" 3
_SC_TIMEOUTS,
# 423 "/usr/include/bits/confname.h" 3
_SC_TYPED_MEMORY_OBJECTS,
# 425 "/usr/include/bits/confname.h" 3
_SC_USER_GROUPS,
# 427 "/usr/include/bits/confname.h" 3
_SC_USER_GROUPS_R,
# 429 "/usr/include/bits/confname.h" 3
_SC_2_PBS,
# 431 "/usr/include/bits/confname.h" 3
_SC_2_PBS_ACCOUNTING,
# 433 "/usr/include/bits/confname.h" 3
_SC_2_PBS_LOCATE,
# 435 "/usr/include/bits/confname.h" 3
_SC_2_PBS_MESSAGE,
# 437 "/usr/include/bits/confname.h" 3
_SC_2_PBS_TRACK,
# 439 "/usr/include/bits/confname.h" 3
_SC_SYMLOOP_MAX,
# 441 "/usr/include/bits/confname.h" 3
_SC_STREAMS,
# 443 "/usr/include/bits/confname.h" 3
_SC_2_PBS_CHECKPOINT,
# 446 "/usr/include/bits/confname.h" 3
_SC_V6_ILP32_OFF32,
# 448 "/usr/include/bits/confname.h" 3
_SC_V6_ILP32_OFFBIG,
# 450 "/usr/include/bits/confname.h" 3
_SC_V6_LP64_OFF64,
# 452 "/usr/include/bits/confname.h" 3
_SC_V6_LPBIG_OFFBIG,
# 455 "/usr/include/bits/confname.h" 3
_SC_HOST_NAME_MAX,
# 457 "/usr/include/bits/confname.h" 3
_SC_TRACE,
# 459 "/usr/include/bits/confname.h" 3
_SC_TRACE_EVENT_FILTER,
# 461 "/usr/include/bits/confname.h" 3
_SC_TRACE_INHERIT,
# 463 "/usr/include/bits/confname.h" 3
_SC_TRACE_LOG,
# 466 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_ICACHE_SIZE,
# 468 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_ICACHE_ASSOC,
# 470 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_ICACHE_LINESIZE,
# 472 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_DCACHE_SIZE,
# 474 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_DCACHE_ASSOC,
# 476 "/usr/include/bits/confname.h" 3
_SC_LEVEL1_DCACHE_LINESIZE,
# 478 "/usr/include/bits/confname.h" 3
_SC_LEVEL2_CACHE_SIZE,
# 480 "/usr/include/bits/confname.h" 3
_SC_LEVEL2_CACHE_ASSOC,
# 482 "/usr/include/bits/confname.h" 3
_SC_LEVEL2_CACHE_LINESIZE,
# 484 "/usr/include/bits/confname.h" 3
_SC_LEVEL3_CACHE_SIZE,
# 486 "/usr/include/bits/confname.h" 3
_SC_LEVEL3_CACHE_ASSOC,
# 488 "/usr/include/bits/confname.h" 3
_SC_LEVEL3_CACHE_LINESIZE,
# 490 "/usr/include/bits/confname.h" 3
_SC_LEVEL4_CACHE_SIZE,
# 492 "/usr/include/bits/confname.h" 3
_SC_LEVEL4_CACHE_ASSOC,
# 494 "/usr/include/bits/confname.h" 3
_SC_LEVEL4_CACHE_LINESIZE,
# 498 "/usr/include/bits/confname.h" 3
_SC_IPV6 = 235,
# 500 "/usr/include/bits/confname.h" 3
_SC_RAW_SOCKETS,
# 503 "/usr/include/bits/confname.h" 3
_SC_V7_ILP32_OFF32,
# 505 "/usr/include/bits/confname.h" 3
_SC_V7_ILP32_OFFBIG,
# 507 "/usr/include/bits/confname.h" 3
_SC_V7_LP64_OFF64,
# 509 "/usr/include/bits/confname.h" 3
_SC_V7_LPBIG_OFFBIG,
# 512 "/usr/include/bits/confname.h" 3
_SC_SS_REPL_MAX,
# 515 "/usr/include/bits/confname.h" 3
_SC_TRACE_EVENT_NAME_MAX,
# 517 "/usr/include/bits/confname.h" 3
_SC_TRACE_NAME_MAX,
# 519 "/usr/include/bits/confname.h" 3
_SC_TRACE_SYS_MAX,
# 521 "/usr/include/bits/confname.h" 3
_SC_TRACE_USER_EVENT_MAX,
# 524 "/usr/include/bits/confname.h" 3
_SC_XOPEN_STREAMS,
# 527 "/usr/include/bits/confname.h" 3
_SC_THREAD_ROBUST_PRIO_INHERIT,
# 529 "/usr/include/bits/confname.h" 3
_SC_THREAD_ROBUST_PRIO_PROTECT};
# 535 "/usr/include/bits/confname.h" 3
enum _ZUt19_ {
# 536 "/usr/include/bits/confname.h" 3
_CS_PATH,
# 539 "/usr/include/bits/confname.h" 3
_CS_V6_WIDTH_RESTRICTED_ENVS,
# 543 "/usr/include/bits/confname.h" 3
_CS_GNU_LIBC_VERSION,
# 545 "/usr/include/bits/confname.h" 3
_CS_GNU_LIBPTHREAD_VERSION,
# 548 "/usr/include/bits/confname.h" 3
_CS_V5_WIDTH_RESTRICTED_ENVS,
# 552 "/usr/include/bits/confname.h" 3
_CS_V7_WIDTH_RESTRICTED_ENVS,
# 556 "/usr/include/bits/confname.h" 3
_CS_LFS_CFLAGS = 1000,
# 558 "/usr/include/bits/confname.h" 3
_CS_LFS_LDFLAGS,
# 560 "/usr/include/bits/confname.h" 3
_CS_LFS_LIBS,
# 562 "/usr/include/bits/confname.h" 3
_CS_LFS_LINTFLAGS,
# 564 "/usr/include/bits/confname.h" 3
_CS_LFS64_CFLAGS,
# 566 "/usr/include/bits/confname.h" 3
_CS_LFS64_LDFLAGS,
# 568 "/usr/include/bits/confname.h" 3
_CS_LFS64_LIBS,
# 570 "/usr/include/bits/confname.h" 3
_CS_LFS64_LINTFLAGS,
# 573 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_CFLAGS = 1100,
# 575 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_LDFLAGS,
# 577 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_LIBS,
# 579 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFF32_LINTFLAGS,
# 581 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_CFLAGS,
# 583 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_LDFLAGS,
# 585 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_LIBS,
# 587 "/usr/include/bits/confname.h" 3
_CS_XBS5_ILP32_OFFBIG_LINTFLAGS,
# 589 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_CFLAGS,
# 591 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_LDFLAGS,
# 593 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_LIBS,
# 595 "/usr/include/bits/confname.h" 3
_CS_XBS5_LP64_OFF64_LINTFLAGS,
# 597 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_CFLAGS,
# 599 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_LDFLAGS,
# 601 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_LIBS,
# 603 "/usr/include/bits/confname.h" 3
_CS_XBS5_LPBIG_OFFBIG_LINTFLAGS,
# 606 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_CFLAGS,
# 608 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_LDFLAGS,
# 610 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_LIBS,
# 612 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFF32_LINTFLAGS,
# 614 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_CFLAGS,
# 616 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS,
# 618 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_LIBS,
# 620 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_ILP32_OFFBIG_LINTFLAGS,
# 622 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_CFLAGS,
# 624 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_LDFLAGS,
# 626 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_LIBS,
# 628 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LP64_OFF64_LINTFLAGS,
# 630 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS,
# 632 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS,
# 634 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_LIBS,
# 636 "/usr/include/bits/confname.h" 3
_CS_POSIX_V6_LPBIG_OFFBIG_LINTFLAGS,
# 639 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_CFLAGS,
# 641 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_LDFLAGS,
# 643 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_LIBS,
# 645 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFF32_LINTFLAGS,
# 647 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_CFLAGS,
# 649 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS,
# 651 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_LIBS,
# 653 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_ILP32_OFFBIG_LINTFLAGS,
# 655 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_CFLAGS,
# 657 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_LDFLAGS,
# 659 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_LIBS,
# 661 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LP64_OFF64_LINTFLAGS,
# 663 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS,
# 665 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS,
# 667 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_LIBS,
# 669 "/usr/include/bits/confname.h" 3
_CS_POSIX_V7_LPBIG_OFFBIG_LINTFLAGS,
# 672 "/usr/include/bits/confname.h" 3
_CS_V6_ENV,
# 674 "/usr/include/bits/confname.h" 3
_CS_V7_ENV};
# 48 "/usr/include/ctype.h" 3
enum _ZUt20_ {
# 49 "/usr/include/ctype.h" 3
_ISupper = 256,
# 50 "/usr/include/ctype.h" 3
_ISlower = 512,
# 51 "/usr/include/ctype.h" 3
_ISalpha = 1024,
# 52 "/usr/include/ctype.h" 3
_ISdigit = 2048,
# 53 "/usr/include/ctype.h" 3
_ISxdigit = 4096,
# 54 "/usr/include/ctype.h" 3
_ISspace = 8192,
# 55 "/usr/include/ctype.h" 3
_ISprint = 16384,
# 56 "/usr/include/ctype.h" 3
_ISgraph = 32768,
# 57 "/usr/include/ctype.h" 3
_ISblank = 1,
# 58 "/usr/include/ctype.h" 3
_IScntrl,
# 59 "/usr/include/ctype.h" 3
_ISpunct = 4,
# 60 "/usr/include/ctype.h" 3
_ISalnum = 8};
# 33 "/usr/include/pthread.h" 3
enum _ZUt21_ {
# 34 "/usr/include/pthread.h" 3
PTHREAD_CREATE_JOINABLE,
# 36 "/usr/include/pthread.h" 3
PTHREAD_CREATE_DETACHED};
# 43 "/usr/include/pthread.h" 3
enum _ZUt22_ {
# 44 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_TIMED_NP,
# 45 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_RECURSIVE_NP,
# 46 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ERRORCHECK_NP,
# 47 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ADAPTIVE_NP,
# 50 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_NORMAL = 0,
# 51 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_RECURSIVE,
# 52 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ERRORCHECK,
# 53 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_DEFAULT = 0,
# 57 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_FAST_NP = 0};
# 65 "/usr/include/pthread.h" 3
enum _ZUt23_ {
# 66 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_STALLED,
# 67 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_STALLED_NP = 0,
# 68 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ROBUST,
# 69 "/usr/include/pthread.h" 3
PTHREAD_MUTEX_ROBUST_NP = 1};
# 77 "/usr/include/pthread.h" 3
enum _ZUt24_ {
# 78 "/usr/include/pthread.h" 3
PTHREAD_PRIO_NONE,
# 79 "/usr/include/pthread.h" 3
PTHREAD_PRIO_INHERIT,
# 80 "/usr/include/pthread.h" 3
PTHREAD_PRIO_PROTECT};
# 114 "/usr/include/pthread.h" 3
enum _ZUt25_ {
# 115 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_PREFER_READER_NP,
# 116 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_PREFER_WRITER_NP,
# 117 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_PREFER_WRITER_NONRECURSIVE_NP,
# 118 "/usr/include/pthread.h" 3
PTHREAD_RWLOCK_DEFAULT_NP = 0};
# 155 "/usr/include/pthread.h" 3
enum _ZUt26_ {
# 156 "/usr/include/pthread.h" 3
PTHREAD_INHERIT_SCHED,
# 158 "/usr/include/pthread.h" 3
PTHREAD_EXPLICIT_SCHED};
# 165 "/usr/include/pthread.h" 3
enum _ZUt27_ {
# 166 "/usr/include/pthread.h" 3
PTHREAD_SCOPE_SYSTEM,
# 168 "/usr/include/pthread.h" 3
PTHREAD_SCOPE_PROCESS};
# 175 "/usr/include/pthread.h" 3
enum _ZUt28_ {
# 176 "/usr/include/pthread.h" 3
PTHREAD_PROCESS_PRIVATE,
# 178 "/usr/include/pthread.h" 3
PTHREAD_PROCESS_SHARED};
# 199 "/usr/include/pthread.h" 3
enum _ZUt29_ {
# 200 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_ENABLE,
# 202 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_DISABLE};
# 206 "/usr/include/pthread.h" 3
enum _ZUt30_ {
# 207 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_DEFERRED,
# 209 "/usr/include/pthread.h" 3
PTHREAD_CANCEL_ASYNCHRONOUS};
# 53 "../network/vdif_receiver_rb.h"
struct vdif_rx_tt;
# 92 "/usr/include/sys/time.h" 3
enum __itimer_which {
# 95 "/usr/include/sys/time.h" 3
ITIMER_REAL,
# 98 "/usr/include/sys/time.h" 3
ITIMER_VIRTUAL,
# 102 "/usr/include/sys/time.h" 3
ITIMER_PROF};
# 78 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
enum cufftResult_t {
# 79 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_SUCCESS,
# 80 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_PLAN,
# 81 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_ALLOC_FAILED,
# 82 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_TYPE,
# 83 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_VALUE,
# 84 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INTERNAL_ERROR,
# 85 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_EXEC_FAILED,
# 86 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_SETUP_FAILED,
# 87 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_SIZE,
# 88 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_UNALIGNED_DATA,
# 89 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INCOMPLETE_PARAMETER_LIST,
# 90 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_INVALID_DEVICE,
# 91 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_PARSE_ERROR,
# 92 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_NO_WORKSPACE,
# 93 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_NOT_IMPLEMENTED,
# 94 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_LICENSE_ERROR,
# 95 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_NOT_SUPPORTED};
# 121 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
enum cufftType_t {
# 122 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_R2C = 42,
# 123 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_C2R = 44,
# 124 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_C2C = 41,
# 125 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_D2Z = 106,
# 126 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_Z2D = 108,
# 127 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_Z2Z = 105};
# 131 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
enum cufftCompatibility_t {
# 132 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
CUFFT_COMPATIBILITY_FFTW_PADDING = 1};
# 60 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
enum cudaXtCopyType_t {
# 61 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_XT_COPY_HOST_TO_DEVICE,
# 62 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_XT_COPY_DEVICE_TO_HOST,
# 63 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_XT_COPY_DEVICE_TO_DEVICE};
# 67 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
enum libFormat_t {
# 68 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_FORMAT_CUFFT,
# 69 "/usr/local/cuda-8.0/bin/..//include/cudalibxt.h"
LIB_FORMAT_UNDEFINED};
# 79 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtSubFormat_t {
# 80 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_INPUT,
# 81 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_OUTPUT,
# 82 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_INPLACE,
# 83 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_INPLACE_SHUFFLED,
# 84 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_XT_FORMAT_1D_INPUT_SHUFFLED,
# 85 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_FORMAT_UNDEFINED};
# 91 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtCopyType_t {
# 92 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_HOST_TO_DEVICE,
# 93 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_DEVICE_TO_HOST,
# 94 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_DEVICE_TO_DEVICE,
# 95 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_COPY_UNDEFINED};
# 101 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtQueryType_t {
# 102 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_QUERY_1D_FACTORS,
# 103 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_QUERY_UNDEFINED};
# 173 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
enum cufftXtCallbackType_t {
# 174 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_COMPLEX,
# 175 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_COMPLEX_DOUBLE,
# 176 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_REAL,
# 177 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_LD_REAL_DOUBLE,
# 178 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_COMPLEX,
# 179 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_COMPLEX_DOUBLE,
# 180 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_REAL,
# 181 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_ST_REAL_DOUBLE,
# 182 "/usr/local/cuda-8.0/bin/..//include/cufftXt.h"
CUFFT_CB_UNDEFINED};
# 17 "../kernels/cuda/arithmetic_winfunc_kernels.h"
struct cu_window_cb_params_tt;
# 55 "kfftspec.h"
struct fftspec_config_tt;
# 128 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_voidIvEUt_E {
# 128 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_voidIvE7__valueE = 1};
# 148 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIbEUt_E {
# 148 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIbE7__valueE = 1};
# 155 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIcEUt_E {
# 155 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIcE7__valueE = 1};
# 162 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIaEUt_E {
# 162 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIaE7__valueE = 1};
# 169 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIhEUt_E {
# 169 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIhE7__valueE = 1};
# 177 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIwEUt_E {
# 177 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIwE7__valueE = 1};
# 201 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIsEUt_E {
# 201 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIsE7__valueE = 1};
# 208 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerItEUt_E {
# 208 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerItE7__valueE = 1};
# 215 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIiEUt_E {
# 215 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIiE7__valueE = 1};
# 222 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIjEUt_E {
# 222 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIjE7__valueE = 1};
# 229 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIlEUt_E {
# 229 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIlE7__valueE = 1};
# 236 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerImEUt_E {
# 236 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerImE7__valueE = 1};
# 243 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIxEUt_E {
# 243 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIxE7__valueE = 1};
# 250 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIyEUt_E {
# 250 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIyE7__valueE = 1};
# 268 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt13__is_floatingIfEUt_E {
# 268 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt13__is_floatingIfE7__valueE = 1};
# 275 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt13__is_floatingIdEUt_E {
# 275 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt13__is_floatingIdE7__valueE = 1};
# 282 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt13__is_floatingIeEUt_E {
# 282 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt13__is_floatingIeE7__valueE = 1};
# 358 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_charIcEUt_E {
# 358 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_charIcE7__valueE = 1};
# 366 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_charIwEUt_E {
# 366 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_charIwE7__valueE = 1};
# 381 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_byteIcEUt_E {
# 381 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_byteIcE7__valueE = 1};
# 388 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_byteIaEUt_E {
# 388 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_byteIaE7__valueE = 1};
# 395 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt9__is_byteIhEUt_E {
# 395 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt9__is_byteIhE7__valueE = 1};
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIeEUt_E {
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIeE7__valueE};
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIdEUt_E {
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIdE7__valueE};
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
enum _ZNSt12__is_integerIfEUt_E {
# 138 "/usr/include/c++/4.8.2/bits/cpp_type_traits.h" 3
_ZNSt12__is_integerIfE7__valueE};
# 153 "/usr/include/bits/mathinline.h" 3
union _ZZ10__signbitlEUt_;
# 212 "/usr/lib/gcc/x86_64-redhat-linux/4.8.5/include/stddef.h" 3
typedef unsigned long size_t;
# 1 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 1 3
# 38 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
# 1 "/usr/local/cuda-8.0/bin/..//include/host_defines.h" 1 3
# 39 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3





typedef __attribute__((device_builtin_texture_type)) unsigned long long __texture_type__;
typedef __attribute__((device_builtin_surface_type)) unsigned long long __surface_type__;
# 196 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
extern __attribute__((device)) __attribute__((used)) void* malloc(size_t);
extern __attribute__((device)) __attribute__((used)) void free(void*);


static __attribute__((device)) void __nv_sized_free(void *p, size_t sz) { free(p); }
static __attribute__((device)) void __nv_sized_array_free(void *p, size_t sz) { free(p); }


extern __attribute__((device)) void __assertfail(
  const void *message,
  const void *file,
  unsigned int line,
  const void *function,
  size_t charsize);
# 254 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
static __attribute__((device)) void __assert_fail(
  const char *__assertion,
  const char *__file,
  unsigned int __line,
  const char *__function)
{
  __assertfail(
    (const void *)__assertion,
    (const void *)__file,
                  __line,
    (const void *)__function,
    sizeof(char));
}
# 284 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 3
# 1 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 1 3
# 56 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 3
# 1 "/usr/local/cuda-8.0/include/device_types.h" 1 3
# 53 "/usr/local/cuda-8.0/include/device_types.h" 3
# 1 "/usr/local/cuda-8.0/include/host_defines.h" 1 3
# 54 "/usr/local/cuda-8.0/include/device_types.h" 2 3







enum __attribute__((device_builtin)) cudaRoundMode
{
    cudaRoundNearest,
    cudaRoundZero,
    cudaRoundPosInf,
    cudaRoundMinInf
};
# 57 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3


# 1 "/usr/local/cuda-8.0/include/driver_types.h" 1 3
# 156 "/usr/local/cuda-8.0/include/driver_types.h" 3
enum __attribute__((device_builtin)) cudaError
{





    cudaSuccess = 0,





    cudaErrorMissingConfiguration = 1,





    cudaErrorMemoryAllocation = 2,





    cudaErrorInitializationError = 3,
# 191 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchFailure = 4,
# 200 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorPriorLaunchFailure = 5,
# 211 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchTimeout = 6,
# 220 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchOutOfResources = 7,





    cudaErrorInvalidDeviceFunction = 8,
# 235 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorInvalidConfiguration = 9,





    cudaErrorInvalidDevice = 10,





    cudaErrorInvalidValue = 11,





    cudaErrorInvalidPitchValue = 12,





    cudaErrorInvalidSymbol = 13,




    cudaErrorMapBufferObjectFailed = 14,




    cudaErrorUnmapBufferObjectFailed = 15,





    cudaErrorInvalidHostPointer = 16,





    cudaErrorInvalidDevicePointer = 17,





    cudaErrorInvalidTexture = 18,





    cudaErrorInvalidTextureBinding = 19,






    cudaErrorInvalidChannelDescriptor = 20,





    cudaErrorInvalidMemcpyDirection = 21,
# 316 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorAddressOfConstant = 22,
# 325 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorTextureFetchFailed = 23,
# 334 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorTextureNotBound = 24,
# 343 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorSynchronizationError = 25,





    cudaErrorInvalidFilterSetting = 26,





    cudaErrorInvalidNormSetting = 27,







    cudaErrorMixedDeviceExecution = 28,






    cudaErrorCudartUnloading = 29,




    cudaErrorUnknown = 30,







    cudaErrorNotYetImplemented = 31,
# 392 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorMemoryValueTooLarge = 32,






    cudaErrorInvalidResourceHandle = 33,







    cudaErrorNotReady = 34,






    cudaErrorInsufficientDriver = 35,
# 427 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorSetOnActiveProcess = 36,





    cudaErrorInvalidSurface = 37,





    cudaErrorNoDevice = 38,





    cudaErrorECCUncorrectable = 39,




    cudaErrorSharedObjectSymbolNotFound = 40,




    cudaErrorSharedObjectInitFailed = 41,





    cudaErrorUnsupportedLimit = 42,





    cudaErrorDuplicateVariableName = 43,





    cudaErrorDuplicateTextureName = 44,





    cudaErrorDuplicateSurfaceName = 45,
# 489 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorDevicesUnavailable = 46,




    cudaErrorInvalidKernelImage = 47,







    cudaErrorNoKernelImageForDevice = 48,
# 515 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorIncompatibleDriverContext = 49,






    cudaErrorPeerAccessAlreadyEnabled = 50,






    cudaErrorPeerAccessNotEnabled = 51,





    cudaErrorDeviceAlreadyInUse = 54,






    cudaErrorProfilerDisabled = 55,







    cudaErrorProfilerNotInitialized = 56,






    cudaErrorProfilerAlreadyStarted = 57,






     cudaErrorProfilerAlreadyStopped = 58,







    cudaErrorAssert = 59,






    cudaErrorTooManyPeers = 60,





    cudaErrorHostMemoryAlreadyRegistered = 61,





    cudaErrorHostMemoryNotRegistered = 62,




    cudaErrorOperatingSystem = 63,





    cudaErrorPeerAccessUnsupported = 64,






    cudaErrorLaunchMaxDepthExceeded = 65,







    cudaErrorLaunchFileScopedTex = 66,







    cudaErrorLaunchFileScopedSurf = 67,
# 640 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorSyncDepthExceeded = 68,
# 652 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorLaunchPendingCountExceeded = 69,




    cudaErrorNotPermitted = 70,





    cudaErrorNotSupported = 71,
# 672 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorHardwareStackError = 72,







    cudaErrorIllegalInstruction = 73,
# 689 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorMisalignedAddress = 74,
# 700 "/usr/local/cuda-8.0/include/driver_types.h" 3
    cudaErrorInvalidAddressSpace = 75,







    cudaErrorInvalidPc = 76,







    cudaErrorIllegalAddress = 77,





    cudaErrorInvalidPtx = 78,




    cudaErrorInvalidGraphicsContext = 79,





    cudaErrorNvlinkUncorrectable = 80,




    cudaErrorStartupFailure = 0x7f,







    cudaErrorApiFailureBase = 10000
};




enum __attribute__((device_builtin)) cudaChannelFormatKind
{
    cudaChannelFormatKindSigned = 0,
    cudaChannelFormatKindUnsigned = 1,
    cudaChannelFormatKindFloat = 2,
    cudaChannelFormatKindNone = 3
};




struct __attribute__((device_builtin)) cudaChannelFormatDesc
{
    int x;
    int y;
    int z;
    int w;
    enum cudaChannelFormatKind f;
};




typedef struct cudaArray *cudaArray_t;




typedef const struct cudaArray *cudaArray_const_t;

struct cudaArray;




typedef struct cudaMipmappedArray *cudaMipmappedArray_t;




typedef const struct cudaMipmappedArray *cudaMipmappedArray_const_t;

struct cudaMipmappedArray;




enum __attribute__((device_builtin)) cudaMemoryType
{
    cudaMemoryTypeHost = 1,
    cudaMemoryTypeDevice = 2
};




enum __attribute__((device_builtin)) cudaMemcpyKind
{
    cudaMemcpyHostToHost = 0,
    cudaMemcpyHostToDevice = 1,
    cudaMemcpyDeviceToHost = 2,
    cudaMemcpyDeviceToDevice = 3,
    cudaMemcpyDefault = 4
};






struct __attribute__((device_builtin)) cudaPitchedPtr
{
    void *ptr;
    size_t pitch;
    size_t xsize;
    size_t ysize;
};






struct __attribute__((device_builtin)) cudaExtent
{
    size_t width;
    size_t height;
    size_t depth;
};






struct __attribute__((device_builtin)) cudaPos
{
    size_t x;
    size_t y;
    size_t z;
};




struct __attribute__((device_builtin)) cudaMemcpy3DParms
{
    cudaArray_t srcArray;
    struct cudaPos srcPos;
    struct cudaPitchedPtr srcPtr;

    cudaArray_t dstArray;
    struct cudaPos dstPos;
    struct cudaPitchedPtr dstPtr;

    struct cudaExtent extent;
    enum cudaMemcpyKind kind;
};




struct __attribute__((device_builtin)) cudaMemcpy3DPeerParms
{
    cudaArray_t srcArray;
    struct cudaPos srcPos;
    struct cudaPitchedPtr srcPtr;
    int srcDevice;

    cudaArray_t dstArray;
    struct cudaPos dstPos;
    struct cudaPitchedPtr dstPtr;
    int dstDevice;

    struct cudaExtent extent;
};




struct cudaGraphicsResource;




enum __attribute__((device_builtin)) cudaGraphicsRegisterFlags
{
    cudaGraphicsRegisterFlagsNone = 0,
    cudaGraphicsRegisterFlagsReadOnly = 1,
    cudaGraphicsRegisterFlagsWriteDiscard = 2,
    cudaGraphicsRegisterFlagsSurfaceLoadStore = 4,
    cudaGraphicsRegisterFlagsTextureGather = 8
};




enum __attribute__((device_builtin)) cudaGraphicsMapFlags
{
    cudaGraphicsMapFlagsNone = 0,
    cudaGraphicsMapFlagsReadOnly = 1,
    cudaGraphicsMapFlagsWriteDiscard = 2
};




enum __attribute__((device_builtin)) cudaGraphicsCubeFace
{
    cudaGraphicsCubeFacePositiveX = 0x00,
    cudaGraphicsCubeFaceNegativeX = 0x01,
    cudaGraphicsCubeFacePositiveY = 0x02,
    cudaGraphicsCubeFaceNegativeY = 0x03,
    cudaGraphicsCubeFacePositiveZ = 0x04,
    cudaGraphicsCubeFaceNegativeZ = 0x05
};




enum __attribute__((device_builtin)) cudaResourceType
{
    cudaResourceTypeArray = 0x00,
    cudaResourceTypeMipmappedArray = 0x01,
    cudaResourceTypeLinear = 0x02,
    cudaResourceTypePitch2D = 0x03
};




enum __attribute__((device_builtin)) cudaResourceViewFormat
{
    cudaResViewFormatNone = 0x00,
    cudaResViewFormatUnsignedChar1 = 0x01,
    cudaResViewFormatUnsignedChar2 = 0x02,
    cudaResViewFormatUnsignedChar4 = 0x03,
    cudaResViewFormatSignedChar1 = 0x04,
    cudaResViewFormatSignedChar2 = 0x05,
    cudaResViewFormatSignedChar4 = 0x06,
    cudaResViewFormatUnsignedShort1 = 0x07,
    cudaResViewFormatUnsignedShort2 = 0x08,
    cudaResViewFormatUnsignedShort4 = 0x09,
    cudaResViewFormatSignedShort1 = 0x0a,
    cudaResViewFormatSignedShort2 = 0x0b,
    cudaResViewFormatSignedShort4 = 0x0c,
    cudaResViewFormatUnsignedInt1 = 0x0d,
    cudaResViewFormatUnsignedInt2 = 0x0e,
    cudaResViewFormatUnsignedInt4 = 0x0f,
    cudaResViewFormatSignedInt1 = 0x10,
    cudaResViewFormatSignedInt2 = 0x11,
    cudaResViewFormatSignedInt4 = 0x12,
    cudaResViewFormatHalf1 = 0x13,
    cudaResViewFormatHalf2 = 0x14,
    cudaResViewFormatHalf4 = 0x15,
    cudaResViewFormatFloat1 = 0x16,
    cudaResViewFormatFloat2 = 0x17,
    cudaResViewFormatFloat4 = 0x18,
    cudaResViewFormatUnsignedBlockCompressed1 = 0x19,
    cudaResViewFormatUnsignedBlockCompressed2 = 0x1a,
    cudaResViewFormatUnsignedBlockCompressed3 = 0x1b,
    cudaResViewFormatUnsignedBlockCompressed4 = 0x1c,
    cudaResViewFormatSignedBlockCompressed4 = 0x1d,
    cudaResViewFormatUnsignedBlockCompressed5 = 0x1e,
    cudaResViewFormatSignedBlockCompressed5 = 0x1f,
    cudaResViewFormatUnsignedBlockCompressed6H = 0x20,
    cudaResViewFormatSignedBlockCompressed6H = 0x21,
    cudaResViewFormatUnsignedBlockCompressed7 = 0x22
};




struct __attribute__((device_builtin)) cudaResourceDesc {
 enum cudaResourceType resType;

 union {
  struct {
   cudaArray_t array;
  } array;
        struct {
            cudaMipmappedArray_t mipmap;
        } mipmap;
  struct {
   void *devPtr;
   struct cudaChannelFormatDesc desc;
   size_t sizeInBytes;
  } linear;
  struct {
   void *devPtr;
   struct cudaChannelFormatDesc desc;
   size_t width;
   size_t height;
   size_t pitchInBytes;
  } pitch2D;
 } res;
};




struct __attribute__((device_builtin)) cudaResourceViewDesc
{
    enum cudaResourceViewFormat format;
    size_t width;
    size_t height;
    size_t depth;
    unsigned int firstMipmapLevel;
    unsigned int lastMipmapLevel;
    unsigned int firstLayer;
    unsigned int lastLayer;
};




struct __attribute__((device_builtin)) cudaPointerAttributes
{




    enum cudaMemoryType memoryType;
# 1045 "/usr/local/cuda-8.0/include/driver_types.h" 3
    int device;





    void *devicePointer;





    void *hostPointer;




    int isManaged;
};




struct __attribute__((device_builtin)) cudaFuncAttributes
{





   size_t sharedSizeBytes;





   size_t constSizeBytes;




   size_t localSizeBytes;






   int maxThreadsPerBlock;




   int numRegs;






   int ptxVersion;






   int binaryVersion;





   int cacheModeCA;
};




enum __attribute__((device_builtin)) cudaFuncCache
{
    cudaFuncCachePreferNone = 0,
    cudaFuncCachePreferShared = 1,
    cudaFuncCachePreferL1 = 2,
    cudaFuncCachePreferEqual = 3
};





enum __attribute__((device_builtin)) cudaSharedMemConfig
{
    cudaSharedMemBankSizeDefault = 0,
    cudaSharedMemBankSizeFourByte = 1,
    cudaSharedMemBankSizeEightByte = 2
};




enum __attribute__((device_builtin)) cudaComputeMode
{
    cudaComputeModeDefault = 0,
    cudaComputeModeExclusive = 1,
    cudaComputeModeProhibited = 2,
    cudaComputeModeExclusiveProcess = 3
};




enum __attribute__((device_builtin)) cudaLimit
{
    cudaLimitStackSize = 0x00,
    cudaLimitPrintfFifoSize = 0x01,
    cudaLimitMallocHeapSize = 0x02,
    cudaLimitDevRuntimeSyncDepth = 0x03,
    cudaLimitDevRuntimePendingLaunchCount = 0x04
};




enum __attribute__((device_builtin)) cudaMemoryAdvise
{
    cudaMemAdviseSetReadMostly = 1,
    cudaMemAdviseUnsetReadMostly = 2,
    cudaMemAdviseSetPreferredLocation = 3,
    cudaMemAdviseUnsetPreferredLocation = 4,
    cudaMemAdviseSetAccessedBy = 5,
    cudaMemAdviseUnsetAccessedBy = 6
};




enum __attribute__((device_builtin)) cudaMemRangeAttribute
{
    cudaMemRangeAttributeReadMostly = 1,
    cudaMemRangeAttributePreferredLocation = 2,
    cudaMemRangeAttributeAccessedBy = 3,
    cudaMemRangeAttributeLastPrefetchLocation = 4
};




enum __attribute__((device_builtin)) cudaOutputMode
{
    cudaKeyValuePair = 0x00,
    cudaCSV = 0x01
};




enum __attribute__((device_builtin)) cudaDeviceAttr
{
    cudaDevAttrMaxThreadsPerBlock = 1,
    cudaDevAttrMaxBlockDimX = 2,
    cudaDevAttrMaxBlockDimY = 3,
    cudaDevAttrMaxBlockDimZ = 4,
    cudaDevAttrMaxGridDimX = 5,
    cudaDevAttrMaxGridDimY = 6,
    cudaDevAttrMaxGridDimZ = 7,
    cudaDevAttrMaxSharedMemoryPerBlock = 8,
    cudaDevAttrTotalConstantMemory = 9,
    cudaDevAttrWarpSize = 10,
    cudaDevAttrMaxPitch = 11,
    cudaDevAttrMaxRegistersPerBlock = 12,
    cudaDevAttrClockRate = 13,
    cudaDevAttrTextureAlignment = 14,
    cudaDevAttrGpuOverlap = 15,
    cudaDevAttrMultiProcessorCount = 16,
    cudaDevAttrKernelExecTimeout = 17,
    cudaDevAttrIntegrated = 18,
    cudaDevAttrCanMapHostMemory = 19,
    cudaDevAttrComputeMode = 20,
    cudaDevAttrMaxTexture1DWidth = 21,
    cudaDevAttrMaxTexture2DWidth = 22,
    cudaDevAttrMaxTexture2DHeight = 23,
    cudaDevAttrMaxTexture3DWidth = 24,
    cudaDevAttrMaxTexture3DHeight = 25,
    cudaDevAttrMaxTexture3DDepth = 26,
    cudaDevAttrMaxTexture2DLayeredWidth = 27,
    cudaDevAttrMaxTexture2DLayeredHeight = 28,
    cudaDevAttrMaxTexture2DLayeredLayers = 29,
    cudaDevAttrSurfaceAlignment = 30,
    cudaDevAttrConcurrentKernels = 31,
    cudaDevAttrEccEnabled = 32,
    cudaDevAttrPciBusId = 33,
    cudaDevAttrPciDeviceId = 34,
    cudaDevAttrTccDriver = 35,
    cudaDevAttrMemoryClockRate = 36,
    cudaDevAttrGlobalMemoryBusWidth = 37,
    cudaDevAttrL2CacheSize = 38,
    cudaDevAttrMaxThreadsPerMultiProcessor = 39,
    cudaDevAttrAsyncEngineCount = 40,
    cudaDevAttrUnifiedAddressing = 41,
    cudaDevAttrMaxTexture1DLayeredWidth = 42,
    cudaDevAttrMaxTexture1DLayeredLayers = 43,
    cudaDevAttrMaxTexture2DGatherWidth = 45,
    cudaDevAttrMaxTexture2DGatherHeight = 46,
    cudaDevAttrMaxTexture3DWidthAlt = 47,
    cudaDevAttrMaxTexture3DHeightAlt = 48,
    cudaDevAttrMaxTexture3DDepthAlt = 49,
    cudaDevAttrPciDomainId = 50,
    cudaDevAttrTexturePitchAlignment = 51,
    cudaDevAttrMaxTextureCubemapWidth = 52,
    cudaDevAttrMaxTextureCubemapLayeredWidth = 53,
    cudaDevAttrMaxTextureCubemapLayeredLayers = 54,
    cudaDevAttrMaxSurface1DWidth = 55,
    cudaDevAttrMaxSurface2DWidth = 56,
    cudaDevAttrMaxSurface2DHeight = 57,
    cudaDevAttrMaxSurface3DWidth = 58,
    cudaDevAttrMaxSurface3DHeight = 59,
    cudaDevAttrMaxSurface3DDepth = 60,
    cudaDevAttrMaxSurface1DLayeredWidth = 61,
    cudaDevAttrMaxSurface1DLayeredLayers = 62,
    cudaDevAttrMaxSurface2DLayeredWidth = 63,
    cudaDevAttrMaxSurface2DLayeredHeight = 64,
    cudaDevAttrMaxSurface2DLayeredLayers = 65,
    cudaDevAttrMaxSurfaceCubemapWidth = 66,
    cudaDevAttrMaxSurfaceCubemapLayeredWidth = 67,
    cudaDevAttrMaxSurfaceCubemapLayeredLayers = 68,
    cudaDevAttrMaxTexture1DLinearWidth = 69,
    cudaDevAttrMaxTexture2DLinearWidth = 70,
    cudaDevAttrMaxTexture2DLinearHeight = 71,
    cudaDevAttrMaxTexture2DLinearPitch = 72,
    cudaDevAttrMaxTexture2DMipmappedWidth = 73,
    cudaDevAttrMaxTexture2DMipmappedHeight = 74,
    cudaDevAttrComputeCapabilityMajor = 75,
    cudaDevAttrComputeCapabilityMinor = 76,
    cudaDevAttrMaxTexture1DMipmappedWidth = 77,
    cudaDevAttrStreamPrioritiesSupported = 78,
    cudaDevAttrGlobalL1CacheSupported = 79,
    cudaDevAttrLocalL1CacheSupported = 80,
    cudaDevAttrMaxSharedMemoryPerMultiprocessor = 81,
    cudaDevAttrMaxRegistersPerMultiprocessor = 82,
    cudaDevAttrManagedMemory = 83,
    cudaDevAttrIsMultiGpuBoard = 84,
    cudaDevAttrMultiGpuBoardGroupID = 85,
    cudaDevAttrHostNativeAtomicSupported = 86,
    cudaDevAttrSingleToDoublePrecisionPerfRatio = 87,
    cudaDevAttrPageableMemoryAccess = 88,
    cudaDevAttrConcurrentManagedAccess = 89,
    cudaDevAttrComputePreemptionSupported = 90,
    cudaDevAttrCanUseHostPointerForRegisteredMem = 91
};





enum __attribute__((device_builtin)) cudaDeviceP2PAttr {
    cudaDevP2PAttrPerformanceRank = 1,
    cudaDevP2PAttrAccessSupported = 2,
    cudaDevP2PAttrNativeAtomicSupported = 3
};



struct __attribute__((device_builtin)) cudaDeviceProp
{
    char name[256];
    size_t totalGlobalMem;
    size_t sharedMemPerBlock;
    int regsPerBlock;
    int warpSize;
    size_t memPitch;
    int maxThreadsPerBlock;
    int maxThreadsDim[3];
    int maxGridSize[3];
    int clockRate;
    size_t totalConstMem;
    int major;
    int minor;
    size_t textureAlignment;
    size_t texturePitchAlignment;
    int deviceOverlap;
    int multiProcessorCount;
    int kernelExecTimeoutEnabled;
    int integrated;
    int canMapHostMemory;
    int computeMode;
    int maxTexture1D;
    int maxTexture1DMipmap;
    int maxTexture1DLinear;
    int maxTexture2D[2];
    int maxTexture2DMipmap[2];
    int maxTexture2DLinear[3];
    int maxTexture2DGather[2];
    int maxTexture3D[3];
    int maxTexture3DAlt[3];
    int maxTextureCubemap;
    int maxTexture1DLayered[2];
    int maxTexture2DLayered[3];
    int maxTextureCubemapLayered[2];
    int maxSurface1D;
    int maxSurface2D[2];
    int maxSurface3D[3];
    int maxSurface1DLayered[2];
    int maxSurface2DLayered[3];
    int maxSurfaceCubemap;
    int maxSurfaceCubemapLayered[2];
    size_t surfaceAlignment;
    int concurrentKernels;
    int ECCEnabled;
    int pciBusID;
    int pciDeviceID;
    int pciDomainID;
    int tccDriver;
    int asyncEngineCount;
    int unifiedAddressing;
    int memoryClockRate;
    int memoryBusWidth;
    int l2CacheSize;
    int maxThreadsPerMultiProcessor;
    int streamPrioritiesSupported;
    int globalL1CacheSupported;
    int localL1CacheSupported;
    size_t sharedMemPerMultiprocessor;
    int regsPerMultiprocessor;
    int managedMemory;
    int isMultiGpuBoard;
    int multiGpuBoardGroupID;
    int hostNativeAtomicSupported;
    int singleToDoublePrecisionPerfRatio;
    int pageableMemoryAccess;
    int concurrentManagedAccess;
};
# 1456 "/usr/local/cuda-8.0/include/driver_types.h" 3
typedef __attribute__((device_builtin)) struct __attribute__((device_builtin)) cudaIpcEventHandle_st
{
    char reserved[64];
}cudaIpcEventHandle_t;




typedef __attribute__((device_builtin)) struct __attribute__((device_builtin)) cudaIpcMemHandle_st
{
    char reserved[64];
}cudaIpcMemHandle_t;
# 1478 "/usr/local/cuda-8.0/include/driver_types.h" 3
typedef __attribute__((device_builtin)) enum cudaError cudaError_t;




typedef __attribute__((device_builtin)) struct CUstream_st *cudaStream_t;




typedef __attribute__((device_builtin)) struct CUevent_st *cudaEvent_t;




typedef __attribute__((device_builtin)) struct cudaGraphicsResource *cudaGraphicsResource_t;




typedef __attribute__((device_builtin)) struct CUuuid_st cudaUUID_t;




typedef __attribute__((device_builtin)) enum cudaOutputMode cudaOutputMode_t;
# 60 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3


# 1 "/usr/local/cuda-8.0/include/surface_types.h" 1 3
# 59 "/usr/local/cuda-8.0/include/surface_types.h" 3
# 1 "/usr/local/cuda-8.0/include/driver_types.h" 1 3
# 60 "/usr/local/cuda-8.0/include/surface_types.h" 2 3
# 84 "/usr/local/cuda-8.0/include/surface_types.h" 3
enum __attribute__((device_builtin)) cudaSurfaceBoundaryMode
{
    cudaBoundaryModeZero = 0,
    cudaBoundaryModeClamp = 1,
    cudaBoundaryModeTrap = 2
};




enum __attribute__((device_builtin)) cudaSurfaceFormatMode
{
    cudaFormatModeForced = 0,
    cudaFormatModeAuto = 1
};




struct __attribute__((device_builtin)) surfaceReference
{



    struct cudaChannelFormatDesc channelDesc;
};




typedef __attribute__((device_builtin)) unsigned long long cudaSurfaceObject_t;
# 63 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/texture_types.h" 1 3
# 84 "/usr/local/cuda-8.0/include/texture_types.h" 3
enum __attribute__((device_builtin)) cudaTextureAddressMode
{
    cudaAddressModeWrap = 0,
    cudaAddressModeClamp = 1,
    cudaAddressModeMirror = 2,
    cudaAddressModeBorder = 3
};




enum __attribute__((device_builtin)) cudaTextureFilterMode
{
    cudaFilterModePoint = 0,
    cudaFilterModeLinear = 1
};




enum __attribute__((device_builtin)) cudaTextureReadMode
{
    cudaReadModeElementType = 0,
    cudaReadModeNormalizedFloat = 1
};




struct __attribute__((device_builtin)) textureReference
{



    int normalized;



    enum cudaTextureFilterMode filterMode;



    enum cudaTextureAddressMode addressMode[3];



    struct cudaChannelFormatDesc channelDesc;



    int sRGB;



    unsigned int maxAnisotropy;



    enum cudaTextureFilterMode mipmapFilterMode;



    float mipmapLevelBias;



    float minMipmapLevelClamp;



    float maxMipmapLevelClamp;
    int __cudaReserved[15];
};




struct __attribute__((device_builtin)) cudaTextureDesc
{



    enum cudaTextureAddressMode addressMode[3];



    enum cudaTextureFilterMode filterMode;



    enum cudaTextureReadMode readMode;



    int sRGB;



    float borderColor[4];



    int normalizedCoords;



    unsigned int maxAnisotropy;



    enum cudaTextureFilterMode mipmapFilterMode;



    float mipmapLevelBias;



    float minMipmapLevelClamp;



    float maxMipmapLevelClamp;
};




typedef __attribute__((device_builtin)) unsigned long long cudaTextureObject_t;
# 64 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/vector_types.h" 1 3
# 61 "/usr/local/cuda-8.0/include/vector_types.h" 3
# 1 "/usr/local/cuda-8.0/include/builtin_types.h" 1 3
# 56 "/usr/local/cuda-8.0/include/builtin_types.h" 3
# 1 "/usr/local/cuda-8.0/include/device_types.h" 1 3
# 57 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3





# 1 "/usr/local/cuda-8.0/include/surface_types.h" 1 3
# 63 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/texture_types.h" 1 3
# 64 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3
# 1 "/usr/local/cuda-8.0/include/vector_types.h" 1 3
# 64 "/usr/local/cuda-8.0/include/builtin_types.h" 2 3
# 62 "/usr/local/cuda-8.0/include/vector_types.h" 2 3
# 98 "/usr/local/cuda-8.0/include/vector_types.h" 3
struct __attribute__((device_builtin)) char1
{
    signed char x;
};

struct __attribute__((device_builtin)) uchar1
{
    unsigned char x;
};


struct __attribute__((device_builtin)) __attribute__((aligned(2))) char2
{
    signed char x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(2))) uchar2
{
    unsigned char x, y;
};

struct __attribute__((device_builtin)) char3
{
    signed char x, y, z;
};

struct __attribute__((device_builtin)) uchar3
{
    unsigned char x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) char4
{
    signed char x, y, z, w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) uchar4
{
    unsigned char x, y, z, w;
};

struct __attribute__((device_builtin)) short1
{
    short x;
};

struct __attribute__((device_builtin)) ushort1
{
    unsigned short x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) short2
{
    short x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(4))) ushort2
{
    unsigned short x, y;
};

struct __attribute__((device_builtin)) short3
{
    short x, y, z;
};

struct __attribute__((device_builtin)) ushort3
{
    unsigned short x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(8))) short4 { short x; short y; short z; short w; };
struct __attribute__((device_builtin)) __attribute__((aligned(8))) ushort4 { unsigned short x; unsigned short y; unsigned short z; unsigned short w; };

struct __attribute__((device_builtin)) int1
{
    int x;
};

struct __attribute__((device_builtin)) uint1
{
    unsigned int x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(8))) int2 { int x; int y; };
struct __attribute__((device_builtin)) __attribute__((aligned(8))) uint2 { unsigned int x; unsigned int y; };

struct __attribute__((device_builtin)) int3
{
    int x, y, z;
};

struct __attribute__((device_builtin)) uint3
{
    unsigned int x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) int4
{
    int x, y, z, w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) uint4
{
    unsigned int x, y, z, w;
};

struct __attribute__((device_builtin)) long1
{
    long int x;
};

struct __attribute__((device_builtin)) ulong1
{
    unsigned long x;
};






struct __attribute__((device_builtin)) __attribute__((aligned(2*sizeof(long int)))) long2
{
    long int x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(2*sizeof(unsigned long int)))) ulong2
{
    unsigned long int x, y;
};



struct __attribute__((device_builtin)) long3
{
    long int x, y, z;
};

struct __attribute__((device_builtin)) ulong3
{
    unsigned long int x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) long4
{
    long int x, y, z, w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) ulong4
{
    unsigned long int x, y, z, w;
};

struct __attribute__((device_builtin)) float1
{
    float x;
};
# 274 "/usr/local/cuda-8.0/include/vector_types.h" 3
struct __attribute__((device_builtin)) __attribute__((aligned(8))) float2 { float x; float y; };




struct __attribute__((device_builtin)) float3
{
    float x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) float4
{
    float x, y, z, w;
};

struct __attribute__((device_builtin)) longlong1
{
    long long int x;
};

struct __attribute__((device_builtin)) ulonglong1
{
    unsigned long long int x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) longlong2
{
    long long int x, y;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) ulonglong2
{
    unsigned long long int x, y;
};

struct __attribute__((device_builtin)) longlong3
{
    long long int x, y, z;
};

struct __attribute__((device_builtin)) ulonglong3
{
    unsigned long long int x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) longlong4
{
    long long int x, y, z ,w;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) ulonglong4
{
    unsigned long long int x, y, z, w;
};

struct __attribute__((device_builtin)) double1
{
    double x;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) double2
{
    double x, y;
};

struct __attribute__((device_builtin)) double3
{
    double x, y, z;
};

struct __attribute__((device_builtin)) __attribute__((aligned(16))) double4
{
    double x, y, z, w;
};
# 362 "/usr/local/cuda-8.0/include/vector_types.h" 3
typedef __attribute__((device_builtin)) struct char1 char1;
typedef __attribute__((device_builtin)) struct uchar1 uchar1;
typedef __attribute__((device_builtin)) struct char2 char2;
typedef __attribute__((device_builtin)) struct uchar2 uchar2;
typedef __attribute__((device_builtin)) struct char3 char3;
typedef __attribute__((device_builtin)) struct uchar3 uchar3;
typedef __attribute__((device_builtin)) struct char4 char4;
typedef __attribute__((device_builtin)) struct uchar4 uchar4;
typedef __attribute__((device_builtin)) struct short1 short1;
typedef __attribute__((device_builtin)) struct ushort1 ushort1;
typedef __attribute__((device_builtin)) struct short2 short2;
typedef __attribute__((device_builtin)) struct ushort2 ushort2;
typedef __attribute__((device_builtin)) struct short3 short3;
typedef __attribute__((device_builtin)) struct ushort3 ushort3;
typedef __attribute__((device_builtin)) struct short4 short4;
typedef __attribute__((device_builtin)) struct ushort4 ushort4;
typedef __attribute__((device_builtin)) struct int1 int1;
typedef __attribute__((device_builtin)) struct uint1 uint1;
typedef __attribute__((device_builtin)) struct int2 int2;
typedef __attribute__((device_builtin)) struct uint2 uint2;
typedef __attribute__((device_builtin)) struct int3 int3;
typedef __attribute__((device_builtin)) struct uint3 uint3;
typedef __attribute__((device_builtin)) struct int4 int4;
typedef __attribute__((device_builtin)) struct uint4 uint4;
typedef __attribute__((device_builtin)) struct long1 long1;
typedef __attribute__((device_builtin)) struct ulong1 ulong1;
typedef __attribute__((device_builtin)) struct long2 long2;
typedef __attribute__((device_builtin)) struct ulong2 ulong2;
typedef __attribute__((device_builtin)) struct long3 long3;
typedef __attribute__((device_builtin)) struct ulong3 ulong3;
typedef __attribute__((device_builtin)) struct long4 long4;
typedef __attribute__((device_builtin)) struct ulong4 ulong4;
typedef __attribute__((device_builtin)) struct float1 float1;
typedef __attribute__((device_builtin)) struct float2 float2;
typedef __attribute__((device_builtin)) struct float3 float3;
typedef __attribute__((device_builtin)) struct float4 float4;
typedef __attribute__((device_builtin)) struct longlong1 longlong1;
typedef __attribute__((device_builtin)) struct ulonglong1 ulonglong1;
typedef __attribute__((device_builtin)) struct longlong2 longlong2;
typedef __attribute__((device_builtin)) struct ulonglong2 ulonglong2;
typedef __attribute__((device_builtin)) struct longlong3 longlong3;
typedef __attribute__((device_builtin)) struct ulonglong3 ulonglong3;
typedef __attribute__((device_builtin)) struct longlong4 longlong4;
typedef __attribute__((device_builtin)) struct ulonglong4 ulonglong4;
typedef __attribute__((device_builtin)) struct double1 double1;
typedef __attribute__((device_builtin)) struct double2 double2;
typedef __attribute__((device_builtin)) struct double3 double3;
typedef __attribute__((device_builtin)) struct double4 double4;







struct __attribute__((device_builtin)) dim3
{
    unsigned int x, y, z;





};

typedef __attribute__((device_builtin)) struct dim3 dim3;
# 64 "/usr/local/cuda-8.0/bin/..//include/builtin_types.h" 2 3
# 285 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3
# 1 "/usr/local/cuda-8.0/bin/..//include/device_launch_parameters.h" 1 3
# 71 "/usr/local/cuda-8.0/bin/..//include/device_launch_parameters.h" 3
uint3 __attribute__((device_builtin)) extern const threadIdx;
uint3 __attribute__((device_builtin)) extern const blockIdx;
dim3 __attribute__((device_builtin)) extern const blockDim;
dim3 __attribute__((device_builtin)) extern const gridDim;
int __attribute__((device_builtin)) extern const warpSize;
# 286 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3
# 1 "/usr/local/cuda-8.0/include/crt/storage_class.h" 1 3
# 286 "/usr/local/cuda-8.0/bin/..//include/crt/device_runtime.h" 2 3
# 214 "/usr/lib/gcc/x86_64-redhat-linux/4.8.5/include/stddef.h" 2 3
# 142 "/usr/include/bits/types.h" 3
typedef int __pid_t;
# 148 "/usr/include/bits/types.h" 3
typedef long __time_t;
# 149 "/usr/include/bits/types.h" 3
typedef unsigned __useconds_t;
# 150 "/usr/include/bits/types.h" 3
typedef long __suseconds_t;
# 153 "/usr/include/bits/types.h" 3
typedef int __key_t;
# 181 "/usr/include/bits/types.h" 3
typedef long __ssize_t;
# 198 "/usr/include/bits/types.h" 3
typedef unsigned __socklen_t;
# 30 "/usr/include/bits/time.h" 3
struct timeval {
# 32 "/usr/include/bits/time.h" 3
__time_t tv_sec;
# 33 "/usr/include/bits/time.h" 3
__suseconds_t tv_usec;};
# 48 "/usr/include/stdio.h" 3
typedef struct _IO_FILE FILE;
# 102 "/usr/include/stdio.h" 3
typedef __ssize_t ssize_t;
# 122 "/usr/include/sys/types.h" 3
typedef __key_t key_t;
# 60 "/usr/include/bits/pthreadtypes.h" 3
typedef unsigned long pthread_t;
# 69 "/usr/include/bits/pthreadtypes.h" 3
typedef union pthread_attr_t pthread_attr_t;
# 34 "/usr/include/bits/socket.h" 3
typedef __socklen_t socklen_t;
# 28 "/usr/include/bits/sockaddr.h" 3
typedef unsigned short sa_family_t;
# 49 "/usr/include/stdint.h" 3
typedef unsigned short uint16_t;
# 51 "/usr/include/stdint.h" 3
typedef unsigned uint32_t;
# 31 "/usr/include/netinet/in.h" 3
typedef uint32_t in_addr_t;
# 32 "/usr/include/netinet/in.h" 3
struct in_addr {
# 34 "/usr/include/netinet/in.h" 3
in_addr_t s_addr;};
# 118 "/usr/include/netinet/in.h" 3
typedef uint16_t in_port_t;
# 238 "/usr/include/netinet/in.h" 3
struct sockaddr_in {
# 240 "/usr/include/netinet/in.h" 3
sa_family_t sin_family;
# 241 "/usr/include/netinet/in.h" 3
in_port_t sin_port;
# 242 "/usr/include/netinet/in.h" 3
struct in_addr sin_addr;
# 245 "/usr/include/netinet/in.h" 3
unsigned char sin_zero[8];};
# 71 "../network/vdif_receiver_rb.h"
typedef struct vdif_rx_tt vdif_rx_t;
# 143 "/usr/local/cuda-8.0/bin/..//include/cufft.h"
typedef int cufftHandle;
# 17 "../kernels/cuda/arithmetic_winfunc_kernels.h"
struct cu_window_cb_params_tt {
# 18 "../kernels/cuda/arithmetic_winfunc_kernels.h"
size_t fftlen;};
# 19 "../kernels/cuda/arithmetic_winfunc_kernels.h"
typedef struct cu_window_cb_params_tt cu_window_cb_params_t;
# 55 "kfftspec.h"
struct fftspec_config_tt {
# 58 "kfftspec.h"
int dasfd;
# 60 "kfftspec.h"
double T_int;
# 61 "kfftspec.h"
double T_int_wish;
# 62 "kfftspec.h"
double bw;
# 63 "kfftspec.h"
int nchan;
# 64 "kfftspec.h"
int nfft;
# 65 "kfftspec.h"
int nsubints;
# 66 "kfftspec.h"
int do_cross;
# 67 "kfftspec.h"
int nspecs;
# 68 "kfftspec.h"
int do_window;
# 69 "kfftspec.h"
int window_type;
# 70 "kfftspec.h"
float window_overlap;
# 73 "kfftspec.h"
int nGPUs;
# 74 "kfftspec.h"
int nstreams;
# 77 "kfftspec.h"
vdif_rx_t *rx;
# 78 "kfftspec.h"
char *raw_format;
# 79 "kfftspec.h"
int raw_nsubbands;
# 80 "kfftspec.h"
int raw_nbits;
# 83 "kfftspec.h"
struct timeval h_rawbuf_midtimes[8][4];
# 84 "kfftspec.h"
unsigned char *h_rawbufs[8][4];
# 85 "kfftspec.h"
unsigned char *d_rawbufs[8][4];
# 86 "kfftspec.h"
size_t rawbuflen;
# 87 "kfftspec.h"
unsigned char *h_random;
# 90 "kfftspec.h"
int devicemap[8];
# 91 "kfftspec.h"
struct CUstream_st *sid[8][4];
# 92 "kfftspec.h"
cufftHandle cufftplans[8][4];
# 93 "kfftspec.h"
struct cudaDeviceProp devprops[8];
# 96 "kfftspec.h"
float *d_fft_in[8][4];
# 97 "kfftspec.h"
float *d_fft_out[8][4];
# 98 "kfftspec.h"
cu_window_cb_params_t h_cufft_userparams;
# 99 "kfftspec.h"
cu_window_cb_params_t *d_cufft_userparams[8];
# 102 "kfftspec.h"
float *d_powspecs[8][4];
# 103 "kfftspec.h"
float *h_powspecs[8][4];
# 104 "kfftspec.h"
float *h_crosspecs[8][4];
# 105 "kfftspec.h"
double spec_weight[8][4];
# 108 "kfftspec.h"
const char *scanname;
# 109 "kfftspec.h"
const char *experiment;
# 110 "kfftspec.h"
const char *station;
# 111 "kfftspec.h"
const char *observer;
# 112 "kfftspec.h"
char *out_filename;
# 115 "kfftspec.h"
struct CUevent_st *process_cuda_starttimes[8][4];
# 116 "kfftspec.h"
struct CUevent_st *process_cuda_stoptimes[8][4];
# 117 "kfftspec.h"
struct CUevent_st *process_cuda_inputdata_overwriteable[8][4];
# 118 "kfftspec.h"
struct CUevent_st *process_cuda_spectrum_available[8][4];
# 119 "kfftspec.h"
struct CUevent_st *estart[8][4];
# 120 "kfftspec.h"
struct CUevent_st *estop[8][4];};
# 121 "kfftspec.h"
typedef struct fftspec_config_tt fftspec_config_t;
# 153 "/usr/include/bits/mathinline.h" 3
union _ZZ10__signbitlEUt_ {
# 153 "/usr/include/bits/mathinline.h" 3
long double __l;
# 153 "/usr/include/bits/mathinline.h" 3
int __i[3];};
# 72 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) __attribute__((__nothrow__)) void *memset(void *, int, size_t);
# 135 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) int fprintf(FILE *__restrict__, const char *__restrict__, ...);
# 129 "/usr/local/cuda-8.0/bin/..//include/common_functions.h"
 __attribute__((device_builtin)) extern __attribute__((device)) int printf(const char *__restrict__, ...);
# 127 "/usr/include/bits/mathinline.h" 3
 __attribute__((device_builtin)) extern __attribute__((device)) __inline__ __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__nothrow__)) __attribute__((__const__)) int __signbitf(float);
# 139 "/usr/include/bits/mathinline.h" 3
 __attribute__((device_builtin)) extern __attribute__((device)) __inline__ __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__nothrow__)) __attribute__((__const__)) int __signbit(double);
# 151 "/usr/include/bits/mathinline.h" 3
 __attribute__((device_builtin)) extern __attribute__((device)) __inline__ __attribute__((__always_inline__)) __attribute__((__gnu_inline__)) __attribute__((__nothrow__)) __attribute__((__const__)) int __signbitl(long double);
# 959 "/usr/include/bits/mathinline.h" 3
# 1 "/usr/local/cuda-8.0/bin/..//include/common_functions.h" 1 3
# 249 "/usr/local/cuda-8.0/bin/..//include/common_functions.h" 3
# 1 "/usr/local/cuda-8.0/include/math_functions.h" 1 3
# 10327 "/usr/local/cuda-8.0/include/math_functions.h" 3
# 1 "/usr/local/cuda-8.0/include/math_functions.hpp" 1 3
# 10328 "/usr/local/cuda-8.0/include/math_functions.h" 2 3



# 1 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.h" 1 3
# 270 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.h" 3
# 1 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.hpp" 1 3
# 271 "/usr/local/cuda-8.0/include/math_functions_dbl_ptx3.h" 2 3
# 10332 "/usr/local/cuda-8.0/include/math_functions.h" 2 3
# 250 "/usr/local/cuda-8.0/bin/..//include/common_functions.h" 2 3
# 960 "/usr/include/bits/mathinline.h" 2 3
