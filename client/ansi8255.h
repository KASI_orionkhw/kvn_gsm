/*************************************************************/
/* Configuration of 82C55 Control Word                       */
/* D7 : Mode Set Flag (1=Active)                             */
/* D6, D5 : Mode Selection (00=Mode0, 01=Mode1, 1X=Mode2)    */
/*          Mode0 - Basic IO                                 */
/*          Mode1 - Strobed IO                               */
/*          Mode2 - Bi Directional Bus                       */
/* D4 : Port A (1=Input, 0=Output)                           */
/* D3 : Port C_upper (1=Input, 0=Output)                     */
/* D2 : Mode Selection (0=Mode0, 1=Mode1)                    */
/* D1 : Port B (1=Input, 0=Output)                           */
/* D0 : Port C_lower (1=Input, 0=Output)                     */
/* Convention PXXXX = P+A+B+C_up+C_lo                        */
/*************************************************************/

/* Mode0 *****************************************************/
#define POOOO 0X0080
#define POOOI 0X0081
#define POIOO 0X0082
#define POIOI 0X0083
#define POOIO 0X0088
#define POOII 0X0089
#define POIIO 0X008A
#define POIII 0X008B

#define PIOOO 0X0090
#define PIOOI 0X0091
#define PIIOO 0X0092
#define PIIOI 0X0093
#define PIOIO 0X0098
#define PIOII 0X0099
#define PIIIO 0X009A
#define PIIII 0X009B

/* Basic Port Definiotions ***********************************/
#define porta0 0X0300
#define portb0 0X0301
#define portc0 0X0302
#define portd0 0X0303

#define porta1 0X0304
#define portb1 0X0305
#define portc1 0X0306
#define portd1 0X0307

#define porta2 0X0308
#define portb2 0X0309
#define portc2 0X030a
#define portd2 0X030b

#define porta3 0X030c
#define portb3 0X030d
#define portc3 0X030e
#define portd3 0X030f

#define porta4 0X0310
#define portb4 0X0311
#define portc4 0X0312
#define portd4 0X0313

#define porta5 0X0314
#define portb5 0X0315
#define portc5 0X0316
#define portd5 0X0317

#define porta6 0X0318
#define portb6 0X0319
#define portc6 0X031a
#define portd6 0X031b

#define porta7 0X031c
#define portb7 0X031d
#define portc7 0X031e
#define portd7 0X031f
