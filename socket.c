#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <arpa/inet.h>

int create_server(int server_port)
{
    int server_sockfd;
    int server_len;
    struct sockaddr_in server_address;
    
    server_sockfd = socket(AF_INET,SOCK_STREAM,0); 
    server_address.sin_family = AF_INET;
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(server_port);
    server_len = sizeof(server_address);
    
    bind(server_sockfd,(struct sockaddr *)&server_address,server_len);
    
    listen(server_sockfd,3);

    return server_sockfd;
}


int connect_server(char *server_address,int socket_port)
{
    int sockfd;
    struct sockaddr_in address;
    int result;
    int len;
        
    sockfd = socket(AF_INET,SOCK_STREAM,0);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(server_address);
    address.sin_port = htons(socket_port);
    len = sizeof(address);
    result = connect(sockfd,(struct sockaddr *)&address,len);
    if(result == -1){
	perror("oops:client1");
	return -1;
//	exit(1);
    }
    
    return sockfd;
}
