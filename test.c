#include "kvn_dsm.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
	kvn_dsm_streams_t testbit;

	testbit.word16=0x8A;
	printf("word : 0x%2X\n", testbit.word16);

	printf("auto1:%d auto2:%d auto3:%d auto4:%d\n",testbit.bit.auto1, testbit.bit.auto2, testbit.bit.auto3, testbit.bit.auto4); 
	printf("cross12r:%d cross12i:%d cross34r:%d cross34i:%d\n",testbit.bit.cross12r, testbit.bit.cross12i, testbit.bit.cross34r, testbit.bit.cross34i); 

	testbit.word16=0x80;
	printf("word : 0x%2X\n", testbit.word16);

	printf("auto1:%d auto2:%d auto3:%d auto4:%d\n",testbit.bit.auto1, testbit.bit.auto2, testbit.bit.auto3, testbit.bit.auto4); 
	printf("cross12r:%d cross12i:%d cross34r:%d cross34i:%d\n",testbit.bit.cross12r, testbit.bit.cross12i, testbit.bit.cross34r, testbit.bit.cross34i); 

	testbit.word16=0x5A;
	printf("word : 0x%2X\n", testbit.word16);

	printf("auto1:%d auto2:%d auto3:%d auto4:%d\n",testbit.bit.auto1, testbit.bit.auto2, testbit.bit.auto3, testbit.bit.auto4); 
	printf("cross12r:%d cross12i:%d cross34r:%d cross34i:%d\n",testbit.bit.cross12r, testbit.bit.cross12i, testbit.bit.cross34r, testbit.bit.cross34i); 
	return 0;
}
