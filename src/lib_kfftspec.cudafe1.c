# 1 "lib_kfftspec.cu"
# 170 "/usr/include/stdio.h" 3
extern struct _IO_FILE *stderr;
# 100 "lib_kfftspec.cu"
static const char *__nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_C_windowfunc_names[4];
# 112 "lib_kfftspec.cu"
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_rx_udpoffset;
# 112 "lib_kfftspec.cu"
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_rx_cpu;
# 112 "lib_kfftspec.cu"
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_rx_is_file;
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_do_terminate;
char *rx_source = 0;
static const char __T20[59];
static const char __T21[67];
static const char __T22[48];
static const char __T23[52];
static const char __T24[47];
static const char __T25[89];
static const char __T26[42];
# 1140 "lib_kfftspec.cu"
static int _ZZ28fftspec_store_results_KVNDSMPK17fftspec_config_ttiiE12fudge_inited;
static kvn_dsm_writer_t *_ZZ28fftspec_store_results_KVNDSMPK17fftspec_config_ttiiE10fudge_wdsm;
# 1177 "lib_kfftspec.cu"
static int _ZZ30fftspec_store_results_KFFTSPECPK17fftspec_config_ttiiE12fudge_inited;
static char _ZZ30fftspec_store_results_KFFTSPECPK17fftspec_config_ttiiE11ks_filename[4096];
static const char __T27[72];
# 100 "lib_kfftspec.cu"
static const char *__nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_C_windowfunc_names[4] = {((const char *)"boxcar"),((const char *)"Hann"),((const char *)"Hamming"),((const char *)"HFT248D")};
# 112 "lib_kfftspec.cu"
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_rx_udpoffset = 0;
# 112 "lib_kfftspec.cu"
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_rx_cpu = 0;
# 112 "lib_kfftspec.cu"
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_rx_is_file = 0;
static int __nv_static_33__20_lib_kfftspec_cpp1_ii_350f4d54_do_terminate = 0;
static const char __T20[59] = "int fftspec_process_segment(fftspec_config_tt *, int, int)";
static const char __T21[67] = "int fftspec_process_segment_dualpol(fftspec_config_tt *, int, int)";
static const char __T22[48] = "int fftspec_init_condition(fftspec_config_tt *)";
static const char __T23[52] = "int fftspec_check_config(const fftspec_config_tt *)";
static const char __T24[47] = "int fftspec_adjust_config(fftspec_config_tt *)";
static const char __T25[89] = "int fftspec_estimate_memory(const fftspec_config_tt *, unsigned long *, unsigned long *)";
static const char __T26[42] = "int fftspec_allocate(fftspec_config_tt *)";
# 1140 "lib_kfftspec.cu"
static int _ZZ28fftspec_store_results_KVNDSMPK17fftspec_config_ttiiE12fudge_inited = 0;
static kvn_dsm_writer_t *_ZZ28fftspec_store_results_KVNDSMPK17fftspec_config_ttiiE10fudge_wdsm = ((kvn_dsm_writer_t *)0LL);
# 1177 "lib_kfftspec.cu"
static int _ZZ30fftspec_store_results_KFFTSPECPK17fftspec_config_ttiiE12fudge_inited = 0;
static const char __T27[72] = "int fftspec_store_results_KFFTSPEC(const fftspec_config_tt *, int, int)";
